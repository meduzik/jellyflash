## Memory Management

### Allocators

#### Linear Allocator

Allocates memory for temporary objects.

#### Hash Node Allocator / Pooled Allocators

Allocates nodes of equal size, usually for hash maps.

#### Off-heap Allocator

Allocates large blocks of memory for long term storage

#### Managed Heap

Allocates GC objects. GC objects have specific layout:

```
ptr | pointer to vtable
----+-------------------
ptr | gc specific field
----+-------------------
    | ...object payload
```

### Compacting GC?

Pros:
 - Less memory fragmentation, less chance of memory exhaustion
 - No need to thread all live objects.

Cons:
 - 2x memory usage?

### Mark & Sweep

Refs on stack are inaccessible in wasm, and we don't want any code to keep them alive. So we collect only after a vm lock expires (and while we hold a vm lock).

### Incremental Collection?

In [gc-specific field] we hold a pointer to the next object in a linked list of all reachable objects. When writing something to the heap, we mark the object written as reachable.