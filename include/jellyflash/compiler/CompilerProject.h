#pragma once
#include <jellylib/types.h>

namespace jly {

class CompilerProject {
public:
	CompilerProject();
	void setStdlibPath(std::string_view path);
	void setOutputPath(std::string_view path);
	void setStubsPath(std::string_view path);
	void setMain(std::string_view id);
	void loadConfig(std::string_view path);
	void build();
private:
	std::string stdlib_path;
	std::string output_path;
	std::string stubs_path;
	std::string main;
	jly::vector<std::string> configs;
};

}
