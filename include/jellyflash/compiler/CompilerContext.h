#pragma once
#include <jellyflash/source/SourceInfo.h>
#include <jellyflash/parser/AST.h>
#include <jellylib/utils/Writer.h>
#include <jellyflash/utils/Diagnostic.h>
#include "CompilerEnv.h"

namespace jly{

class CompilerEnv;
class CompilationUnit;
class CUPackage;


class CompilerContext{
public:
	CompilerContext(CompilerEnv* env):
		env(env),
		cu(nullptr){
	}

	void setUnit(CompilationUnit* cu){
		this->cu = cu;
	}

	CompilationUnit* getUnit(){
		return cu;
	}

	CompilerEnv* getEnv(){
		return env;
	}

	SourceLoc getLoc(srcspan_t span);

	template<class... Args>
	void warning(Args&&... args) {
		report(DiagnosticLevel::Warning, std::forward<Args>(args)...);
	}

	template<class... Args>
	void error(Args&&... args) {
		report(DiagnosticLevel::Error, std::forward<Args>(args)...);
	}

	template<class... Args>
	[[noreturn]]
	void fatal(Args&&... args) {
		report(DiagnosticLevel::Fatal, std::forward<Args>(args)...);
		exit(1);
	}

	template<class... Args>
	void report(DiagnosticLevel level, StringRef code, StringRef message, Args&&... args){
		report(level, nullptr, code, message, std::forward<Args>(args)...);
	}

	template<class... Args>
	void report(DiagnosticLevel level, srcspan_t span, StringRef code, StringRef message, Args&&... args){
		report(level, getLoc(span), code, message, std::forward<Args>(args)...);
	}

	template<class... Args>
	void report(DiagnosticLevel level, const SourceLoc& loc, StringRef code, StringRef message, Args&&... args) {
		report(level, &loc, code, message, std::forward<Args>(args)...);
	}

	template<class... Args>
	void report(DiagnosticLevel level, const SourceLoc* loc, StringRef code, StringRef message, Args&&... args) {
		auto slice = MemPool::GetStackSlice();
		DiagnosticArgument arg_arr[] = {
			DiagnosticArgument(std::forward<Args>(args))...,
			DiagnosticArgument()
		};
		reportInternal(level, loc, code, message, ArrayRef(arg_arr, sizeof(arg_arr) / sizeof(DiagnosticArgument) - 1));
	}

	CUPackage* resolvePackage(ArrayRef<ast::ID> ids);
	CompilationUnit* resolveUnit(ArrayRef<ast::ID> ids);
	void checkPermissions(ir::Definition* def);

	ir::Intrinsics* getIntrinsics(){
		return env->getIntrinsics();
	}
private:
	void reportInternal(DiagnosticLevel level, const SourceLoc* loc, StringRef code, StringRef message, ArrayRef<DiagnosticArgument> args);

	CompilerEnv* env;
	CompilationUnit* cu;
};

}
