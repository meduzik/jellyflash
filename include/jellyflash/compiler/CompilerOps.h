#pragma once

#include "CompilerContext.h"
#include <jellyflash/codegen/Codegen.h>
#include <jellyflash/ir/File.h>
#include <jellyflash/ir/Linker.h>
#include <jellyflash/icode/ICodeCompiler.h>

namespace jly{

class CompilerEnv;

class CompilerOps{
public:
	CompilerOps(CompilerEnv* env, CompilationUnit* cu);

	void require(CompilationUnit::State state);

	Linker* getLinker();

	icode::ICodeCompiler* getICodeCompiler(){
		return &compiler;
	}

	void linkTimeOptimize();
private:
	void parse();
	void resolve();
	void prepareLink();
	void link();
	void prepareCompiler();
	void compileCode();
	void prepareCodegen();
	void runCodegen();

	CompilerContext cc;
	CompilationUnit* cu;
	Codegen codegen;
	icode::ICodeCompiler compiler;
	Linker linker;
	CompilationUnit::State state;
	bool executing;
};

}

