#pragma once
#include <jellylib/types.h>
#include <jellylib/types/StringRef.h>
#include <jellyflash/ir/Intrinsic.h>
#include <jellyflash/codegen/CodegenInfo.h>

namespace jly{
namespace ir {
class FileScope;
}

class CompilationUnit;
class CUPackage;
class CompilerOps;

class CompilerEnv{
public:
	CompilerEnv();

	~CompilerEnv();
	CompilerEnv(const CompilerEnv&) = delete;
	CompilerEnv(CompilerEnv&&) = delete;
	CompilerEnv& operator=(const CompilerEnv&) = delete;
	CompilerEnv& operator=(CompilerEnv&&) = delete;

	CUPackage* getPackageRoot(){ 
		return &package;
	}

	void request(CompilationUnit* cu);
	void request(CUPackage* package);

	void loadIntrinsics();
	void compile(CompilationUnit* cu);
	
	ir::Intrinsics* getIntrinsics(){
		return &intrinsics;
	}

	ir::TypeVector* getVectorType(ir::Type* type);

	void registerClassPath(CodegenClassPathInfo* cp);

	void setInliningAllowed(bool value) {
		allow_inlining = value;
	}

	bool isInliningAllowed() const {
		return allow_inlining;
	}

	void referenceClass(StringRef path);
private:
	void collect();
	void codegen();
	void lto();

	template<class T>
	T* resolve(CUPackage* package, StringRef name);

	void makePrimitive(ir::Class* class_, ir::TypePrim* type);

	jly::vector< std::unique_ptr<CompilerOps> > ops;
	std::set< CodegenClassPathInfo* > cps;
	CUPackage package;
	ir::Intrinsics intrinsics;
	bool allow_loading = true;
	bool allow_inlining = false;
};


}

