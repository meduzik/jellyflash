#pragma once

#include <jellylib/common.h>
#include <jellyflash/source/SourceInfo.h>
#include <jellylib/memory/MemPool.h>
#include <jellyflash/ir/Lookup.h>

namespace jly{

namespace ir{
class File;
struct Definition;
class PackageLookup;
}

class CUPackage;
class CompilerContext;
class CompilerOps;

struct CodegenCU;
struct CodegenPackage;
struct CodegenClassPathInfo;

struct CUPackageCache{
};

class CompilerEnv;

class CompilationUnit{
public:
	enum State {
		Init = 0,
		Parsed = 1,
		Resolved = 2,
		LinkerPrepared = 3,
		Linked = 4,
		CompilerPrepared = 5,
		CodeCompiled = 6,
		CodegenPrepared = 7,
		CodegenCompleted = 8
	};

	CompilationUnit(CompilerEnv* env, CUPackage* package, std::string name, CodegenClassPathInfo* codegen_classpath):
		package(package),
		name(name),
		requested(false),
		IR(nullptr),
		codegen(nullptr),
		env(env),
		codegen_classpath(codegen_classpath)
	{
	}

	CompilationUnit(const CompilationUnit&) = delete;
	CompilationUnit(CompilationUnit&&) = delete;
	~CompilationUnit();

	void loadSourceFile(CompilerContext& cc);

	SourceFile* getSourceFile(){
		return file.get();
	}
	const std::filesystem::path& getPath(){
		return path;
	}
	void setPath(std::filesystem::path path);
	bool isRequested(){
		return requested;
	}
	void setRequested(){
		this->requested = true;
	}
	CUPackage* getPackage(){
		return package;
	}
	void setIR(ir::File* IR){
		this->IR = IR;
	}

	ir::File* getIR(){
		return IR;
	}

	void setOps(CompilerOps* ops){
		this->ops = ops;
	}
	CompilerOps* getOps(){
		return ops;
	}

	StringRef getName() const{
		return name;
	}

	const std::string& getFullName();

	void require(State state);

	CodegenCU* codegen;
	CodegenClassPathInfo* codegen_classpath;
	bool codegen_disabled = false;
private:
	std::filesystem::path path;
	bool requested;
	CUPackage* package;
	ir::File* IR;
	std::unique_ptr<SourceFile> file;
	CompilerEnv* env;
	CompilerOps* ops;
	std::string fullname;
	std::string name;
};

class CUPackage {
public:
	CUPackage(CompilerEnv* env, CUPackage* parent, std::string name):
		requested(false),
		parent(parent),
		name(name),
		has_fullname(false),
		codegen(nullptr),
		env(env)
	{
	}

	CUPackage* findChild(const StringRef& name);
	CUPackage* getChild(const StringRef& name);
	CompilationUnit* findUnit(const StringRef& name);
	CompilationUnit* createUnit(const StringRef& name, CodegenClassPathInfo* codegen);
	ArrayRef<CompilationUnit*> getUnits() const;
	bool isRequested() {
		return requested;
	}
	void setRequested() {
		this->requested = true;
	}
	StringRef getName(){
		return name;
	}
	CUPackage* getParent(){
		return parent;
	}

	CUPackageCache* getCache(){
		return& cache;
	}

	const std::string& getFullName();

	CodegenPackage* codegen;
private:
	std::map<std::string, CUPackage, std::less<>> children;
	std::map<std::string, CompilationUnit, std::less<>> units;
	jly::vector<CompilationUnit*> units_vector;
	bool requested;
	std::string name;
	std::string fullname;
	bool has_fullname;
	CompilerEnv* env;
	CUPackage* parent;
	CUPackageCache cache;
};


}
