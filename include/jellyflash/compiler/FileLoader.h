#pragma once
#include "CompilerContext.h"

namespace jly{



class FileLoader{
public:
	FileLoader(CompilerContext* ctx);
	void loadClassPath(const std::filesystem::path& path, CodegenClassPathInfo* classPath);
private:
	CompilerContext* ctx;

	void loadPackage(CUPackage* package, const std::filesystem::path& path, CodegenClassPathInfo* classPath);
};

}
