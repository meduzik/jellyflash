#pragma once
#include <jellyflash/compiler/CompilerContext.h>
#include <jellyflash/ir/File.h>
#include <jellyflash/ir/CLass.h>
#include <jellyflash/ir/Resolver.h>
#include <jellyflash/ir/Linker.h>
#include "ICode.h"

namespace jly::icode{

struct FlowNode;

struct ScopeID;

struct GotoNode{
	srcspan_t span;
	StringRef name;
	BasicBlock* bb;
	ScopeID* scope_id;

	GotoNode(
		srcspan_t span,
		StringRef name,
		BasicBlock* bb,
		ScopeID* scope_id
	):
		span(span),
		name(name),
		bb(bb),
		scope_id(scope_id)
	{
	}
};

struct ScopeFinallyInfo {
	BasicBlock* entry_bb;
	InstrPhi* phi;
	InstrSwitchBr* br;
	slice_vector< std::pair<BasicBlock*, BasicBlock*> > targets;

	ScopeFinallyInfo(
		MemPool::Slice* tmp,
		BasicBlock* entry_bb,
		InstrPhi* phi,
		InstrSwitchBr* br
	) :
		targets(tmp),
		entry_bb(entry_bb),
		phi(phi),
		br(br) {
	}
};

struct LabelInfo;

struct ScopeID {
	ScopeID* parent;
	ScopeFinallyInfo* finally = nullptr;
	bool barrier;
	i32 level;
	LabelInfo* break_ = nullptr;
	LabelInfo* continue_ = nullptr;
	
	ScopeID(ScopeID* parent, bool barrier) :
		parent(parent),
		barrier(barrier)
	{
		if ( parent ){
			level = parent->level + 1;
		}else{
			level = 0;
		}
	}
};

struct LabelInfo;
struct InstrBuilder;

struct FunctionFrameState {
	FunctionFrameState* parent;
	ir::Type* this_type = nullptr;
	Value* this_value = nullptr;
	ir::Type* ret_type = nullptr;
	slice_vector<LocalVar*> vars;
	slice_vector<Upvalue*> upvals;
	slice_vector<Closure*> closures;
	slice_umap<StringRef, Reference> cache;
	slice_umap<ast::ExprClosure*, Closure*> closure_map;
	slice_umap<StringRef, LabelInfo*> labels;
	slice_vector<GotoNode*> goto_late;
	BasicBlock* entry;
	ir::Class* class_context = nullptr;
	ir::Class* class_immediate = nullptr;
	slice_vector<ir::TypeVector*> vec_type_params;
	ScopeID* scope_id = nullptr;
	FlowNode* node = nullptr;
	InstrBuilder* builder = nullptr;
	HandlerScope* handler = nullptr;
	bool super_call = false;
	bool this_accessed = false;
	FuncFrame* realization = nullptr;
	i32 statements = 0;

	slice_vector<LabelInfo*> break_targets;
	slice_vector<LabelInfo*> continue_targets;

	slice_vector<ScopeFinallyInfo*> finally_late;
	slice_multimap<BasicBlock*, BasicBlock*> indirect_edges;

	slice_uset<BasicBlock*> active_bbs;
	ArrayRef<BasicBlock*> reachable_bbs = nullptr;
	ArrayRef<HandlerScope*> reachable_handlers = nullptr;

	FunctionFrameState(FunctionFrameState* parent, MemPool::Slice* tmp):
		parent(parent),
		vars(tmp),
		closures(tmp),
		cache(tmp),
		upvals(tmp),
		closure_map(tmp),
		vec_type_params(tmp),
		goto_late(tmp),
		labels(tmp),
		break_targets(tmp),
		continue_targets(tmp),
		finally_late(tmp),
		indirect_edges(tmp),
		active_bbs(tmp)
	{
		if ( parent ){
			class_context = parent->class_context;
		}
	}
};

struct InstrBuilder{
	BasicBlock* bb;
	slice_vector<Instr*> instrs;
	MemPool::Slice* mem;

	InstrBuilder(BasicBlock* bb, MemPool::Slice* mem, MemPool::Slice* tmp):
		bb(bb),
		instrs(tmp),
		mem(mem)
	{
	}

	void setBlock(BasicBlock* bb){
		this->bb = bb;
	}

	template<class T, class... Args>
	T* emit(srcspan_t span, Args&&... args){
		T* instr = mem->create<T>(std::forward<Args>(args)...);
		instr->span = span;
		if (bb) {
			bb->instrs.push_back(instr);
		}
		if ( IsTerminator(instr) ){
			bb = nullptr;
		}
		return instr;
	}
};

enum class CompareOp {
	EQ,
	NEQ,
	LE,
	LT,
	GE,
	GT,
	StrictEQ,
	StrictNEQ
};

enum class CastType {
	Explicit,
	Implicit
};

enum class Termination{
	Never,
	Always,
	Maybe
};

enum class CompareClass {
	Dynamic,
	Boolean,
	Numeric,
	String,
	Function,
	Reference
};

struct LabelInfo{
	SourceLoc loc;
	StringRef name;
	BasicBlock* bb;
	ScopeID* scope_id;

	LabelInfo(
		SourceLoc loc,
		StringRef name,
		BasicBlock* bb,
		ScopeID* scope_id
	):
		loc(loc),
		name(name),
		bb(bb),
		scope_id(scope_id)
	{
	}
};

class ICodeCompiler{
public:
	ICodeCompiler();
	void prepare(CompilerContext* cc, MemPool::Slice* mem, MemPool::Slice& tmp);
	void run();
	void setTempPool(MemPool::Slice& tmp);

	void enterFileScope(ir::FileScope* scope);
	void compileSignature(ir::Class* class_context, ir::FunctionSignature* signature);
	void compileParam(ir::Class* class_context, ir::FunctionParam* param);

	void compileVarInitializer(icode::VarInitializer* initializer);

	void compileInterfaceMethod(ir::InterfaceMethod* method);
	void compileClassMethod(ir::ClassMethod* method);
	void compileGlobalFunc(ir::GlobalFunction* func);
	
	void compileGlobalVarInitializer(ir::GlobalVar* var);
	void compileStaticInitializer(ir::Class* class_);

	ConstantString* createStringLiteral(StringRef contents);
private:
	void compileUnit();
	void compileScope(ir::FileScope* scope);

	void compileClosure(Closure* closure);

	struct TemporaryNSScope;
	TemporaryNSScope enterClassContext(ir::Class* class_);

	void beginFrame();
	void initSignature(ir::FunctionSignature* signature);
	void initThis(ir::Type* type);
	FuncFrame* endFrame(srcspan_t span);
	void computeFinally();

	template<class It>
	auto clone(It&& begin, It&& end) ->ArrayRef< 
		std::remove_const_t<
			std::remove_reference_t<decltype(*std::declval<It>())> 
		>
	>;

	template<class T>
	ArrayRef<std::remove_const_t<T>> to_array(const std::initializer_list<T>& values);

	void processBody(srcspan_t span, ArrayRef<ast::Stmt*> stmts);
	void collectStmts(ArrayRef<ast::Stmt*> stmts);
	void collectStmt(ast::Stmt* stmt);

	
	void compileStmts(InstrBuilder* builder, ArrayRef<ast::Stmt*> stmts);
	void compileStmt(InstrBuilder* builder, ast::Stmt* stmt);

	void compileExprForSideEffects(InstrBuilder* builder, ast::Expr* expr);
	
	// shallow evaluation of an expression
	// keeps the last component as a reference if possible
	Reference compileExpr(InstrBuilder* builder, ast::Expr* ast);
	// fully evaluates an expression and cast its result to target_type, if non-null
	Value* evaluateExpr(InstrBuilder* builder, ast::Expr* ast, ir::Type* target_type);
	// fully evaluates an expression
	Value* evaluateExprUntyped(InstrBuilder* builder, ast::Expr* ast);
	// casts value to target_type, generates all the neccessary instructions
	Value* cast_to(InstrBuilder* builder, srcspan_t span, Value* val, ir::Type* target_type, CastType type);
	Value* cast_as(InstrBuilder* builder, srcspan_t span, Value* val, ir::Type* target_type);
	Value* castToNumeric(InstrBuilder* builder, srcspan_t span, Value* val);

	Constant* castConstant(srcspan_t span, Constant* val, ir::Type* target_type, CastType type);

	Constant* constantToInt32(srcspan_t span, double value, bool is_explicit);
	Constant* constantToUInt32(srcspan_t span, double value, bool is_explicit);

	Constant* getNumberConstant(double value);

	Constant* tryReadConstantVarInitializer(VarInitializer* initializer);

	// calls head reference with args arguments
	Value* call(InstrBuilder* builder, srcspan_t span, Reference head, ArrayRef<ast::Expr*> args);
	Value* callGeneric(InstrBuilder* builder, srcspan_t span, Value* head, ArrayRef<ast::Expr*> args);
	
	ir::Type* concretizeType(ir::Type* type);
	
	ArrayRef<Value*> evalArgs(InstrBuilder* builder, srcspan_t span, ir::FunctionSignature* sign, ArrayRef<ast::Expr*> args);
	ArrayRef<Value*> evalArgs(InstrBuilder* builder, srcspan_t span, ArrayRef<ir::FunctionParam*>, bool rest, ArrayRef<ast::Expr*> args);
	Value* typetest(InstrBuilder* builder, srcspan_t span, Value* val, ir::Type* type);
	// writes value into a reference
	Value* write(InstrBuilder* builder, srcspan_t span, Reference target, Value* value);
	// constructs invalid value of a given type
	Value* getInvalidValue(ir::Type* type = &ir::TypePrim::Any);
	// returns the least common type between two types
	ir::Type* unifyTypes(srcspan_t left, ir::Type* left_ty, srcspan_t right, ir::Type* right_ty);
	ir::Type* unifyArith(srcspan_t span, ir::Type* lhs, ir::Type* rhs);

	ir::Type* refToType(Reference ref);

	ast::BinOp convertOp(ast::AssignOp op);
	NumBinaryOp convertOp(ast::BinOp op);

	Value* applyBinOp(InstrBuilder* builder, srcspan_t span, ast::BinOp op, srcspan_t lhs_loc, Value* lhs, ast::Expr* rhs);
	Value* applyUnOp(InstrBuilder* builder, srcspan_t span, ast::UnOp op, srcspan_t arg_loc, Reference arg);

	Value* unwrap(InstrBuilder* builder, Value* value);
	Value* evaluateRef(InstrBuilder* builder, srcspan_t span, Reference ref);
	Value* evaluateRefAs(InstrBuilder* builder, srcspan_t span, Reference ref, ir::Type* target_type);

	Value* compare(InstrBuilder* builder, srcspan_t span, CompareOp op, srcspan_t left, Value* lhs, srcspan_t right, Value* rhs);

	Value* createPhi(InstrBuilder* builder, srcspan_t span, ir::Type* type, const std::initializer_list< std::pair<BasicBlock*, Value*> >& branches);

	Reference resolve(InstrBuilder* builder, srcspan_t span, Reference lhs, ast::NSID* id);
	Reference resolveGeneric(InstrBuilder* builder, srcspan_t span, Value* lhs, ast::NSID* id);

	Reference resolveInFrameCompute(srcspan_t span, FunctionFrameState* frame, StringRef name);
	Reference resolveInFrame(srcspan_t span, FunctionFrameState* frame, StringRef id);
	Reference resolveClass(srcspan_t span, ir::Class* class_, Value* value, StringRef id);
	ir::ClassMember* resolveInstance(ir::Class* class_, srcspan_t span, StringRef name);

	void require(ir::Class* class_);
	void require(ir::Interface* iface);

	BasicBlock* createBlock();

	bool areTypesEqual(ir::Type* lhs, ir::Type* rhs);
	CompareClass getTypeCompareClass(ir::Type* type);
	bool isPrimitive(ir::Type* type);
	bool isAny(ir::Type* type);
	bool isBoolean(ir::Type* type);
	bool isNumeric(ir::Type* type);
	bool isNumericOrBoolean(ir::Type* type);
	bool isString(ir::Type* type);
	bool isReference(ir::Type* type);
	bool isFunction(ir::Type* type);
	bool isClass(ir::Type* type);
	bool isNullable(ir::Type* type);
	bool isDynamicClass(ir::Type* type);

	bool isConstant(Value* value);

	bool isAccessible(ir::ClassMember* member);

	template<class Visitor>
	void visitStmts(ArrayRef<ast::Stmt*> stmts, Visitor&& v);
	template<class Visitor>
	void visitStmt(ast::Stmt* stmt, Visitor&& v);
	template<class Visitor>
	void visitExpr(ast::Expr* stmts, Visitor&& v);

	void visitVar(ast::VarDef* var);
	void visitClosure(ast::ExprClosure* closure);
	void visitVar(ast::ID id, ast::Type* type);

	void emitReturn(InstrBuilder* builder, srcspan_t span, Value* value);

	void addLocalVar(SourceLoc loc, StringRef name, ir::Type* type);
	void addParamVar(ir::FunctionParam* param);

	StringRef decodeStringLiteral(srcspan_t span, StringRef data);

	CompilerContext* cc;
	CompilationUnit* cu;
	MemPool::Slice* mem;
	MemPool::Slice* tmp;
	ir::File* IR;
	ir::FileScope* scope;
	ir::Intrinsics* intrin;

	Resolver resolver;
	Linker linker;
	ir::NamespaceLookupScope* ns_scope;

	FunctionFrameState* frame = nullptr;

	LabelInfo* createLabel(srcspan_t span, StringRef name, ScopeID* scope_id, BasicBlock* bb);

	ScopeID* getScopeID();
	ScopeID* pushScopeWithFinally(BasicBlock* entry_bb, InstrPhi* phi, InstrSwitchBr* switch_br);
	ScopeID* pushScopeWithContinueBreakTarget(LabelInfo* continue_label, LabelInfo* break_label);
	ScopeID* pushScopeWithBreakTarget(LabelInfo* info);
	ScopeID* pushScope(bool barrier);
	void pushHandler(HandlerScope* handler);
	void popHandler();
	void popScope();
	void nonlocalJump(InstrBuilder* builder, srcspan_t span, ScopeID* from_scope, ScopeID* to_scope, BasicBlock* target);

	ScopeID* getCommonAncestor(ScopeID* left, ScopeID* right);

	void traverseFlowGraph(BasicBlock* entry);
};

}