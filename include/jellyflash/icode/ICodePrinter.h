#pragma once
#include <jellylib/utils/Writer.h>
#include "ICode.h"

namespace jly::icode{

struct PrinterFrameContext{
	slice_umap<BasicBlock*, uz> blocks;
	slice_umap<Instr*, uz> values;
	slice_umap<HandlerScope*, uz> handlers;
	slice_vector<BasicBlock*> blocks_list;
	slice_vector<HandlerScope*> handlers_list;

	PrinterFrameContext(MemPool::Slice* mem) :
		blocks(mem),
		values(mem),
		blocks_list(mem),
		handlers(mem),
		handlers_list(mem)
	{
	}
};

struct PrinterMainContext{
	slice_umap<Closure*, uz> closures;
	slice_vector<Closure*> closures_list;

	PrinterMainContext(MemPool::Slice* mem):
		closures(mem),
		closures_list(mem)
	{
	}
};

class Printer{
public:
	Printer();
	void prepare(FormattedWriter* writer, MemPool::Slice& tmp);

	void prepareMainFrame(FuncFrame* frame);
	void prepareFrame(FuncFrame* frame);

	void writeCode(FuncFrame* frame);
	
	void writeDef(ir::Definition* def);
	void writeDef(ir::ClassMember* def);
	void writeDef(ir::InterfaceMember* def);

	void writeFrame(FuncFrame* frame);

	void writeType(ir::Type* type);
	void writeBlockHeader(BasicBlock* bb);
	void writeBlock(BasicBlock* bb);
	void writeHandler(HandlerScope* handler);
	void writeInstr(Instr* instr);
	void writeUse(Value* value);
	void writeUse(BasicBlock* bb);
	void writeUse(LocalVar* var);
	void writeUse(Upvalue* upvalue);
	void writeUse(Closure* closure);
private:
	
	uz getBlockIndex(BasicBlock* bb);
	uz getHandlerIndex(HandlerScope* handler);
	uz getInstrIndex(Instr* instr);
	uz getClosureIndex(Closure* closure);
	void visitClosure(Closure* closure);

	PrinterFrameContext* ctx;
	PrinterMainContext* main_ctx;

	MemPool::Slice* tmp;
	FormattedWriter* writer;
};

}
