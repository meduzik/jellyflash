#pragma once
#include <jellylib/types/StringRef.h>
#include <jellylib/types.h>
#include <jellylib/patternmatching.h>
#include <jellylib/memory/MemPool.h>
#include <jellyflash/source/SourceInfo.h>
#include <jellyflash/ir/Type.h>
#include <jellyflash/ir/Function.h>
#include <jellyflash/ir/Class.h>
#include <jellyflash/ir/Interface.h>

namespace jly{
struct CodegenClosure;
struct CodegenFrame;
}

namespace jly::ir{

class ClassMethod;
class InterfaceMethod;
class GlobalFunction;
class ClassField;
class GlobalVar;
struct Type;

}

namespace jly::icode{

enum class Intrinsic {
	RestLength
};


struct Reference;

struct Value{
	enum class Tag{
		ConstantBegin,
		CNumber,
		CString,
		CBool,
		CNull,
		CUndefined,
		CBoxed,
		CBadValue,
		ConstantEnd,
		AdHoc,
		Thisval,
		Instr
	} tag;

	ir::Type* type;

	Value(Tag tag, ir::Type* type):
		tag(tag),
		type(type)
	{
	}
};
SYNTHESIZE_PM_BASE(ValueBase, Value);


struct Constant: public Value{
	using Value::Value;

	static bool Discriminate(Value::Tag tag){
		return (tag > Value::Tag::ConstantBegin && tag < Value::Tag::ConstantEnd);
	}
};
SYNTHESIZE_PM_BASE2(ConstantBase, Constant, Value);


struct ConstantNumber : public ConstantBase<Value::Tag::CNumber> {
	double value;

	ConstantNumber(ir::Type* type, double value) : 
		ConstantBase(type),
		value(value) {}
};

struct ConstantString : public ConstantBase<Value::Tag::CString> {
	StringRef value;
	StringRef codegen_hash = nullptr;

	ConstantString(StringRef value) :
		ConstantBase(&ir::TypePrim::String),
		value(value) {}
};

struct ConstantBool : public ConstantBase<Value::Tag::CBool> {
	bool value;

	ConstantBool(bool value) :
		ConstantBase(&ir::TypePrim::Boolean), 
		value(value) {}
};

struct ConstantNull : public ConstantBase<Value::Tag::CNull> {
	ConstantNull(ir::Type* type) :
		ConstantBase(type)
	{
	}
};

struct ConstantUndefined : public ConstantBase<Value::Tag::CUndefined> {
	static const ConstantUndefined _instance;

	ConstantUndefined() :
		ConstantBase(&ir::TypePrim::Void) {
	}

	static ConstantUndefined* Instance(){
		return (ConstantUndefined*)&_instance;
	}
};

struct ConstantBoxed : public ConstantBase<Value::Tag::CBoxed> {
	Constant* contents;

	ConstantBoxed(ir::Type* type, Constant* contents):
		ConstantBase(type),
		contents(contents)
	{
	}
};

struct BadValue : public ConstantBase<Value::Tag::CBadValue> {
	BadValue(ir::Type* type) :
		ConstantBase(type) {
	}
};



struct Thisval : public ValueBase<Value::Tag::Thisval> {
	Thisval(ir::Type* type) :
		ValueBase(type) {
	}
};

struct Instr;

struct ValueInstr: public ValueBase<Value::Tag::Instr>{
	Instr* getInstr();

	ValueInstr(ir::Type* type):
		ValueBase(type)
	{
	}
};

struct ValueAdhoc: public ValueBase<Value::Tag::AdHoc>{
	StringRef value;

	ValueAdhoc(ir::Type* type, StringRef value):
		ValueBase(type),
		value(value)
	{
	}
};

struct Upvalue;

// represents both function parameters, local variables and temporary variables
struct LocalVar {
	uz index;
	SourceLoc loc;
	StringRef name;
	ir::Type* type;
	Upvalue* upvalue = nullptr;
	StringRef codegen_name = nullptr;
	ir::FunctionParam* param = nullptr;
	icode::FuncFrame* frame = nullptr;

	LocalVar(uz index, SourceLoc loc, StringRef name, ir::Type* type):
		index(index),
		loc(loc),
		name(name),
		type(type)
	{
	}
};

// represents values from the outter scope
struct Upvalue{
	LocalVar* var;
	Upvalue* parent;
	ir::Type* type;

	Upvalue(Upvalue* parent) :
		var(parent->var),
		parent(parent),
		type(parent->type) {
	}

	Upvalue(LocalVar* var):
		var(var),
		parent(nullptr),
		type(var->type)
	{
	}
};

struct Instr;
struct HandlerScope;

struct BasicBlock{
	slice_vector<Instr*> instrs;
	HandlerScope* handler;
	bool has_jumps = false;

	BasicBlock(MemPool::Slice* mem, HandlerScope* handler):
		instrs(mem),
		handler(handler)
	{
	}
};

struct FuncFrame;

struct Closure{
	FuncFrame* frame = nullptr;
	ast::ExprClosure* def;
	ir::FunctionSignature* signature = nullptr;
	CodegenClosure* codegen = nullptr;

	Closure(ast::ExprClosure* def):
		def(def)
	{
	}
};

struct HandlerScope{
	HandlerScope* parent = nullptr;
	ArrayRef< std::pair<ir::Type*, BasicBlock*> > filters = nullptr;
	BasicBlock* catch_all = nullptr;

	HandlerScope(HandlerScope* parent):
		parent(parent)
	{
	}
};

struct FuncFrame {
	FuncFrame* parent = nullptr;
	ir::Type* this_type = nullptr;
	bool this_upvalued = false;
	ArrayRef<LocalVar*> locals = nullptr;
	ArrayRef<Upvalue*> upvals = nullptr;
	ArrayRef<Closure*> closures = nullptr;
	ArrayRef<BasicBlock*> blocks = nullptr;
	ArrayRef<HandlerScope*> handlers = nullptr;
	CodegenFrame* codegen = nullptr;
	i32 statements = 0;
	
	BasicBlock* entry = nullptr;
};

struct Instr{
	enum class Tag {
		// === CONTROL FLOW ===
		// returns from the function
		Ret,
		// dispatches control flow based on the Boolean value
		CondBr,
		// uncoditional jump
		Br,
		// dispatches control flow based on the integer value
		SwitchBr,
		// throws an exception
		Throw,
		// invalid instruction (codegen bug or unreachable control flow)
		Unreachable,
		// rethrow the exception
		Resume,

		// === EXCEPTIONS ===
		// designates the basic block as an exception landing pad
		CatchPad,

		// === VALUE SELECTION ===
		// phi node (selects one of many values depending on the control flow)
		Phi,
		// choice node (selects one of two values depending on a Boolean value)
		Choice,

		// === INVOCATIONS ===
		// calls a global function
		CallGlobal,
		// calls a class static or instance method
		CallClassMethod,

		CallVectorMethod,
		// calls an interface method
		CallIntefaceMethod,
		// calls a closure
		CallClosure,
		// calls a dynamic function
		CallDynamic,

		// creates a new class instance
		NewClass,
		// creates a new class instance where the class is resolved at runtime
		NewDynamic,
		// creates a new vector instance
		NewVector,

		// === CLOSURES ===
		// creates a Function closure of a global function
		GlobalFuncClosure,
		// creates a Function closure of a static or instance class method
		ClassMethodClosure,
		// creates a Function closure of an interface method
		InterfaceMethodClosure,
		// create a Function closure of a local function
		LocalClosure,
		// returns a Class object of a given class
		ClassClosure,
		// returns a Class object of a given interface
		InterfaceClosure,

		// === STORAGE ===
		// reads a value of a local variable
		ReadLocal,
		// writes a value into a local variable
		WriteLocal,
		// reads a value of an upvalue
		ReadUpvalue,
		// writes a value into an upvalue
		WriteUpvalue,
		// reads a value of a class field (static or instance)
		ReadClassField,
		// writes a value into a class field (static or instance)
		WriteClassField,
		// reads a value of a global var
		ReadGlobalVar,
		// writes a value into a global var
		WriteGlobalVar,
		// reads a dynamic property (property name is not known statically)
		ReadDynamicIndex,
		// writes a dynamic property (property name is not known statically)
		WriteDynamicIndex,
		// reads a value from a vector (index is numeric)
		ReadVectorIndex,
		// writes a value into a vector (index is numeric)
		WriteVectorIndex,
		// reads a byte from a byte array
		ReadByteArrayIndex,
		// writes a byte into a byte array
		WriteByteArrayIndex,
		// deletes a dynamic property
		DeleteDynamicIndex,

		InstrWriteNamedField,
		InstrWriteArrayIndex,

		// === ITERATION ===
		// starts iteration of an object
		IteratorNew,
		// advances the iterator and transfers the control flow
		// appropriately
		IteratorNext,

		// === BOXING ===
		// converts RestParams to Array
		RestToArray,
		// returns the length of the RestParams
		RestLength,

		ConsVector,
		
		// === COMPARISON OPERATORS ===
		// compares two values dynamically
		// supports eq, neq, seq, sneq
		DynCmp,
		// compares two booleans
		// supports eq, neq, lt, gt, le, ge
		BoolCmp,
		// compares two numbers
		// supports eq, neq, lt, gt, le, ge
		NumCmp,
		// compares two strings
		// supports eq, neq, lt, gt, le, ge
		StringCmp,
		// compares two values by reference
		// supports eq, neq
		RefCmp,
		// compares two functions
		// supports eq, neq
		FuncCmp,

		// === ARITHMETICS ===
		// string concatenation
		StringConcat,
		// binary numeric operator
		NumBinary,
		// unary numeric operator
		NumUnary,
		// binary bit operator
		BitBinary,
		// unary bit operator
		BitUnary,

		// checks if a key is in an object
		DynIn,

		// === TYPE CAST OPERATORS ===
		// type operator where the type is static
		TypeCast,
		// type operator where the type is dynamic
		TypeCastDyn
	} tag;
	ValueInstr result_value;
	srcspan_t span = srcspan_t::undefined();
	uz index = 0;

	Instr(Tag tag, ir::Type* type):
		tag(tag),
		result_value(type)
	{
	}

	Value* toValue(){
		return &result_value;
	}
};
SYNTHESIZE_PM_BASE(InstrBase, Instr);


enum class NumBinaryOp{
	Add,
	Sub,
	Mul,
	Div,
	Mod,
	BitAnd,
	BitOr,
	BitXor,
	Shl,
	AShr,
	LShr
};

struct InstrNumBinary : public InstrBase<Instr::Tag::NumBinary> {
	NumBinaryOp op;
	Value* lhs;
	Value* rhs;

	InstrNumBinary(ir::Type* type, NumBinaryOp op, Value* lhs, Value* rhs) :
		InstrBase(type),
		op(op),
		lhs(lhs),
		rhs(rhs)
	{
	}
};

struct InstrStringConcat : public InstrBase<Instr::Tag::StringConcat> {
	Value* lhs;
	Value* rhs;

	InstrStringConcat(ir::Type* type, Value* lhs, Value* rhs) :
		InstrBase(type),
		lhs(lhs),
		rhs(rhs) 
	{
	}
};

enum class NumUnaryOp {
	BitInvert,
	Negate,
};

struct InstrNumUnary : public InstrBase<Instr::Tag::NumUnary> {
	NumUnaryOp op;
	Value* arg;

	InstrNumUnary(ir::Type* type, NumUnaryOp op, Value* arg) :
		InstrBase(type),
		op(op),
		arg(arg)
	{
	}
};

enum class BitBinaryOp{
	And,
	Or,
	Xor,
	Shl,
	LShr,
	AShr
};

enum class BitUnaryOp{
	Invert
};


// returns a value
struct InstrRet: public InstrBase<Instr::Tag::Ret>{
	Value* value;

	InstrRet(Value* value):
		InstrBase(nullptr),
		value(value){}
};

// conditional branch
struct InstrCondBr : public InstrBase<Instr::Tag::CondBr> {
	Value* cond;
	BasicBlock* bb_true;
	BasicBlock* bb_false;

	InstrCondBr(Value* cond, BasicBlock* bb_true, BasicBlock* bb_false) :
		InstrBase(nullptr),
		cond(cond),
		bb_true(bb_true),
		bb_false(bb_false)
	{}
};

struct InstrSwitchBr : public InstrBase<Instr::Tag::SwitchBr> {
	Value* cond;
	BasicBlock* bb_default;
	ArrayRef< std::pair<u32, BasicBlock*> > bb_branches;

	InstrSwitchBr(
		Value* cond, 
		BasicBlock* bb_default, 
		ArrayRef< std::pair<u32, BasicBlock*> > bb_branches
	) :
		InstrBase(nullptr),
		cond(cond),
		bb_default(bb_default),
		bb_branches(bb_branches)
	{
	}
};

// unconditional branch
struct InstrBr : public InstrBase<Instr::Tag::Br> {
	BasicBlock* bb;

	InstrBr(BasicBlock* bb):
		InstrBase(nullptr),
		bb(bb){
	}
};

struct InstrUnreachable : public InstrBase<Instr::Tag::Unreachable> {
	InstrUnreachable() :
		InstrBase(nullptr)
	{}
};

struct InstrThrow : public InstrBase<Instr::Tag::Throw> {
	Value* value;

	InstrThrow(Value* value) :
		InstrBase(nullptr),
		value(value)
	{
	}
};

struct InstrResume : public InstrBase<Instr::Tag::Resume> {
	InstrResume() :
		InstrBase(nullptr)
	{
	}
};

struct InstrCatchPad : public InstrBase<Instr::Tag::CatchPad> {
	InstrCatchPad(ir::Type* type) :
		InstrBase(type)
	{
	}
};

struct InstrCallGlobal: public InstrBase<Instr::Tag::CallGlobal> {
	ir::GlobalFunction* func;
	ArrayRef<Value*> args;

	InstrCallGlobal(ir::GlobalFunction* func, ArrayRef<Value*> args) : 
		InstrBase(func->signature->retTy),
		func(func),
		args(args){
	}
};

struct InstrCallDynamic: public InstrBase<Instr::Tag::CallDynamic> {
	Value* func;
	ArrayRef<Value*> args;

	InstrCallDynamic(Value* func, ArrayRef<Value*> args) :
		InstrBase(&ir::TypePrim::Any),
		func(func),
		args(args){
	}
};

enum class CmpOp{
	EQ,
	NEQ,
	LT,
	GT,
	LE,
	GE,
	SEQ,
	SNEQ
};

// Compares boolean to boolean.
// Both operands must be of type Boolean.
// Strict comparison operators aren't supported.
struct InstrBoolCmp : public InstrBase<Instr::Tag::BoolCmp> {
	CmpOp op;
	Value* lhs;
	Value* rhs;

	InstrBoolCmp(CmpOp op, Value* lhs, Value* rhs) :
		InstrBase(&ir::TypePrim::Boolean),
		op(op),
		lhs(lhs),
		rhs(rhs) {
	}
};

// Compares number to number.
// Both operands must be of numeric (int, uint, Number) types, but may be
// of different types.
// Strict comparison operators aren't supported.
struct InstrNumCmp : public InstrBase<Instr::Tag::NumCmp> {
	CmpOp op;
	Value* lhs;
	Value* rhs;

	InstrNumCmp(CmpOp op, Value* lhs, Value* rhs) :
		InstrBase(&ir::TypePrim::Boolean),
		op(op),
		lhs(lhs),
		rhs(rhs)
	{
	}
};

// Compares string to string.
// Both operands must be of type String.
struct InstrStringCmp : public InstrBase<Instr::Tag::StringCmp> {
	CmpOp op;
	Value* lhs;
	Value* rhs;

	InstrStringCmp(CmpOp op, Value* lhs, Value* rhs) :
		InstrBase(&ir::TypePrim::Boolean),
		op(op),
		lhs(lhs),
		rhs(rhs) {
	}
};

// Compares two values by reference.
// Values may be of different types, but must be of reference types.
// Only EQ and NEQ operators are supported.
struct InstrRefCmp : public InstrBase<Instr::Tag::RefCmp> {
	CmpOp op;
	Value* lhs;
	Value* rhs;

	InstrRefCmp(CmpOp op, Value* lhs, Value* rhs) :
		InstrBase(&ir::TypePrim::Boolean),
		op(op),
		lhs(lhs),
		rhs(rhs) {
	}
};

struct InstrFuncCmp : public InstrBase<Instr::Tag::FuncCmp> {
	CmpOp op;
	Value* lhs;
	Value* rhs;

	InstrFuncCmp(CmpOp op, Value* lhs, Value* rhs) :
		InstrBase(&ir::TypePrim::Boolean),
		op(op),
		lhs(lhs),
		rhs(rhs) {
	}
};

// Compares two values dynamically.
// Values must be of type *.
struct InstrDynCmp : public InstrBase<Instr::Tag::DynCmp> {
	CmpOp op;
	Value* lhs;
	Value* rhs;

	InstrDynCmp(CmpOp op, Value* lhs, Value* rhs) :
		InstrBase(&ir::TypePrim::Boolean),
		op(op),
		lhs(lhs),
		rhs(rhs) {
	}
};


struct InstrCallClassMethod : public InstrBase<Instr::Tag::CallClassMethod> {
	ir::ClassMethod* method;
	Value* thisval;
	ArrayRef<Value*> args;
	bool nonvirtual;

	InstrCallClassMethod(ir::ClassMethod* method, Value* thisval, ArrayRef<Value*> args, bool nonvirtual):
		InstrBase(method->signature->retTy),
		method(method),
		thisval(thisval),
		args(args),
		nonvirtual(nonvirtual)
	{
	}
};

struct InstrCallVectorMethod : public InstrBase<Instr::Tag::CallVectorMethod> {
	ir::ClassMethod* method;
	Value* thisval;
	ArrayRef<Value*> args;

	InstrCallVectorMethod(ir::Type* ret_type, ir::ClassMethod* method, Value* thisval, ArrayRef<Value*> args) :
		InstrBase(ret_type),
		method(method),
		thisval(thisval),
		args(args) {
	}
};

struct InstrNewClass : public InstrBase<Instr::Tag::NewClass> {
	ir::Class* class_;
	ArrayRef<Value*> args;

	InstrNewClass(ir::Class* class_, ArrayRef<Value*> args) :
		InstrBase(&class_->classType),
		class_(class_),
		args(args) 
	{
	}
};

struct InstrNewVector : public InstrBase<Instr::Tag::NewVector> {
	ir::TypeVector* vec;
	ArrayRef<Value*> args;

	InstrNewVector(ir::TypeVector* vec, ArrayRef<Value*> args) :
		InstrBase(vec),
		vec(vec),
		args(args) 
	{
	}
};

struct InstrNewDynamic : public InstrBase<Instr::Tag::NewDynamic> {
	Value* class_;
	ArrayRef<Value*> args;

	InstrNewDynamic(Value* class_, ArrayRef<Value*> args) :
		InstrBase(&ir::TypePrim::Any),
		class_(class_),
		args(args)
	{
	}
};

struct InstrGlobalFuncClosure : public InstrBase<Instr::Tag::GlobalFuncClosure> {
	ir::GlobalFunction* func;

	InstrGlobalFuncClosure(ir::Type* type, ir::GlobalFunction* func) :
		InstrBase(type),
		func(func)
	{
	}
};

struct InstrLocalClosure : public InstrBase<Instr::Tag::LocalClosure> {
	Closure* func;

	InstrLocalClosure(ir::Type* type, Closure* func) :
		InstrBase(type),
		func(func) {
	}
}; 

struct InstrClassMethodClosure : public InstrBase<Instr::Tag::ClassMethodClosure> {
	ir::ClassMethod* method;
	Value* object;

	InstrClassMethodClosure(ir::Type* type, ir::ClassMethod* method, Value* object) :
		InstrBase(type),
		method(method),
		object(object)
	{
	}
};

struct InstrInterfaceMethodClosure : public InstrBase<Instr::Tag::InterfaceMethodClosure> {
	ir::InterfaceMethod* method;
	Value* object;

	InstrInterfaceMethodClosure(ir::Type* type, ir::InterfaceMethod* method, Value* object) :
		InstrBase(type),
		method(method),
		object(object) {
	}
};

struct InstrClassClosure : public InstrBase<Instr::Tag::ClassClosure> {
	ir::Class* class_;

	InstrClassClosure(ir::Type* type, ir::Class* class_) :
		InstrBase(type),
		class_(class_) {
	}
};

struct InstrInterfaceClosure : public InstrBase<Instr::Tag::InterfaceClosure> {
	ir::Interface* iface;

	InstrInterfaceClosure(ir::Type* type, ir::Interface* iface) :
		InstrBase(type),
		iface(iface) {
	}
};

struct InstrCallInterfaceMethod : public InstrBase<Instr::Tag::CallIntefaceMethod> {
	ir::InterfaceMethod* method;
	Value* thisval;
	ArrayRef<Value*> args;

	InstrCallInterfaceMethod(ir::InterfaceMethod* method, Value* thisval, ArrayRef<Value*> args) :
		InstrBase(method->signature->retTy),
		method(method),
		thisval(thisval),
		args(args) {
	}
};

struct InstrCallClosure : public InstrBase<Instr::Tag::CallClosure> {
	icode::Closure* closure;
	ArrayRef<Value*> args;

	InstrCallClosure(icode::Closure* closure, ArrayRef<Value*> args) :
		InstrBase(closure->signature->retTy),
		closure(closure),
		args(args) {
	}
};

struct InstrPhi: public InstrBase<Instr::Tag::Phi>{
	ArrayRef< std::pair<BasicBlock*, Value*> > branches;

	InstrPhi(ir::Type* type, ArrayRef< std::pair<BasicBlock*, Value*> > branches):
		InstrBase(type),
		branches(branches)
	{
	}
};

struct InstrChoice : public InstrBase<Instr::Tag::Choice> {
	Value* sel;
	Value* val_true;
	Value* val_false;

	InstrChoice(Value* sel, ir::Type* type, Value* val_true, Value* val_false) :
		InstrBase(type),
		sel(sel),
		val_true(val_true),
		val_false(val_false)
	{
	}
};

struct InstrReadLocal : public InstrBase<Instr::Tag::ReadLocal> {
	LocalVar* var;

	InstrReadLocal(LocalVar* var):
		InstrBase(var->type),
		var(var){
	}
};

struct InstrWriteLocal : public InstrBase<Instr::Tag::WriteLocal> {
	LocalVar* var;
	Value* value;

	InstrWriteLocal(LocalVar* var, Value* value) :
		InstrBase(nullptr),
		var(var),
		value(value)
	{
	}
};

struct InstrReadUpvalue : public InstrBase<Instr::Tag::ReadUpvalue> {
	Upvalue* value;

	InstrReadUpvalue(Upvalue* value) :
		InstrBase(value->type),
		value(value) {
	}
};

struct InstrWriteUpvalue : public InstrBase<Instr::Tag::WriteUpvalue> {
	Upvalue* upvalue;
	Value* value;

	InstrWriteUpvalue(Upvalue* upvalue, Value* value) :
		InstrBase(nullptr),
		upvalue(upvalue),
		value(value) 
	{
	}
};

struct InstrReadClassField : public InstrBase<Instr::Tag::ReadClassField> {
	Value* thisval;
	ir::ClassField* field;

	InstrReadClassField(ir::ClassField* field, Value* thisval) :
		InstrBase(field->type),
		thisval(thisval),
		field(field)
	{
	}
};

struct InstrWriteClassField : public InstrBase<Instr::Tag::WriteClassField> {
	Value* thisval;
	ir::ClassField* field;
	Value* value;

	InstrWriteClassField(ir::ClassField* field, Value* thisval, Value* value) :
		InstrBase(nullptr),
		thisval(thisval),
		field(field),
		value(value)
	{
	}
};

struct InstrReadGlobalVar : public InstrBase<Instr::Tag::ReadGlobalVar> {
	ir::GlobalVar* var;

	InstrReadGlobalVar(ir::GlobalVar* var) :
		InstrBase(var->type),
		var(var)
	{
	}
};

struct InstrWriteGlobalVar : public InstrBase<Instr::Tag::WriteGlobalVar> {
	ir::GlobalVar* var;
	Value* value;

	InstrWriteGlobalVar(ir::GlobalVar* var, Value* value) :
		InstrBase(nullptr),
		var(var),
		value(value)
	{
	}
};



struct InstrDeleteDynamicIndex : public InstrBase<Instr::Tag::DeleteDynamicIndex> {
	Value* object;
	Value* index;

	InstrDeleteDynamicIndex(Value* object, Value* index) :
		InstrBase(&ir::TypePrim::Any),
		object(object),
		index(index) {
	}
};


struct InstrReadDynamicIndex : public InstrBase<Instr::Tag::ReadDynamicIndex> {
	Value* object;
	Value* index;

	InstrReadDynamicIndex(Value* object, Value* index) :
		InstrBase(&ir::TypePrim::Any),
		object(object),
		index(index) {
	}
};

struct InstrWriteArrayIndex : public InstrBase<Instr::Tag::InstrWriteArrayIndex> {
	Value* object;
	i32 index;
	Value* value;

	InstrWriteArrayIndex(Value* object, i32 index, Value* value) :
		InstrBase(nullptr),
		object(object),
		index(index),
		value(value)
	{
	}
};

struct InstrWriteNamedField : public InstrBase<Instr::Tag::InstrWriteNamedField> {
	Value* object;
	StringRef name;
	Value* value;
	
	InstrWriteNamedField(Value* object, StringRef name, Value* value) :
		InstrBase(nullptr),
		object(object),
		name(name),
		value(value)
	{
	}
};

struct InstrWriteDynamicIndex : public InstrBase<Instr::Tag::WriteDynamicIndex> {
	Value* object;
	Value* index;
	Value* value;

	InstrWriteDynamicIndex(Value* object, Value* index, Value* value) :
		InstrBase(nullptr),
		object(object),
		index(index),
		value(value) 
	{
	}
};

struct InstrReadVectorIndex : public InstrBase<Instr::Tag::ReadVectorIndex> {
	Value* object;
	Value* index;

	InstrReadVectorIndex(ir::TypeVector* type, Value* object, Value* index) :
		InstrBase(type->eltTy),
		object(object),
		index(index) {
	}
};


struct InstrWriteVectorIndex : public InstrBase<Instr::Tag::WriteVectorIndex> {
	Value* object;
	Value* index;
	Value* value;

	InstrWriteVectorIndex(Value* object, Value* index, Value* value) :
		InstrBase(nullptr),
		object(object),
		index(index),
		value(value) {
	}
};


struct InstrReadByteArrayIndex : public InstrBase<Instr::Tag::ReadByteArrayIndex> {
	Value* object;
	Value* index;

	InstrReadByteArrayIndex(Value* object, Value* index) :
		InstrBase(&ir::TypePrim::Uint),
		object(object),
		index(index) {
	}
};


struct InstrWriteByteArrayIndex : public InstrBase<Instr::Tag::WriteByteArrayIndex> {
	Value* object;
	Value* index;
	Value* value;

	InstrWriteByteArrayIndex(Value* object, Value* index, Value* value) :
		InstrBase(nullptr),
		object(object),
		index(index),
		value(value) {
	}
};


struct InstrDynIn : public InstrBase<Instr::Tag::DynIn> {
	Value* key;
	Value* object;

	InstrDynIn(Value* key, Value* object) :
		InstrBase(&ir::TypePrim::Boolean),
		key(key),
		object(object) {
	}
};




struct InstrConsVector : public InstrBase<Instr::Tag::ConsVector> {
	ir::TypeVector* type;
	Value* arg;

	InstrConsVector(ir::TypeVector* type, Value* arg) :
		InstrBase(type),
		arg(arg),
		type(type)
	{
	}
};


struct InstrRestToArray : public InstrBase<Instr::Tag::RestToArray> {
	Value* value;

	InstrRestToArray(Value* value, ir::Type* type) :
		InstrBase(type),
		value(value) {
	}
};

struct InstrRestLength : public InstrBase<Instr::Tag::RestLength> {
	Value* value;

	InstrRestLength(Value* value) :
		InstrBase(&ir::TypePrim::Uint),
		value(value) 
	{
	}
};

struct InstrIteratorNew : public InstrBase<Instr::Tag::IteratorNew> {
	Value* object;

	InstrIteratorNew(Value* object) :
		InstrBase(nullptr),
		object(object)
	{
	}
};

struct InstrIteratorNext : public InstrBase<Instr::Tag::IteratorNext> {
	enum Kind{
		Key,
		Value
	};

	InstrIteratorNew* iterator;
	Kind kind;
	BasicBlock* next_bb;
	BasicBlock* end_bb;

	InstrIteratorNext(
		InstrIteratorNew* iterator,
		ir::Type* type,
		Kind kind,
		BasicBlock* next_bb, 
		BasicBlock* end_bb
	) :
		InstrBase(type),
		iterator(iterator),
		kind(kind),
		next_bb(next_bb),
		end_bb(end_bb)
	{
	}
};

enum class TypeCastOp{
	Is,
	As,
	To
};

struct InstrTypeCast : public InstrBase<Instr::Tag::TypeCast> {
	Value* object;
	ir::Type* cast_type;
	TypeCastOp op;

	InstrTypeCast(ir::Type* type, Value* object, ir::Type* cast_type, TypeCastOp op) :
		InstrBase(type),
		object(object),
		cast_type(cast_type),
		op(op)
	{
	}
};

struct InstrTypeCastDyn : public InstrBase<Instr::Tag::TypeCastDyn> {
	Value* object;
	Value* cast_type;
	TypeCastOp op;

	InstrTypeCastDyn(ir::Type* type, Value* object, Value* cast_type, TypeCastOp op) :
		InstrBase(type),
		object(object),
		cast_type(cast_type),
		op(op)
	{
	}
};



struct super_tag_t{};
struct dynamic_index_tag {};
struct vector_index_tag {};
struct bytearray_index_tag {};
struct vector_instance_tag{};

struct Reference {
	enum class Tag{
		None,
		Value,
		LocalVar,
		Upvalue,
		Closure,
		Definition,
		Package,
		ClassMember,
		InterfaceMember,
		Super,
		DynamicIndex,
		VectorIndex,
		ByteArrayIndex,
		VectorInstance,
		Intrinsic
	} tag;
	union{
		Value* value;
		LocalVar* local_var;
		ir::Definition* definition;
		CUPackage* package;
		Upvalue* upvalue;
		Closure* closure;
		ir::TypeVector* vector_ty;

		struct {
			ir::ClassMember* member;
			Value* instance;
			bool nonvirtual;
		} class_member;

		struct {
			ir::InterfaceMember* member;
			Value* instance;
		} interface_member;

		struct {
			Value* instance;
			Value* index;
		} dynamic_index;

		struct {
			Intrinsic intrin;
			Value* instance;
		} intrinsic;
	};

	Reference(nullptr_t) :
		tag(Tag::None)
	{
	}

	Reference(super_tag_t) :
		tag(Tag::Super)
	{
	}

	Reference(Intrinsic intrin, Value* instance) :
		tag(Tag::Intrinsic)
	{
		intrinsic = {intrin, instance};
	}

	Reference(vector_instance_tag, ir::TypeVector* type):
		tag(Tag::VectorInstance),
		vector_ty(type)
	{
	}

	Reference(dynamic_index_tag, Value* instance, Value* index) :
		tag(Tag::DynamicIndex) {
		dynamic_index = { instance, index };
	}

	Reference(vector_index_tag, Value* instance, Value* index) :
		tag(Tag::VectorIndex) {
		dynamic_index = { instance, index };
	}

	Reference(bytearray_index_tag, Value* instance, Value* index) :
		tag(Tag::ByteArrayIndex) {
		dynamic_index = { instance, index };
	}

	Reference(Value* value):
		tag(Tag::Value),
		value(value)
	{
	}

	Reference(ir::ClassMember* member, Value* instance):
		tag(Tag::ClassMember)
	{
		class_member = {member, instance, false};
	}

	Reference(ir::InterfaceMember* member, Value* instance) :
		tag(Tag::InterfaceMember) {
		interface_member = { member, instance };
	}

	Reference(ir::Definition* def) :
		tag(Tag::Definition),
		definition(def) {
	}

	Reference(LocalVar* var):
		tag(Tag::LocalVar),
		local_var(var)
	{}

	Reference(Upvalue* upvalue):
		tag(Tag::Upvalue),
		upvalue(upvalue)
	{}

	Reference(Closure* closure) :
		tag(Tag::Closure),
		closure(closure) {
	}

	Reference(CUPackage* package) :
		tag(Tag::Package),
		package(package) {
	}
};


struct VarInitializer{
	ir::Definition* owner;
	Reference ref;
	bool evaluated = 0;
	bool is_immutable;
	ir::Type* type;
	Constant* constant = nullptr;
	FuncFrame* frame_nonconstant = nullptr;
	ast::Expr* expr = nullptr;

	VarInitializer(
		ir::Definition* owner, 
		ir::Type* type, 
		Reference ref, 
		bool is_immutable, 
		ast::Expr* expr
	):
		owner(owner),
		expr(expr),
		type(type),
		ref(ref),
		is_immutable(is_immutable)
	{
	}
};

bool IsTerminator(Instr* instr);

inline Instr* ValueInstr::getInstr(){
	return (Instr*)((char*)this - offsetof(Instr, result_value));
}



}