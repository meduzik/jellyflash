#pragma once

#include <jellyflash/source/SourceInfo.h>
#include <jellylib/memory/MemPool.h>
#include <jellyflash/parser/AST.h>
#include <jellyflash/compiler/CompilerContext.h>
#include "TokenStream.h"

namespace jly {

class Parser{
public:
	ast::File* run(CompilerContext& cc, SourceFile* source, TokenStream& tokens, MemPool::Slice* mem, MemPool::Slice& tmp);
};


}