#pragma once

#include "ASTCommon.h"

namespace jly::ast{

struct PackageDecl;
struct PackageBlockEntry;

struct File {
	PackageDecl* packageDecl;
	ArrayRef<PackageBlockEntry*> entries;

	File(
		PackageDecl* packageDecl,
		ArrayRef<PackageBlockEntry*> entries
	) :
		packageDecl(packageDecl),
		entries(entries) {
	}
};

struct PackageDecl {
	QualID* qid;
	ArrayRef<PackageBlockEntry*> entries;

	PackageDecl(QualID* qid, ArrayRef<PackageBlockEntry*> entries) :
		qid(qid),
		entries(entries) {
	}
};

}