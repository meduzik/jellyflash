#pragma once

#include <jellylib/stdbits.h>
#include <jellylib/types.h>
#include <jellyflash/parser/Token.h>
#include <jellyflash/source/SourceInfo.h>



namespace jly {

class TokenStream {
public:
	jly::vector<token_type> tokens;
	jly::vector<srcoffset_t> offsets;
};

}
