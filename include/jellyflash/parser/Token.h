#pragma once
#include <jellylib/types.h>

namespace jly {

#define TOKEN_LIST \
	X(error) \
	X(space) \
	X(newline) \
	X(line_comment) \
	X(ml_comment) \
	X(open_paren) \
	X(close_paren) \
	X(open_brace) \
	X(close_brace) \
	X(open_bracket) \
	X(close_bracket) \
	X(op_ellipsis) \
	X(op_shl) \
	X(op_ashr) \
	X(op_lshr) \
	X(op_mod) \
	X(op_mul) \
	X(op_add) \
	X(op_sub) \
	X(op_div) \
	X(op_lt) \
	X(op_gt) \
	X(op_le) \
	X(op_ge) \
	X(op_eq) \
	X(op_ne) \
	X(op_logic_or) \
	X(op_logic_and) \
	X(op_bit_and) \
	X(op_bit_or) \
	X(op_bit_xor) \
	X(op_incr) \
	X(op_decr) \
	X(op_tilde) \
	X(op_exclam) \
	X(op_quest) \
	X(op_strict_eq) \
	X(op_strict_ne) \
	X(op_assign) \
	X(op_assign_mul) \
	X(op_assign_div) \
	X(op_assign_mod) \
	X(op_assign_add) \
	X(op_assign_sub) \
	X(op_assign_shl) \
	X(op_assign_ashr) \
	X(op_assign_lshr) \
	X(op_assign_bit_and) \
	X(op_assign_bit_xor) \
	X(op_assign_bit_or) \
	X(op_assign_logic_or) \
	X(op_assign_logic_and) \
	X(op_dot) \
	X(op_semicolon) \
	X(op_comma) \
	X(op_colon) \
	X(op_colon_colon) \
	X(identifier) \
	X(string) \
	X(regexp) \
	X(decimal) \
	X(hexidecimal) \
	X(k_break) \
	X(k_case) \
	X(k_continue) \
	X(k_default) \
	X(k_do) \
	X(k_while) \
	X(k_else) \
	X(k_for) \
	X(k_in) \
	X(k_each) \
	X(k_if) \
	X(k_return) \
	X(k_super) \
	X(k_switch) \
	X(k_throw) \
	X(k_try) \
	X(k_catch) \
	X(k_finally) \
	X(k_with) \
	X(k_dynamic) \
	X(k_final) \
	X(k_internal) \
	X(k_override) \
	X(k_private) \
	X(k_protected) \
	X(k_public) \
	X(k_static) \
	X(k_class) \
	X(k_const) \
	X(k_extends) \
	X(k_function) \
	X(k_get) \
	X(k_implements) \
	X(k_interface) \
	X(k_namespace) \
	X(k_package) \
	X(k_set) \
	X(k_var) \
	X(k_import) \
	X(k_false) \
	X(k_null) \
	X(k_this) \
	X(k_true) \
	X(k_void) \
	X(k_undefined) \
	X(k_new) \
	X(k_delete) \
	X(k_typeof) \
	X(k_as) \
	X(k_instanceof) \
	X(k_is) \
	X(k_goto) \
	X(eof) \


enum class token_type : u16 {
	#define X(t) t,
	TOKEN_LIST
	#undef X
};

const char* get_token_name(token_type tt);

}

