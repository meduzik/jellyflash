#pragma once
#include "ASTCommon.h"

namespace jly::ast{
/*
struct TypeBlockEntry {
	enum class Tag {
		Var,
		Func,
		Static
	} tag;
	srcspan_t span;

	TypeBlockEntry(Tag tag, srcspan_t span)
		: tag(tag)
		, span(span) {
	}
};
SYNTHESIZE_PM_BASE(TypeBlockEntryBase, TypeBlockEntry);


struct FuncDefType :
	public TypeBlockEntryBase<TypeBlockEntry::Tag::Func> {

	ArrayRef<Annotation*> annos;
	Modifiers mods;
	Accessor acc;
	QualID* name;
	FuncSignature* sign;
	FuncBody* body;

	FuncDefType(
		srcspan_t span,
		ArrayRef<Annotation*> annos,
		Modifiers mods,
		Accessor acc,
		QualID* name,
		FuncSignature* sign,
		FuncBody* body
	) :
		TypeBlockEntryBase(span),
		acc(acc),
		name(name),
		sign(sign),
		body(body),
		annos(annos),
		mods(mods) {
	}
};
*/

}
