#pragma once
#include "ASTCommon.h"

namespace jly::ast{


struct Expr {
	enum class Tag {
		Prim,
		Literal,
		Object,
		New,
		Array,
		Ref,
		VectorShortcut,
		Call,
		DotIndex,
		Subscript,
		Instance,
		Assign,
		BinOp,
		Conditional,
		UnOp,
		Closure,
		Seq
	} tag;
	srcspan_t span;

	Expr(Tag tag, srcspan_t span)
		: tag(tag)
		, span(span) {
	}
};
SYNTHESIZE_PM_BASE(ExprBase, Expr);

struct ExprPrim : public ExprBase<Expr::Tag::Prim> {
	enum class Kind {
		Undefined,
		True,
		False,
		Null,
		This,
		Super
	} kind;

	ExprPrim(srcspan_t span, Kind kind) :
		ExprBase(span),
		kind(kind) {
	}
};

struct ExprLiteral : public ExprBase<Expr::Tag::Literal> {
	enum class Kind {
		Decimal,
		Hexidecimal,
		String
	} kind;
	StringRef contents;

	ExprLiteral(srcspan_t span, Kind kind, StringRef contents) :
		ExprBase(span),
		kind(kind),
		contents(contents) {
	}
};

struct ExprClosure : public ExprBase<Expr::Tag::Closure> {
	ID* name;
	FuncSignature* sign;
	FuncBody* body;

	ExprClosure(srcspan_t span, ID* name, FuncSignature* sign, FuncBody* body) :
		ExprBase(span),
		name(name),
		sign(sign),
		body(body) {
	}
};

struct ExprConditional : public ExprBase<Expr::Tag::Conditional> {
	Expr* cond;
	Expr* true_br;
	Expr* false_br;

	ExprConditional(
		srcspan_t span,
		Expr* cond,
		Expr* true_br,
		Expr* false_br
	) :
		ExprBase(span),
		cond(cond),
		true_br(true_br),
		false_br(false_br) {
	}
};

struct ObjectField {
	enum class Kind {
		Number,
		String,
		Identifier
	} kind;
	ast::ID key;
	Expr* value;

	ObjectField(Kind kind, ast::ID key, Expr* value) :
		kind(kind),
		key(key),
		value(value) {
	}
};

struct ExprObject : public ExprBase<Expr::Tag::Object> {
	ArrayRef<ObjectField*> fields;

	ExprObject(srcspan_t span, ArrayRef<ObjectField*> fields) :
		ExprBase(span),
		fields(fields) {
	}
};

struct ExprArray : public ExprBase<Expr::Tag::Array> {
	ArrayRef<Expr*> elts;

	ExprArray(srcspan_t span, ArrayRef<Expr*> elts) :
		ExprBase(span),
		elts(elts) {
	}
};

struct ExprNew : public ExprBase<Expr::Tag::New> {
	Expr* head;
	ArrayRef<Expr*> args;

	ExprNew(srcspan_t span, Expr* head, ArrayRef<Expr*> args) :
		ExprBase(span),
		head(head),
		args(args) {
	}
};

enum class AssignOp {
	Normal,
	Mul,
	Div,
	Mod,
	Add,
	Sub,
	Shl,
	AShr,
	LShr,
	BitAnd,
	BitXor,
	BitOr,
	LogicOr,
	LogicAnd
};

struct ExprAssign : public ExprBase<Expr::Tag::Assign> {
	Expr* lhs;
	Expr* rhs;
	AssignOp op;

	ExprAssign(srcspan_t span, AssignOp op, Expr* lhs, Expr* rhs) :
		ExprBase(span),
		op(op),
		lhs(lhs),
		rhs(rhs) {
	}
};

enum class UnOp {
	PostInc,
	PreInc,
	PostDec,
	PreDec,
	Negate,
	UnPlus,
	BitInvert,
	LogicInvert,
	Delete,
	Typeof,
	Void
};

enum class BinOp {
	Mul,
	Div,
	Mod,
	Add,
	Sub,
	Shl,
	LShr,
	AShr,
	LT,
	GT,
	LE,
	GE,
	EQ,
	NEQ,
	StrictEQ,
	StrictNEQ,
	As,
	In,
	Instanceof,
	Is,
	BitAnd,
	BitXor,
	BitOr,
	LogicAnd,
	LogicOr,
	Comma,
};

struct ExprBinOp : public ExprBase<Expr::Tag::BinOp> {
	Expr* lhs;
	Expr* rhs;
	BinOp op;

	ExprBinOp(srcspan_t span, BinOp op, Expr* lhs, Expr* rhs) :
		ExprBase(span),
		lhs(lhs),
		rhs(rhs),
		op(op) {
	}
};

struct ExprUnOp : public ExprBase<Expr::Tag::UnOp> {
	Expr* expr;
	UnOp op;

	ExprUnOp(srcspan_t span, UnOp op, Expr* expr) :
		ExprBase(span),
		expr(expr),
		op(op) {
	}
};

struct ExprVectorShortcut : public ExprBase<Expr::Tag::VectorShortcut> {
	Type* arg;
	ArrayRef<Expr*> values;

	ExprVectorShortcut(srcspan_t span, Type* arg, ArrayRef<Expr*> values) :
		ExprBase(span),
		arg(arg),
		values(values){
	}
};

struct ExprCall : public ExprBase<Expr::Tag::Call> {
	Expr* head;
	ArrayRef<Expr*> args;

	ExprCall(srcspan_t span, Expr* head, ArrayRef<Expr*> args) :
		ExprBase(span),
		head(head),
		args(args) {
	}
};

struct ExprSeq : public ExprBase<Expr::Tag::Seq> {
	Expr* lhs;
	Expr* rhs;

	ExprSeq(srcspan_t span, Expr* lhs, Expr* rhs) :
		ExprBase(span),
		lhs(lhs),
		rhs(rhs) {
	}
};

struct ExprRef : public ExprBase<Expr::Tag::Ref> {
	NSID* id;

	ExprRef(NSID* id) :
		ExprBase(id->span),
		id(id) {
	}
};

struct ExprDotIndex : public ExprBase<Expr::Tag::DotIndex> {
	Expr* lhs;
	NSID* id;

	ExprDotIndex(srcspan_t span, Expr* lhs, NSID* id) :
		ExprBase(span),
		lhs(lhs),
		id(id) {
	}
};

struct ExprSubscript : public ExprBase<Expr::Tag::Subscript> {
	Expr* lhs;
	Expr* arg;

	ExprSubscript(srcspan_t span, Expr* lhs, Expr* arg) :
		ExprBase(span),
		lhs(lhs),
		arg(arg) {
	}
};

struct Type;

struct ExprInstance : public ExprBase<Expr::Tag::Instance> {
	Expr* lhs;
	Type* arg;

	ExprInstance(srcspan_t span, Expr* lhs, Type* arg) :
		ExprBase(span),
		lhs(lhs),
		arg(arg) {
	}
};



}