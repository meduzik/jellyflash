#pragma once

#include "ASTCommon.h"

namespace jly::ast{


struct Stmt {
	enum class Tag {
		Nop,
		Block,
		Expr,
		If,
		Var,
		Return,
		For,
		ForIn,
		DoWhile,
		While,
		Break,
		Continue,
		Goto,
		Label,
		Try,
		Throw,
		Switch
	} tag;
	srcspan_t span;

	Stmt(Tag tag, srcspan_t span)
		: tag(tag)
		, span(span) {
	}
};
SYNTHESIZE_PM_BASE(StmtBase, Stmt);

struct StmtBlock : public StmtBase<Stmt::Tag::Block> {
	ArrayRef<Stmt*> stmts;

	StmtBlock(srcspan_t span, ArrayRef<Stmt*> stmts) :
		StmtBase(span),
		stmts(stmts) {
	}
};

struct StmtNop : public StmtBase<Stmt::Tag::Nop> {
	StmtNop(srcspan_t span) :
		StmtBase(span) {
	}
};

struct StmtBreak : public StmtBase<Stmt::Tag::Break> {
	StmtBreak(srcspan_t span) :
		StmtBase(span) {
	}
};


struct StmtContinue : public StmtBase<Stmt::Tag::Continue> {
	StmtContinue(srcspan_t span) :
		StmtBase(span) {
	}
};

struct StmtGoto : public StmtBase<Stmt::Tag::Goto> {
	ID name;

	StmtGoto(srcspan_t span, ID name) :
		StmtBase(span),
		name(name) {
	}
};

struct StmtLabel : public StmtBase<Stmt::Tag::Label> {
	ID name;

	StmtLabel(srcspan_t span, ID name) :
		StmtBase(span),
		name(name) {
	}
};

struct CatchClause {
	srcspan_t span;
	ID name;
	Type* type;
	ArrayRef<Stmt*> stmts;

	CatchClause(
		srcspan_t span,
		ID name,
		Type* type,
		ArrayRef<Stmt*> stmts
	) :
		span(span),
		name(name),
		type(type),
		stmts(stmts) {
	}
};

struct StmtTry : public StmtBase<Stmt::Tag::Try> {
	ArrayRef<Stmt*> block;
	ArrayRef<CatchClause*> catches;
	ArrayRef<Stmt*>* finally;

	StmtTry(
		srcspan_t span,
		ArrayRef<Stmt*> block,
		ArrayRef<CatchClause*> catches,
		ArrayRef<Stmt*>* finally
	) :
		StmtBase(span),
		block(block),
		catches(catches),
		finally(finally) {
	}
};

struct SwitchCase {
	srcspan_t span;
	Expr* expr;
	ArrayRef<ast::Stmt*> stmts;

	SwitchCase(
		srcspan_t span,
		Expr* expr,
		ArrayRef<ast::Stmt*> stmts
	) :
		span(span),
		expr(expr),
		stmts(stmts) {
	}
};

struct StmtSwitch : public StmtBase<Stmt::Tag::Switch> {
	Expr* expr;
	ArrayRef<SwitchCase*> cases;

	StmtSwitch(
		srcspan_t span,
		Expr* expr,
		ArrayRef<SwitchCase*> cases
	) :
		StmtBase(span),
		expr(expr),
		cases(cases)
	{
	}
};

struct StmtThrow : public StmtBase<Stmt::Tag::Throw> {
	Expr* expr;

	StmtThrow(srcspan_t span, Expr* expr) :
		StmtBase(span),
		expr(expr) {
	}
};

struct StmtExpr : public StmtBase<Stmt::Tag::Expr> {
	Expr* expr;

	StmtExpr(Expr* expr) :
		StmtBase(expr->span),
		expr(expr) {
	}
};

struct StmtIf : public StmtBase<Stmt::Tag::If> {
	Expr* cond;
	Stmt* then;
	Stmt* else_;

	StmtIf(srcspan_t span, Expr* cond, Stmt* then, Stmt* else_) :
		StmtBase(span),
		cond(cond),
		then(then),
		else_(else_) {
	}
};

struct StmtWhile : public StmtBase<Stmt::Tag::While> {
	Expr* cond;
	Stmt* body;

	StmtWhile(srcspan_t span, Expr* cond, Stmt* body) :
		StmtBase(span),
		cond(cond),
		body(body) {
	}
};

struct StmtDoWhile : public StmtBase<Stmt::Tag::DoWhile> {
	Stmt* body;
	Expr* cond;

	StmtDoWhile(srcspan_t span, Stmt* body, Expr* cond) :
		StmtBase(span),
		cond(cond),
		body(body) {
	}
};

struct StmtVar : public StmtBase<Stmt::Tag::Var> {
	VarDef* def;

	StmtVar(VarDef* def) :
		StmtBase(def->span),
		def(def) {
	}
};

struct ForInit {
	enum class InitKind {
		Var,
		Expr
	} init_kind;
	union {
		StmtVar* var;
		Expr* expr;
	} init;

	ForInit(Expr* init) :
		init_kind(InitKind::Expr) {
		this->init.expr = init;
	}

	ForInit(StmtVar* init) :
		init_kind(InitKind::Var) {
		this->init.var = init;
	}
};

struct StmtForIn : public StmtBase<Stmt::Tag::ForIn> {
	bool each;
	ForInit* decl;
	Expr* gen;
	Stmt* body;

	StmtForIn(srcspan_t span, bool each, ForInit* decl, Expr* gen, Stmt* body) :
		StmtBase(span),
		each(each),
		decl(decl),
		gen(gen),
		body(body) {
	}
};


struct StmtFor : public StmtBase<Stmt::Tag::For> {
	ForInit* init;
	Expr* cond;
	Expr* incr;
	Stmt* body;

	StmtFor(srcspan_t span, ForInit* init, Expr* cond, Expr* incr, Stmt* body) :
		StmtBase(span),
		init(init),
		cond(cond),
		incr(incr),
		body(body) {
	}
};

struct StmtReturn : public StmtBase<Stmt::Tag::Return> {
	Expr* expr;

	StmtReturn(srcspan_t span, Expr* expr) :
		StmtBase(span),
		expr(expr) {
	}
};


}
