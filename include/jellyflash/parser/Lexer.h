#pragma once
#include <jellyflash/source/SourceInfo.h>
#include <jellyflash/compiler/CompilerContext.h>
#include "TokenStream.h"

namespace jly {

class Lexer{
public:
	TokenStream run(CompilerContext& cc, SourceFile* source);
private:
	jly::vector<u32> buffer;
};

}
