#pragma once
#include "ASTCommon.h"

namespace jly::ast{

struct Type {
	enum class Tag {
		Prim,
		Instance,
		Ref
	} tag;
	srcspan_t span;

	Type(Tag tag, srcspan_t span)
		: tag(tag)
		, span(span) {
	}
};
SYNTHESIZE_PM_BASE(TypeBase, Type);

struct TypePrim : public TypeBase<Type::Tag::Prim> {
	enum class Kind {
		Void,
		Star
	} kind;

	TypePrim(srcspan_t span, Kind kind) :
		TypeBase(span),
		kind(kind) {
	}
};

struct TypeInstance : public TypeBase<Type::Tag::Instance> {
	ArrayRef<ID> ids;
	Type* arg;

	TypeInstance(srcspan_t span, ArrayRef<ID> ids, Type* arg) :
		TypeBase(span),
		ids(ids),
		arg(arg) 
	{
	}
};

struct TypeRef : public TypeBase<Type::Tag::Ref> {
	ArrayRef<ID> ids;

	TypeRef(srcspan_t span, ArrayRef<ID> ids) :
		TypeBase(span),
		ids(ids)
	{
	}
};


}
