#pragma once
#include <jellylib/patternmatching.h>
#include <jellylib/utils/BitEnum.h>
#include <jellyflash/parser/Token.h>
#include <jellyflash/source/SourceInfo.h>


namespace jly::ast{

struct Stmt;
struct Expr;
struct Type;

struct ID {
	StringRef id;
	srcspan_t span;

	ID(srcspan_t span, StringRef id)
		: id(id)
		, span(span) {
	}
};

struct QualID {
	srcspan_t span;
	ArrayRef<ID> ids;

	QualID(srcspan_t span, ArrayRef<ID> ids) :
		span(span),
		ids(ids) {
	}
};


struct Namespace {
	ID id;

	Namespace(ID id) :
		id(id) {
	}
};

extern Namespace Public, Private, Internal, Protected;

struct NSID {
	srcspan_t span;
	Namespace* ns;
	ID id;

	NSID(
		srcspan_t span,
		Namespace* ns,
		ID id
	) :
		span(span),
		ns(ns),
		id(id) {
	}
};


struct Modifiers {
	Namespace* ns = nullptr;
	bool m_static = false;
	bool m_final = false;
	bool m_override = false;
	bool m_dynamic = false;
	bool m_native = false;
};

struct AnnotationParam {
	srcspan_t span;
	ID* key;
	Expr* value;

	AnnotationParam(srcspan_t span, ID* key, Expr* value) :
		span(span),
		key(key),
		value(value) {
	}
};

struct Annotation {
	srcspan_t span;
	ID head;
	ArrayRef<AnnotationParam*> params;

	Annotation(
		srcspan_t span,
		ID head,
		ArrayRef<AnnotationParam*> params
	) :
		span(span),
		head(head),
		params(params) {
	}
};


struct VarDecl {
	ID name;
	Type* type;
	Expr* expr;

	VarDecl(ID name, Type* type, Expr* expr) :
		name(name),
		type(type),
		expr(expr) {
	}
};

struct VarDef {
	srcspan_t span;
	enum class Kind {
		Var,
		Const
	} kind;
	ArrayRef<VarDecl*> decls;

	VarDef(srcspan_t span, Kind kind, ArrayRef<VarDecl*> decls) :
		span(span),
		kind(kind),
		decls(decls) {
	}
};



enum class Accessor {
	None,
	Get,
	Set
};

struct Param {
	ID name;
	Type* type;
	Expr* initializer;

	Param(ID name, Type* type, Expr* initializer) :
		name(name),
		type(type),
		initializer(initializer) {
	}
};

struct FuncSignature {
	ArrayRef<Param*> params;
	ID* ellipsis;
	Type* retTy;

	FuncSignature(ArrayRef<Param*> params, ID* ellipsis, Type* retTy) :
		params(params),
		ellipsis(ellipsis),
		retTy(retTy) {
	}
};

struct FuncBody {
	srcspan_t span;
	ArrayRef<Stmt*> stmts;

	FuncBody(srcspan_t span, ArrayRef<Stmt*> stmts) :
		span(span),
		stmts(stmts) {
	}
};

}