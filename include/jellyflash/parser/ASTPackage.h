#pragma once
#include "ASTCommon.h"

namespace jly::ast{

struct PackageBlockEntry {
	enum class Tag {
		Import,
		UseNamespace,
		Class,
		Interface,
		Var,
		Func,
		Namespace,
		Static
	} tag;
	srcspan_t span;

	PackageBlockEntry(Tag tag, srcspan_t span)
		: tag(tag)
		, span(span) {
	}
};
SYNTHESIZE_PM_BASE(PackageBlockEntryBase, PackageBlockEntry);


struct ImportDef : public PackageBlockEntryBase<PackageBlockEntry::Tag::Import> {
	QualID* qid;

	ImportDef(srcspan_t span, QualID* qid) :
		PackageBlockEntryBase(span),
		qid(qid) {
	}
};

struct NamespaceDef : public PackageBlockEntryBase<PackageBlockEntry::Tag::Namespace> {
	ID name;
	ID value;
	ArrayRef<Annotation*> annos;
	Modifiers mods;

	NamespaceDef(
		srcspan_t span,
		ArrayRef<Annotation*> annos,
		Modifiers mods,
		ID name,
		ID value
	) :
		PackageBlockEntryBase(span),
		annos(annos),
		mods(mods),
		name(name),
		value(value) {
	}
};

struct UseNamespace : public PackageBlockEntryBase<PackageBlockEntry::Tag::UseNamespace> {
	ID name;

	UseNamespace(
		srcspan_t span,
		ID name
	) :
		PackageBlockEntryBase(span),
		name(name) {
	}
};

struct ClassDef : public PackageBlockEntryBase<PackageBlockEntry::Tag::Class> {
	ID name;
	QualID* extends;
	ArrayRef<QualID*> implements;
	ArrayRef<PackageBlockEntry*> entries;
	ArrayRef<Annotation*> annos;
	Modifiers mods;

	ClassDef(
		srcspan_t span,
		ArrayRef<Annotation*> annos,
		Modifiers mods,
		ID name,
		QualID* extends,
		ArrayRef<QualID*> implements,
		ArrayRef<PackageBlockEntry*> entries
	) :
		PackageBlockEntryBase(span),
		name(name),
		extends(extends),
		implements(implements),
		entries(entries),
		annos(annos),
		mods(mods) {
	}
};

struct InterfaceDef : public PackageBlockEntryBase<PackageBlockEntry::Tag::Interface> {
	ID name;
	ArrayRef<QualID*> extends;
	ArrayRef<PackageBlockEntry*> entries;
	ArrayRef<Annotation*> annos;
	Modifiers mods;

	InterfaceDef(
		srcspan_t span,
		ArrayRef<Annotation*> annos,
		Modifiers mods,
		ID name,
		ArrayRef<QualID*> extends,
		ArrayRef<PackageBlockEntry*> entries
	) :
		PackageBlockEntryBase(span),
		name(name),
		extends(extends),
		entries(entries),
		annos(annos),
		mods(mods) {
	}
};


struct VarDefPackage :
	public PackageBlockEntryBase<PackageBlockEntry::Tag::Var> {

	ArrayRef<Annotation*> annos;
	Modifiers mods;
	VarDef* def;

	VarDefPackage(
		srcspan_t span,
		ArrayRef<Annotation*> annos,
		Modifiers mods,
		VarDef* def
	) :
		PackageBlockEntryBase(span),
		def(def),
		annos(annos),
		mods(mods) {
	}
};



struct FuncDefPackage :
	public PackageBlockEntryBase<PackageBlockEntry::Tag::Func> {

	ArrayRef<Annotation*> annos;
	Modifiers mods;
	Accessor acc;
	QualID* name;
	FuncSignature* sign;
	FuncBody* body;

	FuncDefPackage(
		srcspan_t span,
		ArrayRef<Annotation*> annos,
		Modifiers mods,
		Accessor acc,
		QualID* name,
		FuncSignature* sign,
		FuncBody* body
	) :
		PackageBlockEntryBase(span),
		acc(acc),
		name(name),
		sign(sign),
		body(body),
		annos(annos),
		mods(mods) {
	}
};



struct StaticBlock :
	public PackageBlockEntryBase<PackageBlockEntry::Tag::Static> {

	ArrayRef<Stmt*> stmts;

	StaticBlock(
		srcspan_t span,
		ArrayRef<Stmt*> stmts
	) :
		PackageBlockEntryBase(span),
		stmts(stmts) {
	}
};


}