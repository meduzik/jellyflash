#pragma once

#include <jellyflash/compiler/CompilationUnit.h>
#include <jellyflash/compiler/CompilerContext.h>
#include <jellyflash/ir/File.h>
#include <jellyflash/icode/ICodeCompiler.h>

#include <jellylib/utils/Writer.h>

namespace jly::lto{

class LTO {
public:
	LTO();

	void run(CompilerContext* cc, MemPool::Slice* mem, MemPool::Slice& tmp);
private:
	void optimizeScope(ir::FileScope* scope);
	void optimizeClass(ir::Class* class_);

	CompilerContext* cc;
	CompilationUnit* cu;
	ir::File* IR;
	MemPool::Slice* mem;
	MemPool::Slice* tmp;

};

}
