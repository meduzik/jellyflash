#pragma once
#include <jellylib/types/StringRef.h>
#include <jellylib/types.h>
#include <jellylib/utils/Writer.h>
#include <jellyflash/source/SourceInfo.h>


namespace jly::ast{
struct ID;
}

namespace jly::ir{
struct Definition;
struct ClassMember;
struct Type;
struct ClassMember;
struct InterfaceMember;
}

namespace jly::icode{
struct Reference;
}


namespace jly::diag{

void write(FormattedWriter& writer, nullptr_t) = delete;
bool write_extra(FormattedWriter& writer, nullptr_t) = delete;

void write(FormattedWriter& writer, double value);
void write(FormattedWriter& writer, StringRef str);
void write(FormattedWriter& writer, ir::Type* type);
bool write_extra(FormattedWriter& writer, ir::Type* type);
void write(FormattedWriter& writer, const SourceLoc& loc);
void write(FormattedWriter& writer, const std::string& str);
void write(FormattedWriter& writer, const std::filesystem::path& path);
void write(FormattedWriter& writer, ir::Definition* def);
void write(FormattedWriter& writer, ir::ClassMember* member);
bool write_extra(FormattedWriter& writer, ir::ClassMember* member);
void write(FormattedWriter& writer, ir::InterfaceMember* member);
bool write_extra(FormattedWriter& writer, ir::InterfaceMember* member);
void write(FormattedWriter& writer, icode::Reference& ref);
bool write_extra(FormattedWriter& writer, icode::Reference& ref);
void write(FormattedWriter& writer, ArrayRef<ast::ID> ids);

void write(FormattedWriter& writer, ArrayRef<ir::Definition*> defs);
void write(FormattedWriter& writer, ArrayRef<ir::ClassMember*> members);
bool write_extra(FormattedWriter& writer, ArrayRef<ir::ClassMember*> members);

bool write_extra(FormattedWriter& writer, ir::Definition* def);

struct decimal {
	template<class T, typename = std::enable_if_t<std::is_integral_v<T>> >
	decimal(T val) {
		auto result = std::to_chars(buffer, buffer + sizeof(buffer), val);
		*result.ptr = 0;
	}

	decimal(double val);

	operator StringRef() {
		return StringRef((cstring)buffer);
	}

	char buffer[64];
};

}

namespace jly{

enum class DiagnosticLevel {
	Warning,
	Error,
	Fatal
};

class DiagnosticArgument {
private:
	template<typename T, typename T2 = void>
	struct has_extra {
		static const bool value = false;
	};

	template<typename T>
	struct has_extra<
		T,
		typename std::enable_if<
			std::is_same_v<
				bool,
				decltype(
					diag::write_extra(
						std::declval<FormattedWriter&>(),
						std::declval<T>()
					)
				)
			>
		>::type
	> {
		static const bool value = true;
	};

	template<class T>
	static void WriterFn(FormattedWriter& writer, void* ref){
		using T_base = std::remove_reference_t<T>;
		T_base* t = (T_base*)ref;
		diag::write(writer, *t);
	}

	template<class T>
	static std::enable_if_t<!has_extra<T>::value, bool> WriteExtraFn(FormattedWriter& writer, void* ref) {
		return false;
	}

	template<class T>
	static std::enable_if_t<has_extra<T>::value, bool> WriteExtraFn(FormattedWriter& writer, void* ref) {
		using T_base = std::remove_reference_t<T>;
		T_base* t = (T_base*)ref;
		return diag::write_extra(writer, *t);
	}
public:
	DiagnosticArgument():
		ref(nullptr)
	{
	}

	template<class T>
	DiagnosticArgument(T&& t):
		ref((void*)&t),
		writer_fn(WriterFn<T>),
		write_extra_fn(WriteExtraFn<T>)
	{
	}

	void write(FormattedWriter& writer) const{
		if ( ref ){
			writer_fn(writer, ref);
		}else{
			jl_unreachable;
		}
	}

	bool writeExtra(FormattedWriter& writer) const{
		if ( ref && write_extra_fn ){
			return write_extra_fn(writer, ref);
		}else{
			return false;
		}
	}

private:
	void* ref;
	void (*writer_fn) (FormattedWriter& writer, void* ref);
	bool (*write_extra_fn) (FormattedWriter& writer, void* ref);
};


}

