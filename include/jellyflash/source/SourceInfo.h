#pragma once
#include <jellylib/stdbits.h>
#include <jellylib/types.h>
#include <jellylib/types/ArrayRef.h>
#include <jellylib/types/StringRef.h>

namespace jly{

using srcoffset_t = u32;

struct srcloc_t{
	u32 line;
	u32 column;
};

class srcspan_t{
public:
	srcspan_t(srcoffset_t loc) :
		begin(loc),
		end(loc){
	}
	
	srcspan_t(srcoffset_t begin, srcoffset_t end) :
		begin(begin),
		end(end) {
	}

	srcoffset_t getBegin() const{
		return begin;
	}

	srcoffset_t getEnd() const{
		return end;
	}

	bool is_undefined() const{
		return begin == ~0u || end == ~0u;
	}

	static srcspan_t undefined(){
		return {~0u, ~0u};
	}

	bool operator==(srcspan_t other){
		return begin == other.begin && end == other.end;
	}

	bool operator!=(srcspan_t other) {
		return begin != other.begin || end != other.end;
	}
private:
	srcoffset_t begin, end;
};

class SourceFile;

class SourceLoc{
public:
	SourceLoc(const SourceFile* file, srcspan_t span):
		file(file),
		span(span){
	}

	const SourceFile& getFile() const{
		return *file;
	}

	srcspan_t getSpan() const {
		return span;
	}
private:
	const SourceFile* file;
	srcspan_t span;
};

class SourceLineMap {
public:
	SourceLineMap();
	u32& pushLine();
	u32 getColByOffset(srcoffset_t offset) const;
	u32 getLineByOffset(srcoffset_t offset) const;
	srcloc_t getLocByOffset(srcoffset_t offset) const;
	std::pair<u32, u32> getLineBoundaries(uz line) const;
private:
	jly::vector<u32> lines;
};

class SourceFile{
public:
	SourceFile(std::string path, jly::vector<u8> data)
		: path(std::move(path))
		, data(std::move(data)){
	}

	StringRef getPath() const{
		return path;
	}

	StringRef getLine(u32 line) const;

	ArrayRef<u8> getData() const{
		return data;
	}

	SourceLineMap& getLineMap(){
		return lineMap;
	}

	const SourceLineMap& getLineMap() const {
		return lineMap;
	}

	static SourceFile ReadFile(const std::filesystem::path& filePath);
private:
	std::string path;
	jly::vector<u8>	data;
	SourceLineMap lineMap;
};


}

