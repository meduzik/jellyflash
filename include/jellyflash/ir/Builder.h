#pragma once
#include <jellyflash/parser/AST.h>
#include <jellyflash/ir/File.h>
#include <jellylib/memory/MemPool.h>
#include <jellylib/types/ConsList.h>
#include <jellyflash/ir/Annotation.h>

namespace jly{

struct FileScope{
	ir::FileScope* IR;
	Cons<ir::ImportPackage*> import_packages;
	Cons<ir::ImportUnit*> import_units;
	Cons<ir::Definition*> definitions;
	Cons< std::pair<ast::ID, ir::NamespaceNode*> > namespaces_late;
	Cons<ir::NamespaceNode*> opened_namespaces;

	FileScope(ir::FileScope* IR, MemPool::Slice* mem):
		IR(IR),
		import_packages(mem),
		import_units(mem),
		definitions(mem),
		namespaces_late(mem),
		opened_namespaces(mem)
	{
	}
};

struct InterfaceBuilder{
	FileScope* scope;
	Cons<ir::InterfaceMethod*> methods;

	InterfaceBuilder(FileScope* scope, MemPool::Slice* mem):
		scope(scope),
		methods(mem)
	{
	}
};

struct ClassBuilder{
	FileScope* scope;
	Cons<ir::ClassField*> static_fields;
	Cons<ir::ClassField*> instance_fields;
	Cons<ir::ClassMethod*> static_methods;
	Cons<ir::ClassMethod*> instance_methods;
	Cons<ir::StaticBlock*> static_blocks;

	ClassBuilder(FileScope* scope, MemPool::Slice* mem):	
		scope(scope),
		static_fields(mem),
		instance_fields(mem),
		static_methods(mem),
		instance_methods(mem),
		static_blocks(mem)
	{
	}
};

class Builder{
public:
	Builder();
	ir::File* run(CompilerContext* cc, ast::File* AST, MemPool::Slice* mem, MemPool::Slice& tmp);
private:
	ir::File* buildFile(ast::File* AST);
	void verifyPackage(ast::QualID* qid);
	void visitEntry(FileScope* scope, ast::PackageBlockEntry* entry, bool in_package);
	void visitImport(FileScope* scope, ast::ImportDef* import);
	void visitClass(FileScope* scope, ast::ClassDef* class_);
	void visitInterface(FileScope* scope, ast::InterfaceDef* iface);
	void visitNamespaceDef(FileScope* scope, ast::NamespaceDef* ns);
	void visitUseNamespace(FileScope* scope, ast::UseNamespace* use_ns);
	void visitGlobalVar(FileScope* scope, ast::VarDefPackage* var);
	void visitGlobalFunction(FileScope* scope, ast::FuncDefPackage* func);
	void fillScope(ir::FileScope* IR, FileScope* scope);
	
	void addClassVar(ClassBuilder* class_builder, ir::Class* class_, ast::VarDefPackage* var);
	void addClassMethod(ClassBuilder* class_builder, ir::Class* class_, ast::FuncDefPackage* func);
	void addClassStaticBlock(ClassBuilder* class_builder, ir::Class* class_, ast::StaticBlock* block);

	void addInterfaceMethod(InterfaceBuilder* interface_build, ir::Interface* iface, ast::FuncDefPackage* func);

	ir::NamespaceNode* buildNamespaceNode(FileScope* scope, ast::ID name);

	ir::NamespaceNode* resolveToplevelNamespace(srcspan_t span, ast::Namespace* ns, bool is_private);
	ir::NamespaceNode* resolveMemberNamespace(FileScope* scope, ast::Namespace* ns);

	ir::Annotation* buildAnnotation(ast::Annotation* anno);

	void expose(ir::Definition* def);
	void exports(FileScope* scope, ir::Definition* def);

	CompilerContext* cc;
	CompilationUnit* cu;
	MemPool::Slice* mem;
	MemPool::Slice* tmp;
	ir::File* IR;
};

}
