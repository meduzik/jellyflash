#pragma once

#include <jellyflash/parser/AST.h>
#include <jellylib/types/StringRef.h>

namespace jly{

std::string to_qual_id(ArrayRef<ast::ID> ids);

}

