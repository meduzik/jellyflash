#pragma once
#include "File.h"
#include <jellyflash/parser/AST.h>
#include <jellylib/types/ArrayRef.h>
#include "Type.h"

namespace jly{
struct CodegenClass;
struct CodegenInterface;
}

namespace jly::icode{
struct FuncFrame;
struct VarInitializer;
}

namespace jly::ir{

enum class Accessor {
	None,
	Get,
	Set
};

class Class;
class Interface;
struct NamespaceNode;
struct FunctionSignature;

struct LinkClass;
struct LinkInterface;

struct Definition {
	enum class Tag {
		Class,
		Interface,
		Namespace,
		Function,
		Var
	} tag;
	FileScope* scope;
	NamespaceNode* ns = nullptr;
	SourceLoc loc;
	StringRef name;

	Definition(Tag tag, FileScope* scope, SourceLoc loc, StringRef name):
		tag(tag),
		scope(scope),
		loc(loc),
		name(name)
	{
	}
};
SYNTHESIZE_PM_BASE(DefinitionBase, Definition);


class NamespaceDef : public DefinitionBase<Definition::Tag::Namespace> {
public:
	StringRef value;
	StringRef codegen_value = nullptr;

	NamespaceDef(
		FileScope* scope,
		SourceLoc loc,
		StringRef name,
		StringRef value
	):
		DefinitionBase(scope, loc, name),
		value(value)
	{
	}
};

struct NamespaceNode{
public:
	NamespaceDef* def;

	NamespaceNode(NamespaceDef* def):
		def(def){
	}
};

struct ClassMember;
class ClassField;
class ClassMethod;
class ClassProprety;
class InterfaceProperty;
class InterfaceMethod;
struct InterfaceMember;

struct StaticBlock{
	ir::FileScope* scope;
	ast::StaticBlock* def = nullptr;
	ir::ClassField* field = nullptr;
	icode::FuncFrame* frame = nullptr;

	StaticBlock(ir::FileScope* scope, ast::StaticBlock* def):
		def(def),
		scope(scope)
	{
	}

	StaticBlock(ir::FileScope* scope, ir::ClassField* field) :
		field(field),
		scope(scope) 
	{
	}
};

extern NamespaceNode NSPublic, NSPrivate, NSInternal, NSProtected;

struct NativeClass{
	ir::Type* primitive = nullptr;
	StringRef payload = nullptr;
	StringRef invoke = nullptr;
	ir::ClassMethod* invoke_method = nullptr;
	bool generate_gc = true;
	bool generate_iter = true;
	bool generate_dynamic = true;
	bool destructor = false;
	bool extra_gc = false;

	NativeClass(){
	}
};

class InterfaceMethodImpl;

class Class: public DefinitionBase<Definition::Tag::Class>{
public:
	ast::ClassDef* AST;

	Class* extends = nullptr;
	ArrayRef<Interface*> implements = nullptr;

	ClassMethod* constructor = nullptr;

	bool has_children = false;
	bool uses_reflection = false;
	bool need_finalizer = false;

	ArrayRef<ClassField*> static_fields = nullptr;
	ArrayRef<ClassMethod*> static_methods = nullptr;
	ArrayRef<ClassProprety*> static_properties = nullptr;

	ArrayRef<ClassField*> instance_fields = nullptr;
	ArrayRef<ClassMethod*> instance_methods = nullptr;
	ArrayRef<ClassProprety*> instance_properties = nullptr;

	ArrayRef<StaticBlock*> static_blocks = nullptr;

	slice_umultimap<StringRef, ClassMember*> instance_lookup;
	slice_umultimap<StringRef, ClassMember*> static_lookup;

	ArrayRef<InterfaceMethodImpl*> implement_methods = nullptr;
	slice_umap<InterfaceMethod*, InterfaceMethodImpl*> implement_methods_map;
	slice_umap<ClassMethod*, ClassMethod*> override_methods_map;

	slice_uset<Interface*> all_implements_set;
	slice_vector<Interface*> all_implements_list;
	slice_uset<Class*> all_bases;

	icode::FuncFrame* static_frame = nullptr;

	bool m_final = false;
	bool m_dynamic = false;
	bool m_native = false;

	NativeClass* native = nullptr;

	TypeClass classType;

	ir::Type* type = nullptr;
	ir::Type* boxType = nullptr;
	
	CodegenClass* codegen = nullptr;
	LinkClass* link = nullptr;

	Class(MemPool::Slice* mem, FileScope* scope, ast::ClassDef* AST, SourceLoc loc, StringRef name):
		DefinitionBase(scope, loc, name),
		AST(AST),
		classType(this),
		type(&classType),
		boxType(&classType),
		instance_lookup(mem),
		static_lookup(mem),
		implement_methods_map(mem),
		all_implements_set(mem),
		all_implements_list(mem),
		all_bases(mem),
		override_methods_map(mem)
	{
	}
};

class Interface : public DefinitionBase<Definition::Tag::Interface> {
public:
	ast::InterfaceDef* AST;

	ArrayRef<Interface*> extends = nullptr;

	TypeInterface type;

	ArrayRef<InterfaceMethod*> methods = nullptr;
	ArrayRef<InterfaceProperty*> properties = nullptr;
	pool_umap<StringRef, InterfaceMember*> lookup;

	CodegenInterface* codegen = nullptr;
	LinkInterface* link = nullptr;

	slice_uset<Interface*> all_implements_set;
	slice_vector<Interface*> all_implements_list;

	u32 hash_base = 0;
	u32 hash_offset = 0;

	Interface(MemPool::Slice* mem, FileScope* scope, ast::InterfaceDef* AST, SourceLoc loc, StringRef name) :
		DefinitionBase(scope, loc, name),
		AST(AST),
		type(this),
		all_implements_set(mem),
		all_implements_list(mem)
	{
	}
};

class GlobalVar : public DefinitionBase<Definition::Tag::Var> {
public:
	ast::VarDecl* def;
	ir::Type* type = nullptr;
	StringRef codegen_name = nullptr;
	icode::VarInitializer* initializer = nullptr;
	bool is_const = false;

	GlobalVar(FileScope* scope, ast::VarDecl* def, SourceLoc loc, StringRef name) :
		DefinitionBase(scope, loc, name),
		def(def) {
	}
};

class GlobalFunction : public DefinitionBase<Definition::Tag::Function> {
public:
	ast::FuncDefPackage* def;
	bool m_native = false;
	FunctionSignature* signature = nullptr;
	icode::FuncFrame* frame = nullptr;
	StringRef codegen_name = nullptr;

	bool inlined = false;

	GlobalFunction(FileScope* scope, ast::FuncDefPackage* def, SourceLoc loc, StringRef name) :
		DefinitionBase(scope, loc, name),
		def(def) {
	}
};


}
