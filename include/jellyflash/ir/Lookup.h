#pragma once

#include <jellylib/memory/MemPool.h>
#include <jellylib/types/StringRef.h>

namespace jly{
class CompilationUnit;
}

namespace jly::ir{

struct Definition;
class NamespaceDef;


using DefMap = pool_umap<StringRef, ir::Definition*>;
using DefMultiMap = pool_umap<StringRef, ir::Definition*>;
using ImportMultiMap = pool_umap<StringRef, CompilationUnit*>;

struct NamespaceLookupScope{
	slice_vector<ir::NamespaceDef*> open_namespaces;

	NamespaceLookupScope(MemPool::Slice* mem)
		: open_namespaces(mem)
	{
	}
};

class PackageLookup {
public:
	PackageLookup(){
	}

	void add(ir::Definition* def);

	const DefMultiMap& getSymbols() {
		return symbols;
	}
private:
	DefMultiMap symbols;
};

class FileLookup;

class FileScopeLookup {
public:
	FileScopeLookup(FileLookup* parent) :
		parent(parent){
	}

	void add(CompilationUnit* cu);

	const ImportMultiMap& getSymbols(){
		return symbols;
	}
private:
	ImportMultiMap symbols;
	FileLookup* parent;
};

class FileLookup {
public:
	FileLookup(){
	}

	const DefMap& getSymbols() {
		return exports;
	}

	ir::Definition* find(StringRef name);

	bool add(ir::Definition* def);
private:
	DefMap exports;
};


}