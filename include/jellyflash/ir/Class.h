#pragma once

#include "Definition.h"
#include "Function.h"

namespace jly::icode{
struct FuncFrame;
}

namespace jly::ir{

class Class;
struct Type;

struct ClassMember {
	enum class Tag {
		Field,
		Method,
		Property
	} tag;
	Class* owner;
	SourceLoc loc;
	StringRef name;
	NamespaceNode* ns = nullptr;

	ClassMember(Tag tag, Class* owner, NamespaceNode* ns, SourceLoc loc, StringRef name) :
		tag(tag),
		owner(owner),
		loc(loc),
		name(name),
		ns(ns)
	{
	}
};
SYNTHESIZE_PM_BASE(ClassMemberBase, ClassMember);


class ClassField: public ClassMemberBase<ClassMember::Tag::Field>{
public:
	bool is_static = false;
	bool is_const = false;
	ast::VarDecl* decl;
	Type* type = nullptr;
	StringRef codegen_name = nullptr;
	icode::VarInitializer* initializer = nullptr;

	ClassField(Class* owner, NamespaceNode* ns, SourceLoc loc, StringRef name, ast::VarDecl* decl) :
		ClassMemberBase(owner, ns, loc, name),
		decl(decl)
	{
	}
};

class ClassMethod: public ClassMemberBase<ClassMember::Tag::Method>{
public:
	bool is_static = false;
	bool is_final = false;
	bool is_override = false;
	bool is_native = false;
	StringRef codegen_name = nullptr;
	bool codegen_virtual = false;

	bool has_overrides = false;

	bool super_call = false;

	Accessor accessor = Accessor::None;

	FunctionSignature* signature = nullptr;
	ClassMethod* base_method = nullptr;
	ClassMethod* root_method = this;
	icode::FuncFrame* frame = nullptr;

	bool inlined = false;

	ast::FuncDefPackage* ast_def;

	ClassMethod(Class* owner, NamespaceNode* ns, SourceLoc loc, StringRef name, ast::FuncDefPackage* ast_def) :
		ClassMemberBase(owner, ns, loc, name),
		ast_def(ast_def)
	{
	}
};

class ClassProprety : public ClassMemberBase<ClassMember::Tag::Property> {
public:
	ClassProprety* base_property = nullptr;
	ClassMethod* getter = nullptr;
	ClassMethod* setter = nullptr;
	bool is_static = false;
	bool is_final = false;
	bool is_override = false;

	ClassProprety(Class* owner, NamespaceNode* ns, SourceLoc loc, StringRef name) :
		ClassMemberBase(owner, ns, loc, name) {
	}
};

class InterfaceMethod;

class InterfaceMethodImpl{
public:
	ir::Class* owner;
	ir::InterfaceMethod* imethod;
	ir::ClassMethod* cmethod;

	InterfaceMethodImpl(ir::Class* owner, ir::InterfaceMethod* imethod, ir::ClassMethod* cmethod): 
		owner(owner),
		imethod(imethod),
		cmethod(cmethod)
	{
	}
};

}


