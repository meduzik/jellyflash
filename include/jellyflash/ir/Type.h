#pragma once
#include <jellylib/patternmatching.h>
#include "File.h"

namespace jly::ir{

class Class;
class Interface;

struct Type {
	enum class Tag {
		Class,
		Interface,
		Prim,
		Var,
		Vector
	} tag;

	Type(Tag tag) :
		tag(tag)
	{
	}
};
SYNTHESIZE_PM_BASE(TypeBase, Type);

struct TypePrim : public TypeBase<Type::Tag::Prim> {
	enum class Kind{
		Any,
		Object,
		Void,
		Null,
		Boolean,
		Number,
		Int,
		Uint,
		String,
		Function,
		Class,
		Rest,
		Var
	} kind;

	static TypePrim 
		Any,
		Object,
		Void,
		Null,
		Boolean,
		Number,
		Int,
		Rest,
		String,
		Class,
		Function,
		Uint;

	ir::Class* class_ = nullptr;

	TypePrim(Kind kind) :
		kind(kind) {
	}
};


struct TypeClass: public TypeBase<Type::Tag::Class> {
	Class* class_;

	TypeClass(Class* class_):
		class_(class_)
	{
	}
};

struct TypeInterface : public TypeBase<Type::Tag::Interface> {
	Interface* iface;

	TypeInterface(Interface* iface) :
		iface(iface) {
	}
};

struct TypeVector : public TypeBase<Type::Tag::Vector> {
	Type* eltTy;

	TypeVector(Type* eltTy) :
		eltTy(eltTy) {
	}
};

struct TypeVar : public TypeBase<Type::Tag::Var> {
	static TypeVar Instance;

	TypeVar() {
	}
};

}
