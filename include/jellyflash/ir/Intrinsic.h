#pragma once
#include "Definition.h"
#include "Type.h"
#include <jellylib/memory/MemPool.h>

namespace jly::ir{

struct IntrinsicNamespaces {
	ir::NamespaceDef* AS3;
};

struct IntrinsicTypes{
	ir::Type* Null = &ir::TypePrim::Null;
	ir::Type* Void = &ir::TypePrim::Void;
	ir::Type* Any = &ir::TypePrim::Any;
	ir::Type* Object = &ir::TypePrim::Object;
	ir::Type* Boolean = &ir::TypePrim::Boolean;
	ir::Type* Int = &ir::TypePrim::Int;
	ir::Type* UInt = &ir::TypePrim::Uint;
	ir::Type* Number = &ir::TypePrim::Number;
	ir::Type* String = &ir::TypePrim::String;
	ir::Type* Function = &ir::TypePrim::Function;
	ir::Type* Class = &ir::TypePrim::Class;
	ir::TypeClass* Array = nullptr;
	ir::TypeClass* ByteArray = nullptr;
	ir::TypeClass* Vector = nullptr;
	pool_umap<ir::Type*, ir::TypeVector*> VectorInstances;
};

struct IntrinsicClasses{
	Class* Boolean = nullptr;
	Class* Number = nullptr;
	Class* Int = nullptr;
	Class* UInt = nullptr;
	Class* Object = nullptr;
	Class* Array = nullptr;
	Class* String = nullptr;
	Class* Function = nullptr;
	Class* ByteArray = nullptr;
	Class* Dictionary = nullptr;
	Class* Vector = nullptr;
	Class* Class = nullptr;
};

struct Intrinsics{
	IntrinsicClasses classes;
	IntrinsicTypes types;
	IntrinsicNamespaces namespaces;
};

}
