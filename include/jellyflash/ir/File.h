#pragma once
#include <jellylib/memory/MemPool.h>
#include <jellyflash/compiler/CompilationUnit.h>
#include "Lookup.h"
#include <jellyflash/parser/AST.h>

namespace jly{
struct CodegenFileScope;
}

namespace jly::icode{
struct ConstantString;
}

namespace jly::ir{

class Class;
class Interface;
struct NamespaceNode;
struct Definition;



class ImportPackage{
public:
	SourceLoc loc;
	CUPackage* package;
	

	ImportPackage(SourceLoc loc, CUPackage* package):
		loc(loc),
		package(package)
	{
	}
};

class ImportUnit{
public:
	SourceLoc loc;
	CompilationUnit* cu;
	StringRef name;

	ImportUnit(SourceLoc loc, StringRef name, CompilationUnit* cu) :
		loc(loc),
		cu(cu),
		name(name)
	{
	}
};


class FileScope {
public:
	File* file;
	bool is_private;
	ArrayRef< ImportUnit* > import_units = nullptr;
	ArrayRef< ImportPackage* > import_packages = nullptr;
	ArrayRef< Definition* > definitions = nullptr;
	
	ArrayRef< NamespaceNode* > opened_namespaces = nullptr;

	FileScopeLookup lookup;
	CodegenFileScope* codegen;

	ArrayRef< std::pair<ast::ID, NamespaceNode*> > namespaces_late = nullptr;
	
	FileScope(File* file, bool is_private);;
};

class File{
public:
	CompilationUnit* cu;
	FileScope package;
	FileScope internal;
	Definition* publicDef = nullptr;
	FileLookup lookup;

	File(CompilationUnit* cu):
		cu(cu),
		package(this, false),
		internal(this, true)
	{
	}
};

}