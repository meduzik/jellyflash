#pragma once
#include <jellylib/patternmatching.h>

namespace jly{

struct Expr{
	enum class Tag{
		This,
		Super,
		ClassField,

	} tag;

	Expr(Tag tag)
		: tag(tag)
	{
	}
};
SYNTHESIZE_PM_BASE(ExprBase, Expr);




}

