#pragma once
#include <jellylib/common.h>
#include <jellyflash/parser/AST.h>
#include <jellylib/memory/MemPool.h>

namespace jly::ir{

struct AnnotationValue{
	enum class Tag{
		Unspecified,
		Null,
		Boolean,
		String,
		Number
	};

	Tag tag;
	union{
		bool boolean;
		StringRef string;
		double number;
	};

	AnnotationValue():
		tag(Tag::Unspecified)
	{
	}

	AnnotationValue(nullptr_t):
		tag(Tag::Null)
	{
	}

	AnnotationValue(bool boolean):
		tag(Tag::Boolean),
		boolean(boolean)
	{
	}

	AnnotationValue(StringRef string):
		tag(Tag::String),
		string(string)
	{
	}

	AnnotationValue(double number) :
		tag(Tag::Number),
		number(number)
	{
	}

	bool as_boolean(bool* out) {
		if (tag == Tag::Boolean) {
			*out = boolean;
			return true;
		}
		return false;
	}

	bool as_string(StringRef* out){
		if ( tag == Tag::String ){
			*out = string;
			return true;
		}
		return false;
	}
};

struct AnnotationNode{
	srcspan_t span;
	StringRef key;
	AnnotationValue value;
};

struct Annotation{
	srcspan_t span;
	StringRef name;
	slice_vector<AnnotationNode> params;

	Annotation(MemPool::Slice* mem, srcspan_t span, StringRef name):
		params(mem),
		span(span),
		name(name)
	{
	}
};

}

