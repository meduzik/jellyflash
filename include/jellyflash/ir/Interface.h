#pragma once

#include "Definition.h"
#include "Function.h"

namespace jly::ir{

class Interface;
struct Type;


struct InterfaceMember {
	enum class Tag {
		Method,
		Property
	} tag;
	Interface* owner;
	SourceLoc loc;
	StringRef name;

	InterfaceMember(Tag tag, Interface* owner, SourceLoc loc, StringRef name) :
		tag(tag),
		owner(owner),
		loc(loc),
		name(name)
	{
	}
};
SYNTHESIZE_PM_BASE(InterfaceMemberBase, InterfaceMember);


class InterfaceMethod : public InterfaceMemberBase<InterfaceMember::Tag::Method> {
public:
	Accessor accessor = Accessor::None;

	FunctionSignature* signature = nullptr;

	ast::FuncDefPackage* ast_def;
	StringRef codegen_name = nullptr;

	InterfaceMethod(Interface* owner, SourceLoc loc, StringRef name, ast::FuncDefPackage* ast_def) :
		InterfaceMemberBase(owner, loc, name),
		ast_def(ast_def) {
	}
};

class InterfaceProperty : public InterfaceMemberBase<InterfaceMember::Tag::Property> {
public:
	InterfaceMethod* getter = nullptr;
	InterfaceMethod* setter = nullptr;

	InterfaceProperty(Interface* owner, SourceLoc loc, StringRef name) :
		InterfaceMemberBase(owner, loc, name) {
	}
};

}
