#pragma once
#include <jellyflash/compiler/CompilerContext.h>
#include <jellyflash/parser/AST.h>
#include <jellyflash/ir/File.h>
#include <jellylib/memory/MemPool.h>
#include <jellylib/types/ConsList.h>
#include "LinkerInfo.h"
#include "Lookup.h"

namespace jly {

class Linker {
public:
	Linker();
	void prepare(CompilerContext* cc, ir::File* IR, MemPool::Slice* mem, MemPool::Slice& tmp);
	void setTempPool(MemPool::Slice& tmp);
	void link();

	ir::ClassMember* resolveStatic(ir::Class* class_, const SourceLoc& loc, StringRef name, ArrayRef<ir::NamespaceDef*> namespaces);
	ir::ClassMember* resolveInstance(ir::Class* class_, const SourceLoc& loc, StringRef name, ArrayRef<ir::NamespaceDef*> namespaces);
	ir::InterfaceMember* resolveInstance(ir::Interface* iface, StringRef name);

	void require(ir::Class* class_);
	void require(ir::Interface* iface);
private:
	ir::ClassMember* resolveUnchecked(ir::Class* class_, slice_umultimap<StringRef, ir::ClassMember*>& lookup, const SourceLoc& loc, StringRef name, ArrayRef<ir::NamespaceDef*> namespaces);
	ir::InterfaceMember* resolveInstanceUnchecked(ir::Interface* iface, StringRef name);

	void linkClass(ir::Class* class_);
	void linkInterface(ir::Interface* iface);

	void linkScope(ir::FileScope* scope);
	
	void linkFields(ir::Class* class_, ArrayRef<ir::ClassField*> fields, bool is_static);
	void linkMethods(ir::Class* class_, ArrayRef<ir::ClassMethod*> methods, bool is_static);
	void linkMethods(ir::Interface* iface, ArrayRef<ir::InterfaceMethod*> methods);

	ir::LinkClass* getClassLinkInfo(ir::Class* class_);
	ir::LinkInterface* getInterfaceLinkInfo(ir::Interface* iface);

	bool matchSignature(ir::FunctionSignature* sign1, ir::FunctionSignature* sign2);
	void checkSignatureGetter(ir::ClassMethod* method, ir::FunctionSignature* sign);
	void checkSignatureSetter(ir::ClassMethod* method, ir::FunctionSignature* sign);

	bool isPrivate(ir::ClassMember* member);

	ir::File* IR;
	CompilerContext* cc;
	CompilationUnit* cu;
	MemPool::Slice* mem;
	MemPool::Slice* tmp;
};

}
