#pragma once
#include <jellyflash/compiler/CompilerContext.h>
#include <jellyflash/parser/AST.h>
#include <jellyflash/ir/File.h>
#include <jellylib/memory/MemPool.h>
#include <jellylib/types/ConsList.h>
#include "Lookup.h"

namespace jly {

namespace ir{
struct Type;
}

class Resolver {
public:
	Resolver();
	void prepare(CompilerContext* cc, ir::File* IR, MemPool::Slice* mem, MemPool::Slice& tmp);
	void setTempPool(MemPool::Slice& tmp);
	void run();

	ir::Type* resolveType(ir::FileScope* scope, ast::Type* type);
	ir::FunctionSignature* resolveSignature(const SourceLoc& loc, ir::FileScope* scope, ast::FuncSignature* def, ir::Type* def_ret_ty = nullptr);
	ir::Definition* resolveDefinition(ir::FileScope* scope, srcspan_t span, ArrayRef<ast::ID> ids);

private:
	void resolveScope(ir::FileScope* scope);
	ArrayRef<ir::Interface*> resolveInterfaces(ir::FileScope* scope, ArrayRef<ast::QualID*> ifaces);
	void resolveFromUnit(Cons<ir::Definition*>& candidates, CompilationUnit* cu, StringRef id);
	void resolveClass(ir::FileScope* scope, ir::Class* class_);
	void resolveInterface(ir::FileScope* scope, ir::Interface* iface);
	void resolveFunction(ir::FileScope* scope, ir::GlobalFunction* func);
	void resolveVariable(ir::FileScope* scope, ir::GlobalVar* var);
	void resolveClassMethod(ir::FileScope* scope, ir::ClassMethod* method, bool is_constructor);
	void resolveClassField(ir::FileScope* scope, ir::ClassField* field);
	void resolveInterfaceMethod(ir::FileScope* scope, ir::InterfaceMethod* method);
	
	ir::File* IR;
	CompilerContext* cc;
	CompilationUnit* cu;
	MemPool::Slice* mem;
	MemPool::Slice* tmp;
};

}
