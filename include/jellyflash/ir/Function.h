#pragma once

#include <jellyflash/parser/AST.h>
#include "Type.h"

namespace jly::icode{
struct Value;
}

namespace jly::ir{

struct FunctionSignature;

struct FunctionParam{
	FunctionSignature* owner;
	SourceLoc loc;
	StringRef name;
	Type* type;
	ast::Expr* initializer_def;
	StringRef codegen_name = nullptr;
	icode::Value* default_value = nullptr;

	FunctionParam(
		FunctionSignature* owner,
		SourceLoc loc,
		StringRef name,
		Type* type,
		ast::Expr* initializer_def
	):
		owner(owner),
		loc(loc),
		name(name),
		type(type),
		initializer_def(initializer_def)
	{
	}
};

struct FunctionSignature{
	ArrayRef<FunctionParam*> params = nullptr;
	FunctionParam* rest = nullptr;
	Type* retTy;

	FunctionSignature(
		Type* retTy
	) :
		retTy(retTy)
	{	
	}
};

}
