#pragma once
#include <jellylib/stdbits.h>
#include <jellylib/memory/MemPool.h>

namespace std {
template <> 
struct hash<filesystem::path> {
	size_t operator()(const filesystem::path& p) const {
		return filesystem::hash_value(p);
	}
};
}

namespace jly{

namespace ir{
struct Definition;
}

namespace icode {
class Printer;
struct BasicBlock;
struct Instr;
struct Value;
struct Closure;
struct LocalVar;
}

struct CodegenClassPathInfo {
	std::filesystem::path include_path;
	std::filesystem::path source_path;
	std::filesystem::path native_stubs_path;

	pool_uset<std::filesystem::path> written_files;
};

struct CodegenCU{
	std::string header_include;
	std::string header_impl_include;
	std::string ns;

	pool_uset<CompilationUnit*> strong_dependencies;
	pool_uset<CompilationUnit*> weak_dependencies;
	pool_uset<CompilationUnit*> impl_dependencies;
	pool_uset<ir::Definition*> header_weak_dependencies;

	pool_umap<StringRef, icode::ConstantString*> string_pool;
};

struct CodegenPackage{
	std::string ns;
};

struct CodegenFileScope{
};

struct CodegenInterface {
	StringRef name = nullptr;
	slice_vector<ir::InterfaceMethod*> vtable;

	CodegenInterface(MemPool::Slice* mem) :
		vtable(mem) {
	}
};

struct CodegenInterfaceImpl{
	ir::Class* owner;
	ir::Interface* iface;
	slice_vector<ir::InterfaceMethodImpl*> vtable;
	StringRef name = nullptr;

	CodegenInterfaceImpl(MemPool::Slice* mem, ir::Class* owner, ir::Interface* iface):
		owner(owner),
		iface(iface),
		vtable(mem)
	{
	}
};

struct CodegenClass{
	StringRef name = nullptr;
	slice_vector< std::pair<ir::ClassMethod*, ir::ClassMethod*> > vtable;
	slice_umap<ir::Interface*, CodegenInterfaceImpl*> implements_map;
	slice_vector<ir::Interface*> implements_list;

	CodegenClass(MemPool::Slice* mem):
		vtable(mem),
		implements_list(mem),
		implements_map(mem)
	{
	}
};

struct CodegenClosure{
	ir::Type* this_type;

	CodegenClosure(ir::Type* this_type):
		this_type(this_type)
	{
	}
};

struct CodegenFrame{
	icode::Printer* printer = nullptr;
	icode::FuncFrame* owner = nullptr;
	slice_umap<icode::BasicBlock*, uz> bb_index;
	uz instr_index = 1;
	slice_uset<CompilationUnit*> static_cus;
	slice_vector<icode::LocalVar*> ar_locals;
	icode::Closure* closure = nullptr;
	uz rest_index = 0;
	u32 last_line = ~0u;
	uz ar_idx = 0;
	StringRef ar_name = nullptr;
	StringRef closure_name = nullptr;
	StringRef gclosure_name = nullptr;
	bool has_ar = false;

	CodegenFrame(MemPool::Slice* mem, icode::FuncFrame* owner):
		bb_index(mem),
		static_cus(mem),
		ar_locals(mem),
		owner(owner)
	{
	}
};


}