#pragma once
#include <jellylib/stdbits.h>
#include <jellylib/types/StringRef.h>
#include <jellylib/types.h>

namespace jly{

class RawWriter{
public:
	RawWriter(std::ostream* out):
		out(out),
		ptr(cache),
		avail(CacheSize)
	{
		
	}

	RawWriter(const RawWriter&) = delete;

	void write(const char ch){
		if (jl_unlikely(!avail)) {
			flush();
		}
		*ptr = ch;
		ptr++;
		avail--;
	}

	void write(const StringRef& str){
		uz sz = str.getSize();
		if ( jl_unlikely(avail <= sz) ){
			flush();
			if ( str.getSize() >= avail ){
				out->write((const char*)str.getData(), sz);
				return;
			}
		}
		if ( str.getSize() ){
			memcpy(ptr, str.getData(), sz);
			ptr += sz;
			avail -= sz;
		}
	}

	void flush(){
		if ( ptr != cache ){
			out->write((const char*)cache, ptr - cache);
			ptr = cache;
			avail = CacheSize;
		}
	}

	~RawWriter(){
		flush();
	}
private:
	static const uz CacheSize = 16 * 1024;
	u8 cache[CacheSize];
	u8* ptr;
	uz avail;
	std::ostream* out;
};

class FormattedWriter{
	struct Indenter {
		Indenter(FormattedWriter* writer, i32 val):
			writer(writer),
			val(val)
		{
			writer->indent(val);
		}
		Indenter(Indenter&& other):
			writer(other.writer),
			val(other.val){
			other.writer = nullptr;
		}
		Indenter& operator=(Indenter&& other) {
			reset();
			writer = other.writer;
			val = other.val;
			other.writer = nullptr;
		}
		~Indenter(){
			reset();
		}
		void reset(){
			if (writer) {
				writer->indent(-val);
				writer = nullptr;
			}
		}

		i32 val;
		FormattedWriter* writer;
	};
public:
	FormattedWriter(std::ostream* out):
		writer(out),
		indent_level(0),
		empty_line(true)
	{
	}

	Indenter indented(){
		return Indenter(this, 1);
	}

	Indenter unindented() {
		return Indenter(this, -1);
	}

	void write(char ch) {
		startline();
		writer.write(ch);
	}

	void write(const StringRef& str){
		startline();
		writer.write(str);
	}

	void writeln(const StringRef& str) {
		write(str);
		newline();
	}

	void writeln() {
		newline();
	}
private:
	RawWriter writer;
	i32 indent_level;
	bool empty_line;
	static const StringRef Tabs;

	void startline(){
		if (empty_line) {
			if ( indent_level ){
				writer.write({Tabs.getData(), (uz)indent_level});
			}
			empty_line = false;
		}
	}

	void indent(i32 val){
		indent_level += val;
	}

	void newline() {
		writer.write('\n');
		empty_line = true;
	}
};

}

