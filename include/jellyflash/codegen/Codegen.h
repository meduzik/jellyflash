#pragma once

#include <jellyflash/compiler/CompilationUnit.h>
#include <jellyflash/compiler/CompilerContext.h>
#include <jellyflash/ir/File.h>
#include <jellyflash/icode/ICodeCompiler.h>

#include "CodegenInfo.h"
#include <jellylib/utils/Writer.h>

namespace jly{

enum class MangleKind{
	Normal,
	Static,
	Local
};

struct CodegenCall{
	uz rest_index = 0;
	uz arg_count = 0;
};

class Codegen{
public:
	Codegen();
	
	void prepare(CompilerContext* cc, MemPool::Slice* mem, MemPool::Slice& tmp);
	void generate(MemPool::Slice* mem, MemPool::Slice& tmp);
	void generateNativeStubs(MemPool::Slice* mem, MemPool::Slice& tmp);
private:
	void genHeader(FormattedWriter* writer);
	void genInlineDefinitions(FormattedWriter* writer);
	void genPrivateDefinitions(FormattedWriter* writer);
	void genImpl(FormattedWriter* writer);
	void genNativeStubs(FormattedWriter* writer);

	bool writePackageNS(CUPackage* package);
	void writePackageInclPath(CUPackage* package);

	void preparePackage(CUPackage* package);

	void prepareScope(ir::FileScope* scope);
	void prepareType(ir::Type* type);
	void prepareDefinitionOwner(ir::Definition* def);
	void prepareSignature(ir::FileScope* scope, ir::Class* class_, ir::FunctionSignature* signature);
	void prepareFrame(ir::FileScope* scope, ir::Class* class_, icode::FuncFrame* frame);
	void prepareImplVTable(ir::Class* class_);

	bool doesHaveNativeStubs();

	// codegen accesses stuff from cu, needs cu to be codegened
	void uses(ir::Definition* def);
	void uses(CompilationUnit* cu);

	void writeScopeHeader(ir::FileScope* scope);
	void writeScopeForwardDecl(ir::FileScope* scope);
	void writeScopeImpl(ir::FileScope* scope);
	void writeScopeNativeStubs(ir::FileScope* scope);
	void writeForwardDecl(ir::Definition* def);

	void visitScopeImpl(ir::FileScope* scope);
	void requireTypeImpl(ir::Type* type);
	void requireSignatureImpl(ir::FunctionSignature* signature);
	void visitClassMethodImpl(ir::ClassMethod* method);
	void visitClassFieldImpl(ir::ClassField* field);
	void requireDefinitionImpl(ir::Definition* definition);

	void writeInterfaceVTable(ir::Interface* iface);

	StringRef generateQualifiedName(ir::Definition* def);

	template<class FnFuncName, class FnThisUnwrap, class FnFuncInvoke>
	void writeGenericProxyImpl(
		ir::FunctionSignature* signature,
		FnFuncName&& write_func_name,
		FnThisUnwrap&& write_this_unwrap,
		FnFuncInvoke&& write_func_invoke,
		ir::Type* ret_type = nullptr
	);
	void writeGenericProxyImpl(icode::Closure* closure);
	void writeGenericProxyImpl(ir::GlobalFunction* func);
	void writeGenericCreateImpl(ir::Class* class_);

	void writeClassVTableContents(ir::Class* owner, ir::Class* class_);
	void writeClassConstructor(ir::Class* class_);
	void writeClassVTable(ir::Class* class_);
	void writeClassInstanceFields(ir::Class* owner, ir::Class* class_);
	void writeClassStaticFields(ir::Class* class_);
	void writeClassField(ir::Class* owner, ir::ClassField* field);
	void writeClassStaticMethod(ir::ClassMethod* method);
	void writeClassInstanceMethod(ir::ClassMethod* method);
	void writeInterfaceMethod(ir::InterfaceMethod* method);
	void writeGenericProxy(ir::ClassMethod* method);
	void writeGenericProxyImpl(ir::ClassMethod* method);

	void writeTypeClass(ir::Type* type);

	void writeInterfaceImplRef(ir::Class* class_, ir::Interface* iface);

	void writeSignature(ir::FunctionSignature* signature, bool first_arg, bool param_names);
	void writeProxyPass(ir::FunctionSignature* sginature, bool first_arg);

	void writeMainFrame(icode::FuncFrame* frame);
	void writeFrame(icode::FuncFrame* frame);
	void writeActivationRecord(icode::FuncFrame* frame);
	void writePreFrame(icode::FuncFrame* frame);
	void writePreFrameForwardDeclare(icode::FuncFrame* frame);
	void writeClosure(icode::Closure* closure);
	void writeForwardDeclareClosure(icode::Closure* closure);

	void writeStaticNS();

	void writeRef(ir::Definition* def);
	void writeRef(ir::ClassMethod* method);
	void writeRef(ir::ClassField* field);
	void writePrivateNS(CompilationUnit* cu);
	void writeNamespaceForScope(ir::FileScope* scope);
	void writeNamespaceForPackage(CUPackage* package);

	void writeClassImpl(ir::Class* class_);
	void writeInterfaceImpl(ir::Interface* iface);

	void writeClassClass(ir::Class* class_);
	void writeInterfaceClass(ir::Interface* iface);

	void writeAttributesDecl(ir::GlobalFunction* func);
	void writeAttributes(ir::GlobalFunction* func);
	void writeAttributesDecl(ir::ClassMethod* method);
	void writeAttributes(ir::ClassMethod* method);

	void writeClassConstructorImpl(ir::Class* class_);
	void writeClassNewImpl(ir::Class* class_);
	void writeClassMethodImpl(ir::ClassMethod* method);
	void writeClassStaticFieldImpl(ir::ClassField* field);

	void writeStaticInitImpl(ir::FileScope* scope);
	bool writeStaticGCImpl(ir::FileScope* scope);

	void writeFieldType(ir::Type* type);
	void writeParamType(ir::Type* type);
	void writeReturnType(ir::Type* type);
	void writeTypeDesignator(ir::Type* type);

	bool writeGCVisit(ir::Type* type, StringRef object, StringRef glue, StringRef field);

	void uwrapClosurePayload(StringRef value, ir::Type* type);

	void codegenUnpackVec(ir::Type* type);
	void codegenPackVec(ir::Type* type);

	void requireStaticInitiailization(ir::Class* class_);
	void requireStaticInitiailization(ir::GlobalVar* class_);
	void codegenRunStaticInitialize(CompilationUnit* target_unit);
	
	bool willCompileInCurrentMode(ir::ClassMethod* method);
	bool willCompileInCurrentMode(ir::GlobalFunction* func);

	void codegenObjectRef(icode::Value* value);
	void codegenPreVisitMainFrame(ir::FileScope* scope, ir::Class* class_, icode::FuncFrame* frame);
	void codegenPreVisitFrame(ir::FileScope* scope, ir::Class* class_, icode::FuncFrame* frame);
	void codegenDefaultValue(ir::Type* type);
	void codegenBasicBlock(icode::BasicBlock* bb, icode::BasicBlock* follower);
	void codegenPrologueBasicBlock(icode::BasicBlock* bb, icode::BasicBlock* follower);
	void codegenInstr(icode::Instr* instr, icode::BasicBlock* current_bb, icode::BasicBlock* follower);
	void codegenCmpOperator(icode::CmpOp op);
	void codegenNumOperator(icode::NumBinaryOp op);
	void codegenGoto(icode::BasicBlock* current_bb, icode::BasicBlock* bb, icode::BasicBlock* follower);
	void codegenValue(icode::Value* value);
	void codegenNullCheck(icode::Value* value);
	void codegenRegisterName(icode::Instr* instr);
	void codegenVectorEltType(ir::Type* type);
	void codegenCanonicalType(ir::Type* type);
	void codegenToAny(icode::Value* value);
	void codegenMonomorphic(icode::Value* value);
	void codegenConstantString(icode::ConstantString* string);

	bool codegenCastObject(icode::Value* from, ir::Type* to, StringRef cast_id);
	bool codegenIsObject(icode::Value* from, ir::Type* to);
	void codegenCastTo(icode::Value* from, ir::Type* to);
	void codegenCastAs(icode::Value* from, ir::Type* to);
	void codegenCastIs(icode::Value* from, ir::Type* to);
	void codegenCastGeneric(icode::Value* from, ir::Type* to, StringRef cast_id);
	void codegenCastError(ir::Type* from, ir::Type* to);
	
	void codegenIsNull(icode::Value* value);

	void codegenTypeKindName(ir::Type* type, bool is_target);
	void codegenTypeCastArgs(ir::Type* type);
	void codegenTypeCastValue(icode::Value* value);
	void codegenTypeCastPrefix(ir::Type* to);

	bool isAny(ir::Type* type);

	CodegenCall* codegenPrepareCall(ir::FunctionSignature* signature, ArrayRef<icode::Value*> args);
	void codegenCallParams(CodegenCall* call, ir::FunctionSignature* signature, ArrayRef<icode::Value*> args);

	StringRef mangleIdentifier(icode::LocalVar* var);
	StringRef mangleIdentifier(ir::FunctionParam* param);
	StringRef mangleIdentifier(ir::NamespaceDef* def, StringRef name);
	StringRef mangleIdentifier(ir::NamespaceDef* def, StringRef name, MangleKind kind);
	StringRef mangleIdentifier(ir::NamespaceDef* def, StringRef name, MangleKind kind, ir::Accessor accessor);
	StringRef mangleNamespace(StringRef name);
	StringRef mangleStaticNamespace(StringRef name);

	StringRef generateARName(CodegenFrame* frame);
	StringRef generateClosureName(CodegenFrame* frame);
	StringRef generateGenericClosureName(CodegenFrame* frame);

	void generateStringLiteral(icode::ConstantString* string);

	CompilerContext* cc;
	CompilationUnit* cu;
	CodegenCU* cu_codegen;
	FormattedWriter* writer;
	ir::File* IR;
	icode::Printer* printer;
	MemPool::Slice* mem;
	MemPool::Slice* tmp;
	CodegenFrame* frame;
	//icode::ICodeCompiler icode_compiler;
	uz closure_index = 0;
	uz prop_index = 0;
	uz prop_list_index = 0;
	StringRef static_ns;

	enum class CodegenMode {
		Inline,
		Private
	} mode;

	class PropertyAggregator;
};

}