#pragma once

#include <jellyflash/compiler/CompilationUnit.h>
#include <jellyflash/compiler/CompilerContext.h>
#include <jellyflash/ir/File.h>

#include "CodegenInfo.h"
#include <jellylib/utils/Writer.h>

namespace jly {

class CodegenICode {
public:
	CodegenICode();

	void prepare(CompilerContext* cc, CodegenInfo* info, MemPool::Slice* mem, MemPool::Slice& tmp);
	void generate(MemPool::Slice* mem, MemPool::Slice& tmp);
	void generateNativeStubs(MemPool::Slice* mem, MemPool::Slice& tmp);
private:
	void genHeader(FormattedWriter* writer);
	void genImpl(FormattedWriter* writer);
	void genNativeStubs(FormattedWriter* writer);

	bool writePackageNS(CUPackage* package);
	void writePackageInclPath(CUPackage* package);

	void preparePackage(CUPackage* package);

	void prepareScope(ir::FileScope* scope);
	void prepareType(ir::Type* type);

	void prepareSignature(ir::FunctionSignature* signature);

	bool doesHaveNativeStubs();

	// codegen accesses stuff from cu, needs cu to be codegened
	void uses(ir::Definition* def);
	void uses(CompilationUnit* cu);

	void generateProxyImpl(ir::Class* class_, ir::InterfaceMethodImpl* method_impl);
	void writeProxyPass(ir::FunctionSignature* signature);

	void writeScopeHeader(ir::FileScope* scope);
	void writeScopeForwardDecl(ir::FileScope* scope);
	void writeScopeImpl(ir::FileScope* scope);
	void writeScopeNativeStubs(ir::FileScope* scope);
	void writeForwardDecl(ir::Definition* def);

	void writeClassField(ir::ClassField* field);
	void writeClassMethod(ir::ClassMethod* method);
	void writeInterfaceMethod(ir::InterfaceMethod* method);

	void writeSignature(ir::FunctionSignature* signature);

	void writeFrame(icode::FuncFrame* frame);

	void writeRef(ir::Definition* def);
	void writePrivateNS(CompilationUnit* cu);
	void writeNamespaceForScope(ir::FileScope* scope);
	void writeNamespaceForPackage(CUPackage* package);

	void writeMethodImpl(ir::ClassMethod* method);

	void writeFieldType(ir::Type* type);
	void writeParamType(ir::Type* type);
	void writeReturnType(ir::Type* type);

	StringRef mangleIdentifier(ir::NamespaceDef* def, StringRef name);
	StringRef mangleIdentifier(ir::NamespaceDef* def, StringRef name, MangleKind kind);
	StringRef mangleIdentifier(ir::NamespaceDef* def, StringRef name, MangleKind kind, ir::Accessor accessor);
	StringRef mangleNamespace(StringRef name);

	CodegenInfo* info;
	CompilerContext* cc;
	CompilationUnit* cu;
	CodegenCU* cu_codegen;
	FormattedWriter* writer;
	ir::File* IR;
	MemPool::Slice* mem;
	MemPool::Slice* tmp;

};

}