#include <jellyflash/parser/Lexer.h>
#include "lexer.jlex.h"

namespace jly{

TokenStream Lexer::run(CompilerContext& cc, SourceFile* source){
	lexer::Lexer lexer;
	lexer::init(&lexer);
	lexer::set_state(&lexer, lexer::State::Default);

	auto data = source->getData();
	const uz ChunkSize = 1024 * 1024;

	buffer.resize(2 * ChunkSize + 32);
	u32* tokensBuffer = buffer.data();
	u32* offsetsBuffer = buffer.data() + ChunkSize + 16;

	uz end = data.getSize();

	jly::vector<token_type> tokens;
	jly::vector<srcoffset_t> offsets;
	offsets.push_back(0);

	for ( uz begin = 0; begin < end; begin += ChunkSize){
		lexer::set_buffers(&lexer, tokensBuffer, offsetsBuffer);
		lexer::feed(&lexer, data.getData() + begin, std::min(end - begin, ChunkSize), begin);
		lexer::run(&lexer);
		if ( begin + ChunkSize >= end ){
			// the last one
			lexer::finalize(&lexer);
		}
		token_type* tokensChunk = (token_type*)lexer::convert_tokens_ids(&lexer);
		u32* offsetsChunk = lexer::get_tokens_ends(&lexer);
		uz count = lexer::get_tokens_count(&lexer);
		
		offsets.reserve(offsets.size() + count);
		tokens.reserve(tokens.size() + count);
		std::copy(offsetsChunk, offsetsChunk + count, std::back_inserter(offsets));
		std::copy(tokensChunk, tokensChunk + count, std::back_inserter(tokens));
	}

	// fill in newlines
	uz n = tokens.size();
	auto& lineMap = source->getLineMap();
	for ( uz i = 0; i < end; i++ ){
		if ( data[i] == '\r' ){
			if ( i + 1 < end && data[i + 1] == '\n' ){
				i++;
			}
			lineMap.pushLine() = (u32)i + 1;
		}else if (data[i] == '\n') {
			lineMap.pushLine() = (u32)i + 1;
		}
	}
	lineMap.pushLine() = offsets.back();

	return { std::move(tokens), std::move(offsets) };
}

}

