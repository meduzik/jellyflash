#include <jellyflash/parser/Token.h>

namespace jly{


const char* get_token_name(token_type tt){
	switch ( tt ){
	#define X(t) case token_type::t: return #t;
	TOKEN_LIST
	#undef X
	default: return "invalid_token";
	}
}


}