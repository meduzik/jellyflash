#include <jellyflash/parser/Parser.h>
#include <jellyflash/parser/Lexer.h>
#include <jellylib/types/ConsList.h>

namespace jly {


class ParserImpl{
public:
	ParserImpl(
		CompilerContext* cc,
		SourceFile* sourceFile,
		MemPool::Slice* mem,
		MemPool::Slice* tmp,
		TokenStream* tokenStream
	) :
		cc(cc),
		sourceFile(sourceFile),
		mem(mem),
		tmp(tmp),
		tokenStream(tokenStream),
		in_allowed(false)
	{
		tokens = tokenStream->tokens.data();
		offsets = tokenStream->offsets.data();
		tokenCount = tokenStream->tokens.size();
		data = sourceFile->getData().getData();
		lastAdvancePos = 0;
		pos = 0;
		_consume();
	}

	ast::File* run(){
		auto file = parseFile();
		parseEOF();
		
		return file;
	}
private:
	ast::File* parseFile(){
		ast::PackageDecl* packageDecl = parsePackage();
		auto entries = parseBlockEntries(true);
		return mem->create<ast::File>(packageDecl, entries);
	}

	ArrayRef<ast::PackageBlockEntry*> parseBlockEntries(bool allow_nesting){
		Cons<ast::PackageBlockEntry*> entries(tmp);
		parseBlockEntriesImpl(entries, allow_nesting);
		return entries.get(mem);
	}

	void parseBlockEntriesImpl(Cons<ast::PackageBlockEntry*>& entries, bool allow_nesting) {
		while (true) {
			if (allow_nesting && peek() == token_type::open_brace ){
				advance();
				parseBlockEntriesImpl(entries, true);
				expect(token_type::close_brace);
				continue;
			}

			ast::PackageBlockEntry* entry = parseBlockEntry();
			if (entry) {
				entries.push(entry);
			} else {
				break;
			}
		}
	}

	ast::PackageBlockEntry* parseBlockEntry(){
		while ( peek() == token_type::op_semicolon ) advance();

		switch ( peek() ){
		case token_type::k_import:{
			return parseImport();
		}break;
		case token_type::eof:
		case token_type::close_brace:{
			return nullptr;
		}break;
		case token_type::open_brace: {
			auto pos = span();
			advance();
			auto stmts = parse_stmts();
			expect(token_type::close_brace);
			return mem->create<ast::StaticBlock>(pos, stmts);
		}break;
		default:{
			if ( peek() == token_type::identifier && contents() == "use" ){
				auto begin = loc();
				advance();
				expect(token_type::k_namespace);
				auto name = id();
				expect(token_type::op_semicolon);
				return mem->create<ast::UseNamespace>(span_from(begin), name);
			}

			auto annos = annotations();
			auto mods = modifiers();
			switch ( peek() ){
			case token_type::k_namespace:{
				return parse_namespace(annos, mods);
			}break;
			case token_type::k_class:{
				return parse_class(annos, mods);
			}break;
			case token_type::k_interface: {
				return parse_interface(annos, mods);
			}break;
			case token_type::k_var: 
			case token_type::k_const: {
				return parse_var_node<ast::VarDefPackage>(annos, mods);
			}break;
			case token_type::k_function: {
				return parse_func_node<ast::FuncDefPackage>(annos, mods);
			}break;
			default:{
				report("expected class, interface, variable, method, or namespace declaration");
			}break;
			}
		}break;
		}
	}
	
	ast::VarDef* parse_var() {
		ast::VarDef::Kind kind;
		auto begin = loc();
		switch ( peek() ){
		case token_type::k_const:{
			kind = ast::VarDef::Kind::Const;
			advance();
		}break;
		case token_type::k_var: {
			kind = ast::VarDef::Kind::Var;
			advance();
		}break;
		default:{
			report("expected 'var' or 'const'");
		}break;
		}
		auto span = span_from(begin);
		Cons<ast::VarDecl*> decls(tmp);
		while ( true ){
			auto decl = parse_var_decl();
			decls.push(decl);
			if ( peek() == token_type::op_comma ){
				advance();
			}else{
				break;
			}
		}
		return mem->create<ast::VarDef>(span, kind, decls.get(mem));
	}

	ast::Type* m_typebinder(){
		if (peek() == token_type::op_colon) {
			advance();
			return parse_type();
		}
		return nullptr;
	}

	ast::VarDecl* parse_var_decl(){
		ast::ID name = id();
		ast::Type* type = m_typebinder();
		ast::Expr* initializer = nullptr;
		if ( peek() == token_type::op_assign ){
			advance();
			initializer = parse_expr(PrecMax);
		}
		return mem->create<ast::VarDecl>(name, type, initializer);
	}

	ast::Type* parse_type() {
		auto type = m_parse_type();
		if ( !type ){
			report("expected type");
		}
		return type;
	}

	ast::Type* m_parse_type(){
		switch ( peek() ){
		case token_type::k_void: {
			auto pos = span();
			advance();
			return mem->create<ast::TypePrim>(pos, ast::TypePrim::Kind::Void);
		}break;
		case token_type::op_mul:{
			auto pos = span();
			advance();
			return mem->create<ast::TypePrim>(pos, ast::TypePrim::Kind::Star);
		}break;
		case token_type::op_assign_mul: {
			auto loc = span();
			tokens[pos] = token_type::op_assign;
			auto type = mem->create<ast::TypePrim>(loc, ast::TypePrim::Kind::Star);
			return type;
		}break;
		case token_type::identifier:{
			auto begin = loc();
			Cons<ast::ID> ids(tmp);
			ids.push(id());
			while ( true ){
				if ( peek() == token_type::op_dot ){
					advance();
				}else{
					break;
				}
				if ( peek() == token_type::op_lt ){
					advance();
					auto arg = parse_type();
					auto instance = mem->create<ast::TypeInstance>(span_from(begin), ids.get(mem), arg);
					close_instance();
					return instance;
				}else{
					ids.push(id());
				}
			}
			return mem->create<ast::TypeRef>(span_from(begin), ids.get(mem));
		}break;
		default:
			return nullptr;
		}
	}

	ast::NSID* nsid(){
		auto begin = loc();
		ast::ID name = id();
		ast::Namespace* ns = nullptr;
		if ( peek() == token_type::op_colon_colon ){
			advance();
			ns = mem->create<ast::Namespace>(name);
			name = id();
		}
		return mem->create<ast::NSID>(span_from(begin), ns, name);
	}

	enum Prec {
		PrecProp,
		PrecNew,
		PrecCall,
		PrecPostfix,
		PrecUnary,
		PrecMultiplicative,
		PrecAdditive,
		PrecShift,
		PrecRelational,
		PrecEq,
		PrecBitAnd,
		PrecBitXor,
		PrecBitOr,
		PrecLogicAnd,
		PrecLogicOr,
		PrecTernary,
		PrecAssign,
		PrecComma,
		PrecMax
	};

	ast::Expr* parse_expr(Prec prec){
		ast::Expr* expr = m_parse_expr(prec);
		if ( !expr ){
			report("expected expression");
		}
		return expr;
	}

	bool token_to_assign(token_type tt, ast::AssignOp* op){
		switch ( tt ){
		case token_type::op_assign: *op = ast::AssignOp::Normal; return true;
		case token_type::op_assign_mul: *op = ast::AssignOp::Mul; return true;
		case token_type::op_assign_div: *op = ast::AssignOp::Div; return true;
		case token_type::op_assign_mod: *op = ast::AssignOp::Mod; return true;
		case token_type::op_assign_add: *op = ast::AssignOp::Add; return true;
		case token_type::op_assign_sub: *op = ast::AssignOp::Sub; return true;
		case token_type::op_assign_shl: *op = ast::AssignOp::Shl; return true;
		case token_type::op_assign_ashr: *op = ast::AssignOp::AShr; return true;
		case token_type::op_assign_lshr: *op = ast::AssignOp::LShr; return true;
		case token_type::op_assign_bit_and: *op = ast::AssignOp::BitAnd; return true;
		case token_type::op_assign_bit_xor: *op = ast::AssignOp::BitXor; return true;
		case token_type::op_assign_bit_or: *op = ast::AssignOp::BitOr; return true;
		case token_type::op_assign_logic_and: *op = ast::AssignOp::LogicAnd; return true;
		case token_type::op_assign_logic_or: *op = ast::AssignOp::LogicOr; return true;
		default: return false;
		}
		return true;
	}

	void close_instance(){
		// ugly hack to fix Vector.<Vector.< int >>
		//                                       ^
		if ( peek() == token_type::op_gt ){
			advance();
		}else if ( peek() == token_type::op_ashr ){
			tokens[pos] = token_type::op_gt;
		}else if ( peek() == token_type::op_lshr ){
			tokens[pos] = token_type::op_ashr;
		}
	}

	ast::Expr* m_parse_expr(Prec prec) {
		ast::Expr* expr = prim_expr();
		if ( !expr ){
			return nullptr;
		}

		while(true){
			auto rhs_begin = loc();
			auto tt = peek();
			if ( tt == token_type::op_dot && prec >= PrecProp ){
				advance();
				tt = peek();
				switch ( tt ){
				case token_type::op_lt:{
					advance();
					auto type = m_parse_type();
					close_instance(); 
					expr = mem->create<ast::ExprInstance>(span_from(rhs_begin), expr, type);
					continue;
				}break;
				default:{
					if ( is_identifier(tt) ){
						auto id = nsid();
						expr = mem->create<ast::ExprDotIndex>(span_from(rhs_begin), expr, id);
						continue;
					}else{
						report("expected identifier or <type sepcialization>");
					}
				}break;
				}
			}
			if (tt == token_type::open_bracket && prec >= PrecProp) {
				advance();
				auto arg = parse_expr(PrecMax);
				expect(token_type::close_bracket);
				expr = mem->create<ast::ExprSubscript>(span_from(rhs_begin), expr, arg);
				continue;
			}
			if ( tt == token_type::open_paren && prec >= PrecCall ){
				auto args = parse_call_args();
				expr = mem->create<ast::ExprCall>(span_from(rhs_begin), expr, args);
				continue;
			}
			ast::AssignOp assign_op;
			if ( prec >= PrecAssign && token_to_assign(tt, &assign_op) ) {
				advance();
				auto rhs = parse_expr(PrecAssign);
				expr = mem->create<ast::ExprAssign>(span_from(rhs_begin), assign_op, expr, rhs);
				continue;
			}
		#define BINOP(Token, _prec, Binop) \
			if (tt == Token && prec >= _prec) { \
				advance(); \
				auto rhs = parse_expr((Prec)(_prec - 1)); \
				expr = mem->create<ast::ExprBinOp>(span_from(rhs_begin), Binop, expr, rhs); \
				continue; \
			} 
			BINOP(token_type::op_add, PrecAdditive, ast::BinOp::Add);
			BINOP(token_type::op_sub, PrecAdditive, ast::BinOp::Sub);

			BINOP(token_type::op_ashr, PrecShift, ast::BinOp::AShr);
			BINOP(token_type::op_lshr, PrecShift, ast::BinOp::LShr);
			BINOP(token_type::op_shl, PrecShift, ast::BinOp::Shl);

			BINOP(token_type::op_mul, PrecMultiplicative, ast::BinOp::Mul);
			BINOP(token_type::op_div, PrecMultiplicative, ast::BinOp::Div);
			BINOP(token_type::op_mod, PrecMultiplicative, ast::BinOp::Mod);

			BINOP(token_type::op_eq, PrecEq, ast::BinOp::EQ);
			BINOP(token_type::op_ne, PrecEq, ast::BinOp::NEQ);
			BINOP(token_type::op_strict_eq, PrecEq, ast::BinOp::StrictEQ);
			BINOP(token_type::op_strict_ne, PrecEq, ast::BinOp::StrictNEQ);

			BINOP(token_type::op_lt, PrecRelational, ast::BinOp::LT);
			BINOP(token_type::op_le, PrecRelational, ast::BinOp::LE);
			BINOP(token_type::op_gt, PrecRelational, ast::BinOp::GT);
			BINOP(token_type::op_ge, PrecRelational, ast::BinOp::GE);
			BINOP(token_type::k_as, PrecRelational, ast::BinOp::As);
			BINOP(token_type::k_in, PrecRelational, ast::BinOp::In);
			BINOP(token_type::k_instanceof, PrecRelational, ast::BinOp::Instanceof);
			BINOP(token_type::k_is, PrecRelational, ast::BinOp::Is);

			BINOP(token_type::op_logic_and, PrecLogicAnd, ast::BinOp::LogicAnd);

			BINOP(token_type::op_logic_or, PrecLogicOr, ast::BinOp::LogicOr);

			BINOP(token_type::op_bit_and, PrecBitAnd, ast::BinOp::BitAnd);
			BINOP(token_type::op_bit_or, PrecBitOr, ast::BinOp::BitOr);
			BINOP(token_type::op_bit_xor, PrecBitXor, ast::BinOp::BitXor);

			if ( prec >= PrecTernary && tt == token_type::op_quest ){
				advance();
				auto true_br = parse_expr(PrecMax);
				expect(token_type::op_colon);
				auto false_br = parse_expr(PrecMax);
				expr = mem->create<ast::ExprConditional>(span_from(rhs_begin), expr, true_br, false_br);
				continue;
			}

			if (prec >= PrecPostfix && tt == token_type::op_incr) {
				advance();
				expr = mem->create<ast::ExprUnOp>(span_from(rhs_begin), ast::UnOp::PostInc, expr);
				continue;
			}
			if (prec >= PrecPostfix && tt == token_type::op_decr) {
				advance();
				expr = mem->create<ast::ExprUnOp>(span_from(rhs_begin), ast::UnOp::PostDec, expr);
				continue;
			}

			break;
		}

		return expr;
	}

	ArrayRef<ast::Expr*> parse_call_args(){
		expect(token_type::open_paren);
		Cons<ast::Expr*> args(tmp);
		auto expr = m_parse_expr(PrecMax);
		if (expr) {
			args.push(expr);
			while (true) {
				if ( peek() == token_type::op_comma ){
					advance();
				}else{
					break;
				}
				expr = parse_expr(PrecMax);
				args.push(expr);
			}
		}
		expect(token_type::close_paren);
		return args.get(mem);
	}

	ast::UnOp token_to_unop(token_type tt){
		switch ( tt ){
		case token_type::op_incr: return ast::UnOp::PreInc;
		case token_type::op_decr: return ast::UnOp::PreDec;
		case token_type::op_add: return ast::UnOp::UnPlus;
		case token_type::op_sub: return ast::UnOp::Negate;
		case token_type::op_tilde: return ast::UnOp::BitInvert;
		case token_type::op_exclam: return ast::UnOp::LogicInvert;
		case token_type::k_delete: return ast::UnOp::Delete;
		case token_type::k_typeof: return ast::UnOp::Typeof;
		case token_type::k_void: return ast::UnOp::Void;
		default: jl_unreachable;
		}
	}

	ast::Expr* prim_expr(){
		auto begin = loc();
		auto tt = peek();
		switch ( tt ){
		case token_type::k_undefined:{
			return mem->create<ast::ExprPrim>(take(), ast::ExprPrim::Kind::Undefined);
		}break;
		case token_type::k_true: {
			return mem->create<ast::ExprPrim>(take(), ast::ExprPrim::Kind::True);
		}break;
		case token_type::k_false: {
			return mem->create<ast::ExprPrim>(take(), ast::ExprPrim::Kind::False);
		}break;
		case token_type::k_null: {
			return mem->create<ast::ExprPrim>(take(), ast::ExprPrim::Kind::Null);
		}break;
		case token_type::k_this: {
			return mem->create<ast::ExprPrim>(take(), ast::ExprPrim::Kind::This);
		}break;
		case token_type::k_super: {
			return mem->create<ast::ExprPrim>(take(), ast::ExprPrim::Kind::Super);
		}break;
		case token_type::op_lt:{
			advance();
			auto arg = parse_type();
			expect(token_type::op_gt);
			advance();
			Cons<ast::Expr*> elts(tmp);
			while (true) {
				if (peek() == token_type::close_bracket) {
					break;
				}
				elts.push(parse_expr(PrecMax));
				if (peek() == token_type::op_comma) {
					advance();
				} else {
					break;
				}
			}
			expect(token_type::close_bracket);
			return mem->create<ast::ExprVectorShortcut>(span_from(begin), arg, elts.get(mem));
		}break;
		case token_type::k_function:{
			advance();
			ast::ID* name = nullptr;
			if ( peek() == token_type::identifier ){
				name = mem->create<ast::ID>(id());
			}
			auto span = span_from(begin);
			auto sign = parse_signature();
			auto body = parse_body();
			return mem->create<ast::ExprClosure>(span, name, sign, body);
		}break;
		case token_type::decimal:{
			auto expr = mem->create<ast::ExprLiteral>(span(), ast::ExprLiteral::Kind::Decimal, contents());
			advance();
			return expr;
		}break;
		case token_type::hexidecimal: {
			auto expr = mem->create<ast::ExprLiteral>(span(), ast::ExprLiteral::Kind::Hexidecimal, contents());
			advance();
			return expr;
		}break;
		case token_type::string:{
			auto expr = mem->create<ast::ExprLiteral>(span(), ast::ExprLiteral::Kind::String, contents());
			advance();
			return expr;
		}break;
		case token_type::k_new:{
			advance();
			auto head = parse_expr(PrecNew);
			ArrayRef<ast::Expr*> args = nullptr;
			if ( peek() == token_type::open_paren ){
				args = parse_call_args();
			}
			return mem->create<ast::ExprNew>(span_from(begin), head, args);
		}break;
		case token_type::open_bracket: {
			advance();
			Cons<ast::Expr*> elts(tmp);
			while (true) {
				if (peek() == token_type::close_bracket) {
					break;
				}
				
				elts.push(parse_expr(PrecMax));

				if (peek() == token_type::op_comma) {
					advance();
				} else {
					break;
				}
			}
			expect(token_type::close_bracket);
			return mem->create<ast::ExprArray>(span_from(begin), elts.get(mem));
		}break;
		case token_type::open_brace:{
			advance();
			Cons<ast::ObjectField*> fields(tmp);
			while ( true ){
				if (peek() == token_type::close_brace) {
					break;
				}
				
				ast::ObjectField::Kind kind;
				ast::ID key(span(), contents());

				switch ( peek() ){
				case token_type::decimal:{
					kind = ast::ObjectField::Kind::Number;
				}break;
				case token_type::string: {
					kind = ast::ObjectField::Kind::String;
				}break;
				case token_type::identifier: {
					kind = ast::ObjectField::Kind::Identifier;
				}break;
				default:{
					report("expected key");
				}break;
				}

				advance();

				expect(token_type::op_colon);
				auto value = parse_expr(PrecMax);

				fields.push(mem->create<ast::ObjectField>(kind, key, value));

				if (peek() == token_type::op_comma) {
					advance();
				}else{
					break;
				}
			}
			expect(token_type::close_brace);
			return mem->create<ast::ExprObject>(span_from(begin), fields.get(mem));
		}break;
		case token_type::open_paren:{
			advance();
			auto expr = parse_expr(PrecMax);
			expect(token_type::close_paren);
			return expr;
		}break;
		case token_type::op_incr:
		case token_type::op_decr: 
		case token_type::op_add: 
		case token_type::op_sub: 
		case token_type::op_tilde: 
		case token_type::op_exclam: 
		case token_type::k_delete: 
		case token_type::k_typeof: 
		case token_type::k_void: {
			advance();
			auto expr = parse_expr((Prec)(PrecUnary + 1));
			return mem->create<ast::ExprUnOp>(span_from(begin), token_to_unop(tt), expr);
		}break;
		default:{
			if ( is_identifier(tt) ){
				auto id = nsid();
				return mem->create<ast::ExprRef>(id);
			}
			return nullptr;
		}break;
		}
	}

	ast::FuncSignature* parse_signature(){
		Cons<ast::Param*> params(tmp);
		ast::ID* ellipsis = nullptr;
		expect(token_type::open_paren);
		if ( is_identifier(peek()) || peek() == token_type::op_ellipsis){
			while ( true ){
				if ( peek() == token_type::op_ellipsis ){
					advance();
					auto name = id();
					ellipsis = mem->create<ast::ID>(name);
					break;
				}

				ast::ID name = id();
				ast::Type* type = m_typebinder();
				ast::Expr* expr = nullptr;
				if ( peek() == token_type::op_assign ){
					advance();
					expr = parse_expr(PrecMax);
				}
				params.push(mem->create<ast::Param>(name, type, expr));
				if ( peek() == token_type::op_comma ){
					advance();
				}else{
					break;
				}
			}
		}
		expect(token_type::close_paren);
		auto ret_ty = m_typebinder();
		return mem->create<ast::FuncSignature>(params.get(mem), ellipsis, ret_ty);
	}

	ast::FuncBody* parse_body(){
		auto begin = loc();
		expect(token_type::open_brace);
		auto stmts = parse_stmts();
		expect(token_type::close_brace);
		return mem->create<ast::FuncBody>(span_from(begin), stmts);
	}

	ArrayRef<ast::Stmt*> parse_stmts(){
		Cons<ast::Stmt*> stmts(tmp);
		while ( true ){
			while ( peek() == token_type::identifier ){
				auto pos = this->pos;
				auto lap = this->lastAdvancePos;
				ast::ID name = id();
				if ( peek() == token_type::op_colon ){
					advance();
					stmts.push(mem->create<ast::StmtLabel>(name.span, name));
				}else{
					this->pos = pos;
					this->lastAdvancePos = lap;
					break;
				}
			}

			auto stmt = parse_stmt();
			if ( stmt ){
				stmts.push(stmt);
			}else{
				break;
			}
		}
		return stmts.get(mem);
	}

	ArrayRef<ast::Stmt*> parse_block(){
		expect(token_type::open_brace);
		auto stmts = parse_stmts();
		expect(token_type::close_brace);
		return stmts;
	}

	ast::Stmt* parse_stmt(){
		auto begin = loc();
		switch ( peek() ){
		case token_type::open_brace:{
			auto pos = span();
			advance();
			auto stmts = parse_stmts();
			expect(token_type::close_brace);
			return mem->create<ast::StmtBlock>(pos, stmts);
		}break;
		case token_type::k_return:{
			advance();
			auto expr = m_parse_expr(PrecMax);
			expect(token_type::op_semicolon);
			return mem->create<ast::StmtReturn>(span_from(begin), expr);
		}break;
		case token_type::op_semicolon: {
			advance();
			return mem->create<ast::StmtNop>(span_from(begin));
		}break;
		case token_type::k_throw: {
			advance();
			auto expr = parse_expr(PrecMax);
			expect(token_type::op_semicolon);
			return mem->create<ast::StmtThrow>(span_from(begin), expr);
		}break;
		case token_type::k_break: {
			advance();
			expect(token_type::op_semicolon);
			return mem->create<ast::StmtBreak>(span_from(begin));
		}break;
		case token_type::k_continue: {
			advance();
			expect(token_type::op_semicolon);
			return mem->create<ast::StmtContinue>(span_from(begin));
		}break;
		case token_type::k_goto: {
			advance();
			ast::ID name = id();
			expect(token_type::op_semicolon);
			return mem->create<ast::StmtGoto>(span_from(begin), name);
		}break;
		case token_type::k_try:{
			advance();
			auto span = span_from(begin);
			auto stmts = parse_block();
			ArrayRef<ast::Stmt*>* finally = nullptr;
			Cons<ast::CatchClause*> catches(tmp);
			while ( peek() == token_type::k_catch ){
				auto begin = loc();
				advance();
				expect(token_type::open_paren);
				auto name = id();
				auto type = m_typebinder();
				expect(token_type::close_paren);
				auto span = span_from(begin);
				auto stmts = parse_block();
				catches.push(mem->create<ast::CatchClause>(span, name, type, stmts));
			}
			if ( peek() == token_type::k_finally ){
				advance();
				finally = mem->create< ArrayRef<ast::Stmt*> >(parse_block());
			}
			return mem->create<ast::StmtTry>(span, stmts, catches.get(mem), finally);
		}break;
		case token_type::k_for:{
			advance();
			if ( peek() == token_type::k_each ){
				// for-each
				advance();
				expect(token_type::open_paren);
				ast::ForInit* decl = try_parse_forin_decl();
				if ( !decl ){
					report("expected variable declaration");
				}
				ast::Expr* gen = parse_expr(PrecMax);
				expect(token_type::close_paren);
				ast::Stmt* body = parse_stmt();
				return mem->create<ast::StmtForIn>(begin, true, decl, gen, body);
			}else{
				expect(token_type::open_paren);
				// for(;;) or for-in
				ast::ForInit* init = try_parse_forin_decl();
				if (init){
					// for-in
					ast::Expr* gen = parse_expr(PrecMax);
					expect(token_type::close_paren);
					ast::Stmt* body = parse_stmt();
					return mem->create<ast::StmtForIn>(begin, false, init, gen, body);
				}else{
					// for(;;)
					auto tt = peek();
					ast::ForInit* init;
					if ( tt == token_type::k_var || tt == token_type::k_const ){
						init = mem->create<ast::ForInit>(mem->create<ast::StmtVar>(parse_var()));
					}else{
						auto expr = m_parse_expr(PrecMax);
						if ( expr ){
							init = mem->create<ast::ForInit>(expr);
						}else{
							init = nullptr;
						}
					}
					expect(token_type::op_semicolon);
					auto cond = m_parse_expr(PrecMax);
					expect(token_type::op_semicolon);
					auto incr = m_parse_expr(PrecMax);
					expect(token_type::close_paren);
					ast::Stmt* body = parse_stmt();
					return mem->create<ast::StmtFor>(begin, init, cond, incr, body);
				}
				
			}
		}break;
		case token_type::k_switch:{
			advance();
			expect(token_type::open_paren);
			auto expr = parse_expr(PrecMax);
			expect(token_type::close_paren);
			auto span = span_from(begin);
			expect(token_type::open_brace);
			Cons<ast::SwitchCase*> cases(tmp);
			ast::SwitchCase* default_case = nullptr;
			while ( true ){
				if (peek() == token_type::k_case ){
					auto begin = loc();
					advance();
					auto expr = parse_expr(PrecMax);
					expect(token_type::op_colon);
					auto span = span_from(begin);
					auto stmts = parse_stmts();
					cases.push(mem->create<ast::SwitchCase>(span, expr, stmts));
				}else if ( peek() == token_type::k_default) {
					if ( default_case ){
						report("duplicate default");
					}
					auto begin = loc();
					advance();
					expect(token_type::op_colon);
					auto span = span_from(begin);
					auto stmts = parse_stmts();
					default_case = mem->create<ast::SwitchCase>(span, nullptr, stmts);
					cases.push(default_case);
				}else{
					break;
				}
			}
			expect(token_type::close_brace);
			return mem->create<ast::StmtSwitch>(span, expr, cases.get(mem));
		}break;
		case token_type::k_while:{
			auto pos = span();
			advance();
			expect(token_type::open_paren);
			auto cond = parse_expr(PrecMax);
			expect(token_type::close_paren);
			auto body = parse_stmt();
			return mem->create<ast::StmtWhile>(pos, cond, body);
		}break;
		case token_type::k_do: {
			auto pos = span();
			advance();
			auto body = parse_stmt();
			expect(token_type::k_while);
			expect(token_type::open_paren);
			auto cond = parse_expr(PrecMax);
			expect(token_type::close_paren);
			expect(token_type::op_semicolon);
			return mem->create<ast::StmtDoWhile>(pos, body, cond);
		}break;
		case token_type::k_if:{
			auto pos = span();
			advance();
			expect(token_type::open_paren);
			auto cond = parse_expr(PrecMax);
			expect(token_type::close_paren);
			auto then = parse_stmt();
			ast::Stmt* else_ = nullptr;
			if ( peek() == token_type::k_else ){
				advance();
				else_ = parse_stmt();
			}
			return mem->create<ast::StmtIf>(pos, cond, then, else_);
		}break;
		case token_type::k_var:
		case token_type::k_const:{
			auto def = parse_var();
			expect(token_type::op_semicolon);
			return mem->create<ast::StmtVar>(def);
		}break;
		default:{
			auto expr = m_parse_expr(PrecMax);
			if ( expr ){
				if ( expr->tag != ast::Expr::Tag::Closure ){
					expect(token_type::op_semicolon);
				}
				return mem->create<ast::StmtExpr>(expr);
			}
			return nullptr;
		}break;
		}
	}

	ast::ForInit* try_parse_forin_decl(){
		auto pos = this->pos;
		auto lap = this->lastAdvancePos;
		ast::VarDef* def = nullptr;
		ast::Expr* expr = nullptr;
		auto tt = peek();
		if ( tt == token_type::identifier ){
			expr = mem->create<ast::ExprRef>(nsid());
		}else if ( tt == token_type::k_var || tt == token_type::k_const ){
			auto old = set_in_allowed(false);
			def = parse_var();
			set_in_allowed(old);
		}
		if ( peek() == token_type::k_in ){
			advance();
			if (def){
				return mem->create<ast::ForInit>(
					mem->create<ast::StmtVar>(def)
				);
			}else{
				return mem->create<ast::ForInit>(
					expr
				);
			}
		}else{
			this->pos = pos;
			this->lastAdvancePos = lap;
			return nullptr;
		}
	}

	bool set_in_allowed(bool allowed){
		bool ret = in_allowed;
		in_allowed = allowed;
		return ret;
	}

	template<class T>
	T* parse_func_node(ArrayRef<ast::Annotation*> annos, ast::Modifiers mods) {
		expect(token_type::k_function);
		ast::Accessor acc = ast::Accessor::None;
		switch ( peek() ){
		case token_type::k_get:{
			advance();
			acc = ast::Accessor::Get;
		}break;
		case token_type::k_set: {
			advance();
			acc = ast::Accessor::Set;
		}break;
		}
		auto pos = span();
		ast::QualID* name = qualid();
		auto sign = parse_signature();
		ast::FuncBody* body = nullptr;
		if ( peek() == token_type::op_semicolon ){
			advance();
		}else{
			body = parse_body();
		}
		return mem->create<T>(pos, annos, mods, acc, name, sign, body);
	}

	template<class T>
	T* parse_var_node(ArrayRef<ast::Annotation*> annos, ast::Modifiers mods) {
		auto def = parse_var();
		expect(token_type::op_semicolon);
		return mem->create<T>(def->span, annos, mods, def);
	}

	ast::InterfaceDef* parse_interface(ArrayRef<ast::Annotation*> annos, ast::Modifiers mods) {
		auto begin = loc();
		expect(token_type::k_interface);
		auto name = id();
		auto span = span_from(begin);
		ArrayRef<ast::QualID*> extends = nullptr;
		if (peek() == token_type::k_extends) {
			advance();
			Cons<ast::QualID*> qids(tmp);
			while (true) {
				qids.push(qualid());
				if (peek() == token_type::op_comma) {
					advance();
				} else {
					break;
				}
			}
			extends = qids.get(mem);
		}
		expect(token_type::open_brace);
		auto entries = parseBlockEntries(false);
		expect(token_type::close_brace);
		return mem->create<ast::InterfaceDef>(
			span,
			annos,
			mods,
			name,
			extends,
			entries
		);
	}


	ast::NamespaceDef* parse_namespace(ArrayRef<ast::Annotation*> annos, ast::Modifiers mods) {
		auto begin = loc();
		expect(token_type::k_namespace);
		auto name = id();
		auto pos = span_from(begin);
		expect(token_type::op_assign);
		if ( peek() != token_type::string ){
			report("expected string");
		}
		ast::ID value(span(), contents());
		advance();
		expect(token_type::op_semicolon);
		return mem->create<ast::NamespaceDef>(
			pos,
			annos,
			mods,
			name,
			value
		);
	}

	ast::ClassDef* parse_class(ArrayRef<ast::Annotation*> annos, ast::Modifiers mods){
		auto begin = loc();
		expect(token_type::k_class);
		auto name = id();
		auto span = span_from(begin);
		ast::QualID* extends = nullptr;
		ArrayRef<ast::QualID*> implements = nullptr;
		if ( peek() == token_type::k_extends ){
			advance();
			extends = qualid();
		}
		if ( peek() == token_type::k_implements ){
			advance();
			Cons<ast::QualID*> qids(tmp);
			while ( true ){
				qids.push(qualid());
				if ( peek() == token_type::op_comma ){
					advance();
				}else{
					break;
				}
			}
			implements = qids.get(mem);
		}
		expect(token_type::open_brace);
		auto entries = parseBlockEntries(false);
		expect(token_type::close_brace);
		return mem->create<ast::ClassDef>(
			span, 
			annos, 
			mods, 
			name, 
			extends, 
			implements,
			entries
		);
	}

	ArrayRef<ast::Annotation*> annotations(){	
		Cons<ast::Annotation*> annotations(tmp);
		while ( peek() == token_type::open_bracket ){
			auto begin = loc();
			advance();
			auto head = id();
			Cons<ast::AnnotationParam*> params(tmp);
			if ( peek() == token_type::open_paren ){
				advance();

				if ( peek() != token_type::close_paren ){
					while ( true ){
						auto begin = loc();
						ast::ID* key = nullptr;
						ast::Expr* value = nullptr;
						if (peek() == token_type::identifier) {
							key = mem->create<ast::ID>(id());
							if ( peek() == token_type::op_assign ){
								advance();
								value = parse_expr(PrecMax);
							}
						}else{
							value = parse_expr(PrecMax);
						}
						params.push(mem->create<ast::AnnotationParam>(span_from(begin), key, value));

						if ( peek() == token_type::op_comma ){
							advance();
						}else{
							break;
						}
					}
				}

				expect(token_type::close_paren);
			}
			expect(token_type::close_bracket);
			annotations.push(mem->create<ast::Annotation>(span_from(begin), head, params.get(mem)));
		}
		return annotations.get(mem);
	}

	ast::Modifiers modifiers(){
		ast::Modifiers mods;

		while ( true ){
			switch (peek()) {
			case token_type::k_static:{
				mods.m_static = true;
				advance();
			}break;
			case token_type::k_override: {
				mods.m_override = true;
				advance();
			}break;
			case token_type::k_final: {
				mods.m_final = true;
				advance();
			}break;
			case token_type::k_dynamic: {
				mods.m_dynamic = true;
				advance();
			}break;
			case token_type::k_public:{
				if ( mods.ns ) report("duplicate namespace");
				mods.ns = &ast::Public;
				advance();
			}break;
			case token_type::k_private: {
				if (mods.ns) report("duplicate namespace");
				mods.ns = &ast::Private;
				advance();
			}break;
			case token_type::k_protected: {
				if (mods.ns) report("duplicate namespace");
				mods.ns = &ast::Protected;
				advance();
			}break;
			case token_type::k_internal: {
				if (mods.ns) report("duplicate namespace");
				mods.ns = &ast::Internal;
				advance();
			}break;
			case token_type::identifier:{
				if ( contents() == "native" ){
					mods.m_native = true;
					advance();
				}else{
					if (mods.ns) report("duplicate namespace");
					mods.ns = mem->create<ast::Namespace>(id());
				}
			}break;
			default:{
				return mods;
			}break;
			}
		}
	}



	ast::ImportDef* parseImport(){
		auto begin = loc();
		expect(token_type::k_import);
		ast::QualID* qid = qid_star();
		expect(token_type::op_semicolon);
		return mem->create<ast::ImportDef>(span_from(begin), qid);
	}

	ast::PackageDecl* parsePackage(){
		expect(token_type::k_package);
		ast::QualID* qid = m_qualid();
		expect(token_type::open_brace);
		auto entries = parseBlockEntries(true);
		expect(token_type::close_brace);
		return mem->create<ast::PackageDecl>(qid, entries);
	}

	void expect(token_type tt){
		if ( peek() != tt ){
			report(std::string("expected ") + get_token_name(tt));
		}
		advance();
	}

	srcspan_t take(){
		auto s = span();
		advance();
		return s;
	}

	bool is_identifier(token_type tt){
		switch ( tt ){
		case token_type::identifier: 
		case token_type::k_namespace: 
		case token_type::k_get: 
		case token_type::k_set: 
			return true;
		default: return false;
		}
	}

	ast::ID id(){
		if (is_identifier(peek())){
			ast::ID id = ast::ID(span(), contents());
			advance();
			return id;
		}else{
			report("expected identifier");
		}
	}

	ast::ID id_star(){
		if (peek() == token_type::identifier || peek() == token_type::op_mul) {
			ast::ID id = ast::ID(span(), contents());
			advance();
			return id;
		} else {
			report("expected identifier or *");
		}
	}

	ast::QualID* qualid(){
		auto qid = m_qualid();
		if ( !qid ){
			report("expected identifier");
		}
		return qid;
	}

	ast::QualID* qid_star(){
		auto begin = loc();
		Cons<ast::ID> ids(tmp);
		while (true) {
			bool is_star = (peek() == token_type::op_mul);
			ids.push(id_star());			
			if ( (!is_star) && peek() == token_type::op_dot) {
				advance();
			} else {
				break;
			}
		}
		return mem->create<ast::QualID>(span_from(begin), ids.get(mem));
	}

	ast::QualID* m_qualid(){
		if ( is_identifier(peek()) ) {
			auto begin = loc();
			Cons<ast::ID> ids(tmp);
			while ( true ){
				ids.push(id());
				if ( peek() == token_type::op_dot ){
					advance();
				}else{
					break;
				}
			}
			return mem->create<ast::QualID>(span_from(begin), ids.get(mem));
		}else{
			return nullptr;
		}
	}

	srcoffset_t loc(){
		return offsets[pos];
	}

	srcspan_t span_from(srcoffset_t pos){
		return {pos, offsets[lastAdvancePos]};
	}

	void parseEOF(){
		if ( peek() != token_type::eof ){
			report("expected eof");
		}
	}

	token_type peek(){
		if (pos >= tokenCount) {
			return token_type::eof;
		}
		return tokens[pos];
	}

	StringRef contents(){
		return StringRef(
			(const u8*)(data + offsets[pos]),
			(uz)(offsets[std::min(tokenCount, pos + 1)] - offsets[pos])
		);
	}

	srcspan_t span(){
		return srcspan_t(offsets[pos], offsets[std::min(tokenCount, pos + 1)]);
	}

	void advance(){
		pos++;
		lastAdvancePos = pos;
		_consume();
	}

	void _consume(){
		auto token = peek();
		while (
			token == token_type::space
			|| token == token_type::newline
			|| token == token_type::ml_comment
			|| token == token_type::line_comment
		){
			pos++;
			token = peek();
		}
		if ( token == token_type::error ){
			report("invalid token");
		}
	}

	[[noreturn]]
	void report(const StringRef& message){
		cc->fatal(
			SourceLoc(
				sourceFile,
				span()
			),
			"parser.bad_parse",
			message
		);
		abort();
	}

	SourceFile* sourceFile;
	CompilerContext* cc;
	MemPool::Slice* mem;
	MemPool::Slice* tmp;
	TokenStream* tokenStream;

	const u8* data;
	token_type* tokens;
	u32* offsets;
	uz lastAdvancePos;
	uz pos;
	uz tokenCount;
	bool in_allowed;
};

ast::File* Parser::run(CompilerContext& cc, SourceFile* source, TokenStream& tokens, MemPool::Slice* mem, MemPool::Slice& tmp){
	ParserImpl impl(&cc, source, mem, &tmp, &tokens);
	return impl.run();
}



}
