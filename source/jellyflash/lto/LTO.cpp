#include <jellyflash/lto/LTO.h>

namespace jly::lto{

LTO::LTO(){
}

void LTO::run(CompilerContext* cc, MemPool::Slice* mem, MemPool::Slice& tmp){
	this->cc = cc;
	this->cu = cc->getUnit();
	this->IR = cu->getIR();
	this->tmp = &tmp;
	this->mem = mem;

	optimizeScope(&IR->package);
	optimizeScope(&IR->internal);
}

void LTO::optimizeScope(ir::FileScope* scope){
	for (auto def : scope->definitions) {
		match(def)
		caseof(ir::Class, class_) {
			optimizeClass(class_);
		}
		casedef(_) {
		}
		endmatch
	}
}

void LTO::optimizeClass(ir::Class* class_){
	if ( !class_->has_children ){
		class_->m_final = true;
	}
	if ( class_->m_final ){
		for (auto method : class_->instance_methods) {
			method->is_final = true;
		}
	}
	for (auto method : class_->instance_methods) {
		if ( !method->has_overrides ){
			method->is_final = true;
		}
	}
}



}
