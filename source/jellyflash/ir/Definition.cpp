#include <jellyflash/ir/Definition.h>

namespace jly::ir{

NamespaceDef
	NSPublicDef(nullptr, { nullptr,{0,0} }, "public", "flash.public"),
	NSPrivateDef(nullptr, { nullptr,{0,0} }, "private", "flash.private"),
	NSInternalDef(nullptr, { nullptr,{0,0} }, "internal", "flash.internal"),
	NSProtectedDef(nullptr, { nullptr,{0,0} }, "protected", "flash.protected");

NamespaceNode
	NSPublic(&NSPublicDef),
	NSPrivate(&NSPrivateDef),
	NSInternal(&NSInternalDef),
	NSProtected(&NSProtectedDef);


FileScope::FileScope(File* file, bool is_private) :
	lookup(&file->lookup),
	file(file),
	is_private(is_private) {
}

}

