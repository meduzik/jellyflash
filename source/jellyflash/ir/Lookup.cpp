#include <jellyflash/ir/Lookup.h>
#include <jellyflash/ir/Definition.h>

namespace jly::ir{


void PackageLookup::add(ir::Definition* def) {
	symbols.insert({ def->name, def });
}

bool FileLookup::add(ir::Definition* def) {
	return !exports.insert({ def->name, def }).second;
}

ir::Definition* FileLookup::find(StringRef name){
	auto it = exports.find(name);
	if ( it == exports.end() ){
		return nullptr;
	}
	return it->second;
}

void FileScopeLookup::add(CompilationUnit* cu){
	symbols.insert({ cu->getName(), cu });
}


}