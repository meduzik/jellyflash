#include <jellyflash/ir/Type.h>

namespace jly::ir{

TypePrim
	TypePrim::Any(TypePrim::Kind::Any),
	TypePrim::Object(TypePrim::Kind::Object),
	TypePrim::Void(TypePrim::Kind::Void),
	TypePrim::Boolean(TypePrim::Kind::Boolean),
	TypePrim::Number(TypePrim::Kind::Number),
	TypePrim::Int(TypePrim::Kind::Int),
	TypePrim::Uint(TypePrim::Kind::Uint),
	TypePrim::Class(TypePrim::Kind::Class),
	TypePrim::String(TypePrim::Kind::String),
	TypePrim::Function(TypePrim::Kind::Function),
	TypePrim::Rest(TypePrim::Kind::Rest),
	TypePrim::Null(TypePrim::Kind::Null);

TypeVar
	TypeVar::Instance;

}