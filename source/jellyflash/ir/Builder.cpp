#include <jellyflash/compiler/CompilerEnv.h>
#include <jellyflash/ir/Resolver.h>
#include <jellyflash/ir/Builder.h>
#include <jellyflash/ir/Definition.h>
#include <jellyflash/ir/Lookup.h>
#include <jellyflash/ir/Utils.h>
#include <jellyflash/compiler/CompilationUnit.h>
#include <jellyflash/ir/Class.h>
#include <jellyflash/ir/Interface.h>
#include <jellylib/utils/StringDecode.h>
#include <jellylib/utils/Murmur.h>

namespace jly{

namespace{
u32 mix_hash(StringRef filename, bool is_private, StringRef name, bool is_offset){
	u32 hash = is_offset ? 0xC37C8378u : 0x250E6D8Bu;
	if ( is_private ){
		hash ^= 0xC43A4719u;
	}
	hash = murmur3_32(filename.getData(), filename.getSize(), hash);
	hash = murmur3_32(name.getData(), name.getSize(), hash);
	return hash;
}
}

Builder::Builder(){
}

ir::File* Builder::run(CompilerContext* cc, ast::File* AST, MemPool::Slice* mem, MemPool::Slice& tmp){
	this->cc = cc;
	this->mem = mem;
	this->tmp = &tmp;
	this->cu = cc->getUnit();
	return buildFile(AST);
}

void Builder::fillScope(ir::FileScope* IR, FileScope* scope){
	IR->import_units = scope->import_units.get(mem);
	IR->import_packages = scope->import_packages.get(mem);
	IR->definitions = scope->definitions.get(mem);
	IR->namespaces_late = scope->namespaces_late.get(mem);
	IR->opened_namespaces = scope->opened_namespaces.get(mem);
}

ir::File* Builder::buildFile(ast::File* AST){
	IR = mem->create<ir::File>(cc->getUnit());
	
	verifyPackage(AST->packageDecl->qid);
	{
		FileScope scope(&IR->package, tmp);
		for ( auto entry : AST->packageDecl->entries ){
			visitEntry(&scope, entry, true);
		}
		fillScope(&IR->package, &scope);
	}

	{
		FileScope scope(&IR->internal, tmp);
		for (auto entry : AST->entries) {
			visitEntry(&scope, entry, false);
		}
		fillScope(&IR->internal, &scope);
	}

	return IR;
}

void Builder::visitEntry(FileScope* scope, ast::PackageBlockEntry* entry, bool in_package){
	match(entry)
	caseof(ast::ImportDef, import)
		visitImport(scope, import);
	caseof(ast::ClassDef, class_)
		visitClass(scope, class_);
	caseof(ast::InterfaceDef, iface)
		visitInterface(scope, iface);
	caseof(ast::NamespaceDef, ns)
		visitNamespaceDef(scope, ns);
	caseof(ast::UseNamespace, use_ns)
		visitUseNamespace(scope, use_ns);
	caseof(ast::FuncDefPackage, func_def)
		visitGlobalFunction(scope, func_def);
	caseof(ast::VarDefPackage, var_def)
		visitGlobalVar(scope, var_def);
	casedef(entry)
		cc->error(entry->span, "builder.invalid_def", "invalid definition");
	endmatch
}

void Builder::visitUseNamespace(FileScope* scope, ast::UseNamespace* def){
	scope->opened_namespaces.push(buildNamespaceNode(scope, def->name));
}

void Builder::visitNamespaceDef(FileScope* scope, ast::NamespaceDef* def){
	ir::NamespaceDef* ns = mem->create<ir::NamespaceDef>(scope->IR, cc->getLoc(def->span), def->name.id, def->value.id);
	ns->ns = resolveToplevelNamespace(def->span, def->mods.ns, scope->IR->is_private);
	if (
		def->mods.m_dynamic 
		|| def->mods.m_final 
		|| def->mods.m_native
		|| def->mods.m_override
		|| def->mods.m_static
	){
		cc->error(def->span, "builder.ns_modifier", "namespace cannot have modifiers");
	}
	if ( !scope->IR->is_private ){
		expose(ns);
	}
	exports(scope, ns);
}


void Builder::visitInterface(FileScope* scope, ast::InterfaceDef* def) {
	ir::Interface* iface = mem->create<ir::Interface>(mem, scope->IR, def, cc->getLoc(def->span), def->name.id);
	auto& mods = def->mods;
	if (mods.m_dynamic) {
		cc->error(def->span, "builder.iface_dynamic", "cannot specify 'dynamic' for interface");
	}
	if (mods.m_final) {
		cc->error(def->span, "builder.iface_final", "cannot specify 'final' for interface");
	}
	if (mods.m_native) {
		cc->error(def->span, "builder.iface_native", "cannot specify 'native' for interface");
	}
	if (mods.m_override) {
		cc->error(def->span, "builder.iface_override", "cannot specify 'override' for interface");
	}
	if (mods.m_static) {
		cc->error(def->span, "builder.iface_static", "cannot specify 'static' for interface");
	}

	iface->ns = resolveToplevelNamespace(def->span, def->mods.ns, scope->IR->is_private);

	InterfaceBuilder* ib = tmp->create<InterfaceBuilder>(scope, tmp);

	for (auto entry : def->entries) {
		match(entry)
			caseof(ast::VarDefPackage, var_def)
				cc->error(def->span, "builder.iface_var", "variable definitions are not allowed in an interface");
			caseof(ast::FuncDefPackage, func_def)
				addInterfaceMethod(ib, iface, func_def);
			caseof(ast::StaticBlock, static_block)
				cc->error(def->span, "builder.iface_staticblock", "static code blocks are not allowed in an interface");
			casedef(entry)
				cc->error(def->span, "builder.iface_invalidmember", "invalid interface member");
		endmatch
	}

	iface->methods = ib->methods.get(mem);

	iface->hash_base = mix_hash(iface->scope->file->cu->getFullName(), iface->scope->is_private, iface->name, false);
	iface->hash_offset = mix_hash(iface->scope->file->cu->getFullName(), iface->scope->is_private, iface->name, true);
	iface->hash_offset = iface->hash_offset * 2 + 1;

	if (!scope->IR->is_private) {
		expose(iface);
	}
	exports(scope, iface);
}

void Builder::visitGlobalVar(FileScope* scope, ast::VarDefPackage* def) {
	auto& mods = def->mods;
	if (mods.m_dynamic) {
		cc->error(def->span, "builder.var_dynamic", "cannot specify 'dynamic' for global var");
	}
	if (mods.m_final) {
		cc->error(def->span, "builder.var_final", "cannot specify 'final' for global var");
	}
	if (mods.m_native) {
		cc->error(def->span, "builder.var_native", "cannot specify 'native' for global var");
	}
	if (mods.m_override) {
		cc->error(def->span, "builder.var_override", "cannot specify 'override' for global var");
	}
	if (mods.m_static) {
		cc->error(def->span, "builder.var_static", "cannot specify 'static' for global var");
	}

	auto resolved_ns = resolveToplevelNamespace(def->span, def->mods.ns, scope->IR->is_private);

	for ( auto decl : def->def->decls ){
		ir::GlobalVar* var = mem->create<ir::GlobalVar>(scope->IR, decl, cc->getLoc(decl->name.span), decl->name.id);
		var->ns = resolved_ns;
		if ( def->def->kind == ast::VarDef::Kind::Const ){
			var->is_const = true;
		}
		
		if (!scope->IR->is_private) {
			expose(var);
		}
		exports(scope, var);
	}
}

void Builder::visitGlobalFunction(FileScope* scope, ast::FuncDefPackage* def) {
	ast::QualID* qid = def->name;
	if ( qid->ids.getSize() > 1 ){
		auto package = cc->resolvePackage(qid->ids.slice(0, qid->ids.getSize() - 1));
		if ( package != scope->IR->file->cu->getPackage() ){
			cc->error(qid->span, "builder.qual_name_invalid_package", "qualified function name -- invalid package");
		}
	}
	ir::GlobalFunction* func = mem->create<ir::GlobalFunction>(
		scope->IR, 
		def,
		cc->getLoc(def->span), 
		qid->ids[qid->ids.getSize() - 1].id
	);
	
	auto& mods = def->mods;
	if (mods.m_dynamic) {
		cc->error(def->span, "builder.func_dynamic", "cannot specify 'dynamic' for global function");
	}
	if (mods.m_final) {
		cc->error(def->span, "builder.func_final", "cannot specify 'final' for global function");
	}
	if (mods.m_native) {
		func->m_native = true;
	}
	if (mods.m_override) {
		cc->error(def->span, "builder.func_override", "cannot specify 'override' for global function");
	}
	if (mods.m_static) {
		cc->error(def->span, "builder.func_static", "cannot specify 'static' for global function");
	}

	func->ns = resolveToplevelNamespace(def->span, def->mods.ns, scope->IR->is_private);

	if (!scope->IR->is_private) {
		expose(func);
	}
	exports(scope, func);
}

void Builder::visitClass(FileScope* scope, ast::ClassDef* def){
	ir::Class* class_ = mem->create<ir::Class>(mem, scope->IR, def, cc->getLoc(def->span), def->name.id);
	auto& mods = def->mods;
	if ( mods.m_dynamic ){
		class_->m_dynamic = true;
	}
	if ( mods.m_final ){
		class_->m_final = true;
	}
	if ( mods.m_override ){
		cc->error(def->span, "builder.class_override", "cannot specify 'override' for class");
	}
	if (mods.m_static) {
		cc->error(def->span, "builder.class_static", "cannot specify 'static' for class");
	}
	if (mods.m_native) {
		class_->m_native = true;
	}

	class_->ns = resolveToplevelNamespace(def->span, def->mods.ns, scope->IR->is_private);

	ClassBuilder* cb = tmp->create<ClassBuilder>(scope, tmp);

	for ( auto entry : def->entries ){
		match(entry)
		caseof(ast::VarDefPackage, var_def)
			addClassVar(cb, class_, var_def);
		caseof(ast::FuncDefPackage, func_def)
			addClassMethod(cb, class_, func_def);
		caseof(ast::StaticBlock, static_block)
			addClassStaticBlock(cb, class_, static_block);
		casedef(entry)
			cc->error(def->span, "builder.class_invalidmember", "invalid class member");
		endmatch
	}

	for (auto ann : def->annos) {
		auto annotation = buildAnnotation(ann);
		if ( annotation->name == "jfl_native" ){
			ir::NativeClass* native = mem->create<ir::NativeClass>();
			class_->native = native;
			for ( auto param : annotation->params ){
				if ( param.key == "payload" ){
					if ( !param.value.as_string(&native->payload) ){
						cc->error(param.span, "builder.native_instance_name", "expected instance name");
					}
				}else if ( param.key == "invoke" ){
					if (!param.value.as_string(&native->invoke)) {
						cc->error(param.span, "builder.native_invoke_name", "expected invoke method name");
					}
				} else if (param.key == "extra_gc") {
					if (!param.value.as_boolean(&native->extra_gc)) {
						cc->error(param.span, "builder.native_extra_gc", "expected boolean value");
					}
				} else if (param.key == "gc") {
					if (!param.value.as_boolean(&native->generate_gc)) {
						cc->error(param.span, "builder.native_gc", "expected boolean value");
					}
				} else if (param.key == "destructor") {
					if (!param.value.as_boolean(&native->destructor)) {
						cc->error(param.span, "builder.native_finalize", "expected boolean value");
					}
				} else if (param.key == "iter") {
					if (!param.value.as_boolean(&native->generate_iter)) {
						cc->error(param.span, "builder.native_iter", "expected boolean value");
					}
				} else if (param.key == "dyn") {
					if (!param.value.as_boolean(&native->generate_dynamic)) {
						cc->error(param.span, "builder.native_dyn", "expected boolean value");
					}
				} else {
					cc->error(param.span, "builder.unknown_native_param", "unknown native annotation param");
				}
			}
		}else if(annotation->name == "Reflection"){
			class_->uses_reflection = true;
		}
	}

	class_->static_fields = cb->static_fields.get(mem);
	class_->instance_fields = cb->instance_fields.get(mem);
	class_->static_methods = cb->static_methods.get(mem);
	class_->instance_methods = cb->instance_methods.get(mem);
	class_->static_blocks = cb->static_blocks.get(mem);

	if (!scope->IR->is_private) {
		expose(class_);
	}
	exports(scope, class_);
}

ir::NamespaceNode* Builder::buildNamespaceNode(FileScope* scope, ast::ID name){
	ir::NamespaceNode* node = mem->create<ir::NamespaceNode>(nullptr);
	scope->namespaces_late.push(name, node);
	return node;
}

void Builder::addClassVar(ClassBuilder* class_builder, ir::Class* class_, ast::VarDefPackage* def) {
	if (def->mods.m_dynamic) {
		cc->error(def->span, "builder.field_dynamic", "fields cannot be declared dynamic");
	}
	if (def->mods.m_final) {
		cc->error(def->span, "builder.field_final", "fields cannot be declared final");
	}
	if (def->mods.m_native) {
		cc->error(def->span, "builder.field_native", "fields cannot be declared native");
	}
	if (def->mods.m_override) {
		cc->error(def->span, "builder.field_override", "cannot override field");
	}
	bool is_static = def->mods.m_static;
	bool is_const = (def->def->kind == ast::VarDef::Kind::Const);
	ir::NamespaceNode* ns = resolveMemberNamespace(class_builder->scope, def->mods.ns);
	for (auto decl : def->def->decls) {
		ir::ClassField* field = mem->create<ir::ClassField>(
			class_,
			ns,
			cc->getLoc(decl->name.span),
			decl->name.id,
			decl
			);
		field->is_const = is_const;
		field->is_static = is_static;
		if (is_static) {
			class_builder->static_fields.push(field);
			if ( field->decl->expr ){
				ir::StaticBlock* block = mem->create<ir::StaticBlock>(class_builder->scope->IR, field);
				class_builder->static_blocks.push(block);
			}
		} else {
			class_builder->instance_fields.push(field);
		}
	}
}

void Builder::addClassMethod(ClassBuilder* class_builder, ir::Class* class_, ast::FuncDefPackage* def){
	if (def->mods.m_dynamic){
		cc->error(def->span, "builder.method_dynamic", "methods cannot be declared dynamic");
	}
	auto ids = def->name->ids;
	if ( ids.getSize() > 1 ){
		cc->error(def->span, "builder.method_qual_name", "method name cannot be qualified");
	}
	bool is_static = def->mods.m_static;

	ir::ClassMethod* method = mem->create<ir::ClassMethod>(
		class_,
		resolveMemberNamespace(class_builder->scope, def->mods.ns),
		cc->getLoc(def->span),
		ids[ids.getSize() - 1].id,
		def
	);
	switch (def->acc){
	case ast::Accessor::Get:{
		method->accessor = ir::Accessor::Get;
	}break;
	case ast::Accessor::Set: {
		method->accessor = ir::Accessor::Set;
	}break;
	}
	
	method->is_static = def->mods.m_static;
	method->is_final = def->mods.m_final;
	method->is_native = def->mods.m_native;
	method->is_override = def->mods.m_override;
	if ( method->name == class_->name ){
		if ( is_static ){
			method->is_static = false;
			cc->error(method->loc, "builder.constructor_static", "constructor cannot be static");
		}
		if (method->is_override) {
			method->is_override = false;
			cc->error(method->loc, "builder.constructor_override", "constructor cannot override");
		}
		if (method->is_final) {
			method->is_final = false;
			cc->error(method->loc, "builder.constructor_final", "constructor cannot be final");
		}
		if ( class_->constructor ){
			cc->error(method->loc, "builder.constructor_dup", "duplicate constructor");
			// typecheck anyway
			class_builder->instance_methods.push(method);
		}else{
			class_->constructor = method;
		}
	}else if (is_static) {
		class_builder->static_methods.push(method);
	} else {
		class_builder->instance_methods.push(method);
	}

	for (auto ann : def->annos) {
		auto annotation = buildAnnotation(ann);
		if (annotation->name == "Native") {
			method->is_native = true;
			method->ast_def->body = nullptr;
		}
	}
}

void Builder::addInterfaceMethod(InterfaceBuilder* interface_builder, ir::Interface* iface, ast::FuncDefPackage* def) {
	if (def->mods.m_dynamic) {
		cc->error(def->span, "builder.imethod_dynamic", "methods cannot be declared dynamic");
	}
	auto ids = def->name->ids;
	if (ids.getSize() > 1) {
		cc->error(def->span, "builder.imethod_qual_name", "method name cannot be qualified");
	}
	if (def->mods.m_final) {
		cc->error(def->span, "builder.imethod_final", "interface methods cannot be final");
	}
	if (def->mods.m_native) {
		cc->error(def->span, "builder.imethod_native", "interface methods cannot be declared native");
	}
	if (def->mods.m_override) {
		cc->error(def->span, "builder.imethod_override", "interface methods cannot be declared as 'override'");
	}

	if ( def->mods.ns ){
		cc->error(def->span, "builder.imethod_ns", "interface methods cannot have namespaces");
	}

	ir::InterfaceMethod* method = mem->create<ir::InterfaceMethod>(
		iface,
		cc->getLoc(def->span),
		ids[ids.getSize() - 1].id,
		def
	);
	switch (def->acc) {
	case ast::Accessor::Get: {
		method->accessor = ir::Accessor::Get;
	}break;
	case ast::Accessor::Set: {
		method->accessor = ir::Accessor::Set;
	}break;
	}

	interface_builder->methods.push(method);
}

void Builder::addClassStaticBlock(ClassBuilder* class_builder, ir::Class* class_, ast::StaticBlock* def){
	ir::StaticBlock* block = mem->create<ir::StaticBlock>(class_builder->scope->IR, def);
	class_builder->static_blocks.push(block);
}


void Builder::exports(FileScope* scope, ir::Definition* def){
	if ( IR->lookup.add(def) ){
		cc->error(def->loc, "builder.dup_def", "duplicate definition %1", def->name, IR->lookup.find(def->name));
	}
	scope->definitions.push(def);
}

void Builder::expose(ir::Definition* def) {
	if (IR->publicDef) {
		cc->error(def->loc, "builder.dup_public_def", "second externally visible definition found", IR->publicDef);
	} else {
		IR->publicDef = def;
	}
}

void Builder::visitImport(FileScope* scope, ast::ImportDef* import){
	auto ids = import->qid->ids;
	ArrayRef<ast::ID> package_ids = { ids.getData(), ids.getSize() - 1 };
	auto package = cc->resolvePackage(package_ids);
	if ( !package ){
		cc->error(import->span, "builder.package_not_found", "package %1 not found", package_ids);
		return;
	}
	auto& last_id = ids[ids.getSize() - 1].id;
	if (last_id  == "*" ){
		scope->import_packages.push(mem->create<ir::ImportPackage>(cc->getLoc(import->span), package));
	}else{
		CompilationUnit* cu = package->findUnit(last_id);
		if ( !cu ){
			cc->error(import->span, "builder.definition_not_found",  "definition %1 not found", ids);
			return;
		}
		scope->import_units.push(mem->create<ir::ImportUnit>(cc->getLoc(import->span), last_id, cu));
	}
}

ir::NamespaceNode* Builder::resolveMemberNamespace(FileScope* scope, ast::Namespace* ns) {
	auto own_ns = ns;
	auto resolved_ns = &ir::NSInternal;
	if (own_ns == &ast::Private) {
		resolved_ns = &ir::NSPrivate;
	} else if (own_ns == &ast::Public) {
		resolved_ns = &ir::NSPublic;
	} else if (own_ns == &ast::Internal) {
		resolved_ns = &ir::NSInternal;
	} else if (own_ns == &ast::Protected) {
		resolved_ns = &ir::NSProtected;
	} else if (!own_ns) {
	} else {
		return buildNamespaceNode(scope, ns->id);
	}
	return resolved_ns;
}

ir::NamespaceNode* Builder::resolveToplevelNamespace(srcspan_t span, ast::Namespace* ns, bool is_private){
	auto own_ns = ns;
	auto resolved_ns = &ir::NSInternal;
	if (own_ns == &ast::Private) {
		cc->error(span, "builder.invalid_top_level_ns", "private namespace cannot be used on a top level declaration");
	} else if (own_ns == &ast::Public) {
		if ( is_private ){
			cc->error(span, "builder.invalid_top_level_ns", "non-package definitions can only be internal");
		}else{
			resolved_ns = &ir::NSPublic;
		}
	} else if (own_ns == &ast::Internal) {
	} else if (own_ns == &ast::Protected) {
		cc->error(span, "builder.invalid_top_level_ns", "protected namespace cannot be used on a top level declaration");
	} else if (!own_ns) {
	} else {
		cc->error(span, "builder.invalid_top_level_ns", "user-defined namespace cannot be used on a top level declaration");
	}
	return resolved_ns;
}

void Builder::verifyPackage(ast::QualID* qid){
	CUPackage* my_package = cc->getUnit()->getPackage();
	CUPackage* target_package;
	if ( qid == nullptr ){
		target_package = cc->getEnv()->getPackageRoot();
	}else{
		target_package = cc->resolvePackage(qid->ids);
	}
	if ( my_package != target_package ){
		cc->error("builder.invalid_package", "invalid package %1, expected %2", target_package->getFullName(), my_package->getFullName());
	}
}

ir::Annotation* Builder::buildAnnotation(ast::Annotation* anno){
	ir::Annotation* ret = mem->create<ir::Annotation>(mem, anno->span, anno->head.id);
	for ( auto param : anno->params ){
		ir::AnnotationValue value;
		if ( param->value ){
			match(param->value)
			caseof(ast::ExprPrim, prim){
				switch ( prim->kind ){
				case ast::ExprPrim::Kind::Null:{
					value = nullptr;
				}break;
				case ast::ExprPrim::Kind::True: {
					value = true;
				}break;
				case ast::ExprPrim::Kind::False: {
					value = false;
				}break;
				default:{
					cc->error(param->value->span, "builder.bad_annotation", "bad annotation param"); 
				}break;
				}
			}
			caseof(ast::ExprLiteral, lit){
				switch ( lit->kind ){
				case ast::ExprLiteral::Kind::String:{
					value = DecodeJSONString(mem, *tmp, {lit->contents.getData() + 1, lit->contents.getSize() - 2}, nullptr);
				}break;
				case ast::ExprLiteral::Kind::Decimal:{
					double number;
					auto result = std::from_chars(
						(const char*)lit->contents.getData(),
						(const char*)lit->contents.getData() + lit->contents.getSize(),
						number,
						std::chars_format::general
					);
					if ( result.ec != std::errc() ){
						jl_unreachable;
					}else{
						value = number;
					}
				}break;
				case ast::ExprLiteral::Kind::Hexidecimal:{
					uint32_t number;
					auto result = std::from_chars(
						(const char*)lit->contents.getData() + 2,
						(const char*)lit->contents.getData() + lit->contents.getSize(),
						number,
						16
					);
					if (result.ec != std::errc()) {
						jl_unreachable;
					} else {
						value = (double)number;
					}
				}break;
				default:{
					jl_unreachable;
				}break;
				}
			}
			casedef(_){
				cc->error(param->value->span, "builder.bad_annotation", "bad annotation param");
			}
			endmatch
		}

		ir::AnnotationNode node{
			param->span,
			param->key ? param->key->id : nullptr,
			value
		};

		ret->params.push_back(node);
	}
	return ret;
}

}