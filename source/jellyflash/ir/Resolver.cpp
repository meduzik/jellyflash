#include <jellyflash/ir/Resolver.h>
#include <jellyflash/ir/File.h>
#include <jellyflash/ir/Definition.h>
#include <jellyflash/compiler/CompilerEnv.h>
#include <jellyflash/ir/Utils.h>
#include <jellyflash/ir/Class.h>
#include <jellyflash/ir/Interface.h>
#include <jellyflash/ir/Type.h>
#include <jellyflash/icode/ICode.h>

namespace jly{

Resolver::Resolver(){
}

void Resolver::prepare(CompilerContext* cc, ir::File* IR, MemPool::Slice* mem, MemPool::Slice& tmp){
	this->cc = cc;
	this->IR = IR;
	this->mem = mem;
	this->tmp = &tmp;
	this->cu = cc->getUnit();	
}

void Resolver::setTempPool(MemPool::Slice& tmp){
	this->tmp = &tmp;
}

void Resolver::run(){
	resolveScope(&IR->package);
	resolveScope(&IR->internal);
}

ArrayRef<ir::Interface*> Resolver::resolveInterfaces(ir::FileScope* scope, ArrayRef<ast::QualID*> ifaces){
	Cons<ir::Interface*> implements(tmp);
	for (auto impl : ifaces) {
		ir::Definition* def = resolveDefinition(scope, impl->span, impl->ids);
		if (!def) {
			cc->error(
				impl->span,
				"linker.base_iface_undefined", 
				"cannot resolve base interface %1",
				impl->ids
			);
		} else {
			cc->checkPermissions(def);
			match(def)
			caseof(ir::Interface, base_iface)
				implements.push(base_iface);
			casedef(def)
				cc->error(
					impl->span, 
					"linker.base_iface_invalid",
					"name %1 does not refer to an interface", 
					impl->ids,
					def
				);
			endmatch
		}
	}
	return implements.get(mem);
}

void Resolver::resolveScope(ir::FileScope* scope){
	for ( auto import : scope->import_units ){
		scope->lookup.add(import->cu);
	}

	for (auto ns_node : scope->namespaces_late) {
		ir::Definition* def = resolveDefinition(scope, ns_node.first.span, { ns_node.first });
		if ( def ){
			match(def)
			caseof(ir::NamespaceDef, ns)
				ns_node.second->def = ns;
			casedef(_)
				cc->error(
					ns_node.first.span,
					"resolver.namespace_invalid",
					"name %1 does not refer to a namespace",
					ns_node.first,
					def
				);
			endmatch
		}else{
			cc->error(
				ns_node.first.span,
				"resolver.namespace_unresolved",
				"name %1 does not refer to a namespace",
				ns_node.first
			);
		}
	}
	
	for (auto def : scope->definitions){
		match(def)
		caseof(ir::Class, class_)
			resolveClass(scope, class_);
		caseof(ir::Interface, iface)
			resolveInterface(scope, iface);
		caseof(ir::GlobalFunction, func)
			resolveFunction(scope, func);
		caseof(ir::GlobalVar, var)
			resolveVariable(scope, var);
		endmatch
	}
}

void Resolver::resolveInterface(ir::FileScope* scope, ir::Interface* iface) {
	iface->extends = resolveInterfaces(scope, iface->AST->extends);

	for (auto method : iface->methods) {
		resolveInterfaceMethod(scope, method);
	}
}

void Resolver::resolveClass(ir::FileScope* scope, ir::Class* class_){
	if (class_->AST->extends) {
		ir::Definition* def = resolveDefinition(scope, class_->AST->extends->span, class_->AST->extends->ids);
		if (!def) {
			cc->error(
				class_->AST->extends->span, 
				"resolver.base_class_undefiend",
				"cannot resolve base class %1",
				class_->AST->extends->ids
			);
		} else {
			cc->checkPermissions(def);
			match(def)
			caseof(ir::Class, base_class)
				class_->extends = base_class;
			casedef(def)
				cc->error(
					class_->AST->extends->span, 
					"resolver.base_class_invalid",
					"name %1 does not refer to a class",
					class_->AST->extends->ids,
					def
				);
			endmatch
		}
	}else{
		if ( class_ != cc->getIntrinsics()->classes.Object ){
			class_->extends = cc->getIntrinsics()->classes.Object;
		}
	}
	class_->implements = resolveInterfaces(scope, class_->AST->implements);
	for ( auto field : class_->static_fields ){
		resolveClassField(scope, field);
	}
	for (auto field : class_->instance_fields) {
		resolveClassField(scope, field);
	}
	for (auto method : class_->instance_methods){
		resolveClassMethod(scope, method, false);
	}
	for (auto method : class_->static_methods) {
		resolveClassMethod(scope, method, false);
	}
	if ( class_->constructor ){
		resolveClassMethod(scope, class_->constructor, true);
		auto sign = class_->constructor->signature;
		if ( sign->retTy != &ir::TypePrim::Void ){
			sign->retTy = &ir::TypePrim::Void;
		}
	}
}

void Resolver::resolveClassMethod(ir::FileScope* scope, ir::ClassMethod* method, bool is_constructor){
	if ( method->ast_def ){
		method->signature = resolveSignature(method->loc, scope, method->ast_def->sign, is_constructor ? &ir::TypePrim::Void : nullptr);
	}else{
		method->signature = mem->create<ir::FunctionSignature>(&ir::TypePrim::Void);
	}
}

void Resolver::resolveClassField(ir::FileScope* scope, ir::ClassField* field) {
	field->type = resolveType(scope, field->decl->type);
	if ( field->decl->expr ){
		field->initializer = mem->create<icode::VarInitializer>(
			field->owner,
			field->type,
			icode::Reference(field, nullptr),
			field->is_const,
			field->decl->expr
		);
	}
}

void Resolver::resolveInterfaceMethod(ir::FileScope* scope, ir::InterfaceMethod* method) {
	method->signature = resolveSignature(method->loc, scope, method->ast_def->sign);
}


ir::FunctionSignature* Resolver::resolveSignature(const SourceLoc& loc, ir::FileScope* scope, ast::FuncSignature* def, ir::Type* def_ret_ty){
	
	ir::Type* retTy;
	
	if ( def->retTy ){
		retTy = resolveType(scope, def->retTy);
	}else{
		if (def_ret_ty ){
			retTy = def_ret_ty;
		}else{
			cc->warning(loc, "resolve.no_ret_type", "no return type, the type will be '*'");
			retTy = &ir::TypePrim::Any;
		}
	}

	ir::FunctionSignature* signature = mem->create<ir::FunctionSignature>(
		retTy
	);
	if (def->ellipsis) {
		signature->rest = mem->create<ir::FunctionParam>(
			signature,
			cc->getLoc(def->ellipsis->span),
			def->ellipsis->id,
			&ir::TypePrim::Rest,
			nullptr
		);
	}

	Cons<ir::FunctionParam*> params(tmp);
	for (auto param_def : def->params) {
		ir::FunctionParam* param = mem->create<ir::FunctionParam>(
			signature,
			cc->getLoc(param_def->name.span),
			param_def->name.id,
			resolveType(scope, param_def->type),
			param_def->initializer
		);
		params.push(param);
	}
	signature->params = params.get(mem);

	return signature;
}

ir::Type* Resolver::resolveType(ir::FileScope* scope, ast::Type* type_ast){
	if ( !type_ast ){
		return &ir::TypePrim::Any;
	}

	match (type_ast)
	caseof(ast::TypePrim, prim)
		switch ( prim->kind ){
		case ast::TypePrim::Kind::Star:{
			return &ir::TypePrim::Any;
		}break;
		case ast::TypePrim::Kind::Void: {
			return &ir::TypePrim::Void;
		}break;
		default:{
			cc->fatal(type_ast->span, "resolver.invalid_prim_type", "internal error: prim type not handled");
		}break;
		}
	caseof(ast::TypeRef, ref)
		if ( ref->ids.getSize() == 1 ){
			StringRef name = ref->ids[0].id;
			if ( name == "$$" ){
				return &ir::TypeVar::Instance;
			}
		}

		ir::Definition* def = resolveDefinition(scope, ref->span, ref->ids);
		if ( !def ){
			cc->error(
				type_ast->span,
				"resolver.unresolved_type",
				"definition %1 not found",
				ref->ids
			);
			return &ir::TypePrim::Any;
		}
		match(def)
		caseof(ir::Class, class_)
			return class_->type;
		caseof(ir::Interface, iface)
			return &iface->type;
		casedef(_)
			cc->error(
				type_ast->span, 
				"resolver.not_a_type",
				"name %1 does not refer to a type", 
				ref->ids,
				def
			);
			return &ir::TypePrim::Any;
		endmatch
	caseof(ast::TypeInstance, instance)
		if ( instance->ids.getSize() == 1 && instance->ids[0].id == "Vector" ){
			auto rhs = resolveType(scope, instance->arg);
			return cc->getEnv()->getVectorType(rhs);
		}else{
			cc->error(
				instance->span,
				"resolver.invalid_generic",
				"invalid generic type - must be Vector"
			);
			return &ir::TypePrim::Any;
		}
	casedef(_)
		cc->fatal(
			type_ast->span, 
			"resolver.bad_ast_type",
			"internal error: ast type is not implemented"
		);
	endmatch
}

void Resolver::resolveFromUnit(Cons<ir::Definition*>& candidates, CompilationUnit* cu, StringRef id) {
	cu->require(CompilationUnit::Parsed);
	auto defIR = cu->getIR()->publicDef;
	if (defIR && defIR->name == id) {
		if ( !candidates.empty() && candidates.getLast() == defIR ){
			return;
		}
		candidates.push(defIR);
	}
}

void Resolver::resolveFunction(ir::FileScope* scope, ir::GlobalFunction* func){
	func->signature = resolveSignature(func->loc, scope, func->def->sign);
}

void Resolver::resolveVariable(ir::FileScope* scope, ir::GlobalVar* var){
	var->type = resolveType(scope, var->def->type);

	if (var->def->expr) {
		var->initializer = mem->create<icode::VarInitializer>(
			var, 
			var->type, 
			var,
			var->is_const,
			var->def->expr
		);
	}
}

ir::Definition* Resolver::resolveDefinition(ir::FileScope* scope, srcspan_t span, ArrayRef<ast::ID> ids){
	Cons<ir::Definition*> candidates(tmp);

	StringRef last_id = ids[ids.getSize() - 1].id;

	// Unqualified identifier
	if ( ids.getSize() == 1 ){
		{
			// Look up in the file - internal definitions
			auto& symbols = IR->lookup.getSymbols();
			auto it = symbols.find(last_id);
			if (it != symbols.end()) {
				candidates.push(it->second);
			}
		}

		{
			// Look up in the scope - direct imports
			auto& symbols = scope->lookup.getSymbols();
			auto found = symbols.equal_range(last_id);
			for ( auto it = found.first; it != found.second; it++ ){
				resolveFromUnit(candidates, it->second, last_id);
			}
		}

		for (auto import : scope->import_packages) {
			if (!import->package) {
				continue;
			}
			CompilationUnit* defCU = import->package->findUnit(last_id);
			if ( defCU ) {
				resolveFromUnit(candidates, defCU, last_id);
			}
		}

		// look inside our own package
		// but only if we aren't in private scope
		if ( !scope->is_private ){
			CompilationUnit* defCU = cu->getPackage()->findUnit(last_id);
			if (defCU && defCU != cu) {
				resolveFromUnit(candidates, defCU, last_id);
			}
		}

		{
			// look inside the root package
			CUPackage* package = cc->getEnv()->getPackageRoot();
			// ... except when it's our own -- it was inspected in the previous step
			if ( package != cu->getPackage() ){
				CompilationUnit* defCU = package->findUnit(last_id);
				if (defCU && defCU != cu) {
					resolveFromUnit(candidates, defCU, last_id);
				}
			}
		}
	}else{
		// Try all other packages
		CompilationUnit* defCU = cc->resolveUnit(ids);
		if ( defCU ){
			resolveFromUnit(candidates, defCU, last_id);
		}
	}

	uz size = candidates.count();
	if ( size == 0 ){
		return nullptr;
	}else if ( size == 1 ){
		return candidates.getLast();
	}else{
		cc->error(
			span, 
			"resolver.ambiguous", 
			"ambiguous reference",
			candidates.get(tmp)
		);
	}

	return nullptr;
}


}
