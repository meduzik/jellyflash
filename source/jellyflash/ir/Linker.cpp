#include <jellyflash/ir/Linker.h>
#include <jellyflash/ir/Definition.h>
#include <jellyflash/ir/Class.h>
#include <jellyflash/ir/Interface.h>
#include <jellyflash/ir/Type.h>
#include <jellyflash/compiler/CompilerOps.h>
#include <jellylib/types/ConsList.h>

namespace jly{

Linker::Linker(){
}

void Linker::prepare(CompilerContext* cc, ir::File* IR, MemPool::Slice* mem, MemPool::Slice& tmp){
	this->cc = cc;
	this->IR = IR;
	this->mem = mem;
	this->tmp = &tmp;
	this->cu = cc->getUnit();
}

void Linker::setTempPool(MemPool::Slice& tmp) {
	this->tmp = &tmp;
}

void Linker::link(){
	linkScope(&IR->internal);
	linkScope(&IR->package);
}

void Linker::linkScope(ir::FileScope* scope){
	for ( auto def : scope->definitions ){
		match(def)
		caseof(ir::Class, class_){
			linkClass(class_);
		}
		caseof(ir::Interface, iface){
			linkInterface(iface);
		}
		endmatch
	}
}

void Linker::require(ir::Class* class_){
	getClassLinkInfo(class_);
}

void Linker::require(ir::Interface* iface){
	getInterfaceLinkInfo(iface);
}

void Linker::linkClass(ir::Class* class_){
	if ( class_->link ){
		return;
	}
	class_->link = mem->create<ir::LinkClass>();
	auto parent = class_->extends;
	if ( !parent && class_ != cc->getEnv()->getIntrinsics()->classes.Object){
		parent = cc->getEnv()->getIntrinsics()->classes.Object;
	}
	if (parent){
		parent->has_children = true;
		getClassLinkInfo(parent);
		if ( !class_->m_final && parent->m_final ){
			cc->error(class_->loc, "linker.inherit_final", "cannot inherit from a final class");
		}
		class_->all_bases = parent->all_bases;
		class_->all_bases.insert(parent);
		if (class_->uses_reflection) {
			for (auto base: class_->all_bases){
				base->uses_reflection = true;
			}
		}

		for ( auto& member : parent->instance_lookup ){
			if ( member.second->ns != &ir::NSPrivate ){
				class_->instance_lookup.insert(member);
			}
		}
	}

	linkFields(class_, class_->instance_fields, false);
	linkFields(class_, class_->static_fields, true);
	linkMethods(class_, class_->instance_methods, false);
	linkMethods(class_, class_->static_methods, true);
	if (!class_->constructor) {
		class_->constructor = mem->create<ir::ClassMethod>(
			class_,
			&ir::NSPublic,
			class_->loc,
			class_->name,
			nullptr
		);
		class_->constructor->signature = mem->create<ir::FunctionSignature>(
			&ir::TypePrim::Void
		);
	}

	for (auto impl : class_->implements) {
		getInterfaceLinkInfo(impl);
		if ( class_->all_implements_set.insert(impl).second ){
			class_->all_implements_list.push_back(impl);
		}
		for ( auto extends : impl->all_implements_list ){
			if ( class_->all_implements_set.insert(extends).second ){
				class_->all_implements_list.push_back(extends);
			}
		}
	}

	slice_vector< ir::InterfaceMethodImpl* > implement_methods(tmp);
	slice_umap< ir::InterfaceMethod*, ir::InterfaceMethodImpl* > implement_methods_map(mem);

	decltype(class_->all_implements_list) empty_implements(tmp);
	decltype(class_->all_implements_list)& base_implements = (
		(class_->extends) 
		? 
		class_->extends->all_implements_list
		: 
		empty_implements
	);

	for ( auto method : class_->instance_methods ){
		if ( method->base_method ){
			if ( !matchSignature(method->signature, method->base_method->signature) ){
				method->base_method = nullptr;
				method->root_method = method;
			}
		}
		if ( method->accessor == ir::Accessor::Get ){
			checkSignatureGetter(method, method->signature);
		}else if ( method->accessor == ir::Accessor::Set ){
			checkSignatureSetter(method, method->signature);
		}
	}


	for ( auto iface : class_->all_implements_list ){
		//if ( base_implements.count(iface) == 0 ){
		for ( auto imethod : iface->methods ){
			if ( implement_methods_map.count(imethod) ){
				continue;
			}
			ir::InterfaceMethodImpl* impl = nullptr;
			ir::ClassMethod* candidate = nullptr;
			auto class_member = resolveUnchecked(class_, class_->instance_lookup, imethod->loc, imethod->name, {ir::NSPublic.def});
			if ( class_member ){
				match(class_member)
				caseof(ir::ClassMethod, cmethod)
					if ( imethod->accessor == ir::Accessor::None ){
						candidate = cmethod;
					}
				caseof(ir::ClassProprety, property)
					if (imethod->accessor == ir::Accessor::Get) {
						candidate = property->getter;
					}else if (imethod->accessor == ir::Accessor::Set) {
						candidate = property->setter;
					}
				endmatch
				if ( candidate && candidate->accessor == imethod->accessor ){
					if ( !matchSignature(candidate->signature, imethod->signature) ){
						cc->error(
							candidate->loc,
							"link.bad_impl_signature",
							"method %1 is implemented with invalid signature",
							imethod
						);
					}
					impl = mem->create<ir::InterfaceMethodImpl>(class_, imethod, candidate);
					implement_methods.push_back(impl);
				}else{
					cc->error(
						class_->loc, 
						"link.bad_impl", 
						"class does not implement %1 from %2 (instead, got member %3)",
						imethod, 
						iface,
						class_member
					);
				}
			}else{
				cc->error(class_->loc, "link.not_impl", "class does not implement %1 from %2", imethod, iface);
			}
			implement_methods_map.insert({ imethod, impl });
		}
	}

	class_->implement_methods = ArrayRef(implement_methods).clone(*mem);
	class_->implement_methods_map = implement_methods_map;
	
	for ( auto method : class_->instance_methods ){
		class_->override_methods_map.insert({method->root_method, method});
	}

	// find native methods
	if ( class_->native && class_->native->invoke.getSize() > 0 ){
		auto class_member = resolveUnchecked(class_, class_->static_lookup, class_->loc, class_->native->invoke, { ir::NSPublic.def });
		if ( class_member ){
			match(class_member)
			caseof(ir::ClassMethod, method)
				if ( method->is_static ){
					class_->native->invoke_method = method;
				}
			endmatch
		}
		if ( !class_->native->invoke_method ){
			cc->error(class_->loc, "link.bad_invoke", "class does not have the declared invoke method");
		}
	}

	// add implemented interfaces from the base class
	// this needs to be done after the previous step
	// so we can compute the real implement_methods table
	if ( class_->extends ){
		for (auto impl : class_->extends->all_implements_list) {
			if ( class_->all_implements_set.insert(impl).second ){
				class_->all_implements_list.push_back(impl);
			}
		}
		for ( auto& pair : class_->extends->implement_methods_map ){
			class_->implement_methods_map.insert(pair);
		}
		for (auto& pair : class_->extends->override_methods_map ) {
			class_->override_methods_map.insert(pair);
		}
	}

	class_->link->processing = false;
}

bool Linker::matchSignature(ir::FunctionSignature* sign1, ir::FunctionSignature* sign2){
	if ( sign1->retTy != sign2->retTy ){
		return false;
	}
	if ( (!!sign1->rest) != (!!sign2->rest) ){
		return false;
	}
	if ( sign1->params.getSize() != sign2->params.getSize() ){
		return false;
	}
	uz n = sign1->params.getSize();
	for ( uz i = 0; i < n; i++ ){
		if ( sign1->params[i]->type != sign2->params[i]->type){
			return false;
		}
	}
	return true;
}

void Linker::checkSignatureGetter(ir::ClassMethod* method, ir::FunctionSignature* sign){
	if (sign->rest) {
		cc->error(sign->rest->loc, "link.getter_rest", "getter must not have rest arguments");
	}
	if (sign->params.getSize() != 0) {
		cc->error(method->loc, "link.getter_count", "getter must have zero arguments");
	}
	if (sign->retTy == &ir::TypePrim::Void) {
		cc->error(method->loc, "link.getter_return", "getter must not return void");
	}
}

void Linker::checkSignatureSetter(ir::ClassMethod* method, ir::FunctionSignature* sign){
	if ( sign->rest ){
		cc->error(sign->rest->loc, "link.setter_rest", "setter must not have rest arguments");
	}
	if ( sign->params.getSize() != 1 ){
		cc->error(method->loc, "link.setter_count", "setter must have exactly one argument");
	}
	if (sign->retTy != &ir::TypePrim::Void) {
		cc->error(method->loc, "link.setter_return", "setter must return void");
	}
}

bool isVisible(ir::NamespaceNode* ns1, ir::NamespaceNode* ns2){
	if ( ns1 == &ir::NSPrivate || ns2 == &ir::NSPrivate ){
		return false;
	}
	return true;
}

void Linker::linkFields(ir::Class* class_, ArrayRef<ir::ClassField*> fields, bool is_static){
	auto parent = class_->extends;
	auto& lookup = (is_static) ? class_->static_lookup : class_->instance_lookup;
	for (auto field : fields) {
		auto range = lookup.equal_range(field->name);
		for ( auto it = range.first; it != range.second; it++ ){
			if ( it->second->ns->def == field->ns->def ){
				cc->error(field->loc, "linker.field_conflicts", "conflicting field %1", field->name, it->second);
			}
		}
		lookup.insert({ field->name, field });
	}
}

void Linker::linkMethods(ir::Class* class_, ArrayRef<ir::ClassMethod*> methods, bool is_static){
	auto slice = MemPool::GetStackSlice();
	auto parent = class_->extends;
	auto& lookup = (is_static) ? class_->static_lookup : class_->instance_lookup;
	Cons<ir::ClassProprety*> list_props(&slice);

	for (auto method : methods) {
		auto name = method->name;

		ir::ClassMethod* base_method = nullptr;

		if ( method->accessor != ir::Accessor::None ){
			ir::ClassProprety* prop = nullptr;
			auto range = lookup.equal_range(method->name);
			for (auto it = range.first; it != range.second; it++) {
				auto other = it->second;
				if (other->ns->def == method->ns->def) {
					match(other)
					caseof(ir::ClassProprety, my_prop)
						if (my_prop->owner == class_) {
							prop = my_prop;
						} else {
							prop = mem->create<ir::ClassProprety>(class_, method->ns, method->loc, name);
							prop->base_property = my_prop;
							it->second = prop;
							list_props.push(prop);
							break;
						}
					casedef(_)
						cc->error(method->loc, "linker.name_conflicts", "conflicting instance member name %1", name, other);
					endmatch
				}
			}

			if (!prop) {
				prop = mem->create<ir::ClassProprety>(class_, method->ns, method->loc, name);
				list_props.push(prop);
				lookup.insert({ name, prop });
			}
			ir::ClassMethod** target;
			if (method->accessor == ir::Accessor::Get) {
				target = &prop->getter;
			} else {
				target = &prop->setter;
			}
			if (*target) {
				cc->error(method->loc, "linker.property_accessor_conflicts", "conflicting accessor for %1", name, *target);
			} else {
				*target = method;
			}
			if ( prop && prop->base_property ){
				if (method->accessor == ir::Accessor::Get){
					base_method = prop->base_property->getter;
				}else{
					base_method = prop->base_property->setter;
				}
			}
		}else{
			auto range = lookup.equal_range(method->name);
			for (auto it = range.first; it != range.second; it++) {
				auto other = it->second;
				if ( it->second->ns->def == method->ns->def ){
					match(other)
					caseof(ir::ClassMethod, found_method)
						if ( other->owner != class_ ){
							base_method = found_method;
							it->second = method;
						}else{
							cc->error(method->loc, "linker.method_conflicts", "method %1 conflicts with %2", name, other);
						}
					casedef(_)
						cc->error(method->loc, "linker.method_conflicts", "method %1 conflicts with %2", name, other);
					endmatch
				}
			}

			if ( !base_method ){
				lookup.insert({ method->name, method });
			}
		}

		if ( base_method ){
			if (base_method->is_final || base_method->owner->m_final){
				cc->error(method->loc, "linker.override_final", "cannot override final method", base_method);
			}else if ( !method->is_override ){
				cc->error(method->loc, "linker.override_missing", "method overriding other method must be marked with 'override'", base_method);
			}else{
				method->base_method = base_method;
				method->root_method = base_method->root_method;
				method->base_method->has_overrides = true;
			}
		}else{
			if ( method->is_override ){
				cc->error(method->loc, "linker.overide_no_base", "method marked with 'override' must override another method");
			}
		}
	}

	if ( is_static ){
		class_->static_properties = list_props.get(mem);
	}else{
		class_->instance_properties = list_props.get(mem);
	}
}

void Linker::linkMethods(ir::Interface* iface, ArrayRef<ir::InterfaceMethod*> methods) {
	auto slice = MemPool::GetStackSlice();
	auto& lookup = iface->lookup;
	Cons<ir::InterfaceProperty*> list_props(&slice);

	for (auto method : methods) {
		auto name = method->name;
		auto other = lookup.find(name);

		if (method->accessor != ir::Accessor::None) {
			ir::InterfaceProperty* prop = nullptr;
			if (other != lookup.end()) {
				match(other->second)
				caseof(ir::InterfaceProperty, my_prop)
					prop = my_prop;
				casedef(_)
					cc->error(method->loc, "linker.name_conflicts", "conflicting instance member name %1", name, other->second);
				endmatch
			}

			if (!prop) {
				prop = mem->create<ir::InterfaceProperty>(iface, method->loc, name);
				list_props.push(prop);
				lookup.insert({ name, prop });
			}

			ir::InterfaceMethod** target;
			if (method->accessor == ir::Accessor::Get) {
				target = &prop->getter;
			} else {
				target = &prop->setter;
			}
			if (*target) {
				cc->error(method->loc, "linker.property_accessor_conflicts", "conflicting accessor for %1", name, *target);
			} else {
				*target = method;
			}
		} else {
			if (other != lookup.end()) {
				cc->error(method->loc, "linker.name_conflicts", "conflicting instance member name %1", name, other->second);
			} else {
				lookup.insert({ method->name, method });
			}
		}
	}

	iface->properties = list_props.get(mem);
}

ir::ClassMember* Linker::resolveUnchecked(ir::Class* class_, slice_umultimap<StringRef, ir::ClassMember*>& lookup, const SourceLoc& loc, StringRef name, ArrayRef<ir::NamespaceDef*> namespaces) {
	auto slice = MemPool::GetStackSlice();
	Cons<ir::ClassMember*> candidates(&slice);
	auto range = lookup.equal_range(name);
	for ( auto it = range.first; it != range.second; it++ ){
		auto my_ns = it->second->ns->def;
		for ( auto ns : namespaces ){
			if ( ns == my_ns || ns->value == my_ns->value ){
				candidates.push(it->second);
				break;
			}
		}
	}
	if ( candidates.empty() ){
		return nullptr;
	}
	if ( candidates.count() > 1 ){
		auto list = candidates.get(&slice);
		Cons<ir::ClassMember*> own_candidates(&slice);
		for ( auto candidate : list ){
			if ( candidate->owner == class_ ){
				own_candidates.push(candidate);
			}
		}
		if ( own_candidates.empty() ){
			cc->error(loc, "link.ambiguous_reference", "Name %1 is ambiguous in this context", name, list);
		}else if ( own_candidates.count() > 1 ){
			cc->error(loc, "link.ambiguous_reference", "Name %1 is ambiguous in this context", name, own_candidates.get(&slice));
		}else{
			return own_candidates.getLast();
		}
	}
	return candidates.getLast();
}

ir::ClassMember* Linker::resolveStatic(ir::Class* class_, const SourceLoc& loc, StringRef name, ArrayRef<ir::NamespaceDef*> namespaces) {
	getClassLinkInfo(class_);
	return resolveUnchecked(class_, class_->static_lookup, loc, name, namespaces);
}

ir::ClassMember* Linker::resolveInstance(ir::Class* class_, const SourceLoc& loc, StringRef name, ArrayRef<ir::NamespaceDef*> namespaces) {
	getClassLinkInfo(class_);
	return resolveUnchecked(class_, class_->instance_lookup, loc, name, namespaces);
}

ir::InterfaceMember* Linker::resolveInstanceUnchecked(ir::Interface* iface, StringRef name) {
	auto member = iface->lookup.find(name);
	if (member == iface->lookup.end()) {
		return nullptr;
	} else {
		return member->second;
	}
}

ir::InterfaceMember* Linker::resolveInstance(ir::Interface* iface, StringRef name) {
	getInterfaceLinkInfo(iface);
	return resolveInstanceUnchecked(iface, name);
}

void Linker::linkInterface(ir::Interface* iface){
	if (iface->link) {
		return;
	}
	iface->link = mem->create<ir::LinkInterface>();
	for ( auto extends : iface->extends ){
		getInterfaceLinkInfo(extends);
		for ( auto child_extends : extends->all_implements_list ){
			if ( iface->all_implements_set.insert(child_extends).second ){
				iface->all_implements_list.push_back(child_extends);
			}
		}
		if ( iface->all_implements_set.insert(extends).second ){
			iface->all_implements_list.push_back(extends);
		}
	}

	linkMethods(iface, iface->methods);

	for (auto extends : iface->extends) {
		for (auto it : extends->lookup){
			auto own_member = iface->lookup.find(it.first);
			if ( own_member != iface->lookup.end() ){
				if ( own_member->second != it.second ){
					cc->error(it.second->loc, "link.overlapping_methods", "overlapping methods detected", it.second, own_member->second);
				}
			}else{
				iface->lookup.insert({it.first, it.second});
			}
		}
	}

	iface->link->processing = false;
}

ir::LinkInterface* Linker::getInterfaceLinkInfo(ir::Interface* iface) {
	if ( iface->link ){
		if ( iface->link->processing ){
			cc->fatal(iface->loc, "linker.dependency_loop", "dependency loop detected");
		}
		return iface->link;
	}
	Linker* linker = iface->scope->file->cu->getOps()->getLinker();
	linker->tmp = tmp;
	linker->linkInterface(iface);
	return iface->link;
}

ir::LinkClass* Linker::getClassLinkInfo(ir::Class* class_){
	if (class_->link) {
		if (class_->link->processing) {
			cc->fatal(class_->loc, "linker.dependency_loop", "dependency loop detected");
		}
		return class_->link;
	}
	Linker* linker = class_->scope->file->cu->getOps()->getLinker();
	linker->tmp = tmp;
	linker->linkClass(class_);
	return class_->link;
}

bool Linker::isPrivate(ir::ClassMember* member){
	return member->ns == &ir::NSPrivate;
}


}