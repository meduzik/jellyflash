#include <jellyflash/ir/Utils.h>

namespace jly{

std::string to_qual_id(ArrayRef<ast::ID> ids) {
	std::string s = "";
	for (auto id : ids) {
		if (!s.empty()) {
			s.push_back('.');
		}
		s.append((const char*)id.id.getData(), id.id.getSize());
	}
	return s;
}

}