#include <jellyflash/compiler/CompilerOps.h>
#include <jellyflash/compiler/CompilerEnv.h>
#include <jellyflash/codegen/Codegen.h>
#include <jellyflash/parser/Parser.h>
#include <jellyflash/parser/Lexer.h>
#include <jellyflash/ir/Builder.h>
#include <jellyflash/ir/Resolver.h>
#include <jellyflash/icode/ICodeCompiler.h>
#include <jellyflash/lto/LTO.h>

namespace jly{


namespace {
Lexer lexer;
Parser parser;
Builder ir_builder;
}

CompilerOps::CompilerOps(CompilerEnv* env, CompilationUnit* cu) :
	cc(env),
	cu(cu),
	state(CompilationUnit::Init) {
	cc.setUnit(cu);
}

void CompilerOps::require(CompilationUnit::State state){
	if ( this->state >= state ){
		return;
	}
	if ( executing ){
		cc.fatal("compiler.recursive_request", "internal compiler bug: recursive request detected");
	}
	executing = true;
	while ( this->state < state ){
		CompilationUnit::State old_state = this->state;
		CompilationUnit::State next_state = (CompilationUnit::State)(old_state + 1);
		switch ( next_state ){
		case CompilationUnit::Parsed: parse(); break;
		case CompilationUnit::Resolved: resolve(); break;
		case CompilationUnit::LinkerPrepared: prepareLink(); break;
		case CompilationUnit::Linked: link(); break;
		case CompilationUnit::CompilerPrepared: prepareCompiler(); break;
		case CompilationUnit::CodeCompiled: compileCode(); break;
		case CompilationUnit::CodegenPrepared: prepareCodegen(); break;
		case CompilationUnit::CodegenCompleted: runCodegen(); break;
		}
		this->state = next_state;
	}
	executing = false;
}

void CompilerOps::parse(){
	cu->loadSourceFile(cc);
	auto tokenStream = lexer.run(cc, cu->getSourceFile());
	ast::File* AST;
	{
		auto slice = MemPool::GetStackSlice();
		AST = parser.run(cc, cu->getSourceFile(), tokenStream, MemPool::GetPoolSlice(), slice);
	}
	{
		auto slice = MemPool::GetStackSlice();
		cu->setIR(ir_builder.run(&cc, AST, MemPool::GetPoolSlice(), slice));
	}
}

void CompilerOps::resolve() {
	Resolver resolver;
	auto slice = MemPool::GetStackSlice();
	resolver.prepare(&cc, cu->getIR(), MemPool::GetPoolSlice(), slice);
	resolver.run();
}

void CompilerOps::prepareLink(){
	auto slice = MemPool::GetStackSlice();
	linker.prepare(&cc, cu->getIR(), MemPool::GetPoolSlice(), slice);
}

void CompilerOps::link(){
	auto slice = MemPool::GetStackSlice();
	linker.setTempPool(slice);
	linker.link();
}

void CompilerOps::prepareCompiler(){
	auto slice = MemPool::GetStackSlice();
	compiler.prepare(&cc, MemPool::GetPoolSlice(), slice);
}

void CompilerOps::compileCode(){
	auto slice = MemPool::GetStackSlice();
	compiler.setTempPool(slice);
	compiler.run();
}

void CompilerOps::linkTimeOptimize(){
	auto slice = MemPool::GetStackSlice();
	lto::LTO lto;
	lto.run(&cc, MemPool::GetPoolSlice(), slice);
}

Linker* CompilerOps::getLinker(){
	require(CompilationUnit::State::LinkerPrepared);
	return &linker;
}

void CompilerOps::prepareCodegen(){
	cu->codegen = new CodegenCU();
	auto slice = MemPool::GetStackSlice();
	codegen.prepare(&cc, MemPool::GetPoolSlice(), slice);
}

void CompilerOps::runCodegen(){
	auto slice = MemPool::GetStackSlice();
	codegen.generate(MemPool::GetPoolSlice(), slice);
	codegen.generateNativeStubs(MemPool::GetPoolSlice(), slice);
}

}
