#include <jellyflash/compiler/CompilerEnv.h>
#include <jellyflash/parser/Parser.h>
#include <jellyflash/parser/Lexer.h>
#include <jellyflash/ir/Builder.h>
#include <jellyflash/compiler/CompilerOps.h>

namespace jly{

CompilerEnv::CompilerEnv() :
	package(this, nullptr, "") {
}

CompilerEnv::~CompilerEnv(){
}

void CompilerEnv::request(CompilationUnit* cu) {
	if (!cu->isRequested()) {
		cu->setRequested();
		std::unique_ptr<CompilerOps> op = std::make_unique<CompilerOps>(this, cu);
		cu->setOps(op.get());
		if ( !allow_loading ){
			CompilerContext cc(this);
			cc.fatal("env.late_binding", "compilation unit %1 requested too late", cu->getFullName());
		}
		this->ops.emplace_back(std::move(op));
	}
}

void CompilerEnv::request(CUPackage* package) {
	if ( !package->isRequested() ){
		package->setRequested();
		for ( auto cu : package->getUnits() ){
			request(cu);
		}
	}
}

template<class T>
T* CompilerEnv::resolve(CUPackage* package, StringRef name) {
	CompilerContext cc(this);
	CompilationUnit* cu = package->findUnit(name);
	if (!cu) {
		cc.fatal("intrin.missing_definition", "internal error: definition %1 not found", name);
	}
	cc.setUnit(cu);
	request(cu);
	cu->getOps()->require(CompilationUnit::State::Parsed);
	auto def = cu->getIR()->publicDef;
	if ((!def) || def->name != name) {
		cc.fatal("intrin.bad_definition", "internal error: definition %1 is invalid", name);
	}
	match(def)
	caseof(T, t)
		return t;
	casedef(_)
		cc.fatal("intrin.invalid_definition", "internal error: definition %1 is of invalid kind", name);
	endmatch
}

void CompilerEnv::makePrimitive(ir::Class* class_, ir::TypePrim* type){
	CompilerContext cc(this);
	if ( !class_->native ){
		cc.fatal("intrin.invalid_definition", "internal error: definition %1 is not native", class_);
	}
	class_->native->primitive = type;
	class_->type = type;
	class_->boxType = type;
	type->class_ = class_;
}

void CompilerEnv::referenceClass(StringRef name) {
	std::string path(name);
	auto package = getPackageRoot();
	CompilationUnit* cu = nullptr;
	size_t offset = 0;
	while (true) {
		size_t next_dot = path.find('.', offset);
		if (next_dot == std::string::npos) {
			cu = package->findUnit(path.substr(offset));
			break;
		} else {
			package = package->findChild(path.substr(offset, next_dot - offset));
			if (!package) {
				break;
			}
			offset = next_dot + 1;
		}
	}
	if (cu) {
		request(cu);
	}
}

void CompilerEnv::loadIntrinsics(){
	auto& classes = intrinsics.classes;
	classes.Boolean = resolve<ir::Class>(&package, "Boolean");
	classes.Int = resolve<ir::Class>(&package, "int");
	classes.UInt = resolve<ir::Class>(&package, "uint");
	classes.Number = resolve<ir::Class>(&package, "Number");
	classes.Array = resolve<ir::Class>(&package, "Array");
	classes.Object = resolve<ir::Class>(&package, "Object");
	classes.String = resolve<ir::Class>(&package, "String");
	classes.Function = resolve<ir::Class>(&package, "Function");
	classes.Class = resolve<ir::Class>(&package, "Class");
	classes.ByteArray = resolve<ir::Class>(package.getChild("flash")->getChild("utils"), "ByteArray");
	classes.Dictionary = resolve<ir::Class>(package.getChild("flash")->getChild("utils"), "Dictionary");
	classes.Vector = resolve<ir::Class>(&package, "Vector");
	classes.Vector->scope->file->cu->codegen_disabled = true;

	intrinsics.namespaces.AS3 = resolve<ir::NamespaceDef>(&package, "AS3");

	makePrimitive(classes.Boolean, &ir::TypePrim::Boolean);
	makePrimitive(classes.Int, &ir::TypePrim::Int);
	makePrimitive(classes.UInt, &ir::TypePrim::Uint);
	makePrimitive(classes.Number, &ir::TypePrim::Number);
	makePrimitive(classes.String, &ir::TypePrim::String);
	makePrimitive(classes.Function, &ir::TypePrim::Function);
	makePrimitive(classes.Class, &ir::TypePrim::Class);
	makePrimitive(classes.Object, &ir::TypePrim::Object);
	classes.Object->boxType = &classes.Object->classType;

	intrinsics.types.Array = &classes.Array->classType;
	intrinsics.types.ByteArray = &classes.Class->classType;
	intrinsics.types.Vector = &classes.Vector->classType;

	referenceClass("flash.media.Sound");
	referenceClass("flash.media.SoundMixer");
	referenceClass("flash.media.SoundChannel");
	referenceClass("flash.media.SoundTransform");
	referenceClass("parseFloat");
}


namespace {

void prepareDirectory(const std::filesystem::path& dir){
	if (!std::filesystem::is_directory(dir)) {
		std::filesystem::create_directories(dir);
	}
}

bool cleanupDirectory(CodegenClassPathInfo* info, const std::filesystem::path& dir){
	bool empty = true;
	for ( auto& entry : std::filesystem::directory_iterator(dir) ){
		if ( entry.is_regular_file() ){
			if ( !info->written_files.count(entry) ){
				try {
					std::filesystem::remove(entry);
				} catch (const std::filesystem::filesystem_error&) {
				}
			}else{
				empty = false;
			}
		}else if ( entry.is_directory() ){
			if ( cleanupDirectory(info, entry) ){
				try{
					std::filesystem::remove(entry);
				}catch(const std::filesystem::filesystem_error&){
				}
			}else{
				empty = false;
			}
		}else{
			empty = false;
		}
	}
	return empty;
}

}

void CompilerEnv::compile(CompilationUnit* cu){
	request(cu);
	cu->require(CompilationUnit::Linked);

	codegen();
}

void CompilerEnv::lto(){
	for (size_t i = 0; i < ops.size(); i++) {
		auto& op = ops[i];
		op->linkTimeOptimize();
	}
	allow_loading = false;
}

void CompilerEnv::codegen(){
	for ( auto cp : cps ){
		prepareDirectory(cp->include_path);
		prepareDirectory(cp->source_path);
		if (!cp->native_stubs_path.empty()) {
			prepareDirectory(cp->native_stubs_path);
		}
	}

	for (size_t i = 0; i < ops.size(); i++) {
		auto& op = ops[i];
		op->require(CompilationUnit::CodeCompiled);
	}

	lto();

	for (size_t i = 0; i < ops.size(); i++) {
		auto& op = ops[i];
		op->require(CompilationUnit::CodegenPrepared);
	}

	for (size_t i = 0; i < ops.size(); i++) {
		auto& op = ops[i];
		op->require(CompilationUnit::CodegenCompleted);
	}

	for (auto cp : cps) {
		cleanupDirectory(cp, cp->include_path);
		cleanupDirectory(cp, cp->source_path);
		if ( !cp->native_stubs_path.empty() ){
			cleanupDirectory(cp, cp->native_stubs_path);
		}
	}
}

ir::TypeVector* CompilerEnv::getVectorType(ir::Type* type){
	auto& VectorInstances = intrinsics.types.VectorInstances;
	auto it = VectorInstances.find(type);
	if ( it == VectorInstances.end() ){
		auto instance = MemPool::GetPoolSlice()->create<ir::TypeVector>(type);
		VectorInstances.insert({type, instance});
		return instance;
	}else{
		return it->second;
	}
}

void CompilerEnv::registerClassPath(CodegenClassPathInfo* cp){
	cps.insert(cp);
}


}