#include <jellyflash/compiler/CompilationUnit.h>
#include <jellyflash/compiler/CompilerContext.h>
#include <jellyflash/compiler/CompilerEnv.h>
#include <jellyflash/compiler/CompilerOps.h>
#include <jellyflash/ir/Lookup.h>

namespace jly{

CompilationUnit::~CompilationUnit() = default;

void CompilationUnit::loadSourceFile(CompilerContext& cc){
	try{
		file = std::make_unique<SourceFile>(SourceFile::ReadFile(path));
	}catch(std::filesystem::filesystem_error& err){
		cc.fatal("load.bad_file", "%1", StringRef(err.what()));
	}
}

CUPackage* CUPackage::findChild(const StringRef& name){
	auto it = children.find(name);
	if (it == children.end()) {
		return nullptr;
	}
	return &it->second;
}

CUPackage* CUPackage::getChild(const StringRef& name){
	auto it = children.find(name);
	if (it == children.end()) {
		auto it = children.emplace(
			std::piecewise_construct, 
			std::make_tuple(name.toString()),
			std::make_tuple(env, this, name.toString())
		);
		return &it.first->second;
	}
	return &it->second;
}

CompilationUnit* CUPackage::findUnit(const StringRef& name){
	auto it = units.find(name);
	if ( it == units.end() ){
		return nullptr;
	}
	return &it->second;
}

CompilationUnit* CUPackage::createUnit(const StringRef& name, CodegenClassPathInfo* codegen){
	auto it = units.find(name);
	if (it == units.end()) {
		auto it = units.emplace(
			std::piecewise_construct,
			std::make_tuple(name.toString()),
			std::make_tuple(env, this, name.toString(), codegen)
		);
		units_vector.push_back(&it.first->second);
		return &it.first->second;
	}
	return &it->second;
}

ArrayRef<CompilationUnit*> CUPackage::getUnits() const{
	return units_vector;
}

void CompilationUnit::setPath(std::filesystem::path path){
	this->path = std::move(path);
}

const std::string& CompilationUnit::getFullName(){
	if ( !fullname.empty() ){
		return fullname;
	}

	if ( package->getParent() ){
		fullname = package->getFullName() + '.';
	}
	fullname += name;
	return fullname;
}

const std::string& CUPackage::getFullName(){
	if ( !has_fullname ){
		if ( !parent ){
		}else if ( !parent->parent ){
			fullname = name;
		}else{
			fullname = parent->getFullName() + "." + name;
		}
		has_fullname = true;
	}
	return fullname;
}

void CompilationUnit::require(State state){
	if ( !isRequested() ){
		env->request(this);
	}
	ops->require(state);
}

}
