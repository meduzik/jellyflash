#include <jellyflash/compiler/FileLoader.h>
#include <jellyflash/compiler/CompilerEnv.h>

namespace jly{

FileLoader::FileLoader(CompilerContext* ctx):
	ctx(ctx){
}

void FileLoader::loadClassPath(const std::filesystem::path& path, CodegenClassPathInfo* classPath){
	ctx->getEnv()->registerClassPath(classPath);
	loadPackage(ctx->getEnv()->getPackageRoot(), path, classPath);
}

void FileLoader::loadPackage(CUPackage* package, const std::filesystem::path& path, CodegenClassPathInfo* classPath){
	if ( !std::filesystem::is_directory(path) ){
		ctx->fatal("load.invalid_class_path", "class path %1 is not a directory", path);
	}

	for ( auto file : std::filesystem::directory_iterator(path) ){
		auto& file_path = file.path();
		if ( file.is_directory() ){
			loadPackage(package->getChild(file_path.filename().u8string()), file_path, classPath);
		}else if ( file.is_regular_file() ){
			if ( file_path.extension() == ".as" ){
				CompilationUnit* cu = package->createUnit(file_path.stem().u8string(), classPath);
				if ( !cu->getPath().empty() ){
					ctx->error(
						"load.conflicting_files",
						"conflicting files:\n%1\n%2",
						cu->getPath(),
						file_path.u8string()
					);
				}
				cu->setPath(std::filesystem::absolute(file_path));
			}
		}
	}
}

}