#include <jellyflash/compiler/CompilerContext.h>
#include <jellyflash/compiler/CompilationUnit.h>
#include <jellyflash/compiler/CompilerEnv.h>
#include <jellylib/utils/Writer.h>
#include <jellylib/memory/MemPool.h>
//#define NOLOG

namespace jly {

void CompilerContext::reportInternal(DiagnosticLevel level, const SourceLoc* loc, StringRef code, StringRef message, ArrayRef<DiagnosticArgument> args){
#if defined(NOLOG)
	return;
#endif
	OStreamSink sink(&std::cerr);
	FormattedWriter writer(&sink);

	if ( loc ){
		auto span = loc->getSpan();
		auto begin = loc->getFile().getLineMap().getLocByOffset(span.getBegin());
		DiagnosticArgument(*loc).write(writer);
	}else if ( cu ){
		writer.write(cu->getPath().u8string());
	}
	writer.write(": ");
	switch ( level ){
	case DiagnosticLevel::Error:
	case DiagnosticLevel::Fatal:{
		writer.write("error ");
	}break;
	case DiagnosticLevel::Warning: {
		writer.write("warning ");
	}break;
	default:{
		jl_unreachable;
	}break;
	}
	writer.write(code);
	writer.write(": ");
	{
		auto indent = writer.indented();
		uz begin = 0;
		uz i = 0;
		for ( ; i < message.getSize(); i++ ){
			u8 ch = message[i];
			if ( ch == '%' ){
				writer.write({ message.begin() + begin, message.begin() + i});
				i++;
				begin = i;
				if ( i < message.getSize() ){
					if ( isdigit(message[i]) ){
						u8 idx = message[i] - '1';
						if ( args.getSize() >= idx ){
							args[idx].write(writer);
							begin = i + 1;
						}else{
							writer.write('?');
							writer.write(message[i]);
							begin = i + 1;
						}
					}
				}
			}else if ( ch == '\n' ){
				writer.writeln({ message.begin() + begin, message.begin() + i });
				begin = i + 1;
			}
		}
		writer.write({ message.begin() + begin, message.begin() + i });
		writer.writeln();
		for ( auto arg : args ){
			if ( arg.writeExtra(writer) ){
				writer.writeln();
			}
		}
		if (loc && cu && cu->getSourceFile() != &loc->getFile()) {
			writer.write("from ");
			writer.write(cu->getPath().u8string());
			writer.writeln();
		}
	}
}

SourceLoc CompilerContext::getLoc(srcspan_t span) {
	if ( cu ){
		return { cu->getSourceFile(), span };
	}else{
		return { nullptr, span };
	}
}

CUPackage* CompilerContext::resolvePackage(ArrayRef<ast::ID> ids){
	auto package = env->getPackageRoot();
	for (auto& id : ids) {
		package = package->findChild(id.id);
		if (!package) {
			return nullptr;
		}
	}
	return package;
}

CompilationUnit* CompilerContext::resolveUnit(ArrayRef<ast::ID> ids){
	CUPackage* package = resolvePackage(ids.slice(0, ids.getSize() - 1));
	CompilationUnit* cu = package->findUnit(ids[ids.getSize() - 1].id);
	return cu;
}

void CompilerContext::checkPermissions(ir::Definition* def){
	
}

}
