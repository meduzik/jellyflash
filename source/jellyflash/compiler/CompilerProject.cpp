#include <jellyflash/compiler/CompilerProject.h>
#include <jellyflash/compiler/FileLoader.h>
#include <jellyflash/compiler/CompilerEnv.h>

#undef match
#include <picojson/picojson.h>


namespace jly{

CompilerProject::CompilerProject(){
}

void CompilerProject::setStdlibPath(std::string_view path){
	this->stdlib_path = path;
}

void CompilerProject::setOutputPath(std::string_view path){
	this->output_path = path;
}

void CompilerProject::setStubsPath(std::string_view path) {
	this->stubs_path = path;
}

void CompilerProject::setMain(std::string_view id) {
	this->main = id;
}


void CompilerProject::loadConfig(std::string_view path){
	this->configs.push_back(std::string(path));
}

void CompilerProject::build(){
	std::filesystem::path out_dir(output_path);
	std::filesystem::path stubs_dir(stubs_path);

	auto slice = MemPool::GetStackSlice();

	CompilerEnv env;
	CompilerContext cc(&env);
	FileLoader loader(&cc);

	auto processConfig = [&](std::string_view path){
		std::filesystem::path config_file(path);
		std::filesystem::path config_dir = config_file.parent_path();

		picojson::value config_data;

		{
			std::ifstream file_in(config_file, std::ios::binary);
			if (!file_in) {
				cc.fatal("project.open_config", "Cannot open config: %1", path);
			}
			std::string err = picojson::parse(config_data, file_in);
			if (!err.empty()) {
				cc.fatal("project.parse_config", "Cannot parse config %1: %2", path, err);
			}
		}

		picojson::object& config_object = config_data.get<picojson::object>();

		auto& groups = config_object.at("groups").get<picojson::array>();
		for (auto& group: groups) {
			auto& group_data = group.get<picojson::object>();
			auto& group_name = group_data.at("name").get<std::string>();

			std::filesystem::path group_out_dir = out_dir / group_name;

			CodegenClassPathInfo* class_path = slice.create<CodegenClassPathInfo>();
			class_path->source_path = group_out_dir / "source";
			class_path->include_path = group_out_dir / "include";
			if (!stubs_path.empty()) {
				class_path->native_stubs_path = stubs_dir / group_name;
			}

			auto& source_paths = group_data.at("source-paths").get<picojson::array>();
			for (auto& source_path: source_paths) {
				loader.loadClassPath(config_dir / source_path.get<std::string>(), class_path);
			}
		}
	};

	processConfig(stdlib_path + "/config.json");
	env.setInliningAllowed(false);
	env.loadIntrinsics();

	for (auto& config: configs) {
		processConfig(config);
	}

	auto package = env.getPackageRoot();
	CompilationUnit* cu = nullptr;
	size_t offset = 0;
	while (true) {
		size_t next_dot = main.find('.', offset);
		if (next_dot == std::string::npos) {
			cu = package->findUnit(main.substr(offset));
			break;
		} else {
			package = package->findChild(main.substr(offset, next_dot - offset));
			if (!package) {
				break;
			}
			offset = next_dot + 1;
		}
	}
	if (!cu) {
		cc.fatal("project.missing_main", "Cannot find main class %1", main);
	}
	env.compile(cu);
}


}

