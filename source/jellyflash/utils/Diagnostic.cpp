#include <jellyflash/utils/Diagnostic.h>
#include <jellyflash/parser/AST.h>
#include <jellyflash/ir/Definition.h>
#include <jellyflash/ir/Class.h>
#include <jellyflash/ir/Interface.h>
#include <jellyflash/icode/ICodeCompiler.h>

namespace jly::diag{

namespace {

void write_def_fullname(FormattedWriter& writer, ir::Definition* def){
	if (def->scope->is_private) {
		writer.write(def->scope->file->cu->getFullName());
		writer.write(".$private::");
	}else{
		writer.write(def->scope->file->cu->getPackage()->getFullName());
		if ( def->scope->file->cu->getPackage()->getParent() ){
			writer.write(".");
		}
	}
	writer.write(def->name);
}


const u8 hex_digit[] = "0123456789ABCDEF";

}

void write(FormattedWriter& writer, double value){
	writer.write(decimal(value));
}

void write(FormattedWriter& writer, StringRef str){
	writer.write('\'');
	for ( auto ch : str ){
		if ( ch < 128 && isprint(ch) && ch != '\'' && ch != '\\' ){
			writer.write(ch);
		}else{
			writer.write('\\');
			switch ( ch ){
			case '\\':
			case '\"':{
				writer.write(ch);
			}break;
			case '\n': {
				writer.write('n');
			}break;
			case '\r': {
				writer.write('r');
			}break;
			case '\t': {
				writer.write('t');
			}break;
			case '\0': {
				writer.write('0');
			}break;
			default: {
				writer.write('x');
				writer.write(hex_digit[ch / 16 % 16]);
				writer.write(hex_digit[ch % 16]);
			}break;
			}
		}
	}
	writer.write('\'');
}

decimal::decimal(double val) {
	sprintf(buffer, "%la", val);
}

void write(FormattedWriter& writer, ir::Type* type){
	match(type)
	caseof(ir::TypeClass, class_)
		writer.write("class ");
		write(writer, class_->class_);
	caseof(ir::TypeInterface, iface)
		writer.write("interface ");
		write(writer, iface->iface);
	caseof(ir::TypeVector, vec)
		writer.write("Vector.<");
		write(writer, vec->eltTy);
		writer.write(">");
	caseof(ir::TypePrim, prim)
		switch ( prim->kind ){
		case ir::TypePrim::Kind::Any:{
			writer.write("*");
		}break;
		case ir::TypePrim::Kind::Boolean: {
			writer.write("Boolean");
		}break;
		case ir::TypePrim::Kind::Int: {
			writer.write("int");
		}break;
		case ir::TypePrim::Kind::Null: {
			writer.write("null");
		}break;
		case ir::TypePrim::Kind::Number: {
			writer.write("Number");
		}break;
		case ir::TypePrim::Kind::Uint: {
			writer.write("uint");
		}break;
		case ir::TypePrim::Kind::Void: {
			writer.write("void");
		}break;
		case ir::TypePrim::Kind::Class: {
			writer.write("Class");
		}break;
		case ir::TypePrim::Kind::String: {
			writer.write("String");
		}break;
		case ir::TypePrim::Kind::Function: {
			writer.write("Function");
		}break;
		case ir::TypePrim::Kind::Object: {
			writer.write("Object");
		}break;
		case ir::TypePrim::Kind::Rest: {
			writer.write("Rest...");
		}break;
		default:{
			jl_unreachable;
		}break;
		}
	caseof(ir::TypeVar, var)
		writer.write("$$");
	casedef(_)
		jl_unreachable;
	endmatch
}

bool write_extra(FormattedWriter& writer, ir::Type* type){
	match(type)
	caseof(ir::TypeClass, class_)
		return write_extra(writer, class_->class_);
	caseof(ir::TypeInterface, iface)
		return write_extra(writer, iface->iface);
	caseof(ir::TypeVector, vec)
		return write_extra(writer, vec->eltTy);
	caseof(ir::TypePrim, prim)
		return false;
	caseof(ir::TypeVar, var)
		return false;
	casedef(_)
		jl_unreachable;
	endmatch
}

void write(FormattedWriter& writer, const SourceLoc& loc){
	auto& file = loc.getFile();
	auto span = loc.getSpan();
	writer.write(file.getPath());
	writer.write('(');
	auto resolved_loc = file.getLineMap().getLocByOffset(span.getBegin());
	writer.write(decimal(resolved_loc.line + 1));
	writer.write(',');
	writer.write(decimal(resolved_loc.column + 1));
	writer.write(')');
}

void write(FormattedWriter& writer, const std::string& str){
	write(writer, StringRef(str));
}

void write(FormattedWriter& writer, const std::filesystem::path& path){
	write(writer, path.u8string());
}

void write(FormattedWriter& writer, ir::Definition* def){
	write_def_fullname(writer, def);
}

void write(FormattedWriter& writer, ir::ClassMember* member){
	write(writer, member->owner);
	writer.write('.');
	writer.write(member->name);
}

void write(FormattedWriter& writer, ir::InterfaceMember* member){
	write(writer, member->owner);
	writer.write('.');
	writer.write(member->name);
}

bool write_extra(FormattedWriter& writer, ir::ClassMember* member) {
	write(writer, member->loc);
	writer.write(": ");
	match(member)
	caseof(ir::ClassMethod, method)
		if ( method->is_static ){
			if (method->accessor == ir::Accessor::Get) {
				writer.write("static getter ");
			} else if (method->accessor == ir::Accessor::Set) {
				writer.write("static setter ");
			} else {
				writer.write("static method ");
			}
		}else{
			if (method->accessor == ir::Accessor::Get) {
				writer.write("class getter ");
			} else if (method->accessor == ir::Accessor::Set) {
				writer.write("class setter ");
			} else {
				writer.write("class method ");
			}
		}
	caseof(ir::ClassProprety, prop)
		if (prop->is_static) {
			writer.write("static property ");
		}else{
			writer.write("class property ");
		}
	caseof(ir::ClassField, field)
		if ( field->is_static ){
			writer.write("static field ");
		}else{
			writer.write("class field ");
		}
	casedef(_)
		jl_unreachable;
	endmatch
	write(writer, member);
	return true;
}

bool write_extra(FormattedWriter& writer, ir::InterfaceMember* member) {
	write(writer, member->loc);
	writer.write(": ");
	match(member)
	caseof(ir::InterfaceMethod, method)
		if ( method->accessor == ir::Accessor::Get ){
			writer.write("interface getter ");
		}else if (method->accessor == ir::Accessor::Set) {
			writer.write("interface setter ");
		}else{
			writer.write("interface method ");
		}
	caseof(ir::InterfaceProperty, prop)
		writer.write("interface property ");
	casedef(_)
		jl_unreachable;
	endmatch
	write(writer, member);
	return true;
}

bool write_extra(FormattedWriter& writer, icode::Reference& ref) {
	switch (ref.tag) {
	case icode::Reference::Tag::Value: {
	}break;
	case icode::Reference::Tag::ClassMember: {
		return write_extra(writer, ref.class_member.member);
	}break;
	case icode::Reference::Tag::Closure: {
	}break;
	case icode::Reference::Tag::Definition: {
		return write_extra(writer, ref.definition);
	}break;
	case icode::Reference::Tag::InterfaceMember: {
		return write_extra(writer, ref.interface_member.member);
	}break;
	case icode::Reference::Tag::LocalVar: {
		write(writer, ref.local_var->loc);
		writer.write(": local variable ");
		write(writer, ref.local_var->name);
		return true;
	}break;
	case icode::Reference::Tag::None: {
	}break;
	case icode::Reference::Tag::Package: {
	}break;
	case icode::Reference::Tag::Upvalue: {
		write(writer, ref.upvalue->var->loc);
		writer.write(": upvalue ");
		write(writer, ref.upvalue->var->name);
		return true;
	}break;
	default: {
		jl_unreachable;
	}break;
	}
	return false;
}

void write(FormattedWriter& writer, icode::Reference& ref){
	switch ( ref.tag ){
	case icode::Reference::Tag::Value:{
		writer.write("value of type ");
		write(writer, ref.value->type);
	}break;
	case icode::Reference::Tag::ClassMember:{
		write(writer, ref.class_member.member);
	}break;
	case icode::Reference::Tag::Closure: {
		writer.write("function closure");
	}break;
	case icode::Reference::Tag::Definition: {
		write(writer, ref.definition);
	}break;
	case icode::Reference::Tag::InterfaceMember: {
		write(writer, ref.interface_member.member);
	}break;
	case icode::Reference::Tag::LocalVar: {
		writer.write("local variable ");
		write(writer, ref.local_var->name);
	}break;
	case icode::Reference::Tag::None: {
		writer.write("none-value");
	}break;
	case icode::Reference::Tag::Package: {
		writer.write("package");
	}break;
	case icode::Reference::Tag::Upvalue: {
		writer.write("upvalue ");
		write(writer, ref.local_var->name);
	}break;
	default:{
		jl_unreachable;
	}break;
	}
}

void write(FormattedWriter& writer, ArrayRef<ast::ID> ids){
	if ( ids.getSize() == 0 ){
		writer.write("''");
		return;
	}

	bool is_first = true;
	for ( auto& id : ids ){
		if ( is_first ){
			is_first = false;
		}else{
			writer.write('.');
		}
		writer.write(id.id);
	}
}

void write(FormattedWriter& writer, ArrayRef<ir::Definition*> defs){
	for ( auto def : defs ){
		write(writer, def);
		writer.writeln();
	}
}

void write(FormattedWriter& writer, ArrayRef<ir::ClassMember*> members) {
	bool first = true;
	for (auto member : members) {
		if ( first ){
			first = false;
		}else{
			writer.write(", ");
		}
		write(writer, member);
	}
}

bool write_extra(FormattedWriter& writer, ArrayRef<ir::ClassMember*> members){
	bool first = true;
	for (auto member : members) {
		if (first) {
			first = false;
		} else {
			writer.writeln();
		}
		write_extra(writer, member);
	}
	return members.getSize() > 0;
}

bool write_extra(FormattedWriter& writer, ir::Definition* def){
	write(writer, def->loc);
	writer.write(": ");
	switch ( def->tag ){
	case ir::Definition::Tag::Class:{
		writer.write("class ");
	}break;
	case ir::Definition::Tag::Function: {
		writer.write("global function ");
	}break;
	case ir::Definition::Tag::Interface: {
		writer.write("interface ");
	}break;
	case ir::Definition::Tag::Namespace: {
		writer.write("namespace ");
	}break;
	case ir::Definition::Tag::Var: {
		writer.write("global variable ");
	}break;
	default:{
		jl_unreachable;
	}break;
	}
	write(writer, def);
	return true;
}


}
