#pragma once

#include <jellyflash/icode/ICodeCompiler.h>

namespace jly::icode{

template<class It>
auto ICodeCompiler::clone(It&& begin, It&& end)->ArrayRef< std::remove_const_t<std::remove_reference_t<decltype(*std::declval<It>())>> > {
	using T = std::remove_const_t < std::remove_reference_t<decltype(*std::declval<It>())> >;
	uz count = end - begin;
	auto buf = mem->createArray<T>(count);
	std::copy(begin, end, buf);
	return {buf, count};
}

template<class T>
ArrayRef<std::remove_const_t<T>> ICodeCompiler::to_array(const std::initializer_list<T>& values){
	return clone(values.begin(), values.end());
}

struct ICodeCompiler::TemporaryNSScope {
	TemporaryNSScope(
		slice_vector<ir::NamespaceDef*>& open_namespaces,
		uz reset_to
	) :
		open_namespaces(open_namespaces),
		reset_to(reset_to) {
	}

	~TemporaryNSScope() {
		if (!std::uncaught_exceptions()) {
			open_namespaces.resize(reset_to);
		}
	}

	slice_vector<ir::NamespaceDef*>& open_namespaces;
	uz reset_to;
};

}