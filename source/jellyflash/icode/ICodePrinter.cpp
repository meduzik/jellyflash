#include <jellyflash/icode/ICodePrinter.h>
#include <jellyflash/utils/Diagnostic.h>
#include <jellylib/utils/StringDecode.h>

namespace jly::icode{

Printer::Printer(){
}

void Printer::prepare(FormattedWriter* writer, MemPool::Slice& tmp){
	this->tmp = &tmp;
	this->writer = writer;
}

void Printer::prepareMainFrame(FuncFrame* frame){
	main_ctx = tmp->create<PrinterMainContext>(tmp);
}

void Printer::prepareFrame(FuncFrame* frame){
	ctx = tmp->create<PrinterFrameContext>(tmp);
	for (auto block : frame->blocks) {
		getBlockIndex(block);
	}
	for (auto handler : frame->handlers) {
		getHandlerIndex(handler);
	}
}

void Printer::writeCode(FuncFrame* frame){
	prepareMainFrame(frame);

	for (auto closure : frame->closures) {
		visitClosure(closure);
	}

	{
		writer->writeln("code:");
		auto indent = writer->indented();
		writeFrame(frame);
	}
}

void Printer::visitClosure(Closure* closure) {
	getClosureIndex(closure);
	for (auto closure : closure->frame->closures) {
		visitClosure(closure);
	}
}

void Printer::writeFrame(FuncFrame* frame) {
	prepareFrame(frame);

	if ( frame->locals.getSize() > 0 ){
		writer->writeln("locals:");
		{
			auto indenter = writer->indented();
			for ( auto local : frame->locals ){
				writer->write(local->name);
				writer->write(" : ");
				writeType(local->type);
				writer->writeln();
			}
		}
	}
	if ( frame->upvals.getSize() > 0 ){
		writer->writeln("upvals:");
		{
			auto indenter = writer->indented();
			for (auto upval : frame->upvals) {
				writer->write(upval->var->name);
				writer->write(" : ");
				writeType(upval->type);
				writer->writeln();
			}
		}
	}

	writer->writeln("code:");
	{
		auto indenter = writer->indented();
		for ( auto block : frame->blocks ){
			writeBlock(block);
		}
	}
	if ( frame->handlers.getSize() ){
		writer->writeln("handlers:");
		{
			auto indenter = writer->indented();
			for ( auto handler : frame->handlers ) {
				writeHandler(handler);
			}
		}
	}
}

uz Printer::getBlockIndex(BasicBlock* bb){
	jl_assert(bb);
	auto it = ctx->blocks.find(bb);
	if ( it == ctx->blocks.end() ){
		ctx->blocks_list.push_back(bb);
		auto idx = ctx->blocks.size();
		ctx->blocks.insert({bb, idx});
		return idx;
	}
	return it->second;
}

uz Printer::getHandlerIndex(HandlerScope* handler){
	auto it = ctx->handlers.find(handler);
	if (it == ctx->handlers.end()) {
		ctx->handlers_list.push_back(handler);
		auto idx = ctx->handlers.size();
		ctx->handlers.insert({ handler, idx });
		return idx;
	}
	return it->second;
}

uz Printer::getInstrIndex(Instr* instr){
	auto it = ctx->values.find(instr);
	if (it == ctx->values.end()) {
		auto idx = ctx->values.size();
		ctx->values.insert({ instr, idx });
		return idx;
	}
	return it->second;
}

uz Printer::getClosureIndex(Closure* closure) {
	auto it = main_ctx->closures.find(closure);
	if (it == main_ctx->closures.end()) {
		main_ctx->closures_list.push_back(closure);
		auto idx = main_ctx->closures.size();
		main_ctx->closures.insert({ closure, idx });
		return idx;
	}
	return it->second;
}

void Printer::writeBlockHeader(BasicBlock* bb) {
	writer->write("block_");
	writer->write(diag::decimal(getBlockIndex(bb)));
	if (bb->handler) {
		writer->write(" [handler_");
		writer->write(diag::decimal(getHandlerIndex(bb->handler)));
		writer->write("]");
	}
	writer->writeln(":");
}


void Printer::writeBlock(BasicBlock* bb){
	writer->write("block_");
	writer->write(diag::decimal(getBlockIndex(bb)));
	if ( bb->handler ){
		writer->write(" [handler_");
		writer->write(diag::decimal(getHandlerIndex(bb->handler)));
		writer->write("]");
	}
	writer->writeln(":");
	{
		auto indent = writer->indented();
		for ( auto instr : bb->instrs ){
			writeInstr(instr);
			writer->writeln();
		}
	}
}

void Printer::writeHandler(HandlerScope* handler) {
	writer->write("handler_");
	writer->write(diag::decimal(getHandlerIndex(handler)));
	if (handler->parent) {
		writer->write(" [handler_");
		writer->write(diag::decimal(getHandlerIndex(handler->parent)));
		writer->write("]");
	}
	writer->writeln(":");
	{
		auto indent = writer->indented();

		writer->write("default: ");
		if ( handler->catch_all ){
			writeUse(handler->catch_all);
		}else{
			writer->write("null");
		}
		writer->writeln();

		for (auto& filter : handler->filters) {
			writeType(filter.first);
			writer->write(" -> ");
			writeUse(filter.second);
			writer->writeln();
		}
	}
}


namespace{

void write_op(FormattedWriter* writer, CmpOp op){
	switch (op) {
	case CmpOp::EQ: {
		writer->write("eq");
	}break;
	case CmpOp::NEQ: {
		writer->write("neq");
	}break;
	case CmpOp::SEQ: {
		writer->write("seq");
	}break;
	case CmpOp::SNEQ: {
		writer->write("sneq");
	}break;
	case CmpOp::GT: {
		writer->write("gt");
	}break;
	case CmpOp::GE: {
		writer->write("ge");
	}break;
	case CmpOp::LT: {
		writer->write("lt");
	}break;
	case CmpOp::LE: {
		writer->write("le");
	}break;
	default: {
		jl_unreachable;
	}break;
	}
}

}

void Printer::writeInstr(Instr* instr){
	if (instr->tag == Instr::Tag::IteratorNew || instr->result_value.type) {
		writer->write("r");
		writer->write(diag::decimal(getInstrIndex(instr)));
		writer->write(" = ");
	}
	match(instr)
	caseof(InstrBr, br)
		writer->write("br ");
		writeUse(br->bb);
	caseof(InstrCallClassMethod, call)
		writer->write("call_cmethod ");
		writeDef(call->method);
		if ( call->thisval ){
			writer->write(' ');
			writeUse(call->thisval);
		}else{
			writer->write(" static");
		}
		for ( auto arg : call->args ){
			writer->write(' ');
			writeUse(arg);
		}
	caseof(InstrCallClosure, call){
		writer->write("call_closure ");
		writer->write(diag::decimal(getClosureIndex(call->closure)));
		for (auto arg : call->args) {
			writer->write(' ');
			writeUse(arg);
		}
	}
	caseof(InstrCallVectorMethod, call)
		writer->write("call_vecmethod ");
		writeDef(call->method);
		if (call->thisval) {
			writer->write(' ');
			writeUse(call->thisval);
		} else {
			writer->write(" static");
		}
		for (auto arg : call->args) {
			writer->write(' ');
			writeUse(arg);
		}
	caseof(InstrCallInterfaceMethod, call)
		writer->write("call_imethod ");
		writeDef(call->method);
		writer->write(' ');
		writeUse(call->thisval);
		for (auto arg : call->args) {
			writer->write(' ');
			writeUse(arg);
		}
	caseof(InstrCallDynamic, call)
		writer->write("call_dynamic ");
		writeUse(call->func);
		for (auto arg : call->args) {
			writer->write(' ');
			writeUse(arg);
		}
	caseof(InstrCallGlobal, call)
		writer->write("call_global ");
		writeDef(call->func);
		for (auto arg : call->args) {
			writer->write(' ');
			writeUse(arg);
		}
	caseof(InstrNewClass, new_)
		writer->write("newclass ");
		writeDef(new_->class_);
		for (auto arg : new_->args) {
			writer->write(' ');
			writeUse(arg);
		}
	caseof(InstrNewVector, new_)
		writer->write("newvector ");
		writeType(new_->vec);
		for (auto arg : new_->args) {
			writer->write(' ');
			writeUse(arg);
		}
	caseof(InstrNewDynamic, new_)
		writer->write("newdynamic ");
		writeUse(new_->class_);
		for (auto arg : new_->args) {
			writer->write(' ');
			writeUse(arg);
		}
	caseof(InstrClassClosure, closure)
		writer->write("classclosure ");
		writeDef(closure->class_);
	caseof(InstrInterfaceClosure, closure)
		writer->write("ifaceclosure ");
		writeDef(closure->iface);
	caseof(InstrGlobalFuncClosure, closure)
		writer->write("funcclosure ");
		writeDef(closure->func);
	caseof(InstrClassMethodClosure, closure)
		writer->write("cmethod_closure ");
		writeDef(closure->method);
		writer->write(' ');
		if ( closure->object ){
			writeUse(closure->object);
		}else{
			writer->write("static");
		}
	caseof(InstrInterfaceMethodClosure, closure)
		writer->write("imethod_closure ");
		writeDef(closure->method);
		writer->write(' ');
		writeUse(closure->object);
	caseof(InstrLocalClosure, closure)
		writer->write("local_closure ");
		writeUse(closure->func);
	caseof(InstrNumUnary, unop)
		writer->write("num_unop ");
		writeType(unop->result_value.type);
		writer->write(' ');
		switch (unop->op) {
		case NumUnaryOp::BitInvert: {
			writer->write('~');
		}break;
		case NumUnaryOp::Negate: {
			writer->write('!');
		}break;	
		default: {
			jl_unreachable;
		}break;
		}
		writer->write(' ');
		writeUse(unop->arg);
	caseof(InstrNumBinary, binop)
		writer->write("num_binop ");
		writeType(binop->result_value.type);
		writer->write(' ');
		switch ( binop->op ){
		case NumBinaryOp::Add:{
			writer->write('+');
		}break;
		case NumBinaryOp::Sub: {
			writer->write('-');
		}break;
		case NumBinaryOp::Div: {
			writer->write('/');
		}break;
		case NumBinaryOp::Mul: {
			writer->write('*');
		}break;
		case NumBinaryOp::Mod: {
			writer->write('%');
		}break;
		case NumBinaryOp::BitAnd: {
			writer->write('&');
		}break;
		case NumBinaryOp::BitOr: {
			writer->write('|');
		}break;
		case NumBinaryOp::BitXor: {
			writer->write('^');
		}break;
		case NumBinaryOp::Shl: {
			writer->write("<<");
		}break;
		case NumBinaryOp::AShr: {
			writer->write(">>");
		}break;
		case NumBinaryOp::LShr: {
			writer->write(">>>");
		}break;
		default:{
			jl_unreachable;
		}break;
		}
		writer->write(' ');
		writeUse(binop->lhs);
		writer->write(' ');
		writeUse(binop->rhs);
	caseof(InstrStringConcat, concat)
		writer->write("string_concat ");
		writeUse(concat->lhs);
		writer->write(' ');
		writeUse(concat->rhs);
	caseof(InstrRefCmp, cmp)
		writer->write("ref_cmp ");
		write_op(writer, cmp->op);
		writer->write(' ');
		writeUse(cmp->lhs);
		writer->write(' ');
		writeUse(cmp->rhs);
	caseof(InstrFuncCmp, cmp)
		writer->write("func_cmp ");
		write_op(writer, cmp->op);
		writer->write(' ');
		writeUse(cmp->lhs);
		writer->write(' ');
		writeUse(cmp->rhs);
	caseof(InstrDynCmp, cmp)
		writer->write("dyn_cmp ");
		write_op(writer, cmp->op);
		writer->write(' ');
		writeUse(cmp->lhs);
		writer->write(' ');
		writeUse(cmp->rhs);
	caseof(InstrBoolCmp, cmp)
		writer->write("bool_cmp ");
		write_op(writer, cmp->op);
		writer->write(' ');
		writeUse(cmp->lhs);
		writer->write(' ');
		writeUse(cmp->rhs);
	caseof(InstrStringCmp, cmp)
		writer->write("str_cmp ");
		write_op(writer, cmp->op);
		writer->write(' ');
		writeUse(cmp->lhs);
		writer->write(' ');
		writeUse(cmp->rhs);
	caseof(InstrNumCmp, cmp)
		writer->write("num_cmp ");
		write_op(writer, cmp->op);
		writer->write(' ');
		writeUse(cmp->lhs);
		writer->write(' ');
		writeUse(cmp->rhs);
	caseof(InstrCondBr, condbr)
		writer->write("condbr ");
		writeUse(condbr->cond);
		writer->write(' ');
		writeUse(condbr->bb_true);
		writer->write(' ');
		writeUse(condbr->bb_false);
	caseof(InstrUnreachable, unreachable)
		writer->write("unreachable");
	caseof(InstrResume, resume)
		writer->write("resume");
	caseof(InstrCatchPad, catchpad)
		writer->write("catchpad ");
		writeType(catchpad->result_value.type);
	caseof(InstrChoice, choice)
		writer->write("choice ");
		writeUse(choice->sel);
		writer->write(' ');
		writeType(choice->result_value.type);
		writer->write(' ');
		writeUse(choice->val_true);
		writer->write(' ');
		writeUse(choice->val_false);
	caseof(InstrPhi, phi)
		writer->write("phi ");
		writeType(phi->result_value.type);
		for ( auto branch : phi->branches ){
			writer->write(" (");
			writeUse(branch.first);
			writer->write(" -> ");
			writeUse(branch.second);
			writer->write(")");
		}
	caseof(InstrSwitchBr, switch_)
		writer->write("switch_br ");
		writeUse(switch_->cond);
		writer->write(' ');
		writeUse(switch_->bb_default);
		for (auto branch : switch_->bb_branches) {
			writer->write(" (");
			writer->write(diag::decimal(branch.first));
			writer->write(" -> ");
			writeUse(branch.second);
			writer->write(")");
		}
	caseof(InstrReadClassField, read)
		writer->write("read_cfield ");
		writeDef(read->field);
		if ( read->thisval ){
			writer->write(' ');
			writeUse(read->thisval);
		}
	caseof(InstrReadGlobalVar, read)
		writer->write("read_global ");
		writeDef(read->var);
	caseof(InstrReadLocal, read)
		writer->write("read_local ");
		writeUse(read->var);
	caseof(InstrReadUpvalue, read)
		writer->write("read_upval ");
		writeUse(read->value);
	caseof(InstrReadDynamicIndex, read)
		writer->write("read_dynindex ");
		writeUse(read->object);
		writer->write(' ');
		writeUse(read->index);
	caseof(InstrReadVectorIndex, read)
		writer->write("read_vecindex ");
		writeType(read->result_value.type);
		writer->write(' ');
		writeUse(read->object);
		writer->write(' ');
		writeUse(read->index);
	caseof(InstrReadByteArrayIndex, read)
		writer->write("read_bytearrayindex ");
		writeType(read->result_value.type);
		writer->write(' ');
		writeUse(read->object);
		writer->write(' ');
		writeUse(read->index);
	caseof(InstrWriteDynamicIndex, write)
		writer->write("write_dynindex ");
		writeUse(write->object);
		writer->write(' ');
		writeUse(write->index);
		writer->write(' ');
		writeUse(write->value);
	caseof(InstrWriteArrayIndex, write)
		writer->write("write_arrindex ");
		writeUse(write->object);
		writer->write(' ');
		writer->write(diag::decimal(write->index));
		writer->write(' ');
		writeUse(write->value);
	caseof(InstrWriteNamedField, write)
		writer->write("write_arrindex ");
		writeUse(write->object);
		writer->write(' ');
		writer->write(write->name);
		writer->write(' ');
		writeUse(write->value);
	caseof(InstrDeleteDynamicIndex, delete_)
		writer->write("delete_dynindex ");
		writeUse(delete_->object);
		writer->write(' ');
		writeUse(delete_->index);
	caseof(InstrWriteVectorIndex, write)
		writer->write("write_vecindex ");
		writeUse(write->object);
		writer->write(' ');
		writeUse(write->index);
		writer->write(' ');
		writeUse(write->value);
	caseof(InstrWriteByteArrayIndex, write)
		writer->write("write_bytearray ");
		writeUse(write->object);
		writer->write(' ');
		writeUse(write->index);
		writer->write(' ');
		writeUse(write->value);
	caseof(InstrRet, ret){
		writer->write("ret ");
		if ( ret->value ){
			writeUse(ret->value);
		}else{
			writer->write("void");
		}
	}
	caseof(InstrThrow, throw_){
		writer->write("throw ");
		writeUse(throw_->value);
	}
	caseof(InstrWriteClassField, write){
		writer->write("write_cfield ");
		writeDef(write->field);
		if (write->thisval) {
			writer->write(' ');
			writeUse(write->thisval);
		}
		writer->write(' ');
		writeUse(write->value);
	}
	caseof(InstrWriteGlobalVar, write){
		writer->write("write_global ");
		writeDef(write->var);
		writer->write(' ');
		writeUse(write->value);
	}
	caseof(InstrWriteLocal, write){
		writer->write("write_local ");
		writeUse(write->var);
		writer->write(' ');
		writeUse(write->value);
	}
	caseof(InstrWriteUpvalue, write){
		writer->write("write_upval ");
		writeUse(write->upvalue);
		writer->write(' ');
		writeUse(write->value);
	}
	caseof(InstrDynIn, test){
		writer->write("dyn_in ");
		writeUse(test->key);
		writer->write(" ");
		writeUse(test->object);
	}
	caseof(InstrRestToArray, cast){
		writer->write("rest_to_array ");
		writeUse(cast->value);
	}
	caseof(InstrRestLength, rest_len){
		writer->write("rest_len ");
		writeUse(rest_len->value);
	}
	caseof(InstrIteratorNew, iter){
		writer->write("iterator_new ");
		writeUse(iter->object);
	}
	caseof(InstrIteratorNext, iter){
		writer->write("iterator_next ");
		writeUse(&iter->iterator->result_value);
		writer->write(' ');
		if ( iter->kind == InstrIteratorNext::Kind::Key ){
			writer->write("key");
		}else{
			writer->write("value");
		}
		writer->write(' ');
		writeType(iter->result_value.type);
		writer->write(' ');
		writeUse(iter->next_bb);
		writer->write(' ');
		writeUse(iter->end_bb);
	}
	caseof(InstrConsVector, cons){
		writer->write("vec_cons ");
		writeType(cons->type);
		writer->write(" ");
		writeUse(cons->arg);
	}
	caseof(InstrTypeCast, cast){
		writer->write("typeop ");
		switch ( cast->op ){
		case TypeCastOp::Is:{
			writer->write("is ");
		}break;
		case TypeCastOp::As: {
			writer->write("as ");
		}break;
		case TypeCastOp::To: {
			writer->write("to ");
		}break;
		default:{
			jl_unreachable;
		}break;
		}
		writeUse(cast->object);
		writer->write(' ');
		writeType(cast->cast_type);
	}
	caseof(InstrTypeCastDyn, cast) {
		writer->write("typeop_dyn ");
		switch (cast->op) {
		case TypeCastOp::Is: {
			writer->write("is ");
		}break;
		case TypeCastOp::As: {
			writer->write("as ");
		}break;
		case TypeCastOp::To: {
			writer->write("to ");
		}break;
		default: {
			jl_unreachable;
		}break;
		}
		writeUse(cast->object);
		writer->write(' ');
		writeUse(cast->cast_type);
	}
	casedef(_){
		writer->write("?bad_instr?");
		jl_unreachable;
	}
	endmatch
}

void Printer::writeUse(LocalVar* var){
	writer->write(var->name);
}

void Printer::writeUse(Upvalue* upvalue){
	writer->write(upvalue->var->name);
}

void Printer::writeDef(ir::Definition* def){
	def->scope->file->cu->require(CompilationUnit::State::CodegenPrepared);
	diag::write(*writer, def);
}

void Printer::writeDef(ir::ClassMember* def){
	diag::write(*writer, def);
}

void Printer::writeDef(ir::InterfaceMember* def){
	diag::write(*writer, def);
}

void Printer::writeUse(BasicBlock* bb){
	writer->write("block_");
	writer->write(diag::decimal(getBlockIndex(bb)));
}

void Printer::writeUse(Closure* closure){
	writer->write("closure_");
	writer->write(diag::decimal(getClosureIndex(closure)));
}

void Printer::writeUse(Value* value){
	match(value)
	caseof(ValueInstr, instr)
		writer->write("r");
		writer->write(diag::decimal(getInstrIndex(instr->getInstr())));
	caseof(ConstantNumber, cnum)
		writeType(cnum->type);
		writer->write('(');
		writer->write(std::to_string(cnum->value));
		writer->write(')');
	caseof(ConstantString, cstr)
		writer->write('"');
		EncodeCLiteral(writer, cstr->value);
		writer->write('"');
	caseof(ConstantBool, cbool)
		if ( cbool->value ){
			writer->write("true");
		}else{
			writer->write("false");
		}
	caseof(ConstantUndefined, cundef)
		writer->write("undefined");
	caseof(ConstantNull, cnull)
		writer->write("(null:");
		writeType(cnull->type);
		writer->write(')');
	caseof(ConstantBoxed, boxed)
		writer->write("boxed[");
		writeUse(boxed->contents);
		writer->write("]");
	caseof(BadValue, badval)
		writer->write("#badval:");
		writeType(badval->type);
	caseof(Thisval, thisval)
		writer->write("this");
	casedef(_)
		writer->write("?badval");
		jl_unreachable;
	endmatch
}

void Printer::writeType(ir::Type* type){
	diag::write(*writer, type);
}

}