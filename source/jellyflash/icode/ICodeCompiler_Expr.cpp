#include "ICodeCompiler.h"

namespace jly::icode{

void ICodeCompiler::compileExprForSideEffects(InstrBuilder* builder, ast::Expr* expr){
	// TODO:: only do this for properties
	evaluateExprUntyped(builder, expr);
}

Reference ICodeCompiler::compileExpr(InstrBuilder* builder, ast::Expr* ast){
	match(ast)
	caseof(ast::ExprArray, array)
		Value* array_object = builder->emit<InstrNewClass>(ast->span, intrin->classes.Array, nullptr)->toValue();
		Value** values = mem->createArray<Value*>(array->elts.getSize());
		i32 count = 0;
		for (auto elt : array->elts) {
			Value* elt_value = evaluateExprUntyped(builder, elt);
			builder->emit<InstrWriteArrayIndex>(elt->span, array_object, count, elt_value);
			count++;
		}
		return array_object;
	caseof(ast::ExprAssign, assign)
		Reference lhs = compileExpr(builder, assign->lhs);
		Value* rhs = nullptr;
		if ( assign->op == ast::AssignOp::Normal ){
			rhs = evaluateExprUntyped(builder, assign->rhs);
		}else{
			rhs = applyBinOp(
				builder, 
				assign->span, 
				convertOp(assign->op), 
				assign->lhs->span, 
				evaluateRef(builder, assign->lhs->span, lhs),
				assign->rhs
			);
		}
		write(builder, assign->span, lhs, rhs);
		return rhs;
	caseof(ast::ExprBinOp, binop)
		Value* lhs = evaluateExprUntyped(builder, binop->lhs);
		return applyBinOp(
			builder,
			binop->span,
			binop->op,
			binop->lhs->span,
			lhs,
			binop->rhs
		);
	caseof(ast::ExprCall, callexpr)
		Reference head = compileExpr(builder, callexpr->head);
		return call(builder, callexpr->span, head, callexpr->args);
	caseof(ast::ExprClosure, closure)
		return frame->closure_map.at(closure);
	caseof(ast::ExprConditional, conditional){
		// evaluate condition
		auto cond_value = evaluateExpr(builder, conditional->cond, &ir::TypePrim::Boolean);
		BasicBlock* true_bb = createBlock();
		BasicBlock* false_bb = createBlock();
		BasicBlock* join_bb = createBlock();
		builder->emit<InstrCondBr>(ast->span, cond_value, true_bb, false_bb);
		// evaluate both branches
		builder->setBlock(true_bb);
		Value* true_value = evaluateExprUntyped(builder, conditional->true_br);
		true_bb = builder->bb;

		builder->setBlock(false_bb);
		Value* false_value = evaluateExprUntyped(builder, conditional->false_br);
		false_bb = builder->bb;

		// compute unified type
		auto unified_type = unifyTypes(
			conditional->true_br->span, 
			true_value->type, 
			conditional->false_br->span,
			false_value->type
		);
		// cast both branches
		builder->setBlock(true_bb);
		true_value = cast_to(builder, conditional->true_br->span, true_value, unified_type, CastType::Implicit);
		auto from_true_bb = builder->bb;
		builder->emit<InstrBr>(ast->span, join_bb);

		builder->setBlock(false_bb);
		false_value = cast_to(builder, conditional->false_br->span, false_value, unified_type, CastType::Implicit);
		auto from_false_bb = builder->bb;
		builder->emit<InstrBr>(ast->span, join_bb);

		// join and select one value
		builder->setBlock(join_bb);
		return createPhi(builder, ast->span, unified_type, {{from_true_bb, true_value}, {from_false_bb, false_value}});
	}
	caseof(ast::ExprDotIndex, dotindex)
		Reference lhs = compileExpr(builder, dotindex->lhs);
		return resolve(builder, dotindex->span, lhs, dotindex->id);
	caseof(ast::ExprInstance, instance)
		Reference lhs = compileExpr(builder, instance->lhs);
		if ( lhs.tag == Reference::Tag::Definition && lhs.definition == intrin->classes.Vector ){
			return {vector_instance_tag{}, cc->getEnv()->getVectorType(resolver.resolveType(scope, instance->arg))};
		}else{
			cc->error(ast->span, "icode.bad_generic", "generic type is not a vector");
			return getInvalidValue();
		}
	caseof(ast::ExprLiteral, literal)
		switch ( literal->kind ){
		case ast::ExprLiteral::Kind::Decimal:{
			double value = 0;
			auto result = std::from_chars(
				(const char*)literal->contents.begin(),
				(const char*)literal->contents.end(), 
				value,
				std::chars_format::general
			);
			if ( result.ptr != (const char*)literal->contents.end() ){
				cc->fatal(ast->span, "icode.invalid_num_literal", "internal error: invalid number literal");
			}
			if ( std::count(literal->contents.begin(), literal->contents.end(), '.') > 0 ){
				return mem->create<ConstantNumber>(&ir::TypePrim::Number, value);
			}else{
				return getNumberConstant(value);
			}
		}break;
		case ast::ExprLiteral::Kind::Hexidecimal: {
			i64 value = 0;
			auto result = std::from_chars(
				(const char*)literal->contents.begin() + 2,
				(const char*)literal->contents.end(),
				value,
				16
			);
			if (result.ptr != (const char*)literal->contents.end()) {
				cc->fatal(ast->span, "icode.invalid_num_literal", "internal error: invalid number literal");
			}
			if ( value != (i64)(double)value ){
				cc->error(ast->span, "icode.inexact_double", "number is not exactly representable in double");
			}
			return getNumberConstant((double)value);
		}break;
		case ast::ExprLiteral::Kind::String: {
			return createStringLiteral(decodeStringLiteral(ast->span, literal->contents));
		}break;
		default:{
			cc->fatal(ast->span, "icode.invalid_literal_type", "internal error: invalid literal type");
		}break;
		}
	caseof(ast::ExprNew, new_)
		Reference ref = compileExpr(builder, new_->head);
		if ( ref.tag == Reference::Tag::Definition ){
			match(ref.definition)
			caseof(ir::Class, class_)
				if ( class_->native && class_->native->primitive && class_ != intrin->classes.Object){
					if (new_->args.getSize() != 1) {
						cc->error(
							new_->span,
							"icode.invalid_new", 
							"new for primitive type %1 is a cast - one argument expected",
							class_->type
						);
						return getInvalidValue();
					}
					auto val = evaluateExprUntyped(builder, new_->args[0]);
					return cast_to(builder, new_->span, val, class_->type, CastType::Explicit);
				}else{
					auto args = evalArgs(builder, new_->span, class_->constructor->signature, new_->args);
					return builder->emit<InstrNewClass>(ast->span, class_, args)->toValue();
				}
			endmatch
		}else if ( ref.tag == Reference::Tag::VectorInstance ){
			auto args = evalArgs(builder, new_->span, intrin->classes.Vector->constructor->signature, new_->args);
			return builder->emit<InstrNewVector>(ast->span, ref.vector_ty, args)->toValue();
		}
		auto val = evaluateRefAs(builder, new_->head->span, ref, intrin->types.Class);
		auto args = evalArgs(builder, new_->span, nullptr, true, new_->args);
		return builder->emit<InstrNewDynamic>(ast->span, val, args)->toValue();
	caseof(ast::ExprObject, object)
		Value* object_value = builder->emit<InstrNewClass>(ast->span, intrin->classes.Object, nullptr)->toValue();
		for ( auto field : object->fields ){
			StringRef key = nullptr;
			switch ( field->kind ){
			case ast::ObjectField::Kind::Identifier:{
				key = field->key.id;
			}break;
			case ast::ObjectField::Kind::Number: {
				/*
				double value = 0;
				auto result = std::from_chars(
					(const char*)field->key.id.begin(),
					(const char*)field->key.id.end(),
					value,
					std::chars_format::general
				);
				if (result.ptr != (const char*)field->key.id.end()) {
					cc->fatal(ast->span, "icode.invalid_num_literal", "internal error: invalid number literal");
				}
				if (std::count(field->key.id.begin(), field->key.id.end(), '.') > 0) {
					return mem->create<ConstantNumber>(&ir::TypePrim::Number, value);
				} else {
					return getNumberConstant(value);
				}
				*/
				cc->fatal(ast->span, "icode.number_as_object_key", "internal error: number literal as object key");
			}break;
			case ast::ObjectField::Kind::String: {
				key = decodeStringLiteral(field->key.span, field->key.id);
			}break;
			default:{
				jl_unreachable;
			}break;
			}
			auto value = evaluateExprUntyped(builder, field->value);
			builder->emit<InstrWriteNamedField>(ast->span, object_value, key, value);
		}
		return object_value;
	caseof(ast::ExprPrim, prim)
		switch ( prim->kind ){
		case ast::ExprPrim::Kind::This:{
			if ( !frame->this_value ){
				cc->error(ast->span, "icode.no_this", "context does not have 'this' object");
				return getInvalidValue();
			}else{
				return frame->this_value;
			}
		}break;
		case ast::ExprPrim::Kind::Undefined: {
			return mem->create<ConstantUndefined>();
		}break;
		case ast::ExprPrim::Kind::Null: {
			return mem->create<ConstantNull>(&ir::TypePrim::Null);
		}break;
		case ast::ExprPrim::Kind::True: {
			return mem->create<ConstantBool>(true);
		}break;
		case ast::ExprPrim::Kind::False: {
			return mem->create<ConstantBool>(false);
		}break;
		case ast::ExprPrim::Kind::Super: {
			auto class_ = frame->class_immediate;
			if ( !class_ ){
				cc->error(prim->span, "icode.super_no_class", "'super' can only be used within class context");
				return getInvalidValue();
			}
			auto base = class_->extends;
			if ( !base ){
				cc->error(prim->span, "icode.super_no_superclass", "class has no superclass");
				return getInvalidValue();
			}
			return super_tag_t{};
		}break;
		default:{
			cc->fatal(ast->span, "icode.not_impl", "prim kind not implemented");
		}break;
		}
	caseof(ast::ExprRef, ref_expr)
		if (ref_expr->id->ns ){
			cc->fatal(ast->span, "icode.not_impl",  "namespace-scoped ids not implemented");
		}
		StringRef name = ref_expr->id->id.id;
		Reference ref = resolveInFrame(ref_expr->span, frame, name);
		return ref;
	caseof(ast::ExprSeq, seq)
		compileExprForSideEffects(builder, seq->lhs);
		return compileExpr(builder, seq->rhs);
	caseof(ast::ExprSubscript, subscript)
		auto value = evaluateExprUntyped(builder, subscript->lhs);
		// only implement subscript for a subset of types...
		auto value_type = value->type;
		auto key = evaluateExprUntyped(builder, subscript->arg);
		if ( isAny(value_type) || value_type == &ir::TypePrim::Rest ){
			return { dynamic_index_tag{}, value, key };
		}
		match(value_type)
		caseof(ir::TypePrim, prim){
			switch ( prim->kind ){
			case ir::TypePrim::Kind::Class:{
				return { dynamic_index_tag{}, value, key };
			}break;
			default:{
				cc->error(ast->span, "icode.bad_subscript", "indexing invalid prim type %1", value_type);
				return getInvalidValue();
			}break;
			}
		}
		caseof(ir::TypeVector, vec)
			// vector elt, cast to int
			auto index = cast_to(builder, subscript->arg->span, key, &ir::TypePrim::Uint, CastType::Implicit);
			return { vector_index_tag{}, value, index };
		caseof(ir::TypeClass, class_){
			if ( class_ == intrin->types.ByteArray ){
				auto index = castToNumeric(builder, subscript->arg->span, key);
				return { bytearray_index_tag{}, value, index };
			}else{
				// dynamic class
				return { dynamic_index_tag{}, value, key };
			}
		}
		endmatch
		
		cc->error(ast->span, "icode.bad_subscript", "cannot index a value of type %1", value_type);
		return getInvalidValue();
	caseof(ast::ExprUnOp, unop)
		return applyUnOp(builder, unop->span, unop->op, unop->expr->span,compileExpr(builder, unop->expr));
	caseof(ast::ExprVectorShortcut, vec)
		cc->error(ast->span, "icode.not_impl", "vector shortcut expr not implemented");
		return getInvalidValue();
	casedef(_)
		cc->fatal(ast->span, "icode.invalid_expr", "internal error: invalid expression compiled");
	endmatch
}

Value* ICodeCompiler::evaluateExpr(InstrBuilder* builder, ast::Expr* ast, ir::Type* target_type){
	Value* value = evaluateExprUntyped(builder, ast);
	if ( target_type ){
		return cast_to(builder, ast->span, value, target_type, CastType::Implicit);
	}else{
		return value;
	}
}

Value* ICodeCompiler::evaluateExprUntyped(InstrBuilder* builder, ast::Expr* ast){
	Reference ref = compileExpr(builder, ast);
	return evaluateRef(builder, ast->span, ref);
}

Value* ICodeCompiler::evaluateRefAs(InstrBuilder* builder, srcspan_t span, Reference ref, ir::Type* type) {
	return cast_to(builder, span, evaluateRef(builder, span, ref), type, CastType::Implicit);
}

Value* ICodeCompiler::evaluateRef(InstrBuilder* builder, srcspan_t span, Reference ref) {
	switch ( ref.tag ){
	case Reference::Tag::None:{
		// already failed somewhere
	}break;
	case Reference::Tag::LocalVar:{
		return builder->emit<InstrReadLocal>(span, ref.local_var)->toValue();
	}break;
	case Reference::Tag::Upvalue:{
		return builder->emit<InstrReadUpvalue>(span, ref.upvalue)->toValue();
	}break;
	case Reference::Tag::Value: {
		return ref.value;
	}break;
	case Reference::Tag::Closure: {
		return builder->emit<InstrLocalClosure>(span, intrin->types.Function, ref.closure)->toValue();
	}break;
	case Reference::Tag::Package:{
		cc->error(span, "icode.eval_package", "package reference can't be evaluated");
	}break;
	case Reference::Tag::Super:{
		cc->error(span, "icode.eval_super", "super is not allowed in the context");
	}break;
	case Reference::Tag::Definition: {
		auto def = ref.definition;
		match(def)
		caseof(ir::GlobalFunction, func)
			return builder->emit<InstrGlobalFuncClosure>(span, intrin->types.Function, func)->toValue();
		caseof(ir::Class, class_)
			return builder->emit<InstrClassClosure>(span, intrin->types.Class, class_)->toValue();
		caseof(ir::Interface, iface)
			return builder->emit<InstrInterfaceClosure>(span, intrin->types.Class, iface)->toValue();
		caseof(ir::GlobalVar, var)
			Constant* constant = tryReadConstantVarInitializer(var->initializer);
			if ( constant ){
				if ( constant->tag == Value::Tag::CString ){
					return createStringLiteral(((ConstantString*)constant)->value);
				}
				return constant;
			}
			return builder->emit<InstrReadGlobalVar>(span, var)->toValue();
		casedef(_)
			cc->error(span, "icode.not_impl", "definition evaluation not implemented");
		endmatch
	}break;
	case Reference::Tag::DynamicIndex: {
		/*
		auto index = ref.dynamic_index.index;
		auto instance = ref.dynamic_index.instance;
		if ( index->type == &ir::TypePrim::String && index->tag == icode::Value::Tag::CString ){
			cc->warning(span, "icode.dynamic_get", "Dynamic access on type %1", instance->type);
		}
		*/
		return builder->emit<InstrReadDynamicIndex>(
			span,
			ref.dynamic_index.instance,
			ref.dynamic_index.index
		)->toValue();
	}break;
	case Reference::Tag::VectorIndex: {
		ir::TypeVector* ty_vec = (ir::TypeVector*)ref.dynamic_index.instance->type;
		return builder->emit<InstrReadVectorIndex>(
			span,
			ty_vec,
			ref.dynamic_index.instance,
			ref.dynamic_index.index
		)->toValue();
	}break;
	case Reference::Tag::ByteArrayIndex: {
		return builder->emit<InstrReadByteArrayIndex>(
			span,
			ref.dynamic_index.instance,
			ref.dynamic_index.index
		)->toValue();
	}break;
	case Reference::Tag::Intrinsic:{
		switch ( ref.intrinsic.intrin ){
		case Intrinsic::RestLength:{
			return builder->emit<InstrRestLength>(span, ref.intrinsic.instance)->toValue();
		}break;
		default:{
			cc->error(span, "icode.bad_intrin_read", "cannot read a value");
			return getInvalidValue();
		}break;
		}
	}break;
	case Reference::Tag::InterfaceMember:{
		auto member = ref.interface_member.member;
		match(member)
		caseof(ir::InterfaceMethod, method)
			return builder->emit<InstrInterfaceMethodClosure>(
				span, 
				intrin->types.Function, 
				method, 
				ref.interface_member.instance
			)->toValue();
		caseof(ir::InterfaceProperty, property)
			if (property->getter) {
				return builder->emit<InstrCallInterfaceMethod>(
					span,
					property->getter,
					ref.interface_member.instance,
					nullptr
				)->toValue();
			} else {
				cc->error(span, "icode.no_getter", "property %1 doesn't have getter", property);
				return getInvalidValue();
			}
		casedef(_)
			cc->error(span, "icode.invalid_class_member", "internal error: invalid class member being read");
		return getInvalidValue();
		endmatch
	}break;
	case Reference::Tag::ClassMember:{
		auto member = ref.class_member.member;
		match(member)
		caseof(ir::ClassField, field)
			if (field->owner == intrin->classes.Vector) {
				cc->error(span, "icode.vector_closure", "attempts to read a Vector field %1", field);
				return getInvalidValue();
			}

			if ( !ref.class_member.instance ){
				Constant* constant = tryReadConstantVarInitializer(field->initializer);
				if (constant) {
					if (constant->tag == Value::Tag::CString) {
						return createStringLiteral(((ConstantString*)constant)->value);
					}
					return constant;
				}
			}
			return builder->emit<InstrReadClassField>(
				span, 
				field, 
				ref.class_member.instance
			)->toValue();
		caseof(ir::ClassMethod, method)
			if ( method->owner == intrin->classes.Vector ){
				cc->error(span, "icode.vector_closure", "attempts to make a Vector method %1 closure", method);
				return getInvalidValue();
			}

			return builder->emit<InstrClassMethodClosure>(
				span, 
				intrin->types.Function, 
				method, 
				ref.class_member.instance
			)->toValue();
		caseof(ir::ClassProprety, property)
			if ( property->getter ){
				if (property->owner == intrin->classes.Vector) {
					return builder->emit<InstrCallVectorMethod>(
						span,
						property->getter->signature->retTy,
						property->getter,
						ref.class_member.instance,
						nullptr
					)->toValue();
				}

				return builder->emit<InstrCallClassMethod>(
					span,
					property->getter, 
					ref.class_member.instance,
					nullptr,
					ref.class_member.nonvirtual
				)->toValue();
			}else{
				cc->error(span, "icode.no_getter", "property %1 doesn't have getter", property);
				return getInvalidValue();
			}
		casedef(_)
			cc->error(span, "icode.invalid_class_member", "internal error: invalid class member being read");
			return getInvalidValue();
		endmatch
	}break;
	case Reference::Tag::VectorInstance:{
		cc->error(span, "icode.vector_instance_eval", "vector instance should not be evaluated");
		return getInvalidValue();
	}break;
	default:{
		cc->fatal(span, "icode.invalid_ref", "internal error: invalid reference evaluated");
	}break;
	}

	return getInvalidValue();
}

Value* ICodeCompiler::call(InstrBuilder* builder, srcspan_t span, Reference head, ArrayRef<ast::Expr*> ast_args){
	switch ( head.tag ){
	case Reference::Tag::Super:{
		auto base_class = frame->class_immediate->extends;
		auto constructor = base_class->constructor;
		if ( !constructor ){
			cc->error(span, "icode.missing_constructor_call", "base class constructor is not defined");
			return getInvalidValue();
		}else{
			frame->super_call = true;
			auto args = evalArgs(builder, span, constructor->signature, ast_args);
			return builder->emit<InstrCallClassMethod>(
				span, 
				constructor, 
				frame->this_value, 
				args, 
				true
			)->toValue();
		}
	}break;
	case Reference::Tag::Definition:{
		match(head.definition)
		caseof(ir::Interface, iface)
			// hard cast
			if (ast_args.getSize() != 1) {
				cc->error(span, "icode.fun_style_cast_args", "function-style cast: expected exactly 1 argument");
			}
			Value* value = ast_args.getSize() > 0 ? evaluateExprUntyped(builder, ast_args[0]) : getInvalidValue();
			for (uz i = 1; i < ast_args.getSize(); i++) {
				evaluateExprUntyped(builder, ast_args[i]);
			}
			return cast_to(builder, span, value, &iface->type, CastType::Explicit);
		caseof(ir::Class, class_)
			// use invoke method
			auto invoke_method = class_->native ? class_->native->invoke_method : nullptr;
			if (invoke_method){
				auto args = evalArgs(builder, span, invoke_method->signature, ast_args);
				return builder->emit<InstrCallClassMethod>(
					span,
					invoke_method,
					nullptr,
					args,
					true
				)->toValue();
			}else{
				// hard cast
				if ( ast_args.getSize() != 1 ){
					cc->error(span, "icode.fun_style_cast_args", "function-style cast: expected exactly 1 argument");
				}
				Value* value = ast_args.getSize() > 0 ? evaluateExprUntyped(builder, ast_args[0]) : getInvalidValue();
				for ( uz i = 1; i < ast_args.getSize(); i++ ){
					evaluateExprUntyped(builder, ast_args[i]);
				}
				return cast_to(builder, span, value, class_->type, CastType::Explicit);
			}
		caseof(ir::GlobalFunction, func)
			auto args = evalArgs(builder, span, func->signature, ast_args);
			return builder->emit<InstrCallGlobal>(span, func, args)->toValue();
		endmatch
	}break;
	case Reference::Tag::VectorInstance:{
		if (ast_args.getSize() != 1) {
			cc->error(span, "icode.vector_instance_cast", "cast-to-vector: expected exactly 1 argument");
		}
		Value* value = ast_args.getSize() > 0 ? evaluateExpr(builder, ast_args[0], intrin->types.Array) : getInvalidValue();
		for (uz i = 1; i < ast_args.getSize(); i++) {
			evaluateExprUntyped(builder, ast_args[i]);
		}
		return builder->emit<InstrConsVector>(span, head.vector_ty, value)->toValue();
	}break;
	case Reference::Tag::InterfaceMember: {
		match(head.interface_member.member)
		caseof(ir::InterfaceMethod, method)
			auto args = evalArgs(builder, span, method->signature, ast_args);
			return builder->emit<InstrCallInterfaceMethod>(
				span,
				method, 
				head.interface_member.instance, 
				args
			)->toValue();
		endmatch
	}break;
	case Reference::Tag::Closure:{
		auto args = evalArgs(builder, span, head.closure->signature, ast_args);
		return builder->emit<InstrCallClosure>(
			span,
			head.closure,
			args
		)->toValue();
	}break;
	case Reference::Tag::ClassMember:{
		match(head.class_member.member)
		caseof(ir::ClassMethod, method)
			if ( method->owner == intrin->classes.Vector ){
				frame->vec_type_params.push_back(((ir::TypeVector*)head.class_member.instance->type));
				auto args = evalArgs(builder, span, method->signature, ast_args);
				auto result = builder->emit<InstrCallVectorMethod>(
					span,
					concretizeType(method->signature->retTy),
					method, 
					head.class_member.instance,
					args
				)->toValue();
				frame->vec_type_params.pop_back();
				return result;
			}else{
				auto args = evalArgs(builder, span, method->signature, ast_args);
				return builder->emit<InstrCallClassMethod>(
					span, 
					method, 
					head.class_member.instance, 
					args, 
					head.class_member.nonvirtual
				)->toValue();
			}
		endmatch
	}break;
	}
	return callGeneric(builder, span, evaluateRef(builder, span, head), ast_args);
}

ir::Type* ICodeCompiler::concretizeType(ir::Type* type){
	if (type == intrin->types.Vector) {
		return frame->vec_type_params.back();
	} else if (type == &ir::TypeVar::Instance) {
		return frame->vec_type_params.back()->eltTy;
	} else {
		return type;
	}
}

ArrayRef<Value*> ICodeCompiler::evalArgs(InstrBuilder* builder, srcspan_t span, ArrayRef<ir::FunctionParam*> params, bool has_rest, ArrayRef<ast::Expr*> ast_args){
	uz max_args = params.getSize();
	uz min_args = max_args;
	while (min_args > 0 && params[min_args - 1]->initializer_def) {
		min_args--;
	}
	uz got_args = ast_args.getSize();
	uz convert_args;

	if (got_args < min_args) {
		cc->error(span, "icode.arg_mismatch", "not enough arguments: got " + std::to_string(got_args) + ", expected " + std::to_string(min_args));
	}
	if (has_rest) {
		convert_args = std::max(min_args, got_args);
	} else {
		if (got_args > max_args) {
			cc->error(span, "icode.arg_mismatch", "too many arguments: got " + std::to_string(got_args) + ", expected " + std::to_string(max_args));
		}
		convert_args = std::max(min_args, std::min(max_args, got_args));
	}

	Value** args = mem->createArray<Value*>(convert_args);
	uz idx = 0;
	for (uz i = 0; i < convert_args; i++) {
		ast::Expr* expr = nullptr;
		ir::Type* type = &ir::TypePrim::Any;
		if (i < params.getSize()) {
			type = concretizeType(params[i]->type);
		}
		if (i < ast_args.getSize()) {
			expr = ast_args[i];
		}
		if (expr) {
			args[i] = evaluateExpr(builder, expr, type);
		} else {
			args[i] = getInvalidValue(type);
		}
	}
	return { args, convert_args };
}

ArrayRef<Value*> ICodeCompiler::evalArgs(InstrBuilder* builder, srcspan_t span, ir::FunctionSignature* sign, ArrayRef<ast::Expr*> ast_args){
	return evalArgs(builder, span, sign->params, sign->rest, ast_args);
}

Value* ICodeCompiler::callGeneric(InstrBuilder* builder, srcspan_t span, Value* head, ArrayRef<ast::Expr*> ast_args){
	if ( isFunction(head->type) || isAny(head->type) ){
		head = cast_to(builder, span, head, intrin->types.Function, CastType::Implicit);
		Value** args = mem->createArray<Value*>(ast_args.getSize());
		for (uz i = 0; i < ast_args.getSize(); i++) {
			args[i] = cast_to(builder, span, evaluateExprUntyped(builder, ast_args[i]), &ir::TypePrim::Any, CastType::Implicit);
		}
		return builder->emit<InstrCallDynamic>(
			span,
			head,
			ArrayRef(args, ast_args.getSize())
		)->toValue();
	}else{
		cc->error(span, "icode.bad_call", "can't call a value of type %1", head->type);
		return getInvalidValue();
	}
}

Reference ICodeCompiler::resolve(InstrBuilder* builder, srcspan_t span, Reference lhs, ast::NSID* id){
	switch ( lhs.tag ){
	case Reference::Tag::Definition:{
		match(lhs.definition)
		caseof(ir::Class, class_)
			auto lookup_context = enterClassContext(class_);

			StringRef name = id->id.id;
			ir::ClassMember* member = linker.resolveStatic(class_, cc->getLoc(span), name, ns_scope->open_namespaces);
			if ( !member ){
				cc->error(span, "icode.class_no_static_member", "class %1 does not have static member %2", class_, name);
				return getInvalidValue();
			}
			return {member, nullptr};
		endmatch
	}break;
	case Reference::Tag::Super:{
		auto class_ = frame->class_immediate->extends;
		auto name = id->id.id;
		auto member = resolveClass(span, class_, frame->this_value, name);
		if ( member.tag == Reference::Tag::ClassMember ){
			member.class_member.nonvirtual = true;
		}
		return member;
	}break;
	}
	return resolveGeneric(builder, span, evaluateRef(builder, span, lhs), id);
}

Reference ICodeCompiler::resolveGeneric(InstrBuilder* builder, srcspan_t span, Value* lhs, ast::NSID* id){
	if (id->ns) {
		cc->error(span, "icode.ns_scoped", "namespace-scoped names not implemented");
	}
	StringRef name = id->id.id;
	if ( isAny(lhs->type) ){
		return {dynamic_index_tag{}, lhs, createStringLiteral(name)};
	}
	match(lhs->type)
	caseof(ir::TypePrim, prim)
		if ( prim->class_ ){
			Reference ref = resolveClass(span, prim->class_, lhs, name);
			return ref;
		}else if ( prim->kind == ir::TypePrim::Kind::Rest ){
			if ( name == "length" ){
				return {Intrinsic::RestLength, lhs};
			}else{
				cc->error(span, "icode.rest_no_member", "rest params do not have member %1", name);
				return getInvalidValue();
			}
		}else{
			cc->error(span, "icode.indexing_prim", "indexing prim type %1 not implemented", lhs->type);
			return getInvalidValue();
		}
	caseof(ir::TypeClass, class_)
		ir::ClassMember* member = resolveInstance(class_->class_, id->id.span, name);
		if ( !member ){
			cc->error(id->span, "icode.class_no_member", "class %1 does not have member %2", class_->class_, name);
			return getInvalidValue();
		}
		return {member, lhs};
	caseof(ir::TypeInterface, iface)
		ir::InterfaceMember* member = linker.resolveInstance(iface->iface, name);
		if (!member) {
			cc->error(id->span, "icode.iface_no_member", "interface %1 does not have member %2", iface->iface, name);
			return getInvalidValue();
		}
		return { member, lhs };
	caseof(ir::TypeVector, vec)
		ir::ClassMember* member = resolveInstance(intrin->classes.Vector, id->id.span, name);
		if (!member) {
			cc->error(id->span, "icode.vector_no_member", "vector %1 does not have member %2", vec, name);
			return getInvalidValue();
		}
		return { member, lhs };
	casedef(_)
		cc->error(id->span, "icode.not_impl", "indexing type %1 is not implemented", lhs->type);
	endmatch
	return getInvalidValue();
}

Value* ICodeCompiler::write(InstrBuilder* builder, srcspan_t span, Reference target, Value* value){
	switch ( target.tag ){
	case Reference::Tag::LocalVar:{
		LocalVar* local_var = target.local_var;
		value = cast_to(builder, span, value, local_var->type, CastType::Implicit);
		builder->emit<InstrWriteLocal>(span, local_var, value);
		return value;
	}break;
	case Reference::Tag::Upvalue:{
		Upvalue* upvalue = target.upvalue;
		value = cast_to(builder, span, value, upvalue->type, CastType::Implicit);
		builder->emit<InstrWriteUpvalue>(span, upvalue, value);
		return value;
	}break;
	case Reference::Tag::DynamicIndex:{
		value = cast_to(builder, span, value, &ir::TypePrim::Any, CastType::Implicit);
		builder->emit<InstrWriteDynamicIndex>(
			span,
			target.dynamic_index.instance,
			target.dynamic_index.index,
			value
		);
		return value;
	}break;
	case Reference::Tag::VectorIndex: {
		ir::TypeVector* ty_vec = (ir::TypeVector*)target.dynamic_index.instance->type;
		value = cast_to(builder, span, value, ty_vec->eltTy, CastType::Implicit);
		builder->emit<InstrWriteVectorIndex>(
			span,
			target.dynamic_index.instance,
			target.dynamic_index.index,
			value
		);
		return value;
	}break;
	case Reference::Tag::ByteArrayIndex: {
		value = cast_to(builder, span, value, &ir::TypePrim::Int, CastType::Implicit);
		builder->emit<InstrWriteByteArrayIndex>(
			span,
			target.dynamic_index.instance,
			target.dynamic_index.index,
			value
		);
		return value;
	}break;
	case Reference::Tag::ClassMember:{
		match(target.class_member.member)
		caseof(ir::ClassField, field)
			value = cast_to(builder, span, value, field->type, CastType::Implicit);
			builder->emit<InstrWriteClassField>(span, field, target.class_member.instance, value);
			return value;
		caseof(ir::ClassProprety, property)
			if (property->setter) {
				if (property->owner == intrin->classes.Vector) {
					return builder->emit<InstrCallVectorMethod>(
						span,
						&ir::TypePrim::Void,
						property->setter,
						target.class_member.instance,
						to_array({ cast_to(
							builder,
							span,
							value,
							property->setter->signature->params[0]->type,
							CastType::Implicit
						) })
					)->toValue();
				}

				return builder->emit<InstrCallClassMethod>(
					span,
					property->setter,
					target.class_member.instance,
					to_array({ cast_to(
						builder,
						span,
						value,
						property->setter->signature->params[0]->type,
						CastType::Implicit
					)}),
					target.class_member.nonvirtual
				)->toValue();
			} else {
				cc->error(span, "icode.no_setter", "property %1 doesn't have setter", property);
				return getInvalidValue();
			}
		casedef(_)
			cc->error(span, "icode.class_member_non_writable", "class member %1 is not writable", target.class_member.member);
			return getInvalidValue();
		endmatch
	}break;
	case Reference::Tag::Definition:{
		match(target.definition)
		caseof(ir::GlobalVar, var_)
			value = cast_to(builder, span, value, var_->type, CastType::Implicit);
			builder->emit<InstrWriteGlobalVar>(span, var_, value);
			return value;
		endmatch
	}break;
	}

	cc->error(span, "icode.reference_non_writable", "reference %1 is not writable", target);
	return getInvalidValue();
}

Value* ICodeCompiler::compare(InstrBuilder* builder, srcspan_t span, CompareOp op, srcspan_t left, Value* lhs, srcspan_t right, Value* rhs){
	cc->error(span, "icode.not_impl", "compare not implemented");
	return getInvalidValue();
}

Value* ICodeCompiler::getInvalidValue(ir::Type* type){
	return mem->create<BadValue>(type);
}

}