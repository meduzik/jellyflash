#include "ICodeCompiler.h"

namespace jly::icode {
Value* ICodeCompiler::cast_to(InstrBuilder* builder, srcspan_t span, Value* val, ir::Type* target_type, CastType type){
	ir::Type* value_type = val->type;
	if (areTypesEqual(value_type, target_type)) {
		if ( type == CastType::Explicit ){
			//cc->warning(span, "icode.explicit_same", "explicit cast from %1 to %2", value_type, target_type);
		}
		return val;
	}

	bool require_explicit = false;

	if ( isConstant(val) ){
		return castConstant(span, (Constant*)val, target_type, type);
	} else if ( value_type == &ir::TypePrim::Void ){
		cc->warning(span, "icode.from_void", "casting from %1 to %2", value_type, target_type);
	} else if ( target_type == &ir::TypePrim::Void ){
		cc->warning(span, "icode.to_void", "casting from %1 to %2", value_type, target_type);
	} else if ( isAny(target_type) || value_type == &ir::TypePrim::Any){
		// ok
	} else if ( value_type == &ir::TypePrim::Null ){
		if ( isNullable(target_type) ){
			// everything is ok
		}else{
			cc->warning(span, "icode.null_to_prim", "casting from %1 to %2", value_type, target_type);
		}
	} else if ( target_type == &ir::TypePrim::Boolean ){
		// ok
	} else if (value_type == &ir::TypePrim::Object) {
		require_explicit = true;
	} else if ( target_type == &ir::TypePrim::String ){
		if ( !isNumeric(value_type) ){
			require_explicit = true;
		}
	} else if ( isNumeric(target_type) ){
		if ( isNumericOrBoolean(value_type) ){
			// ok
		} else if ( isString(value_type) ){
			require_explicit = true;
		} else {
			cc->warning(span, "icode.ref_to_number", "casting from %1 to %2", value_type, target_type);
		}
	} else if ( target_type == &ir::TypePrim::Function && value_type == &ir::TypePrim::Class ){
		cc->warning(span, "icode.class_to_func", "casting from %1 to %2", value_type, target_type);
	} else if (target_type == intrin->types.Array && value_type == &ir::TypePrim::Rest) {
		// rest to array is fine
	} else {
		match2(value_type, target_type)
		caseof2(ir::TypeClass, value_class, ir::TypeClass, test_class) {
			require(value_class->class_);
			require(test_class->class_);
			if (value_class->class_->all_bases.count(test_class->class_)) {
				// ok, upcast
			} else if (!test_class->class_->all_bases.count(value_class->class_)) {
				cc->error(span, "icode.cast_error", "types %1 and %2 are unrelated, the cast never succeedes", value_type, target_type);
			} else {
				require_explicit = true;
			}
		}
		caseof2(ir::TypeClass, value_class, ir::TypeInterface, test_iface) {
			require(value_class->class_);
			require(test_iface->iface);
			if ( value_class->class_->all_implements_set.count(test_iface->iface) ){
				// ok, upcast
			} else if ( value_class->class_->m_final ){
				cc->error(span, "icode.cast_error", "%1 is final and does not implement %2, the cast never succeedes", value_type, target_type);
			}else{
				require_explicit = true;
			}
		}
		caseof2(ir::TypeInterface, value_iface, ir::TypeClass, test_class) {
			require(value_iface->iface);
			require(test_class->class_);
			if (test_class->class_->m_final && !test_class->class_->all_implements_set.count(value_iface->iface)) {
				cc->error(span, "icode.cast_error", "%2 is final and does not implement %1, the cast never succeedes", value_type, target_type);
			}else{
				require_explicit = true;
			}
		}
		caseof2(ir::TypeInterface, value_iface, ir::TypeInterface, test_iface) {
			require(value_iface->iface);
			require(test_iface->iface);
			if ( value_iface->iface->all_implements_set.count(test_iface->iface) ){
				
			}else{
				require_explicit = true;
			}
		}
		casedef2(_1, _2) {
			cc->error(span, "icode.bad_cast", "casting %1 to %2, types are unrelated", value_type, target_type);
		}
		endmatch2
	}

	if ( require_explicit && type != CastType::Explicit ){
		cc->error(span, "icode.implicit_cast", "implicit cast from %1 to %2", value_type, target_type);
	}

	return builder->emit<InstrTypeCast>(span, target_type, val, target_type, TypeCastOp::To)->toValue();
}

Value* ICodeCompiler::cast_as(InstrBuilder* builder, srcspan_t span, Value* val, ir::Type* target_type){
	ir::Type* value_type = val->type;
	if (areTypesEqual(value_type, target_type)) {
		cc->warning(span, "icode.as_same", "types always match");
	} else if (target_type == intrin->types.Array && value_type == &ir::TypePrim::Rest) {
		// rest to array is fine
	} else if ( value_type == &ir::TypePrim::Void) {
		cc->warning(span, "icode.void_as", "casting value of type %1, the result is null", value_type);
	} else if ( target_type == &ir::TypePrim::Object ) {
		// ok, everything to object is ok
	} else if ( isAny(value_type) ){
		// ok, extracting anything from * or Object is ok
	} else if (isNumeric(target_type) && isNumeric(value_type) && target_type != &ir::TypePrim::Number) {
		// ok, numeric cast
	} else {
		match2(value_type, target_type)
		caseof2(ir::TypeClass, value_class, ir::TypeClass, test_class) {
			require(value_class->class_);
			require(test_class->class_);
			if ( value_class->class_->all_bases.count(test_class->class_) ) {
				// ok, upcast
			}else if ( !test_class->class_->all_bases.count(value_class->class_) ) {
				cc->warning(span, "icode.as_always_null", "types %1 and %2 are unrelated, the cast result is always null", value_type, target_type);
			}
		}
		caseof2(ir::TypeClass, value_class, ir::TypeInterface, test_iface) {
			require(value_class->class_);
			require(test_iface->iface);
			if (
				!value_class->class_->all_implements_set.count(test_iface->iface)
				&& 
				value_class->class_->m_final
			) {
				cc->warning(span, "icode.as_always_null", "%1 is final and does not implement %2, the cast result is always null", value_type, target_type);
			}
		}
		caseof2(ir::TypeInterface, value_iface, ir::TypeClass, test_class) {
			require(value_iface->iface);
			require(test_class->class_);
			if ( test_class->class_->m_final && !test_class->class_->all_implements_set.count(value_iface->iface) ) {
				cc->warning(span, "icode.as_always_null", "%2 is final and does not implement %1, the cast result is always null", value_type, target_type);
			}
		}
		caseof2(ir::TypeInterface, value_iface, ir::TypeInterface, test_iface) {
		}
		casedef2(_1, _2) {
			cc->error(span, "icode.bad_cast", "casting %1 to %2, types are unrelated", value_type, target_type);
		}
		endmatch2
	}
	ir::Type* result_type = nullptr;
	if ( isNullable(target_type) ){
		result_type = target_type;
	}else{
		result_type = &ir::TypePrim::Any;
	}
	return builder->emit<InstrTypeCast>(span, result_type, val, target_type, TypeCastOp::As)->toValue();
}

Value* ICodeCompiler::typetest(InstrBuilder* builder, srcspan_t span, Value* val, ir::Type* target_type) {
	ir::Type* value_type = val->type;
	if (areTypesEqual(value_type, target_type)) {
		cc->warning(span, "icode.test_same", "types always match");
		return mem->create<ConstantBool>(true);
	}

	if (target_type == intrin->types.Array && value_type == &ir::TypePrim::Rest) {
		cc->warning(span, "icode.is_always_true", "%1 is %2, check is always true", target_type);
	}else if ( isAny(target_type) ){
		cc->warning(span, "icode.is_any", "checking against %1, did you mean compare with null?", target_type);
	}else if ( isAny(value_type) ){
		// ok, checking any is always ok
	}else if ( isNumeric(target_type) && isNumeric(value_type) && target_type != &ir::TypePrim::Number){
		// ok, numeric test
	}else{
		match2(value_type, target_type)
		caseof2(ir::TypeClass, value_class, ir::TypeClass, test_class){
			require(value_class->class_);
			require(test_class->class_);
			if ( value_class->class_->all_bases.count(test_class->class_) ){
				cc->warning(span, "icode.is_always_true", "%1 extends %2, the check is always true", value_type, target_type);
			}else if ( !test_class->class_->all_bases.count(value_class->class_) ){
				cc->warning(span, "icode.is_always_false", "types %1 and %2 are unrelated, the check is always false", value_type, target_type);
			}
		}
		caseof2(ir::TypeClass, value_class, ir::TypeInterface, test_iface) {
			require(value_class->class_);
			require(test_iface->iface);
			if ( value_class->class_->all_implements_set.count(test_iface->iface) ){
				cc->warning(span, "icode.is_always_true", "%1 implements %2, the check is always true", value_type, target_type);
			}else if ( value_class->class_->m_final ){
				cc->warning(span, "icode.is_always_false", "%1 is final and does not implement %2, the check is always false", value_type, target_type);
			}
		}
		caseof2(ir::TypeInterface, value_iface, ir::TypeClass, test_class) {
			require(value_iface->iface);
			require(test_class->class_);
			if ( test_class->class_->m_final && !test_class->class_->all_implements_set.count(value_iface->iface) ){
				cc->warning(span, "icode.is_always_false", "%1 is final and does not implement %2, the check is always false", value_type, target_type);
			}
		}
		caseof2(ir::TypeInterface, value_iface, ir::TypeInterface, test_iface) {
			require(value_iface->iface);
			require(test_iface->iface);
			if ( value_iface->iface->all_implements_set.count(test_iface->iface) ){
				cc->warning(span, "icode.is_always_true", "%1 extends %2, the check is always true", value_type, target_type);
			}
		}
		casedef2(_1, _2){
			cc->error(span, "icode.bad_typetest", "checking value of type %1 against type %2, types are unrelated", value_type, target_type);
		}
		endmatch2
	}
	return builder->emit<InstrTypeCast>(span, &ir::TypePrim::Boolean, val, target_type, TypeCastOp::Is)->toValue();
}


}
