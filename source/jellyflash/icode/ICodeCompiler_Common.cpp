#include "ICodeCompiler.h"
#include <jellyflash/compiler/CompilerOps.h>

namespace jly::icode {

namespace{
u8 decode_hex(u8 ch){
	if ( ch >= 'a' && ch <= 'f' ){
		return ch - 'a' + 10;
	}else if (ch >= 'A' && ch <= 'F') {
		return ch - 'A' + 10;
	} else if (ch >= '0' && ch <= '9') {
		return ch - '0';
	}else{
		jl_unreachable;
	}
}
}

StringRef ICodeCompiler::decodeStringLiteral(srcspan_t span, StringRef data) {
	slice_vector<u8> r(mem);
	uz n = data.getSize() - 1;
	for (uz i = 1; i < n; i++) {
		u8 ch = data[i];
		if (ch == '\\') {
			i++;
			ch = data[i];
			switch (ch) {
			case 'n': {
				r.push_back('\n');
			}break;
			case 'r': {
				r.push_back('\r');
			}break;
			case '0': {
				r.push_back('\0');
			}break;
			case 't': {
				r.push_back('\t');
			}break;
			case '"':
			case '\'':
			case '\\': {
				r.push_back(ch);
			}break;
			case 'x':{
				i++;
				u8 x1 = data[i];
				i++;
				u8 x2 = data[i];
				r.push_back(decode_hex(x1) * 16 + decode_hex(x2));
			}break;
			default: {
				cc->error(span, "icode.invalid_escape_seq", "invalid escape sequence %1", StringRef(&ch, 1));
			}break;
			}
		} else {
			r.push_back(ch);
		}
	}
	return { r.data(), r.size() };
}

BasicBlock* ICodeCompiler::createBlock(){
	BasicBlock* bb = mem->create<BasicBlock>(mem, frame->handler);
	return bb;
}

Value* ICodeCompiler::createPhi(
	InstrBuilder* builder,
	srcspan_t span,
	ir::Type* type, 
	const std::initializer_list< std::pair<BasicBlock*, Value*> >& branches
){
	return builder->emit<InstrPhi>(span, type, clone(branches.begin(), branches.end()))->toValue();
}

ICodeCompiler::TemporaryNSScope ICodeCompiler::enterClassContext(ir::Class* class_){
	auto& opened_namespaces = ns_scope->open_namespaces;
	uz reset_to = opened_namespaces.size();

	if (class_ == frame->class_context) {
		opened_namespaces.push_back(ir::NSPrivate.def);
		opened_namespaces.push_back(ir::NSProtected.def);
		opened_namespaces.push_back(ir::NSInternal.def);
	} else {
		if (frame->class_context && frame->class_context->all_bases.count(class_)) {
			opened_namespaces.push_back(ir::NSProtected.def);
		}
		if (class_->scope->file->cu->getPackage() == cc->getUnit()->getPackage()) {
			opened_namespaces.push_back(ir::NSInternal.def);
		}
	}

	return {opened_namespaces, reset_to};
}

void ICodeCompiler::require(ir::Class* class_){
	linker.require(class_);
}

void ICodeCompiler::require(ir::Interface* iface){
	linker.require(iface);
}

ir::ClassMember* ICodeCompiler::resolveInstance(ir::Class* class_, srcspan_t span, StringRef name) {
	auto lookup_context = enterClassContext(class_);
	ir::ClassMember* member = linker.resolveInstance(class_, cc->getLoc(span), name, ns_scope->open_namespaces);
	return member;
}

Reference ICodeCompiler::resolveClass(srcspan_t span, ir::Class* class_, Value* value, StringRef name){
	auto lookup_context = enterClassContext(class_);

	ir::ClassMember* instance_member = linker.resolveInstance(class_, cc->getLoc(span), name, ns_scope->open_namespaces);
	if (instance_member && value && isAccessible(instance_member)) {
		return { instance_member, value };
	}

	ir::ClassMember* static_member = linker.resolveStatic(class_, cc->getLoc(span), name, ns_scope->open_namespaces);
	if (static_member && isAccessible(static_member)) {
		return { static_member, nullptr };
	}

	if (static_member) {
		cc->error(span, "icode.inaccessible_static", "attempt to access inaccessable static member %1", static_member);
	}
	if (instance_member) {
		if (!value) {
			cc->error(span, "icode.instance_in_static", "attempt to access instance member %1 in static context", instance_member);
		} else {
			cc->error(span, "icode.inaccessible_instance", "attempt to access inaccessable instance member %1", instance_member);
		}
	}

	return nullptr;
}

Reference ICodeCompiler::resolveInFrameCompute(srcspan_t span, FunctionFrameState* frame, StringRef name) {
	if (frame->parent) {
		Reference ref = resolveInFrame(span, frame->parent, name);
		switch (ref.tag) {
		case Reference::Tag::LocalVar: {
			Upvalue* upvalue = mem->create<Upvalue>(ref.local_var);
			ref.local_var->upvalue = upvalue;
			if (ref.local_var->type == &ir::TypePrim::Rest){
				cc->error(span, "icode.arguments_capture", "closure captures arguments, convert to array");
			}
			frame->upvals.push_back(upvalue);
			return upvalue;
		}break;
		case Reference::Tag::Upvalue: {
			Upvalue* upvalue = mem->create<Upvalue>(ref.upvalue);
			frame->upvals.push_back(upvalue);
			return upvalue;
		}break;
		case Reference::Tag::Closure: {
			return ref;
		}break;
		case Reference::Tag::ClassMember: {
			if (!ref.class_member.instance) {
				return ref;
			}
			if (ref.class_member.instance->tag == Value::Tag::Thisval) {
				if (frame->parent->this_value == ref.class_member.instance) {
					frame->this_value = frame->parent->this_value;
					frame->parent->realization->this_upvalued = true;
					return { ref.class_member.member, frame->this_value };
				}
			}
			cc->fatal(span, "icode.bad_ref_escapes", "this reference should not escape parent's scope");
		}break;
		case Reference::Tag::Package:
		case Reference::Tag::Definition: {
			// simply pass unchanged
			return ref;
		}break;
		case Reference::Tag::None: {
			return ref;
		}break;
		default: {
			cc->fatal(span, "icode.bad_ref_escapes", "this reference should not escape parent's scope");
		}break;
		}
		return ref;
	} else {
		ast::ID id(span, name);

		auto class_context = frame->class_context;
		if (class_context) {
			Reference ref = resolveClass(span, class_context, frame->this_value, name);
			if (ref.tag != Reference::Tag::None) {
				return ref;
			}
		}

		ir::Definition* def = resolver.resolveDefinition(scope, span, id);
		if (def) {
			def->scope->file->cu->require(CompilationUnit::Linked);
			return def;
		}
		auto package = cc->getEnv()->getPackageRoot()->findChild(name);
		if (package) {
			return package;
		}
		cc->error(span, "icode.unresolved_id", "unresolved identifier %1", name);
		return nullptr;
	}
}

Reference ICodeCompiler::resolveInFrame(srcspan_t span, FunctionFrameState* frame, StringRef name){
	auto it = frame->cache.find(name);
	if ( it != frame->cache.end() ){
		return it->second;
	}
	auto ref = resolveInFrameCompute(span, frame, name);
	frame->cache.insert({name, ref});
	return ref;
}

bool ICodeCompiler::isAccessible(ir::ClassMember* member){
	if ( member->ns == &ir::NSPublic ){
		return true;
	}
	auto owner = member->owner;
	if ( owner == frame->class_context){
		return true;
	}
	if ( member->ns == &ir::NSProtected ){
		ir::Class* class_ = frame->class_context;
		while ( class_ ){
			if ( class_ == owner ){
				return true;
			}
			class_ = class_->extends;
		}
	}
	if ( member->ns == &ir::NSInternal ){
		return member->owner->scope->file->cu->getPackage() == cc->getUnit()->getPackage();
	}
	if (member->ns->def && member->ns->def->value == "\"http://adobe.com/AS3/2006/builtin\""){
		return true;
	}
	return false;
}

bool ICodeCompiler::isReference(ir::Type* type){
	return !isPrimitive(type);
}

bool ICodeCompiler::isPrimitive(ir::Type* type){
	return (
		type == &ir::TypePrim::Boolean
		||
		type == &ir::TypePrim::Number
		||
		type == &ir::TypePrim::Int
		||
		type == &ir::TypePrim::Uint
		||
		type == &ir::TypePrim::String
		|| 
		type == &ir::TypePrim::Function
	);
}

bool ICodeCompiler::isAny(ir::Type* type){
	return (type == &ir::TypePrim::Any || type == intrin->types.Object);
}

bool ICodeCompiler::isDynamicClass(ir::Type* type){
	match(type)
	caseof(ir::TypeClass, class_)
		return class_->class_->m_dynamic;
	endmatch
	return false;
}

bool ICodeCompiler::isBoolean(ir::Type* type) {
	return (type == &ir::TypePrim::Boolean);
}

bool ICodeCompiler::isNumeric(ir::Type* type){
	return (
		(type == &ir::TypePrim::Int)
		||
		(type == &ir::TypePrim::Uint)
		||
		(type == &ir::TypePrim::Number)
	);
}

bool ICodeCompiler::isNumericOrBoolean(ir::Type* type) {
	return (
		(type == &ir::TypePrim::Boolean)
		||
		(type == &ir::TypePrim::Int)
		||
		(type == &ir::TypePrim::Uint)
		||
		(type == &ir::TypePrim::Number)
	);
}


bool ICodeCompiler::isString(ir::Type* type){
	return (type == intrin->types.String);
}

bool ICodeCompiler::isFunction(ir::Type* type){
	return (type == intrin->types.Function);
}

bool ICodeCompiler::isClass(ir::Type* type){
	return (type == intrin->types.Class);
}

bool ICodeCompiler::isNullable(ir::Type* type){
	return !isNumericOrBoolean(type);
}

LabelInfo* ICodeCompiler::createLabel(srcspan_t span, StringRef name, ScopeID* scope_id, BasicBlock* bb){
	return tmp->create<LabelInfo>(
		cc->getLoc(span),
		name,
		bb,
		scope_id
	);
}

void ICodeCompiler::traverseFlowGraph(BasicBlock* entry){
	slice_uset<BasicBlock*>& bbs = frame->active_bbs;
	slice_uset<HandlerScope*> handlers(tmp);
	slice_vector<BasicBlock*> bb_list(tmp);
	slice_vector<BasicBlock*> bb_stack(tmp);
	slice_vector<HandlerScope*> handler_list(tmp);
	bbs.insert(entry);
	bb_stack.push_back(entry);
	
	auto add_bb = [&](BasicBlock* target) {
		if ( !target ) {
			return;
		}
		if (bbs.insert(target).second) {
			bb_stack.push_back(target);
		}
	};
	auto add_edge = [&](BasicBlock* bb, BasicBlock* target){
		if ( !bb || !target ){
			return;
		}
		add_bb(target);
	};
	auto add_handler = [&](HandlerScope* handler){
		if (!handler) {
			return;
		}
		if (handlers.insert(handler).second) {
			handler_list.push_back(handler);
		}
	};
	auto visit_handler = [&](HandlerScope* handler){
		if ( handler->parent ){
			add_handler(handler->parent);
		}
		add_bb(handler->catch_all);
		for ( auto filter : handler->filters ){
			add_bb(filter.second);
		}
	};
	auto visit_bb = [&](BasicBlock* bb) {
		bb_list.push_back(bb);

		auto range = frame->indirect_edges.equal_range(bb);
		for ( auto it = range.first; it != range.second; it++ ){
			add_edge(bb, it->second);
		}
		if ( bb->handler ){
			add_handler(bb->handler);
		}
		if (bb->instrs.empty()) {
			return;
		}
		match(bb->instrs.back())
		caseof(InstrBr, br) {
			add_edge(bb, br->bb);
		}
		caseof(InstrCondBr, condbr) {
			add_edge(bb, condbr->bb_false);
			add_edge(bb, condbr->bb_true);
		}
		caseof(InstrSwitchBr, switchbr) {
			for ( auto& branch : switchbr->bb_branches ){
				add_edge(bb, branch.second);
			}
			add_edge(bb, switchbr->bb_default);
		}
		caseof(InstrIteratorNext, iter) {
			add_edge(bb, iter->end_bb);
			add_edge(bb, iter->next_bb);
		}
		endmatch
	};

	uz i = 0, j = 0;
	bool progress = true;

	while ( progress ){
		progress = false;
		while ( !bb_stack.empty() ){
			BasicBlock* bb = bb_stack.back();
			bb_stack.pop_back();
			visit_bb(bb);
			progress = true;
		}
		for (; j < handler_list.size(); j++) {
			visit_handler(handler_list[j]);
			progress = true;
		}
	}

	frame->reachable_bbs = ArrayRef(bb_list).clone(*mem);
	frame->reachable_handlers = ArrayRef(handler_list).clone(*mem);
}

namespace{
u32 ToUInt32(double value){
	if ( !std::isfinite(value) ){
		return 0;
	}
	double int_val = std::fmod(std::floor(value), (double)(1ULL << 32ULL));
	if ( int_val < 0 ){
		return (u32)(i64)int_val;
	}else{
		return (u32)int_val;
	}
}

i32 UIntToInt(u32 value){
	if ( value >= 0x80000000 ){
		return std::numeric_limits<i32>::min() + (value - 0x80000000);
	}else{
		return (i32)value;
	}
}
}

Constant* ICodeCompiler::constantToInt32(srcspan_t span, double value, bool is_explicit){
	double _;
	
	if (is_explicit ){
		return mem->create<ConstantNumber>(&ir::TypePrim::Uint, UIntToInt(ToUInt32(value)));
	}

	if ( std::isnan(value) ){
		cc->error(span, "icode.int_nan", "converting NaN to int");
	}else if ( value < (double)std::numeric_limits<i32>::min() ){
		cc->error(span, "icode.int_underflow", "number %1 is out of int32 range", value);
	}else if (value > (double)std::numeric_limits<i32>::max()) {
		cc->error(span, "icode.int_overflow", "number %1 is out of int32 range", value);
	}else if ( std::modf(value, &_) != 0.0 ){
		cc->error(span, "icode.int_inexact", "number %1 is not an integer", value);
	}else{
		return mem->create<ConstantNumber>(&ir::TypePrim::Int, (i32)value);
	}
	return mem->create<BadValue>(&ir::TypePrim::Int);
}

Constant* ICodeCompiler::getNumberConstant(double value){
	double _;
	if ( 
		std::isnan(value) 
		|| 
		std::isinf(value) 
		||
		value < (double)std::numeric_limits<i32>::min()
		||
		value > (double)std::numeric_limits<u32>::max()
		|| 
		std::modf(value, &_) != 0.0
	) {
		return mem->create<ConstantNumber>(&ir::TypePrim::Number, value);
	}
	if ( value <= (double)std::numeric_limits<i32>::max()){
		return mem->create<ConstantNumber>(&ir::TypePrim::Int, value);
	}else{
		return mem->create<ConstantNumber>(&ir::TypePrim::Uint, value);
	}
}

Constant* ICodeCompiler::constantToUInt32(srcspan_t span, double value, bool is_explicit){
	double _;

	if (is_explicit) {
		return mem->create<ConstantNumber>(&ir::TypePrim::Uint, ToUInt32(value));
	}

	if (std::isnan(value)) {
		cc->error(span, "icode.uint_nan", "converting NaN to uint");
	} else if (value < (double)std::numeric_limits<u32>::min()) {
		cc->error(span, "icode.uint_underflow", "number %1 is out of uint32 range", value);
	} else if (value > (double)std::numeric_limits<u32>::max()) {
		cc->error(span, "icode.uint_overflow", "number %1 is out of uint32 range", value);
	} else if (std::modf(value, &_) != 0.0) {
		cc->error(span, "icode.uint_inexact", "number %1 is not an integer", value);
	} else {
		return mem->create<ConstantNumber>(&ir::TypePrim::Uint, (i32)value);
	}
	return mem->create<BadValue>(&ir::TypePrim::Uint);
}

Constant* ICodeCompiler::tryReadConstantVarInitializer(VarInitializer* initializer) {
	if (!initializer) {
		return nullptr;
	}
	if ( !initializer->is_immutable ){
		return nullptr;
	}
	if ( !initializer->evaluated ){
		auto initializer_cu = initializer->owner->scope->file->cu;
		initializer_cu->require(CompilationUnit::State::CompilerPrepared);
		ICodeCompiler* compiler = initializer_cu->getOps()->getICodeCompiler();
		compiler->setTempPool(*tmp);
		compiler->compileVarInitializer(initializer);
	}
	return initializer->constant;
}

bool ICodeCompiler::isConstant(Value* value){
	match(value)
	caseof(Constant, _)
		return true;
	endmatch
	return false;
}

ConstantString* ICodeCompiler::createStringLiteral(StringRef contents){
	ConstantString* cs = mem->create<ConstantString>(contents);
	return cs;
}

}
