#include <jellyflash/icode/ICode.h>

namespace jly::icode{

const ConstantUndefined ConstantUndefined::_instance;

bool IsTerminator(Instr* instr){
	switch ( instr->tag ){
	case Instr::Tag::Br:
	case Instr::Tag::CondBr:
	case Instr::Tag::SwitchBr:
	case Instr::Tag::Ret:
	case Instr::Tag::Throw:
	case Instr::Tag::Resume:
	case Instr::Tag::IteratorNext:
		return true;
	default:
		return false;
	}
}

}