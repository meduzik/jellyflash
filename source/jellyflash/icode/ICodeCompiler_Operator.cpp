#include "ICodeCompiler.h"

namespace jly::icode{


ast::BinOp ICodeCompiler::convertOp(ast::AssignOp op) {
	switch (op) {
	case ast::AssignOp::Add: return ast::BinOp::Add;
	case ast::AssignOp::AShr: return ast::BinOp::AShr;
	case ast::AssignOp::BitAnd: return ast::BinOp::BitAnd;
	case ast::AssignOp::BitOr: return ast::BinOp::BitOr;
	case ast::AssignOp::BitXor: return ast::BinOp::BitXor;
	case ast::AssignOp::Div: return ast::BinOp::Div;
	case ast::AssignOp::LogicAnd: return ast::BinOp::LogicAnd;
	case ast::AssignOp::LogicOr: return ast::BinOp::LogicOr;
	case ast::AssignOp::LShr: return ast::BinOp::LShr;
	case ast::AssignOp::Mod: return ast::BinOp::Mod;
	case ast::AssignOp::Mul: return ast::BinOp::Mul;
	case ast::AssignOp::Shl: return ast::BinOp::Shl;
	case ast::AssignOp::Sub: return ast::BinOp::Sub;
	default: jl_unreachable;
	}
}

NumBinaryOp ICodeCompiler::convertOp(ast::BinOp op){
	switch ( op ){
	case ast::BinOp::Add: return NumBinaryOp::Add;
	case ast::BinOp::Sub: return NumBinaryOp::Sub;
	case ast::BinOp::Mul: return NumBinaryOp::Mul;
	case ast::BinOp::Div: return NumBinaryOp::Div;
	case ast::BinOp::Mod: return NumBinaryOp::Mod;
	case ast::BinOp::BitAnd: return NumBinaryOp::BitAnd;
	case ast::BinOp::BitOr: return NumBinaryOp::BitOr;
	case ast::BinOp::BitXor: return NumBinaryOp::BitXor;
	case ast::BinOp::Shl: return NumBinaryOp::Shl;
	case ast::BinOp::LShr: return NumBinaryOp::LShr;
	case ast::BinOp::AShr: return NumBinaryOp::AShr;
	default: jl_unreachable;
	}
}

double Compute(ast::BinOp op, double v1, double v2){
	switch ( op ){
	case ast::BinOp::Add: return v1 + v2;
	case ast::BinOp::Mul: return v1 * v2;
	case ast::BinOp::Div: return v1 / v2;
	case ast::BinOp::Sub: return v1 - v2;
	case ast::BinOp::Mod: return std::fmod(v1, v2);
	default: jl_unreachable;
	}
}

Value* ICodeCompiler::applyBinOp(
	InstrBuilder* builder,
	srcspan_t span,
	ast::BinOp op,
	srcspan_t lhs_loc,
	Value* lhs,
	ast::Expr* rhs_ast
) {
	switch ( op ){
	case ast::BinOp::EQ:
	case ast::BinOp::NEQ:
	case ast::BinOp::StrictEQ:
	case ast::BinOp::StrictNEQ:{
		bool eq = (op == ast::BinOp::EQ || op == ast::BinOp::StrictEQ);
		bool strict = (op == ast::BinOp::StrictEQ || op == ast::BinOp::StrictNEQ);

		Value* rhs = evaluateExprUntyped(builder, rhs_ast);
		auto lhs_class = getTypeCompareClass(lhs->type);
		auto rhs_class = getTypeCompareClass(rhs->type);
		if ( lhs_class == CompareClass::Reference && rhs_class == CompareClass::Reference ){
			return builder->emit<InstrRefCmp>(
				span,
				eq ? CmpOp::EQ : CmpOp::NEQ,
				lhs,
				rhs
			)->toValue();
		}else if ( (lhs_class == CompareClass::String && rhs->type == &ir::TypePrim::Null) || (rhs_class == CompareClass::String && lhs->type == &ir::TypePrim::Null) ){
			return builder->emit<InstrStringCmp>(
				span,
				eq ? CmpOp::EQ : CmpOp::NEQ,
				cast_to(builder, lhs_loc, lhs, &ir::TypePrim::String, CastType::Implicit),
				cast_to(builder, rhs_ast->span, rhs, &ir::TypePrim::String, CastType::Implicit)
			)->toValue();
		}else if ( lhs_class == CompareClass::String && rhs_class == CompareClass::String ){
			return builder->emit<InstrStringCmp>(
				span,
				eq ? CmpOp::EQ : CmpOp::NEQ,
				lhs,
				rhs
			)->toValue();
		}else if ( lhs_class == CompareClass::Boolean && rhs_class == CompareClass::Boolean ){
			return builder->emit<InstrBoolCmp>(
				span,
				eq ? CmpOp::EQ : CmpOp::NEQ,
				lhs,
				rhs
			)->toValue();
		}else if ( lhs_class == CompareClass::Function && rhs_class == CompareClass::Function ){
			return builder->emit<InstrFuncCmp>(
				span,
				eq ? CmpOp::EQ : CmpOp::NEQ,
				lhs,
				rhs
			)->toValue();
		} else if ((lhs_class == CompareClass::Function && rhs->type == &ir::TypePrim::Null) || (rhs_class == CompareClass::Function && lhs->type == &ir::TypePrim::Null)) {
			return builder->emit<InstrFuncCmp>(
				span,
				eq ? CmpOp::EQ : CmpOp::NEQ,
				cast_to(builder, lhs_loc, lhs, &ir::TypePrim::Function, CastType::Implicit),
				cast_to(builder, rhs_ast->span, rhs, &ir::TypePrim::Function, CastType::Implicit)
			)->toValue();
		}else if ( lhs_class == CompareClass::Numeric && rhs_class == CompareClass::Numeric ) {
			return builder->emit<InstrNumCmp>(
				span,
				eq ? CmpOp::EQ : CmpOp::NEQ,
				lhs,
				rhs
			)->toValue();
		}else if ( lhs_class == CompareClass::Dynamic || rhs_class == CompareClass::Dynamic ){
			// TODO ref_cmp
			return builder->emit<InstrDynCmp>(
				span,
				eq ? (strict ? CmpOp::SEQ : CmpOp::EQ) : (strict ? CmpOp::SNEQ : CmpOp::NEQ),
				cast_to(builder, lhs_loc, lhs, &ir::TypePrim::Any, CastType::Implicit),
				cast_to(builder, rhs_ast->span, rhs, &ir::TypePrim::Any, CastType::Implicit)
			)->toValue();
		}else if ( !strict ){
			if ( lhs_class == CompareClass::Numeric ){
				return builder->emit<InstrNumCmp>(
					span,
					eq ? CmpOp::EQ : CmpOp::NEQ,
					lhs,
					cast_to(builder, rhs_ast->span, rhs, &ir::TypePrim::Number, CastType::Implicit)
				)->toValue();
			}else if ( rhs_class == CompareClass::Numeric ){
				return builder->emit<InstrNumCmp>(
					span,
					eq ? CmpOp::EQ : CmpOp::NEQ,
					cast_to(builder, lhs_loc, lhs, &ir::TypePrim::Number, CastType::Implicit),
					rhs
				)->toValue();
			}
		}
		cc->error(span, "icode.invalid_cmp", "invalid comparison of types %1 and %2", lhs->type, rhs->type);
		return getInvalidValue();
	}break;
	case ast::BinOp::LE:
	case ast::BinOp::LT:
	case ast::BinOp::GE:
	case ast::BinOp::GT:{
		Value* rhs = evaluateExprUntyped(builder, rhs_ast);
		auto lhs_class = getTypeCompareClass(lhs->type);
		auto rhs_class = getTypeCompareClass(rhs->type);
		CmpOp cmp_op;
		switch ( op ){
		case ast::BinOp::LE:{
			cmp_op = CmpOp::LE;
		}break;
		case ast::BinOp::LT: {
			cmp_op = CmpOp::LT;
		}break;
		case ast::BinOp::GE: {
			cmp_op = CmpOp::GE;
		}break;
		case ast::BinOp::GT: {
			cmp_op = CmpOp::GT;
		}break;
		default:{
			jl_unreachable;
		}break;
		}
		if ( lhs_class == CompareClass::Numeric || rhs_class == CompareClass::Numeric ) {
			return builder->emit<InstrNumCmp>(
				span,
				cmp_op,
				cast_to(builder, lhs_loc, lhs, &ir::TypePrim::Number, CastType::Implicit),
				cast_to(builder, rhs_ast->span, rhs, &ir::TypePrim::Number, CastType::Implicit)
			)->toValue();
		}else if ( lhs_class == CompareClass::Dynamic || rhs_class == CompareClass::Dynamic ){
			cc->error(span, "icode.invalid_cmp", "invalid comparison of types %1 and %2 -- dynamic comparison invoked", lhs->type, rhs->type);
			return getInvalidValue();
		}else if ( lhs_class == CompareClass::String && rhs_class == CompareClass::String ){
			return builder->emit<InstrStringCmp>(
				span,
				cmp_op,
				cast_to(builder, lhs_loc, lhs, intrin->types.String, CastType::Implicit),
				cast_to(builder, rhs_ast->span, rhs, intrin->types.String, CastType::Implicit)
			)->toValue();
		}else{
			cc->error(span, "icode.invalid_cmp", "invalid comparison of types %1 and %2", lhs->type, rhs->type);
			return getInvalidValue();
		}
	}break;
	case ast::BinOp::LogicAnd:
	case ast::BinOp::LogicOr:{
		lhs = cast_to(builder, lhs_loc, lhs, &ir::TypePrim::Boolean, CastType::Implicit);
		BasicBlock* else_bb = createBlock();
		BasicBlock* join_bb = createBlock();
		BasicBlock* exit_l_bb = builder->bb;
		if ( op == ast::BinOp::LogicAnd ){
			builder->emit<InstrCondBr>(span, lhs, else_bb, join_bb);
		}else{
			builder->emit<InstrCondBr>(span, lhs, join_bb, else_bb);
		}
		builder->setBlock(else_bb);
		Value* rhs = evaluateExpr(builder, rhs_ast, &ir::TypePrim::Boolean);
		BasicBlock* exit_r_bb = builder->bb;
		builder->emit<InstrBr>(span, join_bb);
		builder->setBlock(join_bb);
		return createPhi(builder, span, &ir::TypePrim::Boolean, {{exit_l_bb, lhs}, {exit_r_bb, rhs}});
	}break;
	case ast::BinOp::Add:{
		Value* rhs = evaluateExprUntyped(builder, rhs_ast);
		if ( isAny(lhs->type) && isAny(rhs->type) ){
			cc->error(span, "icode.dynamic_add", "addition of two dynamic types, make a cast");
			return getInvalidValue();
		}
		if ( isNumericOrBoolean(lhs->type) && isNumericOrBoolean(rhs->type) ){
			lhs = castToNumeric(builder, lhs_loc, lhs);
			rhs = castToNumeric(builder, rhs_ast->span, rhs);
			ir::Type* unify = unifyArith(span, lhs->type, rhs->type);
			match2(lhs, rhs)
			caseof2(ConstantNumber, num1, ConstantNumber, num2)
				return castConstant(
					span, 
					mem->create<ConstantNumber>(&ir::TypePrim::Number, Compute(op, num1->value, num2->value)),
					unify,
					CastType::Implicit
				);
			endmatch2

			lhs = castToNumeric(builder, lhs_loc, lhs);
			rhs = castToNumeric(builder, rhs_ast->span, rhs);
			return builder->emit<InstrNumBinary>(span, unify, convertOp(op), lhs, rhs)->toValue();
		}
		if ( isString(lhs->type) || isString(rhs->type) ){
			return builder->emit<InstrStringConcat>(
				span,
				intrin->types.String,
				cast_to(builder, lhs_loc, lhs, intrin->types.String, CastType::Implicit),
				cast_to(builder, rhs_ast->span, rhs, intrin->types.String, CastType::Implicit)
			)->toValue();
		}
		cc->error(span, "icode.invalid_add", "cannot add values of types %1 and %2", lhs->type, rhs->type);
		return getInvalidValue();
	}break;
	case ast::BinOp::BitAnd:
	case ast::BinOp::BitOr:
	case ast::BinOp::BitXor:
	case ast::BinOp::Shl:
	case ast::BinOp::AShr:
	case ast::BinOp::LShr:{
		lhs = cast_to(builder, lhs_loc, lhs, &ir::TypePrim::Uint, CastType::Implicit);
		auto rhs = evaluateExpr(builder, rhs_ast, &ir::TypePrim::Uint);
		return builder->emit<InstrNumBinary>(span, &ir::TypePrim::Uint, convertOp(op), lhs, rhs)->toValue();
	}break;
	case ast::BinOp::Sub:
	case ast::BinOp::Mul:
	case ast::BinOp::Div:
	case ast::BinOp::Mod:{
		lhs = castToNumeric(builder, lhs_loc, lhs);
		auto rhs = castToNumeric(builder, rhs_ast->span, evaluateExprUntyped(builder, rhs_ast));

		ir::Type* unify;
		if ( op == ast::BinOp::Div ){
			unify = &ir::TypePrim::Number;
		}else{
			unify = unifyArith(span, lhs->type, rhs->type);
		}

		match2(lhs, rhs)
		caseof2(ConstantNumber, num1, ConstantNumber, num2)
			return castConstant(
				span,
				mem->create<ConstantNumber>(&ir::TypePrim::Number, Compute(op, num1->value, num2->value)),
				unify,
				CastType::Implicit
			);
		endmatch2

		if (op == ast::BinOp::Div) {
			return builder->emit<InstrNumBinary>(
				span,
				unify,
				convertOp(op),
				cast_to(builder, lhs_loc, lhs, &ir::TypePrim::Number, CastType::Implicit),
				cast_to(builder, rhs_ast->span, rhs, &ir::TypePrim::Number, CastType::Implicit)
			)->toValue();
		}

		return builder->emit<InstrNumBinary>(
			span,
			unify, 
			convertOp(op), 
			castToNumeric(builder, lhs_loc, lhs),
			castToNumeric(builder, rhs_ast->span, rhs)
		)->toValue();
	}break;
	case ast::BinOp::As:{
		ir::Type* rhs_as_type = nullptr;
		Reference rhs(nullptr);
		rhs = compileExpr(builder, rhs_ast);
		rhs_as_type = refToType(rhs);
		if (rhs_as_type){
			return cast_as(builder, span, lhs, rhs_as_type);
		}else{
			return builder->emit<InstrTypeCastDyn>(
				span,
				&ir::TypePrim::Boolean,
				cast_to(builder, lhs_loc, lhs, &ir::TypePrim::Any, CastType::Implicit),
				evaluateRefAs(builder, rhs_ast->span, rhs, intrin->types.Class),
				TypeCastOp::As
			)->toValue();
		}
	}break;
	case ast::BinOp::Is:{
		ir::Type* rhs_as_type = nullptr;
		Reference rhs(nullptr);
		rhs = compileExpr(builder, rhs_ast);
		rhs_as_type = refToType(rhs);
		if ( rhs_as_type ){
			return typetest(builder, span, lhs, rhs_as_type);
		}else{
			return builder->emit<InstrTypeCastDyn>(
				span,
				&ir::TypePrim::Boolean,
				cast_to(builder, lhs_loc, lhs, &ir::TypePrim::Any, CastType::Implicit),
				evaluateRefAs(builder, rhs_ast->span, rhs, intrin->types.Class),
				TypeCastOp::Is
			)->toValue();
		}
	}break;
	case ast::BinOp::In:{
		Value* rhs = evaluateExpr(builder, rhs_ast, &ir::TypePrim::Any);
		lhs = cast_to(builder, span, lhs, &ir::TypePrim::Any, CastType::Implicit);
		return builder->emit<InstrDynIn>(span, lhs, rhs)->toValue();
	}break;
	}
	cc->error(span, "icode.not_impl", "binop not implemented");
	return getInvalidValue();
}

Value* ICodeCompiler::applyUnOp(
	InstrBuilder* builder,
	srcspan_t span,
	ast::UnOp op,
	srcspan_t arg_loc,
	Reference arg
) {
	switch ( op ){
	case ast::UnOp::LogicInvert:{
		auto arg_val = evaluateRefAs(builder, span, arg, &ir::TypePrim::Boolean);
		return builder->emit<InstrChoice>(
			span,
			arg_val,
			&ir::TypePrim::Boolean, 
			mem->create<ConstantBool>(false),
			mem->create<ConstantBool>(true)
		)->toValue();
	}break;
	case ast::UnOp::Negate:{
		auto arg_val = castToNumeric(builder, span, evaluateRef(builder, span, arg));
		match(arg_val)
		caseof(ConstantNumber, num)
			return mem->create<ConstantNumber>(arg_val->type, -num->value);
		endmatch
		return builder->emit<InstrNumUnary>(
			span,
			arg_val->type,
			NumUnaryOp::Negate,
			arg_val
		)->toValue();
	}break;
	case ast::UnOp::UnPlus:{
		return castToNumeric(builder, span, evaluateRef(builder, span, arg));
	}break;
	case ast::UnOp::BitInvert: {
		auto arg_val = evaluateRefAs(builder, span, arg, &ir::TypePrim::Int);
		return builder->emit<InstrNumUnary>(
			span,
			arg_val->type,
			NumUnaryOp::BitInvert,
			arg_val
		)->toValue();
	}break;
	case ast::UnOp::PreInc: 
	case ast::UnOp::PreDec: 
	case ast::UnOp::PostInc: 
	case ast::UnOp::PostDec: {
		auto arg_val = castToNumeric(builder, span, evaluateRef(builder, arg_loc, arg));
		Value* ret = arg_val;
		double val = 1;
		if (op == ast::UnOp::PreDec || op == ast::UnOp::PostDec) {
			val = -1;
		}
		Value* result = builder->emit<InstrNumBinary>(
			span,
			arg_val->type, 
			NumBinaryOp::Add,
			arg_val,
			mem->create<ConstantNumber>(&ir::TypePrim::Int, val)
		)->toValue();
		if ( op == ast::UnOp::PreInc || op == ast::UnOp::PreDec ){
			ret = result;
		}
		write(builder, span, arg, result);
		return ret;
	}break;
	case ast::UnOp::Delete:{
		switch ( arg.tag ){
		case Reference::Tag::DynamicIndex:{
			builder->emit<InstrDeleteDynamicIndex>(span, arg.dynamic_index.instance, arg.dynamic_index.index);
			return mem->create<ConstantUndefined>();
		}break;
		}
		cc->error(span, "icode.bad_delete", "can only delete dynamic properties");
		return getInvalidValue();
	}break;
	}
	cc->error(span, "icode.not_impl", "unop not implemented");
	return getInvalidValue();
}


}
