#include "ICodeCompiler.h"
#include <jellyflash/icode/ICodeFlowGraph.h>

namespace jly::icode {


void ICodeCompiler::compileStmts(InstrBuilder* builder, ArrayRef<ast::Stmt*> stmts) {
	for (auto stmt : stmts) {
		compileStmt(builder, stmt);
	}
}

void ICodeCompiler::compileStmt(InstrBuilder* builder, ast::Stmt* stmt) {
	match(stmt)
	caseof(ast::StmtBlock, block)
		compileStmts(builder, block->stmts);
	caseof(ast::StmtBreak, break_)
		if ( !frame->break_targets.empty() ){
			auto target = frame->break_targets.back();
			nonlocalJump(builder, break_->span, getScopeID(), target->scope_id, target->bb);
		}else{
			cc->error(stmt->span, "icode.no_break_target", "cannot find break target");
			builder->emit<InstrUnreachable>(stmt->span);
		}
	caseof(ast::StmtContinue, continue_)
		if (!frame->continue_targets.empty()) {
			auto target = frame->continue_targets.back();
			nonlocalJump(builder, continue_->span, getScopeID(), target->scope_id, target->bb);
		} else {
			cc->error(stmt->span, "icode.no_continue_target",  "cannot find continue target");
			builder->emit<InstrUnreachable>(stmt->span);
		}
	caseof(ast::StmtDoWhile, dowhile){
		auto body_bb = createBlock();
		auto exit_bb = createBlock();
		builder->emit<InstrBr>(stmt->span, body_bb);
		builder->setBlock(body_bb);
		auto cond_bb = createBlock();
		LabelInfo* label_continue = createLabel(stmt->span, "", getScopeID(), cond_bb);
		LabelInfo* label_break = createLabel(stmt->span, "", getScopeID(), exit_bb);
		pushScopeWithContinueBreakTarget(label_continue, label_break);
		compileStmt(builder, dowhile->body);
		popScope();
		builder->emit<InstrBr>(stmt->span, cond_bb);
		builder->setBlock(cond_bb);
		auto cond_value = evaluateExpr(builder, dowhile->cond, &ir::TypePrim::Boolean);
		builder->emit<InstrCondBr>(stmt->span, cond_value, body_bb, exit_bb);
		builder->setBlock(exit_bb);
		frame->statements++;
	}
	caseof(ast::StmtExpr, expr)
		compileExprForSideEffects(builder, expr->expr);
		frame->statements++;
	caseof(ast::StmtFor, for_)
		if ( for_->init ){
			if ( for_->init->init_kind == ast::ForInit::InitKind::Expr ){
				compileExprForSideEffects(builder, for_->init->init.expr);
			}else{
				compileStmt(builder, for_->init->init.var);
			}
		}
		BasicBlock* cond_bb = createBlock();
		BasicBlock* incr_bb = for_->incr ? createBlock() : cond_bb;
		BasicBlock* body_bb = createBlock();
		BasicBlock* exit_bb = createBlock();
		
		builder->emit<InstrBr>(stmt->span, cond_bb);
		builder->setBlock(cond_bb);
		if ( for_->cond ){
			auto cond_value = evaluateExpr(builder, for_->cond, &ir::TypePrim::Boolean);
			builder->emit<InstrCondBr>(stmt->span, cond_value, body_bb, exit_bb);
		}else{
			builder->emit<InstrBr>(stmt->span, body_bb);
		}
		builder->setBlock(body_bb);
		LabelInfo* label_continue = createLabel(stmt->span, "", getScopeID(), incr_bb);
		LabelInfo* label_break = createLabel(stmt->span, "", getScopeID(), exit_bb);
		pushScopeWithContinueBreakTarget(label_continue, label_break);
		compileStmt(builder, for_->body);
		popScope();
		builder->emit<InstrBr>(stmt->span, incr_bb);
		if ( for_->incr ){
			builder->setBlock(incr_bb);
			compileExprForSideEffects(builder, for_->incr);
			builder->emit<InstrBr>(stmt->span, cond_bb);
		}
		builder->setBlock(exit_bb);
		frame->statements++;
	caseof(ast::StmtForIn, forin)
		auto gen = evaluateExprUntyped(builder, forin->gen);
		// must be any, dynamic class, or vector
		auto gen_type = gen->type;
		InstrIteratorNew* iter = nullptr;
		if ( isAny(gen_type) || gen_type->tag == ir::Type::Tag::Vector || isDynamicClass(gen_type) ){
			iter = builder->emit<InstrIteratorNew>(stmt->span, gen);
		}else{
			cc->error(stmt->span, "icode.invalid_forin", "for-in on invalid object of type %1", gen_type);
		}
		BasicBlock* loop_cond = createBlock();
		builder->emit<InstrBr>(stmt->span, loop_cond);
		builder->setBlock(loop_cond);
		BasicBlock* loop_body = createBlock();
		BasicBlock* loop_exit = createBlock();
		
		Value* next_val = nullptr;
		
		if ( isAny(gen_type) || isDynamicClass(gen_type) ){
			next_val = builder->emit<InstrIteratorNext>(
				forin->gen->span,
				iter, 
				&ir::TypePrim::Any,
				forin->each ? InstrIteratorNext::Kind::Value : InstrIteratorNext::Kind::Key,
				loop_body, 
				loop_exit
			)->toValue();
		}else if ( gen_type->tag == ir::Type::Tag::Vector ){
			if ( forin->each ){
				next_val = builder->emit<InstrIteratorNext>(
					forin->gen->span,
					iter,
					((ir::TypeVector*)gen_type)->eltTy, 
					InstrIteratorNext::Kind::Value, 
					loop_body, 
					loop_exit
				)->toValue();
			}else{
				next_val = builder->emit<InstrIteratorNext>(
					forin->gen->span,
					iter, 
					&ir::TypePrim::Uint,
					InstrIteratorNext::Kind::Key,
					loop_body, 
					loop_exit
				)->toValue();
			}
		}else{
			next_val = mem->create<ConstantNull>(&ir::TypePrim::Any);
		}
		builder->setBlock(loop_body);

		Reference ref = nullptr;
		srcspan_t span(0);
		if (forin->decl->init_kind == ast::ForInit::InitKind::Expr) {
			auto init_expr = forin->decl->init.expr;
			span = init_expr->span;
			ref = compileExpr(builder, init_expr);
		} else {
			auto init_var = forin->decl->init.var;
			if ( init_var->def->decls.getSize() > 1 ){
				cc->error(init_var->span, "icode.invalid_forin_var_decl", "too many variables in for-in declaration");
			}
			auto var_decl = init_var->def->decls[0];
			span = var_decl->name.span;
			ref = resolveInFrame(var_decl->name.span, frame, var_decl->name.id);
		}

		if (ref.tag == Reference::Tag::LocalVar) {
			write(builder, span, ref, next_val); 
		} else {
			cc->error(span, "icode.bad_forin_var", "for-in variable must be a local variable, got %1", ref);
		}

		LabelInfo* label_continue = createLabel(stmt->span, "", getScopeID(), loop_cond);
		LabelInfo* label_break = createLabel(stmt->span, "", getScopeID(), loop_exit);
		pushScopeWithContinueBreakTarget(label_continue, label_break);
		compileStmts(builder, forin->body);
		popScope();
		builder->emit<InstrBr>(stmt->span, loop_cond);

		builder->setBlock(loop_exit);
		frame->statements++;
	caseof(ast::StmtGoto, goto_)
		GotoNode* node = tmp->create<GotoNode>(
			goto_->span,
			goto_->name.id,
			builder->bb,
			getScopeID()
		);
		frame->goto_late.push_back(node);
		builder->bb = nullptr;
	caseof(ast::StmtIf, if_){
		auto cond_value = evaluateExpr(builder, if_->cond, &ir::TypePrim::Boolean);
		BasicBlock* then_bb = createBlock();
		BasicBlock* else_bb = createBlock();
		builder->emit<InstrCondBr>(stmt->span, cond_value, then_bb, else_bb);
		builder->setBlock(then_bb);
		compileStmt(builder, if_->then);
		if ( if_->else_ ){
			BasicBlock* exit_bb = createBlock();
			builder->emit<InstrBr>(if_->else_->span, exit_bb);
			builder->setBlock(else_bb);
			compileStmt(builder, if_->else_);
			builder->emit<InstrBr>(if_->else_->span, exit_bb);
			builder->setBlock(exit_bb);
		}else{
			builder->emit<InstrBr>(stmt->span, else_bb);
			builder->setBlock(else_bb);
		}
		frame->statements++;
	}
	caseof(ast::StmtLabel, label)
		BasicBlock* bb = createBlock();
		builder->emit<InstrBr>(stmt->span, bb);
		LabelInfo* label_info = tmp->create<LabelInfo>(
			cc->getLoc(label->span),
			label->name.id,
			bb,
			getScopeID()
		);
		if ( !frame->labels.insert({ label_info->name, label_info}).second ){
			cc->error(label->span, "icode.duplicate_label", "duplicate label name %1", label->name);
		}
		builder->setBlock(bb);
	caseof(ast::StmtNop, _)
		// do nothing
	caseof(ast::StmtReturn, ret){
		if (ret->expr) {
			auto value = evaluateExpr(builder, ret->expr, frame->ret_type);
			emitReturn(builder, ret->span, value);
		} else {
			if (frame->ret_type == &ir::TypePrim::Void) {
				emitReturn(builder, ret->span, mem->create<ConstantUndefined>());
			} else {
				cc->error(stmt->span, "icode.nonvoid_ret", "invalid return in a non-void function");
				builder->emit<InstrUnreachable>(stmt->span);
			}
		}
		frame->statements++;
	}
	caseof(ast::StmtSwitch, switch_)
		BasicBlock* exit_bb = createBlock();
		auto discr_value = evaluateExprUntyped(builder, switch_->expr);
		LabelInfo* label_break = createLabel(stmt->span, "", getScopeID(), exit_bb);
		pushScopeWithBreakTarget(label_break);
		BasicBlock* start_bb = builder->bb;
		BasicBlock* continuation_bb = nullptr;
		BasicBlock* case_bb = nullptr;
		BasicBlock** code_bbs = tmp->createArray<BasicBlock*>(switch_->cases.getSize());
		uz block_index = 0;
		for ( auto case_ : switch_->cases ){
			if ( !case_bb ){
				case_bb = createBlock();
			}
			if ( continuation_bb ){
				builder->setBlock(continuation_bb);
				builder->emit<InstrBr>(case_->span, case_bb);
				continuation_bb = nullptr;
			}
			code_bbs[block_index] = case_bb;
			if ( case_->stmts.getSize() > 0 ){
				builder->setBlock(case_bb);
				compileStmts(builder, case_->stmts);
				continuation_bb = builder->bb;
				case_bb = nullptr;
			}
			block_index++;
		}
		if (continuation_bb) {
			builder->setBlock(continuation_bb);
			builder->emit<InstrBr>(stmt->span, exit_bb);
		}
		block_index = 0;
		builder->setBlock(start_bb);
		BasicBlock* default_bb = nullptr;
		for (auto case_ : switch_->cases) {
			if ( case_->expr ){
				auto next_bb = createBlock();
				auto cond_val = applyBinOp(builder, case_->span, ast::BinOp::StrictEQ, switch_->expr->span, discr_value, case_->expr);
				builder->emit<InstrCondBr>(case_->span, cond_val, code_bbs[block_index], next_bb);
				builder->setBlock(next_bb);
			}else{
				default_bb = code_bbs[block_index];
			}
			block_index++;
		}
		if ( default_bb ){
			builder->emit<InstrBr>(stmt->span, default_bb);
		}else{
			builder->emit<InstrBr>(stmt->span, exit_bb);
		}
		builder->setBlock(exit_bb);
		popScope();
		frame->statements++;
	caseof(ast::StmtThrow, throw_)
		builder->emit<InstrThrow>(stmt->span, evaluateExpr(builder, throw_->expr, &ir::TypePrim::Any));
		frame->statements++;
	caseof(ast::StmtTry, try_)
		BasicBlock* finally_bb = nullptr;
		auto scope_id = getScopeID();

		BasicBlock* saved_bb = builder->bb;

		BasicBlock* catchall_bb = nullptr;
		slice_vector< std::pair<ir::Type*, BasicBlock*> > catches(tmp);
		
		if (try_->finally) {
			pushScope(true);
			finally_bb = createBlock();
			builder->setBlock(finally_bb);
			InstrPhi* phi = builder->emit<InstrPhi>(try_->span, &ir::TypePrim::Int, nullptr);
			compileStmts(builder, *try_->finally);
			auto switch_instr = builder->emit<InstrSwitchBr>(try_->span, phi->toValue(), nullptr, nullptr);
			popScope();
			pushScopeWithFinally(finally_bb, phi, switch_instr);
			catchall_bb = createBlock();
			BasicBlock* rethrow_bb = createBlock();
			builder->setBlock(catchall_bb);
			nonlocalJump(builder, try_->span, getScopeID(), scope_id, rethrow_bb);
			builder->setBlock(rethrow_bb);
			builder->emit<InstrResume>(try_->span);
		}

		BasicBlock* resume_bb = createBlock();

		if ( catchall_bb ){
			HandlerScope* finally_handler = mem->create<HandlerScope>(frame->handler);
			finally_handler->catch_all = catchall_bb;
			pushHandler(finally_handler);
		}
		for ( auto catch_ : try_->catches ){
			pushScope(true);
			auto catch_type = resolver.resolveType(scope, catch_->type);
			BasicBlock* catch_bb = createBlock();
			builder->setBlock(catch_bb);
			Reference catch_var_ref = resolveInFrame(catch_->name.span, frame, catch_->name.id);
			auto catched = builder->emit<InstrCatchPad>(catch_->span, catch_type)->toValue();
			if ( catch_var_ref.tag == Reference::Tag::LocalVar ){
				write(builder, catch_->span, catch_var_ref.local_var, catched);
			}else{
				cc->error(catch_->name.span, "icode.bad_catch", "%1 is not a valid local variable", catch_var_ref);
				builder->emit<InstrUnreachable>(catch_->span);
			}
			if (catch_type){
				catches.push_back({catch_type, catch_bb});
			}
			compileStmts(builder, catch_->stmts);
			if (try_->finally) {
				nonlocalJump(builder, try_->span, getScopeID(), scope_id, resume_bb);
			} else {
				builder->emit<InstrBr>(catch_->span, resume_bb);
			}
			popScope();
		}
		if ( catchall_bb ){
			popHandler();
		}

		HandlerScope* handler = mem->create<HandlerScope>(frame->handler);
		handler->catch_all = catchall_bb;
		handler->filters = clone(catches.begin(), catches.end());

		pushHandler(handler);
		pushScope(true);
		BasicBlock* try_bb = createBlock();
		builder->setBlock(saved_bb);
		builder->emit<InstrBr>(try_->span, try_bb);
		builder->setBlock(try_bb);
		compileStmts(builder, try_->block);
		popScope();
		popHandler();

		if (try_->finally) {
			nonlocalJump(builder, try_->span, getScopeID(), scope_id, resume_bb);
		} else {
			builder->emit<InstrBr>(try_->span, resume_bb);
		}

		if (try_->finally) {
			popScope();
		}

		builder->setBlock(resume_bb);
		frame->statements++;
	caseof(ast::StmtVar, var)
		for ( auto decl : var->def->decls ){
			if ( decl->expr ){
				write(
					builder, 
					decl->expr->span, 
					frame->cache.at(decl->name.id),
					evaluateExprUntyped(builder, decl->expr)
				);
			}
		}
		frame->statements++;
	caseof(ast::StmtWhile, while_){
		BasicBlock* cond_bb = createBlock();
		BasicBlock* exit_bb = createBlock();
		BasicBlock* body_bb = createBlock();
		builder->emit<InstrBr>(while_->span, cond_bb);
		builder->setBlock(cond_bb);
		auto cond_value = evaluateExpr(builder, while_->cond, &ir::TypePrim::Boolean);
		builder->emit<InstrCondBr>(while_->cond->span, cond_value, body_bb, exit_bb);
		builder->setBlock(body_bb);
		LabelInfo* label_continue = createLabel(stmt->span, "", getScopeID(), cond_bb);
		LabelInfo* label_break = createLabel(stmt->span, "", getScopeID(), exit_bb);
		pushScopeWithContinueBreakTarget(label_continue, label_break);
		compileStmt(builder, while_->body);
		popScope();
		builder->emit<InstrBr>(while_->span, cond_bb);
		builder->setBlock(exit_bb);
		frame->statements++;
	}
	casedef(_)
		cc->fatal(stmt->span, "icode.invalid_stmt", "unknown statement");
	endmatch
}

void ICodeCompiler::emitReturn(InstrBuilder* builder, srcspan_t span, Value* value){
	auto scope = frame->scope_id;
	bool has_finally = false;
	while ( scope ){
		if ( scope->finally ){
			has_finally = true;
			break;
		}
		scope = scope->parent;
	}
	if ( has_finally ){
		BasicBlock* bb = createBlock();
		// remove handler from the block
		bb->handler = nullptr;
		nonlocalJump(builder, span, frame->scope_id, nullptr, bb);
		builder->setBlock(bb);
	}
	builder->emit<InstrRet>(span, value);
}


}
