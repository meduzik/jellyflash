#include "ICodeCompiler.h"

namespace jly::icode {

ir::Type* ICodeCompiler::unifyTypes(srcspan_t left, ir::Type* left_ty, srcspan_t right, ir::Type* right_ty) {
	if ( areTypesEqual(left_ty, right_ty) ){
		return left_ty;
	}

	if ( isAny(left_ty) || isAny(right_ty) ){
		return &ir::TypePrim::Any;
	}

	
	if ( isNumeric(left_ty) || isNumeric(right_ty) ){
		return &ir::TypePrim::Number;
	}

	if ( isBoolean(left_ty) || isBoolean(right_ty) ){
		return &ir::TypePrim::Boolean;
	}

	// proper unify
	return left_ty;
}

ir::Type* ICodeCompiler::unifyArith(srcspan_t span, ir::Type* lhs, ir::Type* rhs){
	jl_assert(isNumeric(lhs) && isNumeric(rhs));
	if ( lhs == &ir::TypePrim::Number || rhs == &ir::TypePrim::Number ){
		return &ir::TypePrim::Number;
	}
	/*
	if ( lhs == &ir::TypePrim::Uint && rhs == &ir::TypePrim::Uint ){
		return &ir::TypePrim::Uint;
	}
	*/
	return &ir::TypePrim::Int;
}

Value* ICodeCompiler::castToNumeric(InstrBuilder* builder, srcspan_t span, Value* val){
	if ( isNumeric(val->type) ){
		return val;
	}
	if ( val->type == &ir::TypePrim::Boolean ){
		return cast_to(builder, span, val, &ir::TypePrim::Int, CastType::Implicit);
	}
	return cast_to(builder, span, val, &ir::TypePrim::Number, CastType::Implicit);
}

CompareClass ICodeCompiler::getTypeCompareClass(ir::Type* type){
	if ( isAny(type) ){
		return CompareClass::Dynamic;
	}else if ( isBoolean(type) ){
		return CompareClass::Boolean;
	}else if ( isNumeric(type) ){
		return CompareClass::Numeric;
	}else if ( isString(type) ){
		return CompareClass::String;
	}else if ( isFunction(type) ){
		return CompareClass::Function;
	}else{
		return CompareClass::Reference;
	}
}

Constant* ICodeCompiler::castConstant(srcspan_t span, Constant* val, ir::Type* target_type, CastType type){
	ir::Type* from_type = val->type;
	if (areTypesEqual(from_type, target_type)) {
		return val;
	}
	if ( val->tag == Value::Tag::CBadValue ){
		return mem->create<BadValue>(target_type);
	}
	match(val)
	caseof(ConstantBoxed, boxed)
		return castConstant(span, boxed->contents, target_type, type);
	endmatch
	if ( isBoolean(target_type) ){
		match(val)
		caseof(ConstantNumber, num)
			return mem->create<ConstantBool>(num->value != 0 && !std::isnan(num->value));
		caseof(ConstantString, str)
			return mem->create<ConstantBool>(str->value.getSize() > 0);
		caseof(ConstantNull, _)
			return mem->create<ConstantBool>(false);
		endmatch
	}else if ( isNumeric(target_type) ){
		match(val)
		caseof(ConstantBool, bool_)
			return mem->create<ConstantNumber>(target_type, bool_->value ? true : false);
		caseof(ConstantNumber, num)
			if ( target_type == &ir::TypePrim::Number ){
				return mem->create<ConstantNumber>(target_type, num->value);
			}else if ( target_type == &ir::TypePrim::Int ){
				return constantToInt32(span, num->value, type != CastType::Implicit);
			}else if ( target_type == &ir::TypePrim::Uint ){
				return constantToUInt32(span, num->value, type != CastType::Implicit);
			}
		caseof(ConstantString, str)
			cc->error(span, "icode.not_impl", "constant string to Number not implemented");
			goto end;
		caseof(ConstantUndefined, _)
			return castConstant(
				span, 
				mem->create<ConstantNumber>(&ir::TypePrim::Number, std::numeric_limits<double>::quiet_NaN()),
				target_type,
				type
			);
		caseof(ConstantNull, _)
			return mem->create<ConstantNumber>(target_type, 0.0);
		endmatch
	}else if ( isString(target_type) ){
		match(val)
		caseof(ConstantBool, bool_)
			cc->error(span, "icode.bool_to_string", "boolean to string");
			goto end;
		caseof(ConstantNumber, num)
			return createStringLiteral(StringRef(diag::decimal(num->value)).clone(*mem));
		caseof(ConstantNull, _)
			return mem->create<ConstantNull>(target_type);
		endmatch
	}else{
		if ( target_type == &ir::TypePrim::Any ){
			return mem->create<ConstantBoxed>(target_type, val);
		}
		if (val->tag == Value::Tag::CNull) {
			return mem->create<ConstantNull>(target_type);
		}
		if ( target_type == intrin->types.Object ) {
			return mem->create<ConstantBoxed>(target_type, val);
		}
	}
	cc->error(span, "icode.bad_const_cast", "Bad constant cast from %1 to %2", val->type, target_type);
end:
	return mem->create<BadValue>(target_type);
}


bool ICodeCompiler::areTypesEqual(ir::Type* lhs, ir::Type* rhs){
	if ( lhs == rhs ){
		return true;
	}
	return false;
}

ir::Type* ICodeCompiler::refToType(Reference ref){
	switch ( ref.tag ){
	case Reference::Tag::Definition:{
		match(ref.definition)
		caseof(ir::Class, class_)
			return class_->type;
		caseof(ir::Interface, iface)
			return &iface->type;
		endmatch
	}break;
	case Reference::Tag::VectorInstance:{
		return ref.vector_ty;
	}break;
	}
	return nullptr;
}




}
