#include "ICodeCompiler.h"

namespace jly::icode{

ICodeCompiler::ICodeCompiler(){
}

void ICodeCompiler::prepare(CompilerContext* cc, MemPool::Slice* mem, MemPool::Slice& tmp){
	this->cc = cc;
	this->cu = cc->getUnit();
	this->IR = cu->getIR();
	this->mem = mem;
	this->tmp = &tmp;
	this->intrin = cc->getIntrinsics();
	resolver.prepare(cc, IR, mem, tmp);
	linker.prepare(cc, IR, mem, tmp);
}

void ICodeCompiler::setTempPool(MemPool::Slice& tmp){
	this->tmp = &tmp;
	resolver.setTempPool(tmp);
	linker.setTempPool(tmp);
}

void ICodeCompiler::run(){
	compileUnit();
}

void ICodeCompiler::compileUnit(){
	compileScope(&IR->package);
	compileScope(&IR->internal);
}

void ICodeCompiler::compileScope(ir::FileScope* scope) {
	for (auto def : scope->definitions) {
		match(def)
		caseof(ir::Class, class_){
			compileStaticInitializer(class_);
			if ( class_->constructor ) {
				compileClassMethod(class_->constructor);
			}
			for (auto field : class_->instance_fields) {
				if (field->initializer && field->initializer->expr) {
					compileVarInitializer(field->initializer);
				}
			}
			for (auto field : class_->static_fields) {
				if (field->initializer && field->initializer->expr) {
					compileVarInitializer(field->initializer);
				}
			}
			for (auto method : class_->instance_methods) {
				compileClassMethod(method);
			}
			for (auto method : class_->static_methods) {
				compileClassMethod(method);
			}
		}
		caseof(ir::Interface, iface) {
			for (auto method : iface->methods) {
				compileInterfaceMethod(method);
			}
		}
		caseof(ir::GlobalVar, var){
			compileGlobalVarInitializer(var);
		}
		caseof(ir::GlobalFunction, func){
			compileGlobalFunc(func);
		}
		casedef(_){
			// TODO more codegen
		}
		endmatch
	}
}

}
