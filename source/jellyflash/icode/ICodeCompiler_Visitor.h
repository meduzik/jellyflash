#pragma once
#include "ICodeCompiler.h"

namespace jly::icode{


template<class Visitor>
void ICodeCompiler::visitStmts(ArrayRef<ast::Stmt*> stmts, Visitor&& v) {
	for (auto stmt : stmts) {
		visitStmt(stmt, v);
	}
}

template<class Visitor>
void ICodeCompiler::visitStmt(ast::Stmt* stmt, Visitor&& v) {
	if (!v(stmt)) {
		return;
	}
	match(stmt)
	caseof(ast::StmtBlock, block)
		visitStmts(block->stmts, v);
	caseof(ast::StmtBreak, break_)
	caseof(ast::StmtContinue, continue_)
	caseof(ast::StmtNop, nop)
	caseof(ast::StmtDoWhile, do_while)
		visitExpr(do_while->cond, v);
		visitStmts(do_while->body, v);
	caseof(ast::StmtExpr, expr)
		visitExpr(expr->expr, v);
	caseof(ast::StmtFor, for_)
		if (for_->init) {
			if (for_->init->init_kind == ast::ForInit::InitKind::Expr) {
				visitExpr(for_->init->init.expr, v);
			} else {
				visitStmt(for_->init->init.var, v);
			}
		}
		if (for_->cond) {
			visitExpr(for_->cond, v);
		}
		if (for_->incr) {
			visitExpr(for_->incr, v);
		}
		visitStmt(for_->body, v);
	caseof(ast::StmtForIn, forin)
		if (forin->decl->init_kind == ast::ForInit::InitKind::Expr) {
			visitExpr(forin->decl->init.expr, v);
		} else {
			visitStmt(forin->decl->init.var, v);
		}
		visitExpr(forin->gen, v);
		visitStmt(forin->body, v);
	caseof(ast::StmtGoto, goto_)
	caseof(ast::StmtIf, if_)
		visitExpr(if_->cond, v);
		visitStmt(if_->then, v);
		if (if_->else_) {
			visitStmt(if_->else_, v);
		}
	caseof(ast::StmtLabel, label)
	caseof(ast::StmtReturn, ret)
		if (ret->expr) {
			visitExpr(ret->expr, v);
		}
	caseof(ast::StmtSwitch, switch_)
		visitExpr(switch_->expr, v);
		for (auto case_ : switch_->cases) {
			if (case_->expr) {
				visitExpr(case_->expr, v);
			}
			visitStmts(case_->stmts, v);
		}
	caseof(ast::StmtThrow, throw_)
		visitExpr(throw_->expr, v);
	caseof(ast::StmtTry, try_)
		visitStmts(try_->block, v);
		for (auto catch_ : try_->catches) {
			visitStmts(catch_->stmts, v);
		}
		if (try_->finally) {
			visitStmts(*try_->finally, v);
		}
	caseof(ast::StmtVar, var)
		for (auto decl : var->def->decls) {
			if (decl->expr) {
				visitExpr(decl->expr, v);
			}
		}
	caseof(ast::StmtWhile, while_)
		visitExpr(while_->cond, v);
		visitStmts(while_->body, v);
	casedef(_)
		cc->fatal(stmt->span, "icode.invalid_statement_type", "internal error: statement type not implemented");
	endmatch
}

template<class Visitor>
void ICodeCompiler::visitExpr(ast::Expr* expr, Visitor&& v) {
	if (!v(expr)) {
		return;
	}
	match(expr)
	caseof(ast::ExprArray, array)
		for (auto elt : array->elts) {
			visitExpr(elt, v);
		}
	caseof(ast::ExprAssign, assign)
		visitExpr(assign->lhs, v);
		visitExpr(assign->rhs, v);
	caseof(ast::ExprBinOp, binop)
		visitExpr(binop->lhs, v);
		visitExpr(binop->rhs, v);
	caseof(ast::ExprCall, call)
		visitExpr(call->head, v);
		for (auto arg : call->args) {
			visitExpr(arg, v);
		}
	caseof(ast::ExprClosure, closure)
	caseof(ast::ExprConditional, cond)
		visitExpr(cond->cond, v);
		visitExpr(cond->true_br, v);
		visitExpr(cond->false_br, v);
	caseof(ast::ExprDotIndex, dotindex)
		visitExpr(dotindex->lhs, v);
	caseof(ast::ExprInstance, instance)
		visitExpr(instance->lhs, v);
	caseof(ast::ExprLiteral, lit)
	caseof(ast::ExprNew, new_)
		visitExpr(new_->head, v);
		for (auto arg : new_->args) {
			visitExpr(arg, v);
		}
	caseof(ast::ExprObject, object)
		for (auto field : object->fields) {
			visitExpr(field->value, v);
		}
	caseof(ast::ExprPrim, prim)
	caseof(ast::ExprRef, ref)
	caseof(ast::ExprSeq, seq)
		visitExpr(seq->lhs, v);
		visitExpr(seq->rhs, v);
	caseof(ast::ExprSubscript, subscript)
		visitExpr(subscript->lhs, v);
		visitExpr(subscript->arg, v);
	caseof(ast::ExprUnOp, unop)
		visitExpr(unop->expr, v);
	caseof(ast::ExprVectorShortcut, vector)
		for (auto arg : vector->values) {
			visitExpr(arg, v);
		}
	casedef(_)
		cc->fatal(expr->span, "icode.invalid_expr_type", "internal error: expression type not implemented");
	endmatch
}

}