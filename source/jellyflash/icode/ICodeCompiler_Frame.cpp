#include "ICodeCompiler.h"
#include "ICodeCompiler_Visitor.h"

#include <jellyflash/icode/ICodePrinter.h>

namespace jly::icode {

static constexpr i32 InlineThreshold = 3;

void ICodeCompiler::enterFileScope(ir::FileScope* scope){
	this->scope = scope;
	this->IR = scope->file;

	ns_scope = tmp->create<ir::NamespaceLookupScope>(tmp);
	ns_scope->open_namespaces.push_back(ir::NSPublic.def);
	ns_scope->open_namespaces.push_back(intrin->namespaces.AS3);
	for ( auto ns : scope->opened_namespaces ){
		ns_scope->open_namespaces.push_back(ns->def);
	}
}

void ICodeCompiler::compileGlobalFunc(ir::GlobalFunction* func){
	enterFileScope(func->scope);
	compileSignature(nullptr, func->signature);

	if ( func->def && func->def->body ){
		beginFrame();
		initSignature(func->signature);
		processBody(func->def->body->span, func->def->body->stmts);
		func->frame = endFrame(func->def->body->span);
		func->inlined = func->frame->statements <= InlineThreshold;
	}
}

void ICodeCompiler::compileSignature(ir::Class* class_context, ir::FunctionSignature* signature){
	for ( auto param : signature->params ){
		if ( param->initializer_def ){
			compileParam(class_context, param);
		}
	}
}

void ICodeCompiler::compileVarInitializer(icode::VarInitializer* initializer){
	if ( !initializer ){
		return;
	}
	if ( initializer->evaluated ){
		return;
	}
	initializer->evaluated = true;

	enterFileScope(initializer->owner->scope);
	beginFrame();
	match(initializer->owner)
	caseof(ir::Class, class_)
		frame->class_context = class_;
		frame->class_immediate = class_;
	endmatch

	switch ( initializer->ref.tag ){
	case Reference::Tag::ClassMember:{
		match(initializer->ref.class_member.member)
		caseof(ir::ClassField, field)
			if ( !field->is_static ){
				initThis(frame->class_context->boxType);
				initializer->ref.class_member.instance = frame->this_value;
			}
		endmatch
	}break;
	}
	frame->ret_type = &ir::TypePrim::Void;

	ast::StmtExpr expr(initializer->expr);
	collectStmts(&expr);

	auto value = evaluateExprUntyped(frame->builder, initializer->expr);
	match(value)
	caseof(Constant, constant)
		initializer->constant = castConstant(
			initializer->owner->loc.getSpan(), 
			constant,
			initializer->type, 
			CastType::Implicit
		);
	endmatch

	write(
		frame->builder,
		initializer->owner->loc.getSpan(),
		initializer->ref,
		initializer->constant ? initializer->constant : value
	);

	initializer->frame_nonconstant = endFrame(initializer->owner->loc.getSpan());
}

void ICodeCompiler::compileParam(ir::Class* class_context, ir::FunctionParam* param){
	if ( !param->initializer_def ){
		return;
	}

	beginFrame();
	frame->ret_type = &ir::TypePrim::Void;
	if (class_context ){
		frame->class_context = class_context;
		frame->class_immediate = class_context;
	}
	icode::Value* value = evaluateExpr(frame->builder, param->initializer_def, param->type);
	match(value)
	caseof(icode::Constant, constant)
		if ( constant->tag == Value::Tag::CString ){
			param->default_value = constant;
		}
		param->default_value = constant;
	casedef(_)
		cc->error(param->loc, "icode.nonconst_default_val", "non-constant default value");
		param->default_value = mem->create<BadValue>(param->type);
	endmatch
	endFrame(param->loc.getSpan());
}

void ICodeCompiler::compileInterfaceMethod(ir::InterfaceMethod* method){
	compileSignature(nullptr, method->signature);
}

void ICodeCompiler::compileClassMethod(ir::ClassMethod* method) {
	auto owner = method->owner;
	enterFileScope(owner->scope);

	compileSignature(method->owner, method->signature);

	if (method->ast_def && method->ast_def->body){
		beginFrame();

		frame->class_immediate = owner;
		frame->class_context = frame->class_immediate;

		if (!method->is_static) {
			initThis(owner->boxType);
		}
		initSignature(method->signature);
		processBody(method->ast_def->body->span, method->ast_def->body->stmts);
		method->super_call = frame->super_call;
		method->frame = endFrame(method->ast_def->body->span);

		if (cc->getEnv()->isInliningAllowed()) {
			method->inlined = method->frame->statements <= InlineThreshold;
		}
	}
}

void ICodeCompiler::compileStaticInitializer(ir::Class* class_){
	enterFileScope(class_->scope);
	
	for ( auto block : class_->static_blocks ){
		beginFrame();
		frame->class_context = frame->class_immediate = class_;
		frame->ret_type = &ir::TypePrim::Void;
		if ( block->def ){
			collectStmts(block->def->stmts);
			processBody(block->def->span, block->def->stmts);
		}else{
			ast::StmtExpr expr(block->field->decl->expr);
			collectStmts(&expr);
			write(
				frame->builder,
				block->field->loc.getSpan(), 
				{block->field, nullptr},
				evaluateExprUntyped(frame->builder, block->field->decl->expr)
			);
		}
		block->frame = endFrame(class_->loc.getSpan());
	}
}

void ICodeCompiler::compileGlobalVarInitializer(ir::GlobalVar* var){
	compileVarInitializer(var->initializer);
}

void ICodeCompiler::compileClosure(Closure* closure) {
	beginFrame();
	initSignature(closure->signature);
	processBody(closure->def->body->span, closure->def->body->stmts);
	closure->frame = endFrame(closure->def->body->span);
}

void ICodeCompiler::initThis(ir::Type* type) {
	frame->this_type = type;
	frame->this_value = mem->create<Thisval>(type);
}

void ICodeCompiler::beginFrame() {
	FunctionFrameState* next_frame = tmp->create<FunctionFrameState>(frame, tmp);
	if ( frame ){
		next_frame->this_type = frame->this_type;
	}
	frame = next_frame;
	BasicBlock* bb = createBlock();
	frame->builder = tmp->create<InstrBuilder>(bb, mem, tmp);
	frame->entry = bb;
}

void ICodeCompiler::initSignature(ir::FunctionSignature* signature) {
	frame->ret_type = signature->retTy;
	for (auto param : signature->params) {
		addParamVar(param);
	}
	if ( signature->rest ){
		addParamVar(signature->rest);
	}
}

void ICodeCompiler::processBody(srcspan_t span, ArrayRef<ast::Stmt*> stmts) {
	collectStmts(stmts);

	auto builder = frame->builder;
	
	compileStmts(builder, stmts);

	auto final_bb = builder->bb;

	for (auto node : frame->goto_late) {
		auto it = frame->labels.find(node->name);
		builder->setBlock(node->bb);
		if (it == frame->labels.end()) {
			cc->error(node->span, "icode.bad_goto", "goto target is unresolved");
			builder->emit<InstrUnreachable>(span);
		} else if (it->second->scope_id != node->scope_id) {
			nonlocalJump(builder, node->span, node->scope_id, it->second->scope_id, it->second->bb);
		} else {
			builder->emit<InstrBr>(span, it->second->bb);
		}
	}

	builder->setBlock(final_bb);
}

void ICodeCompiler::collectStmts(ArrayRef<ast::Stmt*> stmts) {
	struct {
		ICodeCompiler* compiler;

		// collect all the closures
		// collect all the variables

		bool operator()(ast::Expr* expr) {
			match(expr)
			caseof(ast::ExprClosure, closure)
				compiler->visitClosure(closure);
			endmatch
				return true;
		}

		bool operator()(ast::Stmt* stmt) {
			match(stmt)
			caseof(ast::StmtVar, var)
				compiler->visitVar(var->def);
			caseof(ast::StmtTry, try_)
				for ( auto catch_ : try_->catches ){
					compiler->visitVar(catch_->name, catch_->type);
				}
			endmatch
			return true;
		}
	} visitor{ this };
	visitStmts(stmts, visitor);
}

void ICodeCompiler::visitVar(ast::VarDef* def) {
	for (auto decl : def->decls) {
		addLocalVar(
			cc->getLoc(decl->name.span),
			decl->name.id,
			resolver.resolveType(scope, decl->type)
		);
	}
}

void ICodeCompiler::visitVar(ast::ID id, ast::Type* type) {
	addLocalVar(
		cc->getLoc(id.span),
		id.id,
		resolver.resolveType(scope, type)
	);
}

void ICodeCompiler::addLocalVar(SourceLoc loc, StringRef name, ir::Type* type) {
	LocalVar* var = mem->create<LocalVar>(
		frame->vars.size(),
		loc,
		name,
		type
	);
	if (!frame->cache.insert({ var->name, Reference(var) }).second) {
		cc->error(var->loc, "icode.duplicate_var", "variable %1 already declared:\n%2", var->name, frame->cache.at(var->name));
	} else {
		frame->vars.push_back(var);
	}
}

void ICodeCompiler::addParamVar(ir::FunctionParam* param){
	LocalVar* var = mem->create<LocalVar>(
		frame->vars.size(),
		param->loc,
		param->name,
		param->type
	);
	var->param = param;
	if (!frame->cache.insert({ var->name, Reference(var) }).second) {
		cc->error(var->loc, "icode.duplicate_var", "duplicate parameter %1", var->name, frame->cache.at(var->name));
	} else {
		frame->vars.push_back(var);
	}
}

void ICodeCompiler::visitClosure(ast::ExprClosure* def) {
	auto* closure = mem->create<Closure>(def);

	ir::FunctionSignature* signature = resolver.resolveSignature(cc->getLoc(closure->def->span), scope, closure->def->sign);
	compileSignature(frame->class_context, signature);
	closure->signature = signature;

	frame->closure_map.insert({def, closure});
	if (def->name) {
		if (!frame->cache.insert({ def->name->id, Reference(closure) }).second) {
			cc->error(
				def->span, 
				"icode.local_name_taken",
				"local name %1 already taken:\n%2", 
				def->name->id,
				frame->cache.at(def->name->id)
			);
		}
	}
	frame->closures.push_back(closure);
}

void ICodeCompiler::computeFinally(){
	auto entry = frame->entry;
	for ( auto finally : frame->finally_late ){
		auto exits = mem->createArray< std::pair<u32, BasicBlock*> >(finally->targets.size());
		auto entries = mem->createArray< std::pair<BasicBlock*, Value*> >(finally->targets.size());
		uz idx = 0;
		for (auto& target : finally->targets) {
			if ( frame->active_bbs.count(target.first) ){
				new(exits + idx)std::pair(idx, target.second);
				new(entries + idx)std::pair(target.first, mem->create<ConstantNumber>(&ir::TypePrim::Uint, (double)idx));
				idx++;
			}
		} finally->phi->branches = { entries, idx };
		finally->br->bb_branches = { exits, idx - 1 };
		finally->br->bb_default = exits[idx - 1].second;
	}
}

FuncFrame* ICodeCompiler::endFrame(srcspan_t span) {
	FuncFrame* func_frame = mem->create<FuncFrame>();
	func_frame->locals = clone(frame->vars.begin(), frame->vars.end());
	func_frame->upvals = clone(frame->upvals.begin(), frame->upvals.end());
	func_frame->entry = frame->entry;
	func_frame->closures = clone(frame->closures.begin(), frame->closures.end());
	func_frame->this_type = frame->this_type;
	func_frame->parent = frame->parent ? frame->parent->realization : nullptr;
	func_frame->this_upvalued = false;
	func_frame->statements = frame->statements;
	frame->realization = func_frame;

	for ( auto local : func_frame->locals ){
		local->frame = func_frame;
	}
	
	auto bb = frame->entry;
	traverseFlowGraph(bb);
	computeFinally();

	auto builder = frame->builder;
	if (builder->bb) {
		auto exit_bb = builder->bb;
		if ( frame->active_bbs.count(exit_bb) ) {
			if ( frame->ret_type == &ir::TypePrim::Void ){
				builder->emit<InstrRet>(span, mem->create<ConstantUndefined>());
			}else{
				cc->error(span, "icode.no_return", "control reaches the end of non-void function");
				builder->emit<InstrUnreachable>(span);
			}
		}
	}

	func_frame->blocks = frame->reachable_bbs;
	func_frame->handlers = frame->reachable_handlers;

	for (auto closure : frame->closures) {
		compileClosure(closure);
	}

	frame = frame->parent;
	return func_frame;
}

ScopeID* ICodeCompiler::getScopeID(){
	return frame->scope_id;
}

ScopeID* ICodeCompiler::pushScopeWithFinally(BasicBlock* entry_bb, InstrPhi* phi, InstrSwitchBr* switch_br){
	ScopeID* scope_id = pushScope(false);
	ScopeFinallyInfo* finally = tmp->create<ScopeFinallyInfo>(
		tmp,
		entry_bb,
		phi,
		switch_br
	);
	scope_id->finally = finally;
	return scope_id;
}

ScopeID* ICodeCompiler::pushScopeWithContinueBreakTarget(LabelInfo* continue_label, LabelInfo* break_label){
	ScopeID* scope_id = pushScope(false);
	scope_id->break_ = break_label;
	scope_id->continue_ = continue_label;
	frame->break_targets.push_back(break_label);
	frame->continue_targets.push_back(continue_label);
	return scope_id;
}

ScopeID* ICodeCompiler::pushScopeWithBreakTarget(LabelInfo* info){
	ScopeID* scope_id = pushScope(false);
	scope_id->break_ = info;
	frame->break_targets.push_back(info);
	return scope_id;
}

ScopeID* ICodeCompiler::pushScope(bool barrier){
	ScopeID* scope_id = tmp->create<ScopeID>(frame->scope_id, barrier);
	frame->scope_id = scope_id;
	return scope_id;
}

void ICodeCompiler::popScope(){
	auto scope_id = frame->scope_id;
	ScopeFinallyInfo* finally = scope_id->finally;
	if (finally) {
		frame->finally_late.push_back(finally);
	}
	if ( scope_id->break_ ){
		frame->break_targets.pop_back();
	}
	if ( scope_id->continue_ ){
		frame->continue_targets.pop_back();
	}
	frame->scope_id = scope_id->parent;
}

void ICodeCompiler::pushHandler(HandlerScope* handler){
	frame->handler = handler;
}

void ICodeCompiler::popHandler(){
	frame->handler = frame->handler->parent;
}

void ICodeCompiler::nonlocalJump(InstrBuilder* builder, srcspan_t span, ScopeID* from_scope, ScopeID* to_scope, BasicBlock* target){
	ScopeID* current = from_scope;
	ScopeID* common = getCommonAncestor(current, to_scope);
	BasicBlock* next_bb = builder->bb;

	if ( next_bb ){
		while ( current != common ){
			if (current->finally) {
				next_bb = createBlock();
				// remove handler from this block
				next_bb->handler = nullptr;
				BasicBlock* from_bb = builder->bb;
				builder->emit<InstrBr>(span, current->finally->entry_bb);
				current->finally->targets.push_back({ from_bb, next_bb });
				frame->indirect_edges.insert({ from_bb, next_bb });
				builder->setBlock(next_bb);
			}
			current = current->parent;
		}

		current = to_scope;
		while ( current != common ){
			if ( current->barrier ){
				cc->error(span, "icode.bad_nonlocal_jump", "non-local jump over a scope boundary");
			}
			current = current->parent;
		}

		builder->setBlock(next_bb);
		builder->emit<InstrBr>(span, target);
	}
}

ScopeID* ICodeCompiler::getCommonAncestor(ScopeID* left, ScopeID* right){
	if ( !left || !right){
		return nullptr;
	}
	while ( left->level > right->level ){
		left = left->parent;
	}
	while (right->level > left->level) {
		right = right->parent;
	}
	while ( left != right ){
		left = left->parent;
		right = right->parent;
	}
	return left;
}

}
