#pragma once
#include "Codegen.h"
#include <jellylib/types/StringBuilder.h>

namespace jly{

template<class FnFuncName, class FnThisUnwrap, class FnFuncInvoke>
void Codegen::writeGenericProxyImpl(
	ir::FunctionSignature* signature,
	FnFuncName&& write_func_name,
	FnThisUnwrap&& write_this_unwrap,
	FnFuncInvoke&& write_func_invoke,
	ir::Type* ret_type
) {
	if ( !ret_type ){
		ret_type = signature->retTy;
	}

	writer->write(IMPL_NS "Any ");
	write_func_name();
	writer->write("(" IMPL_NS "Object* jfl_object," IMPL_NS "RestParams* jfl_params");
	writer->writeln("){");
	{
		auto params = signature->params;
		auto func_scope = writer->indented();
		uz max_params = params.getSize();
		uz min_params = max_params;
		bool has_rest = signature->rest;
		while (min_params > 0 && params[min_params - 1]->default_value) {
			min_params--;
		}
		writer->writeln(IMPL_NS "UInt jfl_size = " IMPL_NS "rest_size(jfl_params);");
		if (min_params > 0) {
			writer->write(MACRO_PREFIX "ARGMINCHECK(jfl_size, ");
			writer->write(diag::decimal(min_params));
			writer->writeln(");");
		}
		if (!has_rest) {
			writer->write(MACRO_PREFIX "ARGMAXCHECK(jfl_size, ");
			writer->write(diag::decimal(max_params));
			writer->writeln(");");
		}
		if constexpr ( !std::is_same_v<FnThisUnwrap, nullptr_t> ) {
			writer->write("auto ");
			writer->write(THIS_NAME);
			writer->write(" = ");
			write_this_unwrap("jfl_object");
			writer->writeln(";");
		}
		for (uz i = 0; i < max_params; i++) {
			writeFieldType(params[i]->type);
			writer->write(" jfl_");
			writer->write(diag::decimal(i));
			bool has_default = params[i]->default_value;
			if (has_default) {
				writer->write(" = ");
				codegenValue(params[i]->default_value);
			}
			writer->writeln(";");
			if (has_default) {
				writer->write("if ( jfl_size > ");
				writer->write(diag::decimal(i));
				writer->writeln(" ){");
			}

			StringBuilder sb(tmp);
			sb.append("rest_get(jfl_params, ");
			sb.append(diag::decimal(i));
			sb.append(")");

			writer->write("jfl_");
			writer->write(diag::decimal(i));
			writer->write(" = ");
			codegenCastTo(tmp->create<icode::ValueAdhoc>(&ir::TypePrim::Any, sb.view()), params[i]->type);
			writer->writeln(";");

			if (has_default) {
				writer->writeln("}");
			}
		}
		if (has_rest) {
			writer->write(IMPL_NS "RestParams jfl_rest = " IMPL_NS "rest_slice(jfl_params, ");
			writer->write(diag::decimal(max_params));
			writer->writeln(");");
		}

		icode::ValueAdhoc result(ret_type, "jfl_result");
		if (ret_type != &ir::TypePrim::Void) {
			writer->write("auto jfl_result = ");
		}
		write_func_invoke();
		writer->write("(");
		bool first_arg = true;
		if constexpr ( !std::is_same_v<FnThisUnwrap, nullptr_t> ) {
			writer->write(THIS_NAME);
			first_arg = false;
		}
		for (uz i = 0; i < max_params; i++) {
			if (first_arg) {
				first_arg = false;
			} else {
				writer->write(", ");
			}
			writer->write("jfl_");
			writer->write(diag::decimal(i));
		}
		if (has_rest) {
			if (first_arg) {
				first_arg = false;
			} else {
				writer->write(", ");
			}
			writer->write("&jfl_rest");
		}
		writer->writeln(");");
		if (ret_type != &ir::TypePrim::Void) {
			writer->write("return ");
			codegenToAny(&result);
			writer->writeln(";");
		} else {
			writer->writeln("return " IMPL_NS "undefined;");
		}
	}
	writer->writeln("};");
}



void Codegen::writeGenericProxyImpl(ir::ClassMethod* method) {
	auto write_name = [&](){
		writer->write(method->owner->codegen->name);
		writer->write("::" PRIVATE_PREFIX GENERIC_PROXY_ID "_");
		writer->write(method->codegen_name);
	};
	auto write_this = [&](StringRef name) {
		uwrapClosurePayload(name, method->owner->boxType);
	};
	auto write_invoke = [&](){
		if (!method->is_static && method->codegen_virtual) {
			writer->write(THIS_NAME "->" CLASS_VTABLE_MEMBER "->");
			writer->write(method->codegen_name);
		} else {
			writeRef(method);
		}
	};
	if ( !method->is_static ){
		writeGenericProxyImpl(
			method->signature,
			write_name,
			write_this,
			write_invoke
		);
	}else{
		writeGenericProxyImpl(
			method->signature,
			write_name,
			nullptr,
			write_invoke
		);
	}
}


void Codegen::writeGenericProxyImpl(ir::GlobalFunction* func) {
	auto write_name = [&]() {
		writer->write(PRIVATE_PREFIX GENERIC_PROXY_ID "_");
		writer->write(func->codegen_name);
	};
	auto write_invoke = [&]() {
		writeRef(func);
	};
	writeGenericProxyImpl(
		func->signature,
		write_name,
		nullptr,
		write_invoke
	);
}

void Codegen::writeGenericCreateImpl(ir::Class* class_){
	auto write_name = [&]() {
		writer->write(class_->codegen->name);
		writer->write("::" STATIC_CREATE_MEMBER);
	};
	auto write_invoke = [&]() {
		writer->write(PRIVATE_PREFIX "New");
	};
	writeGenericProxyImpl(
		class_->constructor->signature,
		write_name,
		nullptr,
		write_invoke,
		&class_->classType
	);
}

void Codegen::writeGenericProxyImpl(icode::Closure* closure) {
	auto write_name = [&]() {
		writer->write(closure->frame->codegen->gclosure_name);
	};
	auto write_this = [&](StringRef name) {
		writer->write("(");
		writer->write(closure->frame->parent->codegen->ar_name);
		writer->write("*)");
		writer->write(name);
	};
	auto write_invoke = [&]() {
		writer->write(closure->frame->codegen->closure_name);
	};
	if ( closure->frame->parent->codegen->has_ar ){
		writeGenericProxyImpl(
			closure->signature,
			write_name,
			write_this,
			write_invoke
		);
	}else{
		writeGenericProxyImpl(
			closure->signature,
			write_name,
			nullptr,
			write_invoke
		);
	}
}

}
