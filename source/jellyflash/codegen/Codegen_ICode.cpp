#include "Codegen.h"
#include <jellyflash/icode/ICodePrinter.h>
#include <jellylib/utils/StringDecode.h>

namespace jly {

void Codegen::codegenPreVisitMainFrame(ir::FileScope* scope, ir::Class* class_, icode::FuncFrame* frame){
	if (!frame) {
		return;
	}

	codegenPreVisitFrame(scope, class_, frame);

	{
		auto tmp = MemPool::GetStackSlice();
		prepareFrame(scope, class_, frame);
	}
}

void Codegen::codegenPreVisitFrame(ir::FileScope* scope, ir::Class* class_, icode::FuncFrame* frame){
	for ( auto bb : frame->blocks ){
		for (auto instr : bb->instrs) {
			if (instr->result_value.type) {
				requireTypeImpl(instr->result_value.type);
			}

			match(instr)
			caseof(icode::InstrReadClassField, read)
				requireDefinitionImpl(read->field->owner);
			caseof(icode::InstrReadGlobalVar, read)
				requireDefinitionImpl(read->var);
			caseof(icode::InstrWriteClassField, write)
				requireDefinitionImpl(write->field->owner);
			caseof(icode::InstrWriteGlobalVar, write)
				requireDefinitionImpl(write->var);
			caseof(icode::InstrCallClassMethod, call)
				requireDefinitionImpl(call->method->owner);
			caseof(icode::InstrCallGlobal, call)
				requireDefinitionImpl(call->func);
			caseof(icode::InstrClassMethodClosure, closure)
				requireDefinitionImpl(closure->method->owner);
			caseof(icode::InstrGlobalFuncClosure, closure)
				requireDefinitionImpl(closure->func);
			caseof(icode::InstrNewClass, new_)
				requireDefinitionImpl(new_->class_);
			caseof(icode::InstrClassClosure, closure)
				requireDefinitionImpl(closure->class_);
			caseof(icode::InstrTypeCast, cast)
				requireTypeImpl(cast->cast_type);
			caseof(icode::InstrInterfaceClosure, closure)
				requireDefinitionImpl(closure->iface);
			endmatch
		}
	}

	for (auto closure : frame->closures) {
		codegenPreVisitFrame(scope, class_, closure->frame);
	}
}

void Codegen::writeMainFrame(icode::FuncFrame* frame) {
	if (!frame) {
		return;
	}

	icode::Printer printer;
	printer.prepare(writer, *tmp);

	//writer->writeln("/*");
	//printer.writeCode(frame);
	//writer->writeln("*/");

	this->printer = &printer;

	printer.prepareMainFrame(frame);

	writeFrame(frame);
}

void Codegen::writeFrame(icode::FuncFrame* frame){
	printer->prepareFrame(frame);
	this->frame = frame->codegen;
	this->frame->printer = printer;

	// force statics
	for (auto bb : frame->blocks) {
		for (auto instr : bb->instrs) {
			match(instr)
			caseof(icode::InstrReadClassField, read)
				requireStaticInitiailization(read->field->owner);
			caseof(icode::InstrCallClassMethod, read)
				if (read->method->is_static) {
					requireStaticInitiailization(read->method->owner);
				}
			caseof(icode::InstrClassMethodClosure, read)
				if (read->method->is_static) {
					requireStaticInitiailization(read->method->owner);
				}
			caseof(icode::InstrReadGlobalVar, read)
				requireStaticInitiailization(read->var);
			caseof(icode::InstrClassClosure, closure)
				requireStaticInitiailization(closure->class_);
			endmatch
		}
	}
	this->frame->static_cus.erase(cu);
	slice_vector<CompilationUnit*> statics(tmp);
	statics.reserve(this->frame->static_cus.size());
	for (auto other_cu : this->frame->static_cus) {
		statics.push_back(other_cu);
	}
	std::sort(statics.begin(), statics.end(), [](CompilationUnit* cu1, CompilationUnit* cu2) {
		return cu1->codegen->header_impl_include < cu2->codegen->header_impl_include;
	});
	for (auto other_cu : statics) {
		codegenRunStaticInitialize(other_cu);
	}

	uz n = frame->blocks.getSize();
	for (uz i = 0; i < n; i++) {
		icode::BasicBlock* bb = frame->blocks[i];
		icode::BasicBlock* follower = i < n - 1 ? frame->blocks[i + 1] : nullptr;
		this->frame->bb_index.insert({ bb, i });
		codegenPrologueBasicBlock(bb, follower);
	}

	if ( frame->codegen->has_ar ){
		writer->write(frame->codegen->ar_name);
		writer->writeln(" " PRIVATE_PREFIX "ar_embed{");
		{
			auto indent = writer->indented();
			writer->write("&");
			writer->write(frame->codegen->ar_name);
			writer->writeln(PRIVATE_PREFIX "VT,");
			writer->writeln("{},");
			auto parent_frame = frame->parent;
			while ( parent_frame && parent_frame->codegen->has_ar ){
				if ( parent_frame == frame->parent ){
					writer->writeln(PRIVATE_PREFIX "closure,");
				}else{
					writer->write(PRIVATE_PREFIX "closure->" PRIVATE_PREFIX "ar_");
					writer->write(diag::decimal(parent_frame->codegen->ar_idx));
					writer->writeln(",");
				}
				parent_frame = parent_frame->parent;
			}
			if ( frame->this_upvalued && !frame->parent){
				writer->writeln(THIS_NAME ",");
			}
			for (auto local : frame->locals) {
				if ( local->upvalue ) {
					if ( local->param ){
						writer->write(local->codegen_name);
					}else{
						codegenDefaultValue(local->type);
					}
					writer->writeln(",");
				}
			}
		}
		writer->writeln("};");
		writer->write(frame->codegen->ar_name);
		writer->write("* " PRIVATE_PREFIX "ar = &" PRIVATE_PREFIX "ar_embed");
		writer->writeln(";");
	}

	for (auto local : frame->locals) {
		if ( !local->param && !local->upvalue ){
			writeFieldType(local->type);
			writer->write(" ");
			writer->write(local->codegen_name);
			writer->write(" = ");
			codegenDefaultValue(local->type);
			writer->writeln(";");
		}
	}
	for (uz i = 0; i < n; i++) {
		icode::BasicBlock* bb = frame->blocks[i];
		icode::BasicBlock* follower = i < n - 1 ? frame->blocks[i + 1] : nullptr;
		codegenBasicBlock(bb, follower);
	}
}

void Codegen::codegenPrologueBasicBlock(icode::BasicBlock* bb, icode::BasicBlock* follower){
	auto visitJump = [&](icode::BasicBlock* bb){
		if ( bb != follower ){
			bb->has_jumps = true;
		}
	};
	
	for (auto instr : bb->instrs) {
		if ( instr->result_value.type && instr->result_value.type != &ir::TypePrim::Void ){
			writeFieldType(instr->result_value.type);
			writer->write(" ");
			codegenRegisterName(instr);
			writer->writeln(";");
		}

		match(instr)
		caseof(icode::InstrBr, br)
			visitJump(br->bb);
		caseof(icode::InstrCondBr, condbr)
			visitJump(condbr->bb_false);
			visitJump(condbr->bb_true);
		caseof(icode::InstrSwitchBr, switchbr)
			visitJump(switchbr->bb_default);
			for ( auto case_ : switchbr->bb_branches ){
				visitJump(case_.second);
			}
		caseof(icode::InstrIteratorNext, iter)
			match(iter->iterator->object->type)
			caseof(ir::TypeVector, vec) {
			}
			casedef(_){
				writeFieldType(instr->result_value.type);
				writer->write(" _");
				codegenRegisterName(iter);
				writer->writeln(";");
			}
			endmatch

			visitJump(iter->next_bb);
			visitJump(iter->end_bb);
		caseof(icode::InstrIteratorNew, iter)
			match(iter->object->type)
			caseof(ir::TypeVector, vec){
				writer->write(IMPL_NS "UInt ");
				codegenRegisterName(instr);
				writer->writeln(";");
			}
			casedef(_){
				writer->write(IMPL_NS "Iterator ");
				codegenRegisterName(instr);
				writer->writeln(";");
			}
			endmatch
		endmatch
	}
}

void Codegen::codegenBasicBlock(icode::BasicBlock* bb, icode::BasicBlock* follower){
	{
		auto unindent = writer->unindented();

		writer->write("// ");
		frame->printer->writeBlockHeader(bb);

		if ( bb->has_jumps ){
			writer->write("jfl_block_");
			writer->write(diag::decimal(frame->bb_index.at(bb)));
			writer->writeln(':');
		}
	}
	for ( auto instr : bb->instrs ){
		codegenInstr(instr, bb, follower);
	}
}

void Codegen::codegenCanonicalType(ir::Type* type){
	match(type)
	caseof(ir::TypePrim, prim)
		writeFieldType(type);
	caseof(ir::TypeClass, class_)
		writer->write(BASE_OBJECT_TYPE);
	caseof(ir::TypeInterface, iface)
		writer->write(BASE_INTERFACE_TYPE);
	caseof(ir::TypeVector, vec)
		writer->write(BASE_OBJECT_TYPE);
	casedef(_)
		jl_unreachable;
	endmatch
}

void Codegen::codegenVectorEltType(ir::Type* type){
	match(type)
	caseof(ir::TypePrim, prim)
		switch ( prim->kind ){
		case ir::TypePrim::Kind::Class:{
			writer->write(BASE_OBJECT_TYPE "*");
		}break;
		default:{
			writeFieldType(type);
		}break;
		}
	caseof(ir::TypeClass, class_)
		writer->write(BASE_OBJECT_TYPE "*");
	caseof(ir::TypeInterface, iface)
		writer->write(BASE_INTERFACE_TYPE);
	caseof(ir::TypeVector, vec)
		writer->write(BASE_OBJECT_TYPE "*");
	casedef(_)
		jl_unreachable;
	endmatch
}

void Codegen::writeInterfaceImplRef(ir::Class* class_, ir::Interface* iface){
	auto impl = class_->codegen->implements_map.at(iface);
	writeRef(impl->owner);
	writer->write("::");
	writer->write(impl->name);
}

void Codegen::codegenRegisterName(icode::Instr* instr){
	writer->write("jfl_r_");
	if ( !instr->index ){
		instr->index = frame->instr_index;
		frame->instr_index++;
	}
	writer->write(diag::decimal(instr->index));
}

CodegenCall* Codegen::codegenPrepareCall(ir::FunctionSignature* signature, ArrayRef<icode::Value*> args){
	CodegenCall* call_info = tmp->create<CodegenCall>();
	return call_info;
}

void Codegen::codegenCallParams(CodegenCall* call, ir::FunctionSignature* signature, ArrayRef<icode::Value*> args){
	for ( uz i = 0; i < signature->params.getSize(); i++ ){
		if ( call->arg_count > 0 ){
			writer->write(", ");
		}
		call->arg_count++;
		if ( i < args.getSize() ){
			bool is_var = signature->params[i]->type == &ir::TypeVar::Instance;
			if (is_var){
				writer->write("(");
				codegenVectorEltType(args[i]->type);
				writer->write(")");
			}
			codegenValue(args[i]);
		}else{
			auto default_value = signature->params[i]->default_value;
			if ( default_value ){
				codegenValue(signature->params[i]->default_value);
			}else{
				cc->error("codegen.bad_default", "required parameter is missing");
				codegenDefaultValue(signature->params[i]->type);
			}
		}
	}
	if ( signature->rest ){
		if (call->arg_count > 0) {
			writer->write(", ");
		}
		call->arg_count++;
		writer->write(IMPL_NS "params({");
		uz begin = signature->params.getSize();
		uz end = args.getSize();
		for (uz i = begin; i < end; i++) {
			if ( i != begin ){
				writer->write(",");
			}
			codegenValue(args[i]);
		}
		writer->write("})");
	}
}

void Codegen::codegenNumOperator(icode::NumBinaryOp op){
	switch (op) {
	case icode::NumBinaryOp::Add: {
		writer->write("+");
	}break;
	case icode::NumBinaryOp::Sub: {
		writer->write("-");
	}break;
	case icode::NumBinaryOp::Mul: {
		writer->write("*");
	}break;
	case icode::NumBinaryOp::Div: {
		writer->write("/");
	}break;
	case icode::NumBinaryOp::BitAnd: {
		writer->write("&");
	}break;
	case icode::NumBinaryOp::BitOr: {
		writer->write("|");
	}break;
	case icode::NumBinaryOp::BitXor: {
		writer->write("^");
	}break;
	case icode::NumBinaryOp::Shl: {
		writer->write("<<");
	}break;
	case icode::NumBinaryOp::AShr:
	case icode::NumBinaryOp::LShr: 
	case icode::NumBinaryOp::Mod: {
		writer->write(", ");
	}break;
	default: {
		jl_unreachable;
	}break;
	}
}

void Codegen::codegenCmpOperator(icode::CmpOp op){
	switch ( op ){
	case icode::CmpOp::EQ:
	case icode::CmpOp::SEQ:{
		writer->write("==");
	}break;
	case icode::CmpOp::NEQ:
	case icode::CmpOp::SNEQ: {
		writer->write("!=");
	}break;
	case icode::CmpOp::GT: {
		writer->write(">");
	}break;
	case icode::CmpOp::GE: {
		writer->write(">=");
	}break;
	case icode::CmpOp::LT: {
		writer->write("<");
	}break;
	case icode::CmpOp::LE: {
		writer->write("<=");
	}break;
	default:{
		jl_unreachable;
	}break;
	}
}

void Codegen::codegenToAny(icode::Value* value){
	if ( value->type == &ir::TypePrim::Any || value->type == &ir::TypePrim::Object ){
		codegenValue(value);
		return;
	}

	writer->write(IMPL_NS "Any(");
	match(value->type)
	caseof(ir::TypePrim, prim)
		switch ( prim->kind ){
		case ir::TypePrim::Kind::Class:{
			writer->write("(" BASE_OBJECT_TYPE "*)");
			codegenValue(value);
		}break;
		default:{
			codegenValue(value);
		}break;
		}
	caseof(ir::TypeClass, class_)
		writer->write("(" BASE_OBJECT_TYPE "*)");
		codegenValue(value);
	caseof(ir::TypeVector, vec)
		writer->write("(" BASE_OBJECT_TYPE "*)");
		codegenValue(value);
	casedef(_)
		codegenValue(value);
	endmatch
	writer->write(")");
}

void Codegen::codegenMonomorphic(icode::Value* value){
	match(value->type)
	caseof(ir::TypeVector, vec)
		writer->write("(" BASE_OBJECT_TYPE "*)");
	caseof(ir::TypeClass, class_)
		writer->write("(" BASE_OBJECT_TYPE "*)");
	caseof(ir::TypeInterface, iface)
		writer->write("(" BASE_INTERFACE_TYPE ")");
	casedef(_)
	endmatch
	codegenValue(value);
}

void Codegen::codegenUnpackVec(ir::Type* type){
	match(type)
	caseof(ir::TypeVector, vec)
		writer->write("(");
		writeFieldType(type);
		writer->write(")");
	caseof(ir::TypeClass, class_)
		writer->write("(");
		writeFieldType(type);
		writer->write(")");
	caseof(ir::TypeInterface, iface)
		writer->write("(");
		writeFieldType(type);
		writer->write(")");
	casedef(_)
	endmatch
}

void Codegen::codegenPackVec(ir::Type* type){
	match(type)
	caseof(ir::TypeVector, vec)
		writer->write("(" BASE_OBJECT_TYPE "*)");
	caseof(ir::TypeClass, class_)
		writer->write("(" BASE_OBJECT_TYPE "*)");
	caseof(ir::TypeInterface, iface)
		writer->write("(" BASE_INTERFACE_TYPE ")");
	casedef(_)
	endmatch
}

void Codegen::requireStaticInitiailization(ir::Class* class_){
	frame->static_cus.insert(class_->scope->file->cu);
}

void Codegen::requireStaticInitiailization(ir::GlobalVar* var){
	frame->static_cus.insert(var->scope->file->cu);
}

void Codegen::codegenRunStaticInitialize(CompilationUnit* target_unit) {
	target_unit->require(CompilationUnit::State::CodegenPrepared);

	writer->write("if ( !");
	writer->write(target_unit->codegen->ns);
	writer->write("::" PRIVATE_PREFIX STATIC_INITIALIZER_GUARD_ID "_");
	writer->write(target_unit->getName());
	writer->writeln(" ){");
	writer->write(target_unit->codegen->ns);
	writer->write("::" PRIVATE_PREFIX STATIC_INITIALIZER_ID "_");
	writer->write(target_unit->getName());
	writer->writeln("();");
	writer->writeln("}");
}


}