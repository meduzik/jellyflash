#include "Codegen.h"

namespace jly {

void Codegen::codegenCastAs(icode::Value* from, ir::Type* to) {
	ir::Type* ty = from->type;
	if ( ty == to ){
		if ( !isNullable(to) ){
			codegenCastTo(from, &ir::TypePrim::Any);
		}else{
			codegenValue(from);
		}
	}else if ( isAny(to) ){
		codegenCastTo(from, to);
	}else if ( isAny(ty) ){
		codegenCastGeneric(from, to, "as");
	}else if ( isNumeric(ty) && isNumeric(to) ){
		if ( to == &ir::TypePrim::Number ){
			writer->write(IMPL_NS "Any((" IMPL_NS "Number)");
			codegenValue(from);
			writer->write(")");
		}else{
			codegenCastGeneric(from, to, "as");
		}
	} else if (to == cc->getIntrinsics()->types.Array && ty == &ir::TypePrim::Rest) {
		writer->write(IMPL_NS "rest_to_array(");
		codegenValue(from);
		writer->write(")");
	} else {
		if (!codegenCastObject(from, to, "as")) {
			codegenCastTo(tmp->create<icode::ConstantNull>(&ir::TypePrim::Null), to);
		}
	}
}

}
