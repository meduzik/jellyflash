#include "Codegen.h"

namespace jly{


void Codegen::genHeader(FormattedWriter* writer) {
	this->writer = writer;

	writer->writeln("#pragma once");
	// obligatory include
	writer->writeln("#include <jfl/common.h>");

	slice_vector<CompilationUnit*> includes(tmp);
	includes.reserve(cu_codegen->strong_dependencies.size());
	for (auto& other_cu : cu_codegen->strong_dependencies) {
		includes.push_back(other_cu);
	}
	std::sort(includes.begin(), includes.end(), [](CompilationUnit* cu1, CompilationUnit* cu2){
		return cu1->codegen->header_include < cu2->codegen->header_include;
	});
	for (auto& other_cu : includes){
		writer->write("#include <");
		writer->write(other_cu->codegen->header_include);
		writer->writeln(">");
	}

	auto cu = cc->getUnit();
	auto package = cu->getPackage();

	for ( auto scope : {&IR->internal, &IR->package} ){
		for (auto def : scope->definitions) {
			match(def)
			caseof(ir::Class, class_) {
				if ( class_->native && class_->native->payload.getSize() > 0 ){
					writer->write("#include <");
					writer->write(cu->codegen->header_impl_include + ".inc");
					writer->writeln(">");
				}
			}
			endmatch
		}
	}

	// generate forward decls
	slice_vector<ir::Definition*> defs_vec(tmp);
	slice_multimap<StringRef, ir::Definition*> defs(tmp);
	for (auto def : cu_codegen->header_weak_dependencies) {
		auto defCU = def->scope->file->cu;
		if (defCU == cu) {
			// do not forward declare stuff from the same CU
			continue;
		}
		if (cu_codegen->strong_dependencies.count(defCU)) {
			// do not forward declare stuff we actually include
			continue;
		}
		defs.insert({ def->scope->file->cu->getPackage()->codegen->ns, def });
	}

	for (auto it = defs.begin(); it != defs.end(); ) {
		auto next_it = defs.upper_bound(it->first);
		auto name = it->first;
		writer->write("namespace ");
		writer->write(name);
		writer->writeln("{");
		defs_vec.clear();
		for (; it != next_it; it++) {
			defs_vec.push_back(it->second);
		}
		std::sort(defs_vec.begin(), defs_vec.end(), [](ir::Definition* d1, ir::Definition* d2){
			return d1->name < d2->name;
		});
		for ( auto def : defs_vec ) {
			writeForwardDecl(def);
		}
		writer->writeln("}");
	}

	writer->write("namespace ");
	writer->write(cu_codegen->ns);
	writer->writeln("{");

	// static initializer
	writer->write("extern bool ");
	writer->write(PRIVATE_PREFIX STATIC_INITIALIZER_GUARD_ID "_");
	writer->write(cu->getName());
	writer->writeln(";");
	writer->writeln("jfl_noinline");
	writer->write("void ");
	writer->write(PRIVATE_PREFIX STATIC_INITIALIZER_ID "_");
	writer->write(cu->getName());
	writer->writeln("();");

	// forward declare own stuff
	writeScopeForwardDecl(&IR->package);
	bool has_internal = (IR->internal.definitions.getSize() > 0);
	if (has_internal) {
		writer->write("namespace ");
		writePrivateNS(cu);
		writer->writeln("{");
		writeScopeForwardDecl(&IR->internal);
		writer->writeln("}");
	}

	writer->writeln();
	writeScopeHeader(&IR->package);

	if (has_internal) {
		writer->write("namespace ");
		writePrivateNS(cu);
		writer->writeln("{");
		writeScopeHeader(&IR->internal);
		writer->writeln("}");
	}

	writer->writeln("}");
}

void Codegen::writeScopeForwardDecl(ir::FileScope* scope) {
	for (auto def : scope->definitions) {
		writeForwardDecl(def);
	}
}

void Codegen::writeForwardDecl(ir::Definition* def) {
	match(def)
	caseof(ir::Class, class_){
		writer->write("struct ");
		writer->write(class_->codegen->name);
		writer->writeln(";");
	}
	caseof(ir::Interface, iface){
		writer->write("struct ");
		writer->write(iface->codegen->name);
		writer->writeln(";");
	}
	endmatch
}

void Codegen::writeScopeHeader(ir::FileScope* scope) {
	for (auto def : scope->definitions) {
		match(def)
		caseof(ir::GlobalVar, var){
			writer->write("extern ");
			writeFieldType(var->type);
			writer->write(" ");
			writer->write(var->codegen_name);
			writer->writeln(";");
		}
		caseof(ir::GlobalFunction, func) {
			writeReturnType(func->signature->retTy);
			writer->write(" ");
			writer->write(func->codegen_name);
			writer->write('(');
			writeSignature(func->signature, true, true);
			writer->writeln(");");

			writer->write(IMPL_NS "Any " PRIVATE_PREFIX GENERIC_PROXY_ID "_");
			writer->write(func->codegen_name);
			writer->write("(" IMPL_NS "Object*," IMPL_NS "RestParams*");
			writer->writeln(");");
		}
		caseof(ir::Class, class_){
			writer->write("struct " PRIVATE_PREFIX VTABLE_ID "_");
			writer->write(class_->codegen->name);
			writer->writeln("{");
			{
				auto public_indent = writer->indented();
				writer->writeln(IMPL_NS "VTableHeader " VTABLE_HEADER ";");
				writeClassVTable(class_);
			}
			writer->writeln("};");
			writer->write("struct ");
			// this is GC requirement
			writer->write("alignas(" IMPL_NS "Object) ");
			writer->write(class_->codegen->name);
			writer->writeln("{");
			{
				auto public_indent = writer->indented();

				writer->writeln("static const " IMPL_NS "Class " CLASS_MEMBER ";");
				writer->writeln("static const " IMPL_NS "TypeInfo " TYPEINFO_MEMBER ";");
				writer->writeln("static " IMPL_NS "GCFunc " GC_MEMBER ";");
				if (class_->native && class_->native->extra_gc) {
					writer->writeln("static void " GC_USER_MEMBER "(");
					writer->write(class_->codegen->name);
					writer->writeln("*);");
				}
				if (class_->need_finalizer){
					writer->writeln("static " IMPL_NS "FinalizeFunc " FINALIZER_MEMBER ";");
				}
				writer->writeln("static " IMPL_NS "DynamicFunc " DYNAMIC_MEMBER ";");
				writer->writeln("static " IMPL_NS "IterFunc " ITERATE_MEMBER ";");

				writer->writeln("static const " IMPL_NS "ClassProperty* " PROPLIST_MEMBER "[];");
				writer->writeln("static const uint32_t " PROPLIST_MASK_MEMBER ";");

				writer->writeln("jfl_noinline");
				writer->writeln("static " IMPL_NS "CreateFunc " STATIC_CREATE_MEMBER ";");

				writer->writeln("jfl_noinline");
				writer->writeln("static " IMPL_NS "DynamicFunc " STATIC_DYNAMIC_MEMBER ";");

				writer->writeln("static const " IMPL_NS "ClassProperty* " STATIC_PROPLIST_MEMBER "[];");
				writer->writeln("static const uint32_t " STATIC_PROPLIST_MASK_MEMBER ";");

				writer->writeln();
				for ( auto iface : class_->codegen->implements_list ){
					auto impl = class_->codegen->implements_map.at(iface);
					if ( impl->owner == class_ ){
						writer->write("static const ");
						writeRef(impl->iface);
						writer->write(" ");
						writer->write(impl->name);
						writer->writeln(";");
					}
				}
				writer->writeln();
				
				writer->write("const " PRIVATE_PREFIX VTABLE_ID "_");
				writer->write(class_->codegen->name);
				writer->writeln("* " CLASS_VTABLE_MEMBER ";");
				writer->writeln(IMPL_NS "ObjectHeader " OBJECT_HEADER_MEMBER ";");
				writeClassInstanceFields(class_, class_);
				if ( class_->m_dynamic ){
					writer->write(IMPL_NS "Dynamic " CLASS_DYNAMIC_MEMBER ";");
				}

				writer->writeln();
				writeClassConstructor(class_);
				for (auto method : class_->instance_methods) {
					writeClassInstanceMethod(method);
					writeGenericProxy(method);
				}
				writer->writeln();
				writeClassStaticFields(class_);
				for ( auto method : class_->static_methods ){
					writeClassStaticMethod(method);
					writeGenericProxy(method);
				}

				if (class_->native && class_->native->payload.getSize() > 0) {
					writer->write("#if defined(");
					writer->write(class_->native->payload);
					writer->writeln("_OWN)");
					writer->write(class_->native->payload);
					writer->writeln("_OWN;");
					writer->writeln("#endif");
				}
			}
			writer->writeln("};");
		}
		caseof(ir::Interface, iface){
			writer->write("struct ");
			writer->write(iface->codegen->name);
			writer->writeln("{");
			{
				auto public_indent = writer->indented();

				writer->writeln("static const " IMPL_NS "Class " CLASS_MEMBER ";");

				writeInterfaceVTable(iface);
			}
			writer->writeln("};");
		}
		casedef(def){
			// no codegen, no report
		}
		endmatch
	}
}


void Codegen::writeClassNewImpl(ir::Class* class_) {
	if ( class_->type != &class_->classType ){
		return;
	}

	writer->writeln("jfl_noinline");
	writer->write("inline ");
	writeReturnType(&class_->classType);
	writer->write(" ");
	writer->write(class_->codegen->name);
	writer->write("::" NEW_METHOD_NAME "(");
	if (class_->constructor) {
		writeSignature(class_->constructor->signature, true, true);
	}
	writer->writeln("){");
	{
		auto func_scope = writer->indented();
		writeReturnType(&class_->classType);
		writer->write(" " PRIVATE_PREFIX "newobject = " IMPL_NS "gc_new<");
		writer->write(class_->codegen->name);
		writer->writeln(">();");
		writer->write("jfl_newobject->" CLASS_VTABLE_MEMBER " = &");
		writer->write(PRIVATE_PREFIX VTABLE_INSTANCE_ID "_");
		writer->write(class_->codegen->name);
		writer->writeln(";");
		writer->write(class_->codegen->name);
		writer->writeln("::" PRECONSTRUCTOR_METHOD_NAME "(" PRIVATE_PREFIX "newobject);");
		writer->write(class_->codegen->name);
		writer->write("::" CONSTRUCTOR_METHOD_NAME "(" PRIVATE_PREFIX "newobject");
		if (class_->constructor) {
			writeProxyPass(class_->constructor->signature, false);
		}
		writer->writeln(");");
		writer->writeln("return " PRIVATE_PREFIX "newobject;");
	}
	writer->writeln("}");
}

void Codegen::writeInterfaceVTable(ir::Interface* iface){
	for ( auto impl : iface->extends ){
		writeInterfaceVTable(impl);
	}
	for ( auto method : iface->codegen->vtable ){
		writeReturnType(method->signature->retTy);
		writer->write(" ");
		writer->write("(*");
		writer->write(method->codegen_name);
		writer->write(")(");
		writer->write(BASE_OBJECT_TYPE);
		writer->write("*");
		writeSignature(method->signature, false, false);
		writer->write(')');
		writer->writeln(";");

		writer->write(IMPL_NS "Any (*" PRIVATE_PREFIX GENERIC_PROXY_ID "_");
		writer->write(method->codegen_name);
		writer->write(") (" IMPL_NS "Object*," IMPL_NS "RestParams*");
		writer->writeln(");");
	}
}

void Codegen::writeClassVTableContents(ir::Class* owner, ir::Class* class_){
	for (auto& pair : class_->codegen->vtable) {
		auto impl_method = pair.second;
		if ( impl_method ){
			writer->write("&");
			writeRef(impl_method->owner);
			writer->write("::");
			writer->write(impl_method->codegen_name);
			writer->writeln(",");
		}else{
			writer->writeln("nullptr,");
		}
	}
}

void Codegen::writeClassVTable(ir::Class* class_){
	for ( auto& pair : class_->codegen->vtable ){
		auto root_method = pair.first;
		auto impl_method = pair.second;

		if ( impl_method ){
			writeReturnType(impl_method->signature->retTy);
			writer->write(" ");
			writer->write("(*");
			writer->write(impl_method->codegen_name);
			writer->write(")(");
			writeParamType(impl_method->owner->boxType);
			writeSignature(impl_method->signature, false, false);
			writer->write(')');
			writer->writeln(";");
		}else{
			writeReturnType(root_method->signature->retTy);
			writer->write(" ");
			writer->write("(*");
			writer->write(root_method->codegen_name);
			writer->write(")(");
			writeParamType(root_method->owner->boxType);
			writeSignature(root_method->signature, false, false);
			writer->write(')');
			writer->writeln(";");
		}
	}
}

void Codegen::writeClassInstanceFields(ir::Class* owner, ir::Class* class_){
	if ( class_->extends ){
		writeClassInstanceFields(owner, class_->extends);
	}
	for (auto field : class_->instance_fields) {
		writeClassField(owner, field);
	}
	if (class_->native && class_->native->payload.getSize() > 0) {
		writer->write("#if defined(");
		writer->write(class_->native->payload);
		writer->writeln(")");
		writer->write(class_->native->payload);
		writer->writeln(";");
		writer->writeln("#endif");
	}
}

void Codegen::writeClassStaticFields(ir::Class* class_) {
	for ( auto field : class_->static_fields ){
		writeClassField(class_, field);
	}
}

void Codegen::writeClassField(ir::Class* owner, ir::ClassField* field) {
	if (field->is_static) {
		writer->write("static ");
	}
	writeFieldType(field->type);
	writer->write(" ");
	if ( owner != field->owner && field->ns == &ir::NSPrivate ){
		writer->write(PRIVATE_PREFIX NS_ID);
		writer->write(field->owner->codegen->name);
		writer->write("_" NS_ID);
	}
	writer->write(field->codegen_name);
	writer->writeln(";");
}

void Codegen::writeClassConstructor(ir::Class* class_){
	if (class_->type != &class_->classType) {
		return;
	}

	writer->writeln("jfl_noinline");
	writer->write("static ");
	writeReturnType(&class_->classType);
	writer->write(" " NEW_METHOD_NAME "(");
	if ( class_->constructor ){
		writeSignature(class_->constructor->signature, true, true);
	}
	writer->writeln(");");

	if ( class_->native && class_->native->destructor ){
		writer->write("~");
		writer->write(class_->codegen->name);
		writer->writeln("();");
	}

	writer->write("static void");
	writer->write(" " PRECONSTRUCTOR_METHOD_NAME "(");
	writeParamType(&class_->classType);
	writer->write(" " THIS_NAME);
	writer->writeln(");");

	writer->write("static void");
	writer->write(" " CONSTRUCTOR_METHOD_NAME "(");
	writeParamType(&class_->classType);
	writer->write(" " THIS_NAME);
	if (class_->constructor) {
		writeSignature(class_->constructor->signature, false, true);
	}
	writer->writeln(");");
}

void Codegen::writeClassInstanceMethod(ir::ClassMethod* method) {
	jl_assert(!method->is_static);

	writeAttributesDecl(method);

	writer->write("static ");
	writeReturnType(method->signature->retTy);
	writer->write(" ");
	writer->write(method->codegen_name);
	writer->write('(');
	writeParamType(method->owner->boxType);
	writer->write(" " THIS_NAME);
	writeSignature(method->signature, false, true);
	writer->writeln(");");
}


void Codegen::writeClassStaticMethod(ir::ClassMethod* method){
	jl_assert(method->is_static);

	writeAttributesDecl(method);

	writer->write("static ");
	writeReturnType(method->signature->retTy);
	writer->write(" ");
	writer->write(method->codegen_name);
	writer->write('(');
	writeSignature(method->signature, true, true);
	writer->writeln(");");
}

void Codegen::writeGenericProxy(ir::ClassMethod* method){
	writer->write("static " IMPL_NS "Any " PRIVATE_PREFIX GENERIC_PROXY_ID "_");
	writer->write(method->codegen_name);
	writer->write("(" IMPL_NS "Object*," IMPL_NS "RestParams*");
	writer->writeln(");");
}


}