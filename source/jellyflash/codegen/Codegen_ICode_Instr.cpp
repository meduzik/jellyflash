#include "Codegen.h"
#include <jellyflash/icode/ICodePrinter.h>
#include <jellylib/utils/StringDecode.h>

namespace jly {

namespace{

void writeSourceLine(FormattedWriter* writer, StringRef str){
	writer->write("// ");
	for (uz i = 0; i < str.getSize(); i++) {
		u8 ch = str[i];
		if ( ch == '\t' ){
			writer->write(' ');
		}else{
			writer->write(ch);
		}
	}
	writer->writeln();
}

void writeSourceInclude(FormattedWriter* writer, StringRef str){
	uz begin = 0;
	for ( uz i = 0; i <= str.getSize(); i++ ){
		if ( i == str.getSize() || str[i] == '\n' || str[i] == '\r' ){
			if ( i - begin > 1 ){
				writeSourceLine(writer, StringRef(str.getData() + begin, (i - begin)));
			}
			begin = i + 1;
		}
	}
}

}

void Codegen::codegenInstr(icode::Instr* instr, icode::BasicBlock* current_bb, icode::BasicBlock* follower) {
	srcoffset_t offset = instr->span.getBegin();
	u32 line = ~0u;
	if ( offset != ~0u ){
		line = cu->getSourceFile()->getLineMap().getLineByOffset(offset);
	}

	if ( frame->last_line != line ){
		if (line == ~0u){
			writer->writeln("// <unknown location>");
		}else{
			writer->write("// ");
			writer->write(cu->getSourceFile()->getPath());
			writer->write(": ");
			
			writer->write(diag::decimal(line + 1));
			writer->writeln(": ");
			writeSourceInclude(writer, cu->getSourceFile()->getLine(line));
		}
		frame->last_line = line;
	}
	writer->write("// ");
	frame->printer->writeInstr(instr);
	writer->writeln();
	match(instr)
	caseof(icode::InstrBr, br)
		codegenGoto(br->bb, current_bb, follower);
	caseof(icode::InstrCondBr, br)
		writer->write("if ( ");
		codegenValue(br->cond);
		writer->writeln(" ) {");
		codegenGoto(br->bb_true, current_bb, follower);
		writer->writeln("} else {");
		codegenGoto(br->bb_false, current_bb, follower);
		writer->writeln("}");
	caseof(icode::InstrSwitchBr, br){
		writer->write("switch ( ");
		codegenValue(br->cond);
		writer->writeln(" ){");
		for ( auto branch : br->bb_branches ){
			writer->write("case ");
			writer->write(diag::decimal(branch.first));
			writer->writeln(":{");
			codegenGoto(branch.second, current_bb, follower);
			writer->writeln("}break;");
		}
		writer->writeln("default:{");
		codegenGoto(br->bb_default, current_bb, follower);
		writer->writeln("}break;");
		writer->writeln("}");
	}
	caseof(icode::InstrUnreachable, _)
		writer->writeln("jfl_unreachable;");
	caseof(icode::InstrRet, ret)
		if (ret->value->type == &ir::TypePrim::Void) {
			writer->writeln("return;");
		} else {
			writer->write("return ");
			codegenValue(ret->value);
			writer->writeln(";");
		}
	caseof(icode::InstrChoice, choice) {
		codegenRegisterName(choice);
		writer->write(" = (");
		codegenValue(choice->sel);
		writer->write(") ? (");
		codegenValue(choice->val_true);
		writer->write(") : (");
		codegenValue(choice->val_false);
		writer->writeln(");");
	}
	caseof(icode::InstrNewClass, new_) {
		auto ctor = new_->class_->constructor;
		auto call_info = codegenPrepareCall(ctor->signature, new_->args);
		codegenRegisterName(new_);
		writer->write(" = ");
		writeRef(new_->class_);
		writer->write("::" PRIVATE_PREFIX "New(");
		codegenCallParams(call_info, ctor->signature, new_->args);
		writer->writeln(");");
	}
	caseof(icode::InstrNewVector, new_){
		CodegenCall* call_info = codegenPrepareCall(
			cc->getIntrinsics()->classes.Vector->constructor->signature,
			new_->args
		);
		codegenRegisterName(new_);
		writer->write(" = " IMPL_NS "Vector<");
		codegenVectorEltType(new_->vec->eltTy);
		writer->write(">::New(&" IMPL_NS "VectorVT<");
		writeFieldType(new_->vec->eltTy);
		writer->write(",");
		codegenVectorEltType(new_->vec->eltTy);
		writer->write(">, &");
		writeTypeClass(new_->vec->eltTy);
		call_info->arg_count++;
		codegenCallParams(
			call_info,
			cc->getIntrinsics()->classes.Vector->constructor->signature,
			new_->args
		);
		writer->writeln(");");
	}
	caseof(icode::InstrConsVector, cons) {
		codegenRegisterName(cons);
		writer->write(" = " IMPL_NS "Vector<");
		codegenVectorEltType(cons->type->eltTy);
		writer->write(">::Cons(&" IMPL_NS "VectorVT<");
		writeFieldType(cons->type->eltTy);
		writer->write(",");
		codegenVectorEltType(cons->type->eltTy);
		writer->write(">, &");
		writeTypeClass(cons->type->eltTy);
		writer->write(", ");
		codegenValue(cons->arg);
		writer->writeln(");");
	}
	caseof(icode::InstrCallVectorMethod, call){
		CodegenCall* call_info = codegenPrepareCall(
			call->method->signature,
			call->args
		);
		codegenNullCheck(call->thisval);
		if ( call->result_value.type != &ir::TypePrim::Void ){
			codegenRegisterName(call);
			writer->write(" = ");
		}
		if ( call->method->signature->retTy == &ir::TypeVar::Instance ){
			writer->write("(");
			writeFieldType(call->result_value.type);
			writer->write(")");
		}
		codegenValue(call->thisval);
		writer->write("->");
		writer->write(call->method->codegen_name);
		writer->write("(");
		codegenCallParams(
			call_info,
			call->method->signature,
			call->args
		);
		writer->writeln(");");
	}
	caseof(icode::InstrReadVectorIndex, read){
		codegenNullCheck(read->object);
		codegenRegisterName(read);
		writer->write(" = ");
		codegenUnpackVec(((ir::TypeVector*)read->object->type)->eltTy);
		codegenValue(read->object);
		writer->write("->get(");
		codegenValue(read->index);
		writer->writeln(");");
	}
	caseof(icode::InstrWriteVectorIndex, write){
		codegenNullCheck(write->object);
		codegenValue(write->object);
		writer->write("->set(");
		codegenValue(write->index);
		writer->write(", ");
		codegenPackVec(((ir::TypeVector*)write->object->type)->eltTy);
		codegenValue(write->value);
		writer->writeln(");");
	}
	caseof(icode::InstrIteratorNew, iter){
		match ( iter->object->type )
		caseof(ir::TypeClass, class_)
			writer->write("if (");
			codegenValue(iter->object);
			writer->write(" != nullptr){");
			codegenValue(iter->object);
			writer->write("->" CLASS_VTABLE_MEMBER "->" VTABLE_HEADER ".iterate(");
			writer->write("(" BASE_OBJECT_TYPE "*)");
			codegenValue(iter->object);
			writer->write(", &");
			codegenRegisterName(iter);
			writer->writeln(");}else{");
			writer->write(IMPL_NS "null_iter(&");
			codegenRegisterName(iter);
			writer->writeln(");}");
		caseof(ir::TypeVector, vec)
			codegenRegisterName(iter);
			writer->writeln(" = 0;");
		caseof(ir::TypePrim, prim)
			writer->write(IMPL_NS "any_iter(");
			codegenValue(iter->object);
			writer->write(", &");
			codegenRegisterName(iter);
			writer->writeln(");");
		casedef(_)
			jl_unreachable;
		endmatch
	}
	caseof(icode::InstrIteratorNext, iter){
		match(iter->iterator->object->type)
		caseof(ir::TypeVector, vec){
			writer->write("if ( ");
			codegenValue(iter->iterator->object);
			writer->write(" == nullptr || ");
			codegenRegisterName(iter->iterator);
			writer->write(" >= ");
			codegenValue(iter->iterator->object);
			writer->writeln("->length ){");
			codegenGoto(iter->end_bb, current_bb, follower);
			writer->writeln("}else{");
			if (iter->kind == icode::InstrIteratorNext::Kind::Key) {
				codegenRegisterName(iter);
				writer->write(" = ");
				codegenRegisterName(iter->iterator);
				writer->writeln(";");
			}else{
				codegenRegisterName(iter);
				writer->write(" = ");
				codegenUnpackVec(vec->eltTy);
				codegenValue(iter->iterator->object);
				writer->write("->ptr[");
				codegenRegisterName(iter->iterator);
				writer->writeln("];");
			}
			codegenRegisterName(iter->iterator);
			writer->writeln("++;");
			codegenGoto(iter->next_bb, current_bb, follower);
			writer->writeln("}");
		}
		casedef(_){
			writer->write("if ( ");
			codegenRegisterName(iter->iterator);
			writer->write(".next(&");
			codegenRegisterName(iter->iterator);
			writer->write(", ");
			if ( iter->kind == icode::InstrIteratorNext::Kind::Key ){
				writer->write("&");
				codegenRegisterName(iter);
				writer->write(", &_");
				codegenRegisterName(iter);
			}else{
				writer->write("&_");
				codegenRegisterName(iter);
				writer->write(", &");
				codegenRegisterName(iter);
			}
			writer->writeln(") ){");
			codegenGoto(iter->next_bb, current_bb, follower);
			writer->writeln("} else {");
			codegenGoto(iter->end_bb, current_bb, follower);
			writer->writeln("}");
		}
		endmatch
	}
	caseof(icode::InstrLocalClosure, closure){
		bool own_closure = closure->func->frame->parent == frame->owner;
		if ( frame->has_ar && own_closure ){
			writer->write(MACRO_PREFIX "PREPARE_CLOSURE(");
			writer->write(frame->ar_name);
			writer->write(", ");
			writer->write(PRIVATE_PREFIX "ar, ");
			writer->write("&" PRIVATE_PREFIX "ar_embed");
			writer->writeln(");");
		}
		codegenRegisterName(closure);
		writer->write(" = {");
		if (frame->has_ar) {
			if ( !own_closure ){
				if ( closure->func->frame->parent == frame->owner->parent ){
					writer->write("(" BASE_OBJECT_TYPE "*)" PRIVATE_PREFIX "closure, ");
				}else{
					writer->write("(" BASE_OBJECT_TYPE "*)" PRIVATE_PREFIX "closure->" PRIVATE_PREFIX "ar_");
					writer->write(diag::decimal(closure->func->frame->parent->codegen->ar_idx));
					writer->write(", ");
				}
			}else{
				writer->write("(" BASE_OBJECT_TYPE "*)" PRIVATE_PREFIX "ar, ");
			}
		}else{
			writer->write("nullptr, ");
		}
		writer->write(closure->func->frame->codegen->gclosure_name);
		writer->writeln("};");
	}
	caseof(icode::InstrGlobalFuncClosure, closure) {
		codegenRegisterName(closure);
		writer->write(" = {");
		writer->write("nullptr, &");
		writeNamespaceForScope(closure->func->scope);
		writer->write("::");
		writer->write(PRIVATE_PREFIX GENERIC_PROXY_ID "_");
		writer->write(closure->func->codegen_name);
		writer->writeln("};");
	}
	caseof(icode::InstrInterfaceMethodClosure, closure) {
		codegenNullCheck(closure->object);
		codegenRegisterName(closure);
		writer->write(" = {");
		codegenValue(closure->object);
		writer->write("." IFACE_OBJECT_MEMBER ", ");
		codegenValue(closure->object);
		writer->write("." IFACE_VTABLE_MEMBER "->" PRIVATE_PREFIX GENERIC_PROXY_ID "_");
		writer->write(closure->method->codegen_name);
		writer->writeln("};");
	}
	caseof(icode::InstrClassMethodClosure, closure) {
		codegenNullCheck(closure->object);
		codegenRegisterName(closure);
		writer->write(" = {");
		if (closure->object) {
			writer->write("((" BASE_OBJECT_TYPE "*)");
			codegenValue(closure->object);
			writer->write(")");
		} else {
			writer->write("nullptr");
		}
		writer->write(", &");
		writeRef(closure->method->owner);
		writer->write("::" PRIVATE_PREFIX GENERIC_PROXY_ID "_");
		writer->write(closure->method->codegen_name);
		writer->writeln("};");
	}
	caseof(icode::InstrClassClosure, closure) {
		codegenRegisterName(closure);
		writer->write(" = (" IMPL_NS "Class*)&");
		writeRef(closure->class_);
		writer->write("::" CLASS_MEMBER);
		writer->writeln(";");
	}
	caseof(icode::InstrInterfaceClosure, closure) {
		codegenRegisterName(closure);
		writer->write(" = (" IMPL_NS "Class*)&");
		writeRef(closure->iface);
		writer->write("::" CLASS_MEMBER);
		writer->writeln(";");
	}
	caseof(icode::InstrRefCmp, cmp) {
		codegenRegisterName(cmp);
		writer->write(" = ");
		bool need_cast = false;
		if (cmp->lhs->type != cmp->rhs->type || cmp->lhs->type->tag == ir::Type::Tag::Interface) {
			need_cast = true;
		}
		if (need_cast) {
			codegenObjectRef(cmp->lhs);
		} else {
			codegenValue(cmp->lhs);
		}
		if (cmp->op == icode::CmpOp::EQ) {
			writer->write(" == ");
		} else if (cmp->op == icode::CmpOp::NEQ) {
			writer->write(" != ");
		} else {
			jl_unreachable;
		}
		if (need_cast) {
			codegenObjectRef(cmp->rhs);
		} else {
			codegenValue(cmp->rhs);
		}
		writer->writeln(";");
	}
	caseof(icode::InstrFuncCmp, cmp) {
		codegenRegisterName(cmp);
		writer->write(" = ");
		if (cmp->op == icode::CmpOp::EQ) {
		} else if (cmp->op == icode::CmpOp::NEQ) {
			writer->write("!");
		} else {
			jl_unreachable;
		}
		writer->write(IMPL_NS "func_eq(");
		codegenValue(cmp->lhs);
		writer->write(", ");
		codegenValue(cmp->rhs);
		writer->writeln(");");
	}
	caseof(icode::InstrStringConcat, concat){
		codegenRegisterName(concat);
		writer->write(" = ");
		writer->write(IMPL_NS "str_concat(");
		codegenValue(concat->lhs);
		writer->write(", ");
		codegenValue(concat->rhs);
		writer->writeln(");");
	}
	caseof(icode::InstrStringCmp, cmp) {
		codegenRegisterName(cmp);
		writer->write(" = ");
		bool reverse = false;
		switch (cmp->op) {
		case icode::CmpOp::EQ: {
			writer->write(IMPL_NS "str_eq");
		}break;
		case icode::CmpOp::NEQ: {
			writer->write("!" IMPL_NS "str_eq");
		}break;
		case icode::CmpOp::LT: {
			writer->write(IMPL_NS "str_lt");
		}break;
		case icode::CmpOp::GT: {
			writer->write(IMPL_NS "str_lt");
			reverse = true;
		}break;
		case icode::CmpOp::LE: {
			writer->write(!IMPL_NS "str_lt");
			reverse = true;
		}break;
		case icode::CmpOp::GE: {
			writer->write(!IMPL_NS "str_lt");
		}break;
		default: {
			jl_unreachable;
		}break;
		}
		writer->write("(");
		codegenValue(reverse ? cmp->rhs : cmp->lhs);
		writer->write(", ");
		codegenValue(reverse ? cmp->lhs : cmp->rhs);
		writer->writeln(");");
	}
	caseof(icode::InstrDynCmp, cmp) {
		codegenRegisterName(cmp);
		writer->write(" = ");
		switch (cmp->op) {
		case icode::CmpOp::EQ: {
			writer->write(IMPL_NS "any_eq");
		}break;
		case icode::CmpOp::NEQ: {
			writer->write("!" IMPL_NS "any_eq");
		}break;
		case icode::CmpOp::SEQ: {
			writer->write(IMPL_NS "any_seq");
		}break;
		case icode::CmpOp::SNEQ: {
			writer->write("!" IMPL_NS "any_seq");
		}break;
		default: {
			jl_unreachable;
		}break;
		}
		writer->write("(");
		codegenValue(cmp->lhs);
		writer->write(", ");
		codegenValue(cmp->rhs);
		writer->writeln(");");
	}
	caseof(icode::InstrTypeCast, cast){
		codegenRegisterName(cast);
		writer->write(" = ");
		switch ( cast->op ){
		case icode::TypeCastOp::To:{
			codegenCastTo(cast->object, cast->cast_type);
		}break;
		case icode::TypeCastOp::As: {
			codegenCastAs(cast->object, cast->cast_type);
		}break;
		case icode::TypeCastOp::Is: {
			codegenCastIs(cast->object, cast->cast_type);
		}break;
		}
		writer->writeln(";");
	}
	caseof(icode::InstrTypeCastDyn, cast) {
		codegenRegisterName(cast);
		writer->write(" = ");
		switch (cast->op) {
		case icode::TypeCastOp::To: {
			jl_unreachable;
		}break;
		case icode::TypeCastOp::As: {
			writer->write(IMPL_NS "any_as_dyn(");
			codegenValue(cast->object);
			writer->write(", ");
			codegenValue(cast->cast_type);
			writer->write(")");
		}break;
		case icode::TypeCastOp::Is: {
			writer->write(IMPL_NS "any_is_dyn(");
			codegenValue(cast->object);
			writer->write(", ");
			codegenValue(cast->cast_type);
			writer->write(")");
		}break;
		}
		writer->writeln(";");
	}
	caseof(icode::InstrNumCmp, cmp){
		codegenRegisterName(cmp);
		writer->write(" = ");
		ir::Type* left_ty = cmp->lhs->type;
		ir::Type* right_ty = cmp->lhs->type;
		if ( left_ty == right_ty ){
			codegenValue(cmp->lhs);
			codegenCmpOperator(cmp->op);
			codegenValue(cmp->rhs);
		}else if ( left_ty == &ir::TypePrim::Number || right_ty == &ir::TypePrim::Number ){
			writer->write("((" IMPL_NS "Number)");
			codegenValue(cmp->lhs);
			writer->write(")");
			codegenCmpOperator(cmp->op);
			writer->write("((" IMPL_NS "Number)");
			codegenValue(cmp->rhs);
			writer->write(")");
		}else{
			writer->write("((int64_t)");
			codegenValue(cmp->lhs);
			writer->write(")");
			codegenCmpOperator(cmp->op);
			writer->write("((int64_t)");
			codegenValue(cmp->rhs);
			writer->write(")");
		}
		writer->writeln(";");
	}
	caseof(icode::InstrBoolCmp, cmp) {
		codegenRegisterName(cmp);
		writer->write(" = ");
		codegenValue(cmp->lhs);
		codegenCmpOperator(cmp->op);
		codegenValue(cmp->rhs);
		writer->writeln(";");
	}
	caseof(icode::InstrNumUnary, unop){
		codegenRegisterName(unop);
		writer->write(" = ");
		writer->write("(");
		writeFieldType(unop->result_value.type);
		writer->write(")");
		switch (unop->op) {
		case icode::NumUnaryOp::BitInvert: {
			writer->write("~");
		}break;
		case icode::NumUnaryOp::Negate: {
			writer->write("-");
		}break;
		}
		writer->write("(");
		codegenValue(unop->arg);
		writer->writeln(");");
	}
	caseof(icode::InstrNumBinary, binop){
		codegenRegisterName(binop);
		writer->write(" = ");
		writer->write("(");
		writeFieldType(binop->result_value.type);
		writer->write(")");
		switch ( binop->op ){
		case icode::NumBinaryOp::AShr:{
			writer->write(IMPL_NS "ashr");
		}break;
		case icode::NumBinaryOp::LShr: {
			writer->write(IMPL_NS "lshr");
		}break;
		case icode::NumBinaryOp::Mod: {
			writer->write(IMPL_NS "mod");
		}break;
		}
		writer->write("(");
		codegenValue(binop->lhs);
		codegenNumOperator(binop->op);
		codegenValue(binop->rhs);
		writer->writeln(");");
	}
	caseof(icode::InstrNumUnary, unop){
		writer->writeln("abort();");
	}
	caseof(icode::InstrReadLocal, read) {
		codegenRegisterName(read);
		writer->write(" = ");
		if (read->var->upvalue) {
			writer->write(PRIVATE_PREFIX "ar->");
			writer->write(read->var->codegen_name);
		} else {
			writer->write(read->var->codegen_name);
		}
		writer->writeln(";");
	}
	caseof(icode::InstrWriteLocal, write) {
		if ( write->var->upvalue ){
			writer->write(PRIVATE_PREFIX "ar->");
			writer->write(write->var->codegen_name);
		}else{
			writer->write(write->var->codegen_name);
		}
		writer->write(" = ");
		codegenValue(write->value);
		writer->writeln(";");
	}
	caseof(icode::InstrReadUpvalue, read) {
		codegenRegisterName(read);
		writer->write(" = ");
		writer->write(PRIVATE_PREFIX "closure->");
		if (read->value->var->frame != frame->owner->parent) {
			writer->write(PRIVATE_PREFIX "ar_");
			writer->write(diag::decimal(read->value->var->frame->codegen->ar_idx));
			writer->write("->");
		}
		writer->write(read->value->var->codegen_name);
		writer->writeln(";");
	}
	caseof(icode::InstrWriteUpvalue, write) {
		writer->write(PRIVATE_PREFIX "closure->");
		if ( write->upvalue->var->frame != frame->owner->parent ){
			writer->write(PRIVATE_PREFIX "ar_");
			writer->write(diag::decimal(write->upvalue->var->frame->codegen->ar_idx));
			writer->write("->");
		}
		writer->write(write->upvalue->var->codegen_name);
		writer->write(" = ");
		codegenValue(write->value);
		writer->writeln(";");
	}
	caseof(icode::InstrReadGlobalVar, read) {
		codegenRegisterName(read);
		writer->write(" = ");
		writeRef(read->var);
		writer->writeln(";");
	}
	caseof(icode::InstrWriteGlobalVar, write) {
		writeRef(write->var);
		writer->write(" = ");
		codegenValue(write->value);
		writer->writeln(";");
	}
	caseof(icode::InstrReadClassField, field) {
		codegenNullCheck(field->thisval);
		codegenRegisterName(field);
		writer->write(" = ");
		if (field->thisval) {
			codegenValue(field->thisval);
			writer->write("->");
			writer->write(field->field->codegen_name);
		} else {
			writeRef(field->field->owner);
			writer->write("::");
			writer->write(field->field->codegen_name);
		}
		writer->writeln(";");
	}
	caseof(icode::InstrWriteClassField, field) {
		codegenNullCheck(field->thisval);
		if (field->thisval) {
			codegenValue(field->thisval);
			writer->write("->");
			writer->write(field->field->codegen_name);
		} else {
			writeRef(field->field->owner);
			writer->write("::");
			writer->write(field->field->codegen_name);
		}
		writer->write(" = ");
		codegenValue(field->value);
		writer->writeln(";");
	}
	caseof(icode::InstrCallGlobal, call) {
		auto call_info = codegenPrepareCall(call->func->signature, call->args);
		if (call->result_value.type != &ir::TypePrim::Void) {
			codegenRegisterName(call);
			writer->write(" = ");
		}
		writeRef(call->func);
		writer->write("(");
		codegenCallParams(call_info, call->func->signature, call->args);
		writer->writeln(");");
	}
	caseof(icode::InstrNewDynamic, call) {
		codegenNullCheck(call->class_);
		if (call->result_value.type != &ir::TypePrim::Void) {
			codegenRegisterName(call);
			writer->write(" = ");
		}
		codegenValue(call->class_);
		writer->write("->create((" BASE_OBJECT_TYPE "*)");
		codegenValue(call->class_);
		writer->write(", " IMPL_NS "params({");
		uz n = call->args.getSize();
		for (uz i = 0; i < n; i++) {
			if (i != 0) {
				writer->write(",");
			}
			codegenValue(call->args[i]);
		}
		writer->writeln("}));");
	}
	caseof(icode::InstrCallClosure, call) {
		bool own_closure = call->closure->frame->parent == frame->owner;
		if (frame->has_ar && own_closure) {
			writer->write(MACRO_PREFIX "PREPARE_CLOSURE(");
			writer->write(frame->ar_name);
			writer->write(", ");
			writer->write(PRIVATE_PREFIX "ar, ");
			writer->write("&" PRIVATE_PREFIX "ar_embed");
			writer->writeln(");");
		}
		auto call_info = codegenPrepareCall(call->closure->signature, call->args);
		if (call->result_value.type != &ir::TypePrim::Void) {
			codegenRegisterName(call);
			writer->write(" = ");
		}
		writer->write(call->closure->frame->codegen->closure_name);
		writer->write("(");
		if (frame->has_ar) {
			if (!own_closure) {
				if (call->closure->frame->parent == frame->owner->parent) {
					writer->write(PRIVATE_PREFIX "closure");
				} else {
					writer->write(PRIVATE_PREFIX "closure->" PRIVATE_PREFIX "ar_");
					writer->write(diag::decimal(call->closure->frame->parent->codegen->ar_idx));
				}
			} else {
				writer->write(PRIVATE_PREFIX "ar");
			}
			call_info->arg_count++;
		} else {
		}
		codegenCallParams(call_info, call->closure->signature, call->args);
		writer->writeln(");");
	}
	caseof(icode::InstrCallDynamic, call) {
		codegenNullCheck(call->func);
		if (call->result_value.type != &ir::TypePrim::Void) {
			codegenRegisterName(call);
			writer->write(" = ");
		}
		codegenValue(call->func);
		writer->write(".invoke(" IMPL_NS "params({");
		uz n = call->args.getSize();
		for (uz i = 0; i < n; i++) {
			if (i != 0) {
				writer->write(",");
			}
			codegenValue(call->args[i]);
		}
		writer->writeln("}));");
	}
	caseof(icode::InstrDynIn, dyn){
		codegenRegisterName(dyn);
		writer->write(" = ");
		writer->write(IMPL_NS "any_in(");
		codegenValue(dyn->object);
		writer->write(", ");
		codegenValue(dyn->key);
		writer->writeln(");");
	}
	caseof(icode::InstrReadDynamicIndex, read) {
		codegenRegisterName(read);
		writer->write(" = ");
		writer->write(IMPL_NS "any_get(");
		codegenToAny(read->object);
		writer->write(", ");
		codegenToAny(read->index);
		writer->writeln(");");
	}
	caseof(icode::InstrWriteDynamicIndex, write) {
		writer->write(IMPL_NS "any_set(");
		codegenToAny(write->object);
		writer->write(", ");
		codegenToAny(write->index);
		writer->write(", ");
		codegenToAny(write->value);
		writer->writeln(");");
	}
	caseof(icode::InstrWriteArrayIndex, write) {
		writer->write(IMPL_NS "array_set(");
		codegenMonomorphic(write->object);
		writer->write(", ");
		writer->write(diag::decimal(write->index));
		writer->write(", ");
		codegenMonomorphic(write->value);
		writer->writeln(");");
	}
	caseof(icode::InstrWriteNamedField, write) {
		writer->write(IMPL_NS "named_field_set(");
		codegenMonomorphic(write->object);
		writer->write(", \"");
		EncodeCLiteral(writer, write->name);
		writer->write("\", ");
		codegenMonomorphic(write->value);
		writer->writeln(");");
	}
	caseof(icode::InstrDeleteDynamicIndex, del) {
		writer->write(IMPL_NS "any_del(");
		codegenToAny(del->object);
		writer->write(", ");
		codegenToAny(del->index);
		writer->writeln(");");
	}
	caseof(icode::InstrRestLength, len){
		codegenRegisterName(len);
		writer->write(" = " IMPL_NS "rest_size(");
		codegenValue(len->value);
		writer->writeln(");");
	}
	caseof(icode::InstrRestToArray, toarr){
		codegenRegisterName(toarr);
		writer->write(" = ");
		writer->write(IMPL_NS "rest_to_array(");
		codegenValue(toarr->value);
		writer->writeln(");");
	}
	caseof(icode::InstrPhi, phi){
		// do nothing
	}
	caseof(icode::InstrCallClassMethod, call) {
		codegenNullCheck(call->thisval);
		auto call_info = codegenPrepareCall(call->method->signature, call->args);
		if (call->result_value.type != &ir::TypePrim::Void) {
			codegenRegisterName(call);
			writer->write(" = ");
		}
		if (call->thisval) {
			if (
				call->nonvirtual
				||
				!call->method->codegen_virtual
				||
				(&call->method->owner->classType != call->method->owner->type)
				) {
				// direct call
				writeRef(call->method);
			} else {
				// vtable call
				codegenValue(call->thisval);
				writer->write("->" CLASS_VTABLE_MEMBER "->");
				writer->write(call->method->codegen_name);
			}
		} else {
			writeRef(call->method);
		}
		writer->write("(");
		if (call->thisval) {
			if (call->method->owner->type != call->thisval->type) {
				writer->write("((");
				writeRef(call->method->owner);
				writer->write("*)");
				codegenValue(call->thisval);
				writer->write(")");
			} else {
				codegenValue(call->thisval);
			}
			call_info->arg_count++;
		}
		codegenCallParams(call_info, call->method->signature, call->args);
		writer->writeln(");");
	}
	caseof(icode::InstrCallInterfaceMethod, call) {
		codegenNullCheck(call->thisval);
		auto call_info = codegenPrepareCall(call->method->signature, call->args);
		if (call->result_value.type != &ir::TypePrim::Void) {
			codegenRegisterName(call);
			writer->write(" = ");
		}
		if (call->thisval) {
			codegenValue(call->thisval);
			writer->write("." IFACE_VTABLE_MEMBER "->");
			writer->write(call->method->codegen_name);
		}
		writer->write("(");
		codegenValue(call->thisval);
		writer->write("." IFACE_OBJECT_MEMBER);
		call_info->arg_count++;
		codegenCallParams(call_info, call->method->signature, call->args);
		writer->writeln(");");
	}
	caseof(icode::InstrThrow, throw_){
		writer->write(IMPL_NS "exc_throw(");
		codegenValue(throw_->value);
		writer->writeln(");");
	}
	caseof(icode::InstrResume, resume){
		writer->writeln(IMPL_NS "exc_resume();");
	}
	caseof(icode::InstrCatchPad, catchpad) {
		codegenRegisterName(catchpad);
		writer->write(" = ");
		codegenCastAs(
			tmp->create<icode::ValueAdhoc>(&ir::TypePrim::Any, IMPL_NS "exc_catchpad()"), 
			catchpad->result_value.type
		);
		writer->writeln(";");
	}
	casedef(_)
		writer->writeln("abort();");
	endmatch
}

void Codegen::codegenGoto(icode::BasicBlock* bb, icode::BasicBlock* current_bb, icode::BasicBlock* follower) {
	for ( auto instr : bb->instrs ){
		match(instr)
		caseof(icode::InstrPhi, phi)
			bool found = false;
			for ( auto branch : phi->branches ){
				if ( branch.first == current_bb ){
					codegenRegisterName(phi);
					writer->write(" = ");
					codegenValue(branch.second);
					writer->writeln(";");
					found = true;
					break;
				}
			}
			jl_assert(found);
		casedef(_)
			break;
		endmatch
	}
	if (bb != follower) {
		writer->write("goto jfl_block_");
		writer->write(diag::decimal(frame->bb_index.at(bb)));
		writer->writeln(";");
	}
}

void Codegen::codegenNullCheck(icode::Value* value){
	if ( !value ){
		return;
	}
	if ( !isNullable(value->type) ){
		return;
	}
	match(value)
	caseof(icode::Thisval, thisval)
		return;
	endmatch
	match(value->type)
	caseof(ir::TypePrim, prim){
		switch ( prim->kind ){
		case ir::TypePrim::Kind::Any:
		case ir::TypePrim::Kind::Object:
		case ir::TypePrim::Kind::String: {
			writer->write(MACRO_PREFIX "CHECKNULLTAG(");
			codegenValue(value);
			writer->writeln(".tag);");
		}break;
		case ir::TypePrim::Kind::Function: {
			writer->write(MACRO_PREFIX "CHECKNULL(!");
			codegenValue(value);
			writer->writeln(".has_function());");
		}break;
		case ir::TypePrim::Kind::Class: {
			writer->write(MACRO_PREFIX "CHECKNULL(");
			codegenValue(value);
			writer->writeln(" == nullptr);");
		}break;
		case ir::TypePrim::Kind::Rest: {
		}break;
		case ir::TypePrim::Kind::Null: {
			writer->write(MACRO_PREFIX "CHECKNULL(true);");
		}break;
		default:{
			jl_unreachable;
		}break;
		}
	}
	caseof(ir::TypeClass, class_){
		writer->write(MACRO_PREFIX "CHECKNULL(");
		codegenValue(value);
		writer->writeln(" == nullptr);");
	}
	caseof(ir::TypeVector, vec){
		writer->write(MACRO_PREFIX "CHECKNULL(");
		codegenValue(value);
		writer->writeln(" == nullptr);");
	}
	caseof(ir::TypeInterface, vec) {
		writer->write(MACRO_PREFIX "CHECKNULL(");
		codegenValue(value);
		writer->writeln("." IFACE_OBJECT_MEMBER " == nullptr);");
	}
	endmatch
}


}