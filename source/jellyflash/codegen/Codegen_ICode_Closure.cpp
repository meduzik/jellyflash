#include "Codegen.h"
#include <jellylib/types/StringBuilder.h>

namespace jly{

void Codegen::writePreFrame(icode::FuncFrame* frame){
	if ( !frame ){
		return;
	}

	if ( frame->closures.getSize() > 0 ){
		writer->write("namespace ");
		writer->writeln("{");

		writePreFrameForwardDeclare(frame);
	
		for (auto closure : frame->closures) {
			writeClosure(closure);
		}

		writer->writeln("}");
	}
}

void Codegen::writePreFrameForwardDeclare(icode::FuncFrame* frame){
	writeActivationRecord(frame);
	for ( auto closure : frame->closures ) {
		writeForwardDeclareClosure(closure);
		writePreFrameForwardDeclare(closure->frame);
	}
}

StringRef Codegen::generateARName(CodegenFrame* frame){
	StringBuilder sb(tmp);
	sb.append(PRIVATE_PREFIX);
	sb.append(static_ns);
	sb.append("_AR_");
	sb.append(diag::decimal(frame->ar_idx));
	return sb.get(mem);
}

StringRef Codegen::generateClosureName(CodegenFrame* frame){
	StringBuilder sb(tmp);
	sb.append(PRIVATE_PREFIX);
	sb.append(static_ns);
	sb.append("closure_");
	sb.append(diag::decimal(frame->ar_idx));
	return sb.get(mem);
}

StringRef Codegen::generateGenericClosureName(CodegenFrame* frame) {
	StringBuilder sb(tmp);
	sb.append(PRIVATE_PREFIX);
	sb.append(static_ns);
	sb.append("gclosure_");
	sb.append(diag::decimal(frame->ar_idx));
	return sb.get(mem);
}

void Codegen::writeActivationRecord(icode::FuncFrame* frame){
	if ( frame->codegen->ar_locals.empty() && !frame->this_upvalued ){
		auto frame_parent = frame->parent;
		if ( !frame_parent || !frame_parent->codegen->has_ar ){
			return;
		}
	}

	frame->codegen->has_ar = true;
	frame->codegen->ar_name = generateARName(frame->codegen);

	writer->write("struct ");
	writer->write("alignas(" IMPL_NS "Object) ");
	writer->write(frame->codegen->ar_name);
	writer->writeln("{");
	{
		auto indent = writer->indented();
		writer->writeln("static const " IMPL_NS "VTable " PRIVATE_PREFIX "VT;");
		writer->writeln("const " IMPL_NS "VTable* " CLASS_VTABLE_MEMBER ";");
		writer->writeln(IMPL_NS "ObjectHeader " OBJECT_HEADER_MEMBER ";");
		if (frame->this_upvalued && !frame->parent) {
			writeFieldType(frame->this_type);
			writer->writeln(" " THIS_NAME ";");
		}
		auto frame_parent = frame->parent;
		while ( frame_parent && frame_parent->codegen->has_ar ){
			writer->write(frame_parent->codegen->ar_name);
			writer->write("* " PRIVATE_PREFIX "ar_");
			writer->write(diag::decimal(frame_parent->codegen->ar_idx));
			writer->writeln(";");
			frame_parent = frame_parent->parent;
		}
		for ( auto local : frame->codegen->ar_locals ){
			writeFieldType(local->type);
			writer->write(" ");
			writer->write(local->codegen_name);
			writer->writeln(";");
		}
	}
	writer->writeln("};");
	writer->write("const " IMPL_NS "VTable ");
	writer->write(frame->codegen->ar_name);
	writer->writeln(PRIVATE_PREFIX "VT{");
	{
		auto indent = writer->indented();
		writer->writeln("{");
		{
			auto indent = writer->indented();
			writer->writeln("&" IMPL_NS "MethodClosure_TI,");
			writer->write("(sizeof(");
			writer->write(frame->codegen->ar_name);
			writer->writeln(") + 31) & ~31,");
			// GC
			writer->writeln("[](" IMPL_NS "Object* jfl_this, void*)->void{");
			{
				auto indent = writer->indented();
				writer->write(frame->codegen->ar_name);
				writer->write("* self = (");
				writer->write(frame->codegen->ar_name);
				writer->writeln("*)jfl_this;");
				if (frame->this_upvalued && !frame->parent) {
					writeGCVisit(frame->this_type, "self", "->", THIS_NAME);
				}
				auto frame_parent = frame->parent;
				while (frame_parent && frame_parent->codegen->has_ar) {
					writer->write(IMPL_NS "gc_visit((" BASE_OBJECT_TYPE "*)self->" PRIVATE_PREFIX "ar_");
					writer->write(diag::decimal(frame_parent->codegen->ar_idx));
					writer->writeln(");");
					frame_parent = frame_parent->parent;
				}
				for (auto local : frame->codegen->ar_locals) {
					writeGCVisit(local->type, "self", "->", local->codegen_name);
				}
			}
			writer->writeln("},");
			// Finalizer
			writer->writeln("nullptr,");
			// Dynamic - never used
			writer->writeln("nullptr,");
			// Iterate - never used
			writer->writeln("nullptr");
		}
		writer->writeln("}");
	}
	writer->writeln("};");
}

void Codegen::writeClosure(icode::Closure* closure) {
	auto signature = closure->signature;
	writeReturnType(signature->retTy);
	writer->write(" ");
	writer->write(closure->frame->codegen->closure_name);
	writer->write("(");
	bool has_ar = false;
	if ( closure->frame->parent->codegen->has_ar ){
		writer->write(closure->frame->parent->codegen->ar_name);
		writer->write("* jfl_closure");
		has_ar = true;
	}
	writeSignature(signature, !has_ar, true);
	writer->writeln("){");
	{
		auto indent = writer->indented();
		writeMainFrame(closure->frame);
	}
	writer->writeln("}");
	writeGenericProxyImpl(closure);

	for (auto subclosure : closure->frame->closures) {
		writeClosure(subclosure);
	}
}

void Codegen::writeForwardDeclareClosure(icode::Closure* closure) {
	closure->frame->codegen->closure_name = generateClosureName(closure->frame->codegen);
	closure->frame->codegen->gclosure_name = generateGenericClosureName(closure->frame->codegen);

	auto signature = closure->signature;
	writeReturnType(signature->retTy);
	writer->write(" ");
	writer->write(closure->frame->codegen->closure_name);
	writer->write("(");
	bool has_ar = false;
	if (closure->frame->parent->codegen->has_ar) {
		writer->write(closure->frame->parent->codegen->ar_name);
		writer->write("* jfl_closure");
		has_ar = true;
	}
	writeSignature(signature, !has_ar, false);
	writer->writeln(");");

	writer->write(IMPL_NS "Any");
	writer->write(" ");
	writer->write(closure->frame->codegen->gclosure_name);
	writer->writeln("(" BASE_OBJECT_TYPE "*, " IMPL_NS "RestParams*);");
}


}