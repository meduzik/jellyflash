#include "Codegen.h"
#include <jellylib/types/StringBuilder.h>


namespace jly{

namespace {

void buildHeaderIncludeString(std::string& out, CUPackage* package) {
	if (package->getParent()) {
		buildHeaderIncludeString(out, package->getParent());
		out.push_back('/');
		out.append(package->getName());
	} else {
		out.append(PREFIX_NS);
	}
}

void buildHeaderIncludeString(std::string& out, CompilationUnit* cu) {
	buildHeaderIncludeString(out, cu->getPackage());
	out.push_back('/');
	out.append(cu->getName());
	out.append(".decl.h");
}

void buildHeaderImplIncludeString(std::string& out, CompilationUnit* cu) {
	buildHeaderIncludeString(out, cu->getPackage());
	out.push_back('/');
	out.append(cu->getName());
	out.append(".h");
}

void buildNamespace(std::string& out, CUPackage* package) {
	if (package->getParent()) {
		buildNamespace(out, package->getParent());
		out.append("::");
		out.append(package->getName());
	} else {
		out.append(PREFIX_NS);
	}
}

}

void Codegen::prepare(CompilerContext* cc, MemPool::Slice* mem, MemPool::Slice& tmp) {
	this->cc = cc;
	this->cu = cc->getUnit();
	this->IR = cu->getIR();
	this->mem = mem;
	this->tmp = &tmp;

	cu->codegen = mem->create<CodegenCU>();
	cu_codegen = cu->codegen;

	CUPackage* package = cu->getPackage();
	preparePackage(package);

	buildHeaderIncludeString(cu_codegen->header_include, cu);
	buildHeaderImplIncludeString(cu_codegen->header_impl_include, cu);
	cu_codegen->ns = package->codegen->ns;
	static_ns = mangleStaticNamespace(this->cu->getFullName());

	prepareScope(&IR->package);
	prepareScope(&IR->internal);
}

void Codegen::preparePackage(CUPackage* package) {
	if (package->codegen) {
		return;
	}

	auto parent = package->getParent();
	if (parent) {
		preparePackage(parent);
	}
	package->codegen = mem->create<CodegenPackage>();
	if (parent) {
		package->codegen->ns = parent->codegen->ns;
		package->codegen->ns += "::";
		package->codegen->ns += package->getName();
	} else {
		package->codegen->ns = PREFIX_NS;
	}
}

void Codegen::prepareScope(ir::FileScope* scope) {
	auto scope_codegen = mem->create<CodegenFileScope>();
	scope->codegen = scope_codegen;

	for (auto def : scope->definitions) {
		match(def)
		caseof(ir::GlobalFunction, func)
			func->codegen_name = mangleIdentifier(nullptr, func->name);
			prepareSignature(scope, nullptr, func->signature);
		caseof(ir::GlobalVar, var)
			var->codegen_name = mangleIdentifier(nullptr, var->name);
			prepareType(var->type);
		caseof(ir::Class, class_)
			class_->need_finalizer = class_->native || class_->m_dynamic;
			class_->codegen = mem->create<CodegenClass>(mem);
			class_->codegen->name = mangleIdentifier(nullptr, class_->name);
			if (class_->extends) {
				prepareDefinitionOwner(class_->extends);
				class_->need_finalizer |= class_->extends->need_finalizer;
				cu_codegen->strong_dependencies.insert(class_->extends->scope->file->cu);
			}
			for (auto impl : class_->implements) {
				prepareDefinitionOwner(impl);
				cu_codegen->strong_dependencies.insert(impl->scope->file->cu);
			}
			for (auto field : class_->instance_fields) {
				field->codegen_name = mangleIdentifier(field->ns->def, field->name);
				prepareType(field->type);
			}
			for (auto field : class_->static_fields) {
				field->codegen_name = mangleIdentifier(field->ns->def, field->name, MangleKind::Static);
				prepareType(field->type);
			}
			for (auto method : class_->instance_methods) {
				method->codegen_name = mangleIdentifier(method->ns->def, method->name, MangleKind::Normal, method->accessor);
				prepareSignature(scope, class_, method->signature);
				if (
					(!method->is_final)
					&& 
					(!class_->m_final)
					&& 
					(method->ns != &ir::NSPrivate)
				){
					method->codegen_virtual = true;
				}
			}
			if ( class_->extends ){
				for ( auto& pair : class_->extends->codegen->vtable ){
					auto it = class_->override_methods_map.find(pair.first);
					class_->codegen->vtable.push_back({ pair.first, it != class_->override_methods_map.end() ? it->second : nullptr });
				}
			}
			for (auto method : class_->instance_methods) {
				if ( method->root_method == method && method->codegen_virtual ){
					class_->codegen->vtable.push_back({method, method});
				}
			}
			for (auto method : class_->static_methods) {
				method->codegen_name = mangleIdentifier(method->ns->def, method->name, MangleKind::Static, method->accessor);
				prepareSignature(scope, class_, method->signature);
			}
			if ( class_->constructor ){
				auto method = class_->constructor;
				method->codegen_name = PRIVATE_PREFIX "Constructor";
				prepareSignature(scope, class_, method->signature);
			}
			prepareImplVTable(class_);
		caseof(ir::Interface, iface)
			iface->codegen = mem->create<CodegenInterface>(mem);
			iface->codegen->name = mangleIdentifier(nullptr, iface->name);
			for (auto extends : iface->extends) {
				prepareDefinitionOwner(extends);
				cu_codegen->strong_dependencies.insert(extends->scope->file->cu);
			}
			for (auto method : iface->methods) {
				method->codegen_name = mangleIdentifier(nullptr, method->name, MangleKind::Normal, method->accessor);
				prepareSignature(scope, nullptr, method->signature);
				iface->codegen->vtable.push_back(method);
			}
		endmatch
	}
}


void Codegen::uses(ir::Definition* def) {
	uses(def->scope->file->cu);
	cu_codegen->header_weak_dependencies.insert(def);
}

void Codegen::uses(CompilationUnit* cu) {
	if (cu != this->cu) {
		cu_codegen->weak_dependencies.insert(cu);
	}
}

void Codegen::prepareDefinitionOwner(ir::Definition* def){
	auto def_cu = def->scope->file->cu;
	if ( cu != def_cu ){
		def_cu->require(CompilationUnit::State::CodegenPrepared);
	}
}

void Codegen::prepareType(ir::Type* type) {
	match(type)
	caseof(ir::TypeClass, type)
		uses(type->class_);
	caseof(ir::TypeInterface, type)
		uses(type->iface);
	caseof(ir::TypePrim, type)
		// do nothing
	caseof(ir::TypeVector, type)
		prepareType(type->eltTy);
	endmatch
}

void Codegen::prepareSignature(ir::FileScope* scope, ir::Class* class_context, ir::FunctionSignature* signature){
	prepareType(signature->retTy);
	for ( auto param : signature->params ){
		param->codegen_name = mangleIdentifier(param);
		prepareType(param->type);
	}
	if ( signature->rest ){
		signature->rest->codegen_name = mangleIdentifier(signature->rest);
	}
}

void Codegen::prepareFrame(ir::FileScope* scope, ir::Class* class_, icode::FuncFrame* frame){
	if ( !frame ){
		return;
	}

	frame->codegen = mem->create<CodegenFrame>(mem, frame);
	frame->codegen->ar_idx = closure_index;
	closure_index++;

	for ( auto local : frame->locals ){
		if (local->param) {
			local->codegen_name = local->param->codegen_name;
		} else {
			local->codegen_name = mangleIdentifier(local);
		}

		if ( local->upvalue ){
			// this local has an upvalue, it should make its way into the 
			// activation record for this frame
			frame->codegen->ar_locals.push_back(local);
		}
	}

	for ( auto closure : frame->closures ){
		closure->codegen = mem->create<CodegenClosure>(frame->this_type);
		prepareSignature(scope, class_, closure->signature);
		requireSignatureImpl(closure->signature);
		prepareFrame(scope, class_, closure->frame);
		closure->frame->codegen->closure = closure;
	}
}

void Codegen::prepareImplVTable(ir::Class* class_){
	for ( auto impl : class_->all_implements_list ){
		requireDefinitionImpl(impl);

		CodegenInterfaceImpl* codegen_impl = mem->create<CodegenInterfaceImpl>(mem, class_, impl);
		StringBuilder sb(tmp);
		sb.append(PRIVATE_PREFIX INTERFACE_IMPL_ID "_");
		sb.append(impl->name);
		codegen_impl->name = sb.get(mem);

		auto write_vtable = [&](ir::Interface* iface){
			for (auto method : iface->codegen->vtable) {
				auto it = class_->implement_methods_map.find(method);
				if (it != class_->implement_methods_map.end()) {
					codegen_impl->vtable.push_back(it->second);
				} else {
					codegen_impl->vtable.push_back(nullptr);
				}
			}
		};

		for (auto impl : impl->extends) {
			write_vtable(impl);
		}
		write_vtable(impl);
		
		if ( class_->extends ){
			auto& parent_implements = class_->extends->codegen->implements_map;
			auto it = parent_implements.find(impl);
			if (it != parent_implements.end()){
				auto parent_codegen_impl = it->second;
				auto& parent_vtable = parent_codegen_impl->vtable;
				bool match = true;
				for ( uz i = 0; i < parent_vtable.size(); i++ ){
					if (parent_vtable[i] != codegen_impl->vtable[i]){
						match = false;
						break;
					}
				}
				if ( match ){
					codegen_impl = parent_codegen_impl;
				}
			}
		}
		if ( class_->codegen->implements_map.insert({impl, codegen_impl}).second ){
			class_->codegen->implements_list.push_back(impl);
		}
	}
}



}