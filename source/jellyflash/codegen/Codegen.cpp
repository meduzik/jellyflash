#include "Codegen.h"

namespace jly {
const StringRef FormattedWriter::Tabs("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t");


Codegen::Codegen():
	static_ns(nullptr)
{
}


namespace {

void computePackagePathInternal(std::filesystem::path& path, CUPackage* package) {
	if (package->getParent()) {
		computePackagePathInternal(path, package->getParent());
		path /= std::string_view(package->getName());
	}
}

std::filesystem::path computePackagePath(const std::filesystem::path& base, CUPackage* package) {
	std::filesystem::path result = base / PREFIX_NS;
	computePackagePathInternal(result, package);
	return result;
}

bool compareContents(const std::filesystem::path& path, const StringRef& cache) {
	std::ifstream f1(path, std::ifstream::binary | std::ifstream::ate);
	if (f1.fail()) {
		return false;
	}
	if (f1.tellg() != cache.getSize()) {
		return false;
	}
	f1.seekg(0, std::ifstream::beg);

	u8 buffer[8192];
	for (size_t i = 0; i < cache.getSize(); i += 8192){
		size_t to_read = std::min((size_t)8192, cache.getSize() - i);
		f1.read((char*)buffer, to_read);
		if ( memcmp(buffer, cache.getData() + i, to_read) ){
			return false;
		}
	}
	return true;
}

void saveCache(const std::filesystem::path& target_path, const StringRef& cache){
	bool need_override;
	if ( std::filesystem::exists(target_path) ){
		need_override = !compareContents(target_path, cache);
		if ( need_override ){
			std::filesystem::remove(target_path);
		}
	}else{
		need_override = true;
	}
	if ( need_override ){
		std::ofstream out(target_path, std::ios::binary);
		out << cache;
	}
}

}

void Codegen::generate(MemPool::Slice* mem, MemPool::Slice& tmp){
	if ( cu->codegen_disabled ){
		return;
	}

	this->mem = mem;
	this->tmp = &tmp;
	
	for ( auto dep : cu_codegen->strong_dependencies ){
		dep->require(CompilationUnit::CodegenPrepared);
	}
	for (auto dep : cu_codegen->weak_dependencies) {
		dep->require(CompilationUnit::CodegenPrepared);
	}

	auto info = cu->codegen_classpath;

	{
		std::filesystem::path header_path = computePackagePath(info->include_path, cu->getPackage());
		std::filesystem::create_directories(header_path);
		header_path.append(std::string_view(cu->getName()));
		header_path.replace_extension(".decl.h");
		slice_string buffer(&tmp);
		{
			StringSink sink(&buffer);
			FormattedWriter writer(&sink);
			genHeader(&writer);
		}
		saveCache(header_path, buffer);
		info->written_files.insert(header_path);
	}
	{
		std::filesystem::path header_path = computePackagePath(info->include_path, cu->getPackage());
		std::filesystem::create_directories(header_path);
		header_path.append(std::string_view(cu->getName()));
		header_path.replace_extension(".h");
		slice_string buffer(&tmp);
		{
			StringSink sink(&buffer);
			FormattedWriter writer(&sink);
			genInlineDefinitions(&writer);
		}
		saveCache(header_path, buffer);
		info->written_files.insert(header_path);
	}
	{
		std::filesystem::path source_path = computePackagePath(info->source_path, cu->getPackage());
		std::filesystem::create_directories(source_path);
		source_path.append(std::string_view(cu->getName()));
		source_path.replace_extension(".cpp");
		slice_string buffer(&tmp);
		{
			StringSink sink(&buffer);
			FormattedWriter writer(&sink);
			genPrivateDefinitions(&writer);
		}
		saveCache(source_path, buffer);
		info->written_files.insert(source_path);
	}
}

void Codegen::generateNativeStubs(MemPool::Slice* mem, MemPool::Slice& tmp) {
	auto info = cu->codegen_classpath;

	if (info->native_stubs_path.empty()){
		return;
	}

	this->mem = mem;
	this->tmp = &tmp;

	if ( doesHaveNativeStubs() ){
		std::filesystem::path stubs_path = computePackagePath(info->native_stubs_path, cu->getPackage());
		std::filesystem::create_directories(stubs_path);
		stubs_path.append(std::string_view(cu->getName()));
		stubs_path.replace_extension(".cpp");
		slice_string buffer(&tmp);
		{
			StringSink sink(&buffer);
			FormattedWriter writer(&sink);
			genNativeStubs(&writer);
		}
		saveCache(stubs_path, buffer);
		info->written_files.insert(stubs_path);
	}
}


}