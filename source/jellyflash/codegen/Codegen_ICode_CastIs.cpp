#include "Codegen.h"

namespace jly {

void Codegen::codegenCastIs(icode::Value* from, ir::Type* to) {
	ir::Type* ty = from->type;
	if (ty == to) {
		if (!isNullable(to)) {
			writer->write("true");
		} else {
			codegenIsNull(from);
		}
	} else if ( isAny(to) ) {
		if (!isNullable(ty)) {
			writer->write("true");
		} else {
			codegenIsNull(from);
		}
	} else if (isAny(ty)) {
		codegenCastGeneric(from, to, "is");
	} else if (isNumeric(ty) && isNumeric(to)) {
		if (to == &ir::TypePrim::Number) {
			writer->write("true");
		} else {
			codegenCastGeneric(from, to, "is");
		}
	} else if (to == cc->getIntrinsics()->types.Array && ty == &ir::TypePrim::Rest) {
		writer->write("true");
	} else {
		if (!codegenIsObject(from, to)) {
			writer->write("false");
		}
	}
}


}
