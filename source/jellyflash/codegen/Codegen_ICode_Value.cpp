#include "Codegen.h"
#include <jellyflash/icode/ICodePrinter.h>
#include <jellylib/utils/StringDecode.h>

namespace jly {

void Codegen::codegenValue(icode::Value* value) {
	match(value)
	caseof(icode::ConstantBool, cbool)
		if (cbool->value) {
			writer->write("true");
		} else {
			writer->write("false");
		}
	caseof(icode::ConstantNull, null)
		match(null->type)
		caseof(ir::TypePrim, prim)
			switch (prim->kind) {
			case ir::TypePrim::Kind::Boolean: {
				writer->write("false");
			}break;
			case ir::TypePrim::Kind::Class: {
				writer->write("nullptr");
			}break;
			case ir::TypePrim::Kind::Function: {
				writer->write("nullptr");
			}break;
			case ir::TypePrim::Kind::String: {
				writer->write("nullptr");
			}break;
			case ir::TypePrim::Kind::Null: {
				writer->write("nullptr");
			}break;
			case ir::TypePrim::Kind::Any: {
				writer->write("nullptr");
			}break;
			case ir::TypePrim::Kind::Object: {
				writer->write("nullptr");
			}break;
			default: {
				jl_unreachable;
			}break;
			}
		caseof(ir::TypeClass, class_)
			writer->write("nullptr");
		caseof(ir::TypeInterface, iface)
			writer->write("nullptr");
		caseof(ir::TypeVector, vec)
			writer->write("nullptr");
		endmatch
	caseof(icode::ConstantNumber, num)
		if (num->type == &ir::TypePrim::Int) {
			if (num->value == (double)std::numeric_limits<i32>::min()) {
				writer->write("((-2147483647)-1)");
			} else {
				writer->write("(");
				writer->write(diag::decimal((i32)num->value));
				writer->write(")");
			}
		} else if (num->type == &ir::TypePrim::Uint) {
			writer->write(diag::decimal((u32)num->value));
			writer->write('u');
		} else if (num->type == &ir::TypePrim::Number) {
			if (std::isnan(num->value)) {
				writer->write(IMPL_NS "NaN");
			} else if (num->value == std::numeric_limits<double>::infinity()) {
				writer->write(IMPL_NS "PositiveInfinity");
			} else if (num->value == -std::numeric_limits<double>::infinity()) {
				writer->write(IMPL_NS "NegativeInfinity");
			} else {
				writer->write("(");
				writer->write(diag::decimal(num->value));
				writer->write(")");
			}
		} else {
			jl_unreachable;
		}
	caseof(icode::ConstantString, string)
		codegenConstantString(string);
	caseof(icode::ConstantBoxed, boxed)
		if ( boxed->type == &ir::TypePrim::Any || boxed->type == &ir::TypePrim::Object ){
			writer->write(IMPL_NS "Any(");
			codegenValue(boxed->contents);
			writer->write(")");
		}else{
			jl_unreachable;
		}
	caseof(icode::ConstantUndefined, _)
		writer->write(IMPL_NS "undefined");
	caseof(icode::Thisval, _)
		if ( frame->closure ){
			writer->write(PRIVATE_PREFIX "closure->");
			if ( frame->owner->parent ){
				auto root_frame = frame->owner;
				while (root_frame->parent ){
					root_frame = root_frame->parent;
				}
				if ( root_frame != frame->owner->parent ){
					writer->write(PRIVATE_PREFIX "ar_");
					writer->write(diag::decimal(root_frame->codegen->ar_idx));
					writer->write("->");
				}
			}
			writer->write(THIS_NAME);
		}else{
			writer->write(THIS_NAME);
		}
	caseof(icode::ValueInstr, instr)
		codegenRegisterName(instr->getInstr());
	caseof(icode::ValueAdhoc, adhoc)
		writer->write(adhoc->value);
	casedef(_)
		writer->write("jfl::badvalue<");
		writeFieldType(value->type);
		writer->write(">()");
	endmatch
}

void Codegen::codegenDefaultValue(ir::Type* type) {
	match(type)
		caseof(ir::TypePrim, prim)
		switch (prim->kind) {
		case ir::TypePrim::Kind::Any: {
			writer->write(IMPL_NS "Any(nullptr)");
		}break;
		case ir::TypePrim::Kind::Object: {
			writer->write(IMPL_NS "Any(nullptr)");
		}break;
		case ir::TypePrim::Kind::Boolean: {
			writer->write("false");
		}break;
		case ir::TypePrim::Kind::Int: {
			writer->write("0");
		}break;
		case ir::TypePrim::Kind::Number: {
			writer->write(IMPL_NS "NaN");
		}break;
		case ir::TypePrim::Kind::Uint: {
			writer->write("0u");
		}break;
		case ir::TypePrim::Kind::String: {
			writer->write("nullptr");
		}break;
		case ir::TypePrim::Kind::Function: {
			writer->write("nullptr");
		}break;
		case ir::TypePrim::Kind::Class: {
			writer->write("nullptr");
		}break;
		default: {
			jl_unreachable;
		}break;
		}
	caseof(ir::TypeClass, class_)
		writer->write("nullptr");
	caseof(ir::TypeInterface, iface)
		writer->write("{nullptr, nullptr}");
	caseof(ir::TypeVector, vec)
		writer->write("nullptr");
	casedef(_)
		jl_unreachable;
	endmatch
}

void Codegen::codegenObjectRef(icode::Value* value){
	match(value->type)
	caseof(ir::TypeInterface, iface){
		codegenValue(value);
		writer->write("." IFACE_OBJECT_MEMBER);
	}
	casedef(_){
		writer->write("(" IMPL_NS "Object*)");
		codegenValue(value);
	}
	endmatch
}

void Codegen::codegenConstantString(icode::ConstantString* string){
	if (string->value.getSize() == 0 ){
		writer->write(IMPL_NS "StringEmpty");
	} else {
		generateStringLiteral(string);
		writer->write(CONST_STRING_NS "::" CONST_STRING_PREFIX);
		writer->write(string->codegen_hash);
	}
}

}