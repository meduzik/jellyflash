#include "Codegen.h"
#include "Codegen_PropertyAggregator.h"
#include <jellylib/utils/StringDecode.h>

namespace jly{

void Codegen::writeClassImpl(ir::Class* class_){
	if (mode == CodegenMode::Private) {
		uz num_implemented = class_->all_implements_list.size();
		uz hashtable_size = next_pot(num_implemented * 2);
		ir::Interface** ifaces_hashtable = tmp->createArray<ir::Interface*>(hashtable_size);
		for ( uz i = 0; i < hashtable_size; i++ ){
			ifaces_hashtable[i] = nullptr;
		}
		for ( auto iface : class_->all_implements_list ){
			u32 hash = iface->hash_base;
			while ( true ){
				if ( ifaces_hashtable[hash % hashtable_size] ){
					hash = hash + iface->hash_offset;
				}else{
					ifaces_hashtable[hash % hashtable_size] = iface;
					break;
				}
			}
		}

		uz num_classes = class_->all_bases.size() + 1;

		writer->write("static const void* ");
		writer->write(PRIVATE_PREFIX "_TI_");
		writer->write(class_->codegen->name);
		writer->write("[");
		writer->write(diag::decimal(num_classes + hashtable_size * 2));
		writer->write("] = ");
		writer->writeln(" {");
		{
			auto table_scope = writer->indented();
			// classes
			auto write_base = [&](auto fn, ir::Class* class_) -> void{
				if ( class_->extends ){
					fn(fn, class_->extends);
				}
				writer->write("&");
				writeClassClass(class_);
				writer->writeln(",");
			};
			write_base(write_base, class_);
			// interfaces
			for ( uz i = 0; i < hashtable_size; i++ ){
				if ( ifaces_hashtable[i] ){
					writer->write("&");
					writeInterfaceClass(ifaces_hashtable[i]);
					writer->writeln(",");
				}else{
					writer->writeln("nullptr,");
				}
			}
			// implementations
			for (uz i = 0; i < hashtable_size; i++) {
				if (ifaces_hashtable[i]) {
					writer->write("&");
					writeInterfaceImplRef(class_, ifaces_hashtable[i]);
					writer->writeln(",");
				} else {
					writer->writeln("nullptr,");
				}
			}
		}
		writer->writeln("};");

		writer->write("const " IMPL_NS "TypeInfo ");
		writer->write(class_->codegen->name);
		writer->writeln("::" TYPEINFO_MEMBER " {");
		{
			auto table_scope = writer->indented();
			// num_classes
			writer->write(diag::decimal(num_classes));
			writer->writeln(", ");
			// num_ifaces
			writer->write(diag::decimal(log2(hashtable_size)));
			writer->writeln(", ");
			// values
			writer->write(PRIVATE_PREFIX "_TI_");
			writer->write(class_->codegen->name);

			writer->writeln();
		}
		writer->writeln("};");

		writer->write("const " IMPL_NS "Class ");
		writer->write(class_->codegen->name);
		writer->write("::" CLASS_MEMBER);
		writer->writeln("{");
		{
			auto table_scope = writer->indented();

			writer->writeln("&" IMPL_NS "Class::jfl_VT,");
			writer->writeln("{},");

			writer->write("&");
			writer->write(class_->codegen->name);
			writer->writeln("::" TYPEINFO_MEMBER ",");

			codegenConstantString(tmp->create<icode::ConstantString>(generateQualifiedName(class_)));
			writer->writeln(",");

			writer->write(diag::decimal(num_classes - 1));
			writer->writeln(",");

			writer->writeln("0,");

			writer->writeln(IMPL_NS "class_kind_t::kind_class,");
			
			writer->write("&");
			writeRef(class_);
			writer->writeln("::" STATIC_CREATE_MEMBER ",");

			writer->write("&");
			writeRef(class_);
			writer->writeln("::" STATIC_DYNAMIC_MEMBER);
		}
		writer->writeln("};");

		for (auto iface : class_->codegen->implements_list) {
			auto impl = class_->codegen->implements_map.at(iface);
			if (impl->owner == class_) {
				writer->write("const ");
				writeRef(impl->iface);
				writer->write(" ");
				writer->write(class_->codegen->name);
				writer->write("::");
				writer->write(impl->name);
				writer->writeln("{");
				{
					auto table_scope = writer->indented();
					for (auto method : impl->vtable) {
						auto isignature = method->imethod->signature;
						writer->write("[](" BASE_OBJECT_TYPE "* jfl_object");
						writeSignature(isignature, false, true);
						writer->write(") -> ");
						writeReturnType(isignature->retTy);
						writer->writeln("{");
						{
							auto func_scope = writer->indented();
							writeFieldType(method->cmethod->owner->boxType);
							writer->write(" jfl_t = (");
							writeFieldType(method->cmethod->owner->boxType);
							writer->writeln(")jfl_object;");
							writer->write("return ");
							if ( method->cmethod->codegen_virtual ){
								writer->write("jfl_t->" CLASS_VTABLE_MEMBER "->");
								writer->write(method->cmethod->codegen_name);
							}else{
								writeRef(method->cmethod);
							}
							writer->write("(jfl_t");
							for (auto param : isignature->params) {
								writer->write(", ");
								writer->write(param->codegen_name);
							}
							if (isignature->rest) {
								writer->write(", ");
								writer->write(isignature->rest->codegen_name);
							}
							writer->writeln(");");
						}
						writer->writeln("},");
						writer->write("&");
						writeRef(method->cmethod->owner);
						writer->write("::" PRIVATE_PREFIX GENERIC_PROXY_ID "_");
						writer->write(method->cmethod->codegen_name);
						writer->writeln(",");
					}
				}
				writer->writeln("};");
			}
		}
	}
	if (mode == CodegenMode::Private) {
		writeClassConstructorImpl(class_);
	}
	for (auto method : class_->static_methods) {
		if (willCompileInCurrentMode(method) && !method->is_native) {
			writeClassMethodImpl(method);
		}
		if (mode == CodegenMode::Private) {
			writeGenericProxyImpl(method);
		}
	}
	for (auto method : class_->instance_methods) {
		if (willCompileInCurrentMode(method) && !method->is_native) {
			writeClassMethodImpl(method);
		}
		if (mode == CodegenMode::Private) {
			writeGenericProxyImpl(method);
		}
	}
	for (auto field : class_->static_fields) {
		if (mode != CodegenMode::Private) {
			continue;
		}
		writeClassStaticFieldImpl(field);
	}

	if (mode == CodegenMode::Private) {
		/* GC */
		if ((!class_->native) || (class_->native->generate_gc)){
			writer->write("void ");
			writer->write(class_->codegen->name);
			writer->writeln("::" GC_MEMBER "(" IMPL_NS "Object* jfl_this, void* ctrl){");
			{
				auto func_scope = writer->indented();
				writeFieldType(&class_->classType);
				writer->write(" self = (");
				writeFieldType(&class_->classType);
				writer->writeln(")jfl_this;");
				auto base = class_;
				while (base){
					writer->writeln("{");
					writeFieldType(&base->classType);
					writer->write(" obj = (");
					writeFieldType(&base->classType);
					writer->writeln(")jfl_this;");
					for (auto field: base->instance_fields){
						writeGCVisit(field->type, "obj", "->", field->codegen_name);
					}
					writer->writeln("}");
					base = base->extends;
				}
				if (class_->m_dynamic){
					writer->writeln(IMPL_NS "gc_visit_dynamic(self->" CLASS_DYNAMIC_MEMBER ");");
				}
				if (class_->native && class_->native->extra_gc){
					writer->writeln(PRIVATE_PREFIX "GC_user(self);");
				}
			}
			writer->writeln("}");
		}

		writer->write("void ");
		writer->write(class_->codegen->name);
		writer->writeln("::" FINALIZER_MEMBER "(" IMPL_NS "Object* jfl_this){");
		{
			auto func_scope = writer->indented();
			writeFieldType(&class_->classType);
			writer->write(" self = (");
			writeFieldType(&class_->classType);
			writer->writeln(")jfl_this;");
			writer->write("self->~");
			writer->write(class_->codegen->name);
			writer->writeln("();");
		}
		writer->writeln("}");

		if ( class_->uses_reflection ){
			PropertyAggregator agg(this, class_->codegen->name);
			if ( class_->boxType == &class_->classType ){
				for (auto field : class_->instance_fields) {
					if (field->ns == &ir::NSPublic) {
						agg.add(
							field->name,
							[&]() {
							auto func_scope = writer->indented();
							writeFieldType(class_->boxType);
							writer->write(" self = (");
							writeFieldType(class_->boxType);
							writer->writeln(")thisobj;");
							writeFieldType(field->type);
							writer->write(" val = self->");
							writer->write(field->codegen_name);
							writer->writeln(";");
							writer->write("return ");
							codegenCastTo(tmp->create<icode::ValueAdhoc>(field->type, "val"), &ir::TypePrim::Any);
							writer->writeln(";");
						},
							[&]() {
							auto func_scope = writer->indented();
							writeFieldType(class_->boxType);
							writer->write(" self = (");
							writeFieldType(class_->boxType);
							writer->writeln(")thisobj;");
							writer->write("self->");
							writer->write(field->codegen_name);
							writer->write(" = ");
							codegenCastTo(tmp->create<icode::ValueAdhoc>(&ir::TypePrim::Any, "val"), field->type);
							writer->writeln(";");
						}
						);
					}
				}

				for (auto prop : class_->instance_properties) {
					if (prop->ns == &ir::NSPublic) {
						agg.add(
							prop->name,
							[&]() {
							auto func_scope = writer->indented();
							writeFieldType(class_->boxType);
							writer->write(" self = (");
							writeFieldType(class_->boxType);
							writer->writeln(")thisobj;");

							auto getter = prop->getter;
							if (getter) {
								writeFieldType(getter->signature->retTy);
								writer->write(" val = ");
								writer->write(getter->owner->codegen->name);
								writer->write("::");
								writer->write(getter->codegen_name);
								writer->write("(");
								codegenCastTo(tmp->create<icode::ValueAdhoc>(class_->boxType, "self"), getter->owner->type);
								writer->writeln(");");
								writer->write("return ");
								codegenCastTo(tmp->create<icode::ValueAdhoc>(getter->signature->retTy, "val"), &ir::TypePrim::Any);
								writer->writeln(";");
							} else {
								writer->write("jfl::raise_writeonly(\"");
								EncodeCLiteral(writer, prop->name);
								writer->writeln("\");");
							}
						},
							[&]() {
							auto func_scope = writer->indented();
							writeFieldType(class_->boxType);
							writer->write(" self = (");
							writeFieldType(class_->boxType);
							writer->writeln(")thisobj;");

							auto setter = prop->setter;
							if (setter) {
								writer->write(setter->owner->codegen->name);
								writer->write("::");
								writer->write(setter->codegen_name);
								writer->write("(");
								codegenCastTo(tmp->create<icode::ValueAdhoc>(class_->boxType, "self"), setter->owner->type);
								writer->write(", ");
								codegenCastTo(tmp->create<icode::ValueAdhoc>(&ir::TypePrim::Any, "val"), setter->signature->params[0]->type);
								writer->writeln(");");
							} else {
								writer->write("jfl::raise_readonly(\"");
								EncodeCLiteral(writer, prop->name);
								writer->writeln("\");");
							}
						}
						);
					}
				}

				for (auto method : class_->instance_methods) {
					if (method->ns == &ir::NSPublic && method->accessor == ir::Accessor::None) {
						agg.add(
							method->name,
							[&]() {
								auto func_scope = writer->indented();
								writer->write(IMPL_NS "Function func(thisobj, &");
								writer->write(method->owner->codegen->name);
								writer->write("::" PRIVATE_PREFIX GENERIC_PROXY_ID "_");
								writer->write(method->codegen_name);
								writer->writeln(");");
								writer->writeln("return " IMPL_NS "Any(func);");

							},
							[&]() {
								auto func_scope = writer->indented();
								writer->write("jfl::raise_readonly(\"");
								EncodeCLiteral(writer, method->name);
								writer->writeln("\");");
							}
						);
					}
				}
			}
			agg.emit(
				[&](){
					writer->write(class_->codegen->name);
					writer->write("::" PROPLIST_MEMBER);
				},
				[&]() {
					writer->write(class_->codegen->name);
					writer->write("::" PROPLIST_MASK_MEMBER);
				}
			);
		}

		/* Dynamic */
		if ((!class_->native) || (class_->native->generate_dynamic)) {
			writer->write(IMPL_NS "Boolean ");
			writer->write(class_->codegen->name);
			writer->writeln("::" DYNAMIC_MEMBER "(" 
				IMPL_NS "Object* jfl_this, "
				IMPL_NS "Any* key, "
				IMPL_NS "Any* value, "
				IMPL_NS "dynamic_op op"
				"){"
			);
			{
				auto func_scope = writer->indented();

				if (class_->m_dynamic) {
					writer->write("if (op == " IMPL_NS " dynamic_op::to_string) { *value = jfl_this; return true; }");
					writer->write("if (op == " IMPL_NS " dynamic_op::value_of) { *value = jfl_this; return true; }");
					if (class_ == cc->getIntrinsics()->classes.Dictionary) {
						writer->write("return ((");
						writeParamType(class_->boxType);
						writer->write(")jfl_this)->" CLASS_DYNAMIC_MEMBER ".");
						writer->writeln("index(*key, value, op);");
					} else {
						writer->write("return ((");
						writeParamType(class_->boxType);
						writer->write(")jfl_this)->" CLASS_DYNAMIC_MEMBER ".");
						writer->writeln("index(" IMPL_NS "any_to_String(*key), value, op);");
					}
				} else {
					if ( class_->uses_reflection ){
						ir::Class* target = class_;
						while (target) {
							writer->write("if ( " IMPL_NS "proplist_index(jfl_this, ");
							writeRef(target);
							writer->write("::" PROPLIST_MEMBER ", ");
							writeRef(target);
							writer->write("::" PROPLIST_MASK_MEMBER ", ");
							writer->writeln("key, value, op) ){ return true; }");
							target = target->extends;
						}
						writer->writeln("return " IMPL_NS "static_index(jfl_this, key, value, op);");
					}else{
						writer->writeln("return " IMPL_NS "static_index(jfl_this, key, value, op);");
					}
				}
			}
			writer->writeln("}");
		}

		/* Iter */
		if ((!class_->native) || (class_->native->generate_iter)) {
			writer->write(IMPL_NS "Boolean ");
			writer->write(class_->codegen->name);
			writer->writeln("::" ITERATE_MEMBER "("
				IMPL_NS "Object* jfl_this, "
				IMPL_NS "Iterator* iter"
				"){"
			);
			{
				auto func_scope = writer->indented();
				if ( class_->m_dynamic ){
					writeFieldType(class_->boxType);
					writer->write(" jfl_t = (");
					writeFieldType(class_->boxType);
					writer->writeln(")jfl_this;");
					writer->writeln("return " IMPL_NS "Dynamic::iterate(&jfl_t->" CLASS_DYNAMIC_MEMBER ", iter);");
				}else{
					writer->writeln("jfl_notimpl;");
				}
			}
			writer->writeln("}");
		}

		/* Create */
		if (class_->type == &class_->classType) {
			writeGenericCreateImpl(class_);
		}else{
			writer->write(IMPL_NS "Any ");
			writer->write(class_->codegen->name);
			writer->writeln("::" STATIC_CREATE_MEMBER "("
				IMPL_NS "Object* jfl_this, "
				IMPL_NS "RestParams* jfl_params"
				"){"
			);
			{
				auto func_scope = writer->indented();
				writer->writeln(IMPL_NS "UInt jfl_size = jfl_params->size;");
				writer->writeln(MACRO_PREFIX "ARGMINCHECK(jfl_size, 1);");
				writer->writeln(MACRO_PREFIX "ARGMAXCHECK(jfl_size, 1);");
				writeFieldType(class_->type);
				writer->write(" result = ");
				codegenCastTo(tmp->create<icode::ValueAdhoc>(&ir::TypePrim::Any, "rest_get(jfl_params, 0)"), class_->type);
				writer->writeln(";");
				writer->write("return ");
				codegenCastTo(tmp->create<icode::ValueAdhoc>(class_->type, "result"), &ir::TypePrim::Any);
				writer->writeln(";");
			}
			writer->writeln("}");
		}


		if ( class_->uses_reflection ) {
			PropertyAggregator agg(this, class_->codegen->name);
			for (auto field : class_->static_fields) {
				if (field->ns == &ir::NSPublic) {
					agg.add(
						field->name,
						[&]() {
							auto func_scope = writer->indented();
							writeFieldType(field->type);
							writer->write(" val = ");
							writeRef(field);
							writer->writeln(";");
							writer->write("return ");
							codegenCastTo(tmp->create<icode::ValueAdhoc>(field->type, "val"), &ir::TypePrim::Any);
							writer->writeln(";");
						},
						[&]() {
							auto func_scope = writer->indented();
							writeRef(field);
							writer->write(" = ");
							codegenCastTo(tmp->create<icode::ValueAdhoc>(&ir::TypePrim::Any, "val"), field->type);
							writer->writeln(";");
						}
					);
				}
			}

			for (auto prop : class_->static_properties) {
				if (prop->ns == &ir::NSPublic) {
					agg.add(
						prop->name,
						[&]() {
							auto func_scope = writer->indented();

							auto getter = prop->getter;
							if (getter) {
								writeFieldType(getter->signature->retTy);
								writer->write(" val = ");
								writer->write(getter->owner->codegen->name);
								writer->write("::");
								writer->write(getter->codegen_name);
								writer->write("(");
								writer->writeln(");");
								writer->write("return ");
								codegenCastTo(tmp->create<icode::ValueAdhoc>(getter->signature->retTy, "val"), &ir::TypePrim::Any);
								writer->writeln(";");
							} else {
								writer->write("jfl::raise_writeonly(\"");
								EncodeCLiteral(writer, prop->name);
								writer->writeln("\");");
							}
						},
						[&]() {
							auto func_scope = writer->indented();

							auto setter = prop->setter;
							if (setter) {
								writer->write(setter->owner->codegen->name);
								writer->write("::");
								writer->write(setter->codegen_name);
								writer->write("(");
								codegenCastTo(tmp->create<icode::ValueAdhoc>(&ir::TypePrim::Any, "val"), setter->signature->params[0]->type);
								writer->writeln(");");
							} else {
								writer->write("jfl::raise_readonly(\"");
								EncodeCLiteral(writer, prop->name);
								writer->writeln("\");");
							}
						}
					);
				}
			}

			for (auto method : class_->static_methods) {
				if (method->ns == &ir::NSPublic && method->accessor == ir::Accessor::None) {
					agg.add(
						method->name,
						[&]() {
							auto func_scope = writer->indented();
							writer->write(IMPL_NS "Function func(nullptr, &");
							writer->write(method->owner->codegen->name);
							writer->write("::" PRIVATE_PREFIX GENERIC_PROXY_ID "_");
							writer->write(method->codegen_name);
							writer->writeln(");");
							writer->writeln("return " IMPL_NS "Any(func);");
						},
						[&]() {
							auto func_scope = writer->indented();
							writer->write("jfl::raise_readonly(\"");
							EncodeCLiteral(writer, method->name);
							writer->writeln("\");");
						}
					);
				}
			}
			agg.emit(
				[&]() {
					writer->write(class_->codegen->name);
					writer->write("::" STATIC_PROPLIST_MEMBER);
				},
					[&]() {
					writer->write(class_->codegen->name);
					writer->write("::" STATIC_PROPLIST_MASK_MEMBER);
				}
			);

			/* Dynamic */
			writer->write(IMPL_NS "Boolean ");
			writer->write(class_->codegen->name);
			writer->writeln("::" STATIC_DYNAMIC_MEMBER "("
				IMPL_NS "Object* jfl_this, "
				IMPL_NS "Any* key, "
				IMPL_NS "Any* value,"
				IMPL_NS "dynamic_op op"
				"){"
			);
			{
				auto func_scope = writer->indented();
				writer->write("if ( " IMPL_NS "proplist_index(jfl_this, ");
				writeRef(class_);
				writer->write("::" STATIC_PROPLIST_MEMBER ", ");
				writeRef(class_);
				writer->write("::" STATIC_PROPLIST_MASK_MEMBER ", ");
				writer->writeln("key, value, op) ){ return true; }");
				writer->writeln("return false;");
			}
			writer->writeln("}");
		}else{
			writer->write(IMPL_NS "Boolean ");
			writer->write(class_->codegen->name);
			writer->writeln("::" STATIC_DYNAMIC_MEMBER "("
				IMPL_NS "Object* jfl_this, "
				IMPL_NS "Any* key, "
				IMPL_NS "Any* value,"
				IMPL_NS "dynamic_op op"
				"){"
			);
			{
				writer->writeln("return false;");
			}
			writer->writeln("}");
		}
	}

	if (mode == CodegenMode::Inline) {
		writer->write("inline const " PRIVATE_PREFIX VTABLE_ID "_");
		writer->write(class_->codegen->name);
		writer->write(" " PRIVATE_PREFIX VTABLE_INSTANCE_ID "_");
		writer->write(class_->codegen->name);
		writer->writeln(" {");
		{
			auto public_indent = writer->indented();

			writer->writeln("{");
			{
				auto hdr_indent = writer->indented();

				writer->write("&");
				writer->write(class_->codegen->name);
				writer->write("::" TYPEINFO_MEMBER);
				writer->writeln(",");

				writer->write("(sizeof(");
				writer->write(class_->codegen->name);
				writer->writeln(") + 31) & ~31,");

				writer->write(class_->codegen->name);
				writer->write("::" GC_MEMBER);
				writer->writeln(",");

				if (class_->need_finalizer) {
					writer->write(class_->codegen->name);
					writer->write("::" FINALIZER_MEMBER);
					writer->writeln(",");
				} else {
					writer->writeln("nullptr,");
				}

				writer->write(class_->codegen->name);
				writer->write("::" DYNAMIC_MEMBER);
				writer->writeln(",");

				writer->write(class_->codegen->name);
				writer->writeln("::" ITERATE_MEMBER);
			}
			writer->writeln("},");

			writeClassVTableContents(class_, class_);
		}
		writer->writeln("};");
		writeClassNewImpl(class_);
	}
}


void Codegen::writeClassConstructorImpl(ir::Class* class_) {
	if (class_->type != &class_->classType) {
		return;
	}

	writer->write("void ");
	writer->write(class_->codegen->name);
	writer->write("::" PRECONSTRUCTOR_METHOD_NAME "(");
	writeParamType(&class_->classType);
	writer->write(" " THIS_NAME);
	writer->writeln("){");
	{
		auto func_scope = writer->indented();

		if (class_->extends) {
			writeRef(class_->extends);
			writer->write("::" PRECONSTRUCTOR_METHOD_NAME "((");
			writeParamType(&class_->extends->classType);
			writer->write(")" THIS_NAME);
			writer->writeln(");");
		}

		codegenRunStaticInitialize(cu);

		for (auto field : class_->instance_fields) {
			writer->write(THIS_NAME "->");
			writer->write(field->codegen_name);
			writer->write(" = ");
			if (field->initializer && field->initializer->constant) {
				codegenValue(field->initializer->constant);
			} else {
				codegenDefaultValue(field->type);
			}
			writer->writeln(";");
		}
	}
	writer->writeln("}");

	if (class_->constructor) {
		writePreFrame(class_->constructor->frame);
	}
	for (auto field : class_->instance_fields) {
		if (field->initializer && !field->initializer->constant && field->initializer->frame_nonconstant) {
			writePreFrame(field->initializer->frame_nonconstant);
		}
	}

	writer->write("void ");
	writer->write(class_->codegen->name);
	writer->write("::" CONSTRUCTOR_METHOD_NAME "(");
	writeParamType(&class_->classType);
	writer->write(" " THIS_NAME);
	if (class_->constructor) {
		writeSignature(class_->constructor->signature, false, true);
	}
	writer->writeln("){");
	{
		auto func_scope = writer->indented();

		for (auto field : class_->instance_fields) {
			if (field->initializer && !field->initializer->constant && field->initializer->frame_nonconstant) {
				writer->writeln("([&](){");
				{
					auto lambda_scope = writer->indented();
					writeMainFrame(field->initializer->frame_nonconstant);
				}
				writer->writeln("})();");
			}
		}

		if (
			class_->extends
			&&
			(
				!class_->constructor
				||
				!class_->constructor->super_call
				)
			) {
			writeRef(class_->extends);
			writer->write("::" CONSTRUCTOR_METHOD_NAME "(");
			writer->write("(");
			writeParamType(class_->extends->boxType);
			writer->write(")" THIS_NAME);
			auto base_ctor = class_->extends->constructor;
			if (base_ctor) {
				for (auto param : base_ctor->signature->params) {
					writer->write(", ");
					if (param->default_value) {
						codegenValue(param->default_value);
					} else {
						codegenDefaultValue(param->type);
					}
				}
				if (base_ctor->signature->rest) {
					writer->write(", {}");
				}
			}
			writer->writeln(");");
		}

		if (class_->constructor) {
			writer->writeln("([&](){");
			{
				auto lambda_scope = writer->indented();
				writeMainFrame(class_->constructor->frame);
			}
			writer->writeln("})();");
		}
	}
	writer->writeln("}");
}

void Codegen::writeClassMethodImpl(ir::ClassMethod* method) {
	writePreFrame(method->frame);

	writeAttributes(method);
	writeReturnType(method->signature->retTy);
	writer->write(" ");
	writer->write(method->owner->codegen->name);
	writer->write("::");
	writer->write(method->codegen_name);
	writer->write('(');
	if (method->is_static) {
		writeSignature(method->signature, true, true);
	} else {
		writeParamType(method->owner->boxType);
		writer->write(" " THIS_NAME);
		writeSignature(method->signature, false, true);
	}
	writer->write(')');
	writer->writeln("{");
	{
		auto func_scope = writer->indented();
		writeMainFrame(method->frame);
	}
	writer->writeln("}");
}

void Codegen::visitClassMethodImpl(ir::ClassMethod* method) {
	requireSignatureImpl(method->signature);
	codegenPreVisitMainFrame(method->owner->scope, method->owner, method->frame);
}

void Codegen::visitClassFieldImpl(ir::ClassField* field) {
	requireTypeImpl(field->type);
	if (field->initializer) {
		codegenPreVisitMainFrame(field->owner->scope, field->owner, field->initializer->frame_nonconstant);
	}
}


}
