#include "Codegen.h"

namespace jly {

void Codegen::codegenCastTo(icode::Value* from, ir::Type* to) {
	ir::Type* ty = from->type;
	if (ty == to) {
		// types match exactly
		codegenValue(from);
	} else if (isAny(ty)) {
		if (to == &ir::TypePrim::Void) {
			writer->write("nullptr");
		} else if (to == &ir::TypePrim::Object) {
			writer->write(IMPL_NS "any_undef_to_null(");
			codegenValue(from);
			writer->write(")");
		} else if (to == &ir::TypePrim::Any) {
			codegenValue(from);
		} else {
			codegenCastGeneric(from, to, "to");
		}
	} else if (isAny(to)) {
		match(ty)
		caseof(ir::TypePrim, prim) {
			switch (prim->kind) {
			case ir::TypePrim::Kind::Void: {
				if (to == &ir::TypePrim::Object) {
					writer->write(IMPL_NS "Any(nullptr)");
				} else {
					writer->write(IMPL_NS "Any(" IMPL_NS "::undefined)");
				}
			}break;
			case ir::TypePrim::Kind::Null: {
				writer->write(IMPL_NS "Any(nullptr)");
			}break;
			case ir::TypePrim::Kind::Class: {
				writer->write(IMPL_NS "Any((" IMPL_NS "Object*)");
				codegenValue(from);
				writer->write(")");
			}break;
			default: {
				writer->write(IMPL_NS "Any(");
				codegenValue(from);
				writer->write(")");
			}break;
			}
		}
		caseof(ir::TypeClass, class_) {
			writer->write(IMPL_NS "Any((" IMPL_NS "Object*)");
			codegenValue(from);
			writer->write(")");
		}
		caseof(ir::TypeInterface, iface) {
			writer->write(IMPL_NS "Any(");
			codegenValue(from);
			writer->write("." IFACE_OBJECT_MEMBER ")");
		}
		caseof(ir::TypeVector, vec) {
			writer->write(IMPL_NS "Any((" IMPL_NS "Object*)");
			codegenValue(from);
			writer->write(")");
		}
		casedef(_) {
			jl_unreachable;
		}
		endmatch
	} else if (to == &ir::TypePrim::Void) {
		writer->write("nullptr");
	} else if (ty == &ir::TypePrim::Void || ty == &ir::TypePrim::Null) {
		match(ty)
		caseof(ir::TypePrim, prim) {
			switch (prim->kind) {
			case ir::TypePrim::Kind::Boolean: {
				writer->write("false");
			}break;
			case ir::TypePrim::Kind::Int: {
				writer->write("0");
			}break;
			case ir::TypePrim::Kind::Uint: {
				writer->write("0u");
			}break;
			case ir::TypePrim::Kind::Number: {
				if (ty == &ir::TypePrim::Null) {
					writer->write("0.0");
				} else {
					writer->write(IMPL_NS "NaN");
				}
			}break;
			case ir::TypePrim::Kind::String: {
				writer->write(IMPL_NS "String(nullptr)");
			}break;
			case ir::TypePrim::Kind::Function: {
				writer->write(IMPL_NS "Function(nullptr)");
			}break;
			case ir::TypePrim::Kind::Class: {
				writer->write("(" IMPL_NS "Class*)nullptr");
			}break;
			default: {
				jl_unreachable;
			}break;
			}
		}
		caseof(ir::TypeClass, class_) {
			writer->write("(");
			writeFieldType(class_);
			writer->write(")nullptr");
		}
		caseof(ir::TypeInterface, iface) {
			writeFieldType(iface);
			writer->write("(nullptr, nullptr)");
		}
		caseof(ir::TypeVector, vec) {
			writer->write("(");
			writeFieldType(vec);
			writer->write(")nullptr");
		}
		casedef(_) {
			jl_unreachable;
		}
		endmatch
	} else if (to == &ir::TypePrim::Boolean) {
		match(ty)
		caseof(ir::TypePrim, prim) {
			switch (prim->kind) {
			case ir::TypePrim::Kind::Int: {
				codegenValue(from);
				writer->write(" != 0");
			}break;
			case ir::TypePrim::Kind::Uint: {
				codegenValue(from);
				writer->write(" != 0u");
			}break;
			case ir::TypePrim::Kind::Number:
			case ir::TypePrim::Kind::String: {
				codegenCastGeneric(from, to, "to");
			}break;
			case ir::TypePrim::Kind::Rest: {
				writer->write("true");
			}break;
			case ir::TypePrim::Kind::Function: {
				codegenValue(from);
				writer->write(".has_function()");
			}break;
			case ir::TypePrim::Kind::Class: {
				codegenValue(from);
				writer->write(" != nullptr");
			}break;
			default: {
				jl_unreachable;
			}break;
			}
		}
		caseof(ir::TypeClass, class_) {
			codegenValue(from);
			writer->write(" != nullptr");
		}
		caseof(ir::TypeInterface, iface) {
			codegenValue(from);
			writer->write("." IFACE_OBJECT_MEMBER " != nullptr");
		}
		caseof(ir::TypeVector, vec) {
			codegenValue(from);
			writer->write(" != nullptr");
		}
		casedef(_) {
			jl_unreachable;
		}
		endmatch
	} else if (ty == &ir::TypePrim::Boolean) {
		match(to)
		caseof(ir::TypePrim, prim) {
			switch (prim->kind) {
			case ir::TypePrim::Kind::Int: {
				codegenValue(from);
				writer->write(" ? 1 : 0");
			}break;
			case ir::TypePrim::Kind::Uint: {
				codegenValue(from);
				writer->write(" ? 1u : 0u");
			}break;
			case ir::TypePrim::Kind::Number: {
				codegenValue(from);
				writer->write(" ? 1.0 : 0.0");
			}break;
			case ir::TypePrim::Kind::String: {
				codegenValue(from);
				writer->write(" ? " IMPL_NS "String(\"true\") : " IMPL_NS "String(\"false\")");
			}break;
			default: {
				codegenCastError(ty, to);
			}break;
			}
		}
		casedef(_) {
			codegenCastError(ty, to);
		}
		endmatch
	} else if (to == &ir::TypePrim::String) {
		codegenCastGeneric(from, to, "to");
	} else if (ty == &ir::TypePrim::String) {
		if (isNumeric(to)) {
			codegenCastGeneric(from, to, "to");
		} else {
			codegenCastError(ty, to);
		}
	} else if (isNumeric(ty)) {
		if (isNumeric(to)) {
			if (to == &ir::TypePrim::Number) {
				writer->write("(" IMPL_NS "Number)");
				codegenValue(from);
			} else if (to == &ir::TypePrim::Uint && ty == &ir::TypePrim::Int) {
				writer->write("(" IMPL_NS "UInt)");
				codegenValue(from);
			} else if (to == &ir::TypePrim::Int && ty == &ir::TypePrim::Uint) {
				writer->write("(" IMPL_NS "Int)");
				codegenValue(from);
			} else {
				codegenCastGeneric(from, to, "to");
			}
		} else {
			codegenCastError(ty, to);
		}
	} else if (isNumeric(to)) {
		match(ty)
		caseof(ir::TypePrim, prim) {
			codegenCastTo(icode::ConstantUndefined::Instance(), to);
		}
		caseof(ir::TypeClass, class_) {
			codegenCastGeneric(from, to, "to");
		}
		caseof(ir::TypeInterface, iface) {
			codegenCastGeneric(from, to, "to");
		}
		caseof(ir::TypeVector, vec) {
			codegenCastTo(icode::ConstantUndefined::Instance(), to);
		}
		casedef(_) {
			jl_unreachable;
		}
		endmatch
	} else if (to == cc->getIntrinsics()->types.Array && ty == &ir::TypePrim::Rest) {
		writer->write(IMPL_NS "rest_to_array(");
		codegenValue(from);
		writer->write(")");
	} else {
		if ( !codegenCastObject(from, to, "to") ){
			codegenCastError(ty, to);
		}
	}
}

}
