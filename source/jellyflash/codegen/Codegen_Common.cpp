#include "Codegen.h"
#include <jellylib/types/StringBuilder.h>
#include <jellylib/crypto/SHA1.h>

namespace jly{

namespace{

std::set<StringRef> keywords = {
	"register",
	"namespace",
	"template",
	"long",
	"union",
	"int",
	"bool",
	"short",
	"errno"
};

const u8 hex_digit[] = "0123456789abcdef";

}

StringRef Codegen::mangleStaticNamespace(StringRef name) {
	StringBuilder sb(tmp);
	sb.append(PRIVATE_PREFIX "privns_");

	for (auto ch : name) {
		if (ch < 128 && isalnum(ch)) {
			sb.append(ch);
		} else {
			sb.append('_');
			switch (ch) {
			case '_': sb.append('_'); break;
			case '/': sb.append('S'); break;
			case '\\': sb.append('B'); break;
			case '"': sb.append('Q'); break;
			case '\'': sb.append('q'); break;
			case '.': sb.append('d'); break;
			default: {
				sb.append(hex_digit[ch / 16]);
				sb.append(hex_digit[ch % 16]);
			}break;
			}
		}
	}

	return sb.get(mem);
}

StringRef Codegen::mangleNamespace(StringRef name) {
	StringBuilder sb(tmp);

	for ( auto ch : name ){
		if (ch < 128 && isalnum(ch) ){
			sb.append(ch);
		}else{
			sb.append('_');
			switch ( ch ){
			case '_': sb.append('_'); break;
			case '/': sb.append('S'); break;
			case '\\': sb.append('B'); break;
			case '"': sb.append('Q'); break;
			case '\'': sb.append('q'); break;
			default: {
				sb.append(hex_digit[ch / 16]);
				sb.append(hex_digit[ch % 16]);
			}break;
			}
		}
	}

	return sb.get(mem);
}

StringRef Codegen::mangleIdentifier(icode::LocalVar* var){
	StringBuilder sb(tmp);
	sb.append(PRIVATE_PREFIX LOCAL_ID "_");
	sb.append(var->name);
	return sb.get(mem);
}

StringRef Codegen::mangleIdentifier(ir::FunctionParam* param){
	StringBuilder sb(tmp);
	sb.append(PRIVATE_PREFIX PARAM_ID "_");
	sb.append(param->name);
	return sb.get(mem);
}

StringRef Codegen::mangleIdentifier(ir::NamespaceDef* def, StringRef name){
	return mangleIdentifier(def, name, MangleKind::Normal, ir::Accessor::None);
}

StringRef Codegen::mangleIdentifier(ir::NamespaceDef* def, StringRef name, MangleKind kind){
	return mangleIdentifier(def, name, kind, ir::Accessor::None);
}

StringRef Codegen::mangleIdentifier(ir::NamespaceDef* def, StringRef name, MangleKind kind, ir::Accessor accessor){
	StringBuilder sb(tmp);

	if ( 
		(def && def != ir::NSPublic.def) 
		||
		kind != MangleKind::Normal 
		|| 
		accessor != ir::Accessor::None
	){
		sb.append(PRIVATE_PREFIX);
	
		if (def) {
			if (def == ir::NSPublic.def) {
				// no prefix
			} else if (def == ir::NSProtected.def) {
				sb.append(PROTECTED_ID);
			} else if (def == ir::NSPrivate.def) {
				sb.append(PRIVATE_ID);
			} else {
				if (def->codegen_value.getSize() == 0) {
					def->codegen_value = mangleNamespace(def->value);
				}
				sb.append(NS_ID);
				sb.append(def->codegen_value);
				sb.append("_" NS_ID);
			}
		}

		switch ( kind ){
		case MangleKind::Static:{
			sb.append(STATIC_ID);
		}break;
		case MangleKind::Local:{
			sb.append(LOCAL_ID);
		}break;
		}

		switch ( accessor ){
		case ir::Accessor::Get:{
			sb.append(GETTER_ID);
		}break;
		case ir::Accessor::Set: {
			sb.append(SETTER_ID);
		}break;
		}

		sb.append('_');
	}

	sb.append(name);

	if (keywords.count(sb.view())) {
		sb.prepend(PRIVATE_PREFIX KEYWORD_ID "_");
	}

	return sb.get(mem);
}

void Codegen::writePackageInclPath(CUPackage* package) {
	if (package->getParent()) {
		writePackageInclPath(package->getParent());
		writer->write(package->getName());
		writer->write("/");
	}
}

bool Codegen::writePackageNS(CUPackage* package) {
	if (package->getParent()) {
		if (writePackageNS(package->getParent())) {
			writer->write("::");
		}
		writer->write(package->getName());
	} else {
		writer->write(PREFIX_NS);
	}
	return true;
}

void Codegen::writePrivateNS(CompilationUnit* cu) {
	writer->write(HIDDEN_NS_PREFIX);
	writer->write(cu->getName());
}

void Codegen::writeNamespaceForPackage(CUPackage* package) {
	writer->write(package->codegen->ns);
}

void Codegen::writeNamespaceForScope(ir::FileScope* scope) {
	writer->write(scope->file->cu->codegen->ns);
	if (scope->is_private) {
		writer->write("::");
		writePrivateNS(scope->file->cu);
	}
}

void Codegen::writeRef(ir::Definition* def) {
	def->scope->file->cu->require(CompilationUnit::State::CodegenPrepared);

	writeNamespaceForScope(def->scope);
	writer->write("::");
	match(def)
	caseof(ir::Class, class_)
		writer->write(class_->codegen->name);
	caseof(ir::Interface, iface)
		writer->write(iface->codegen->name);
	caseof(ir::GlobalFunction, func)
		writer->write(func->codegen_name);
	caseof(ir::GlobalVar, var)
		writer->write(var->codegen_name);
	casedef(_)
		writer->write(def->name);
	endmatch
}

void Codegen::writeRef(ir::ClassMethod* method){
	writeRef(method->owner);
	writer->write("::");
	writer->write(method->codegen_name);
}

void Codegen::writeRef(ir::ClassField* field){
	writeRef(field->owner);
	writer->write("::");
	writer->write(field->codegen_name);
}

void Codegen::writeParamType(ir::Type* type){
	writeFieldType(type);
}

void Codegen::writeTypeDesignator(ir::Type* type){
	match(type)
	caseof(ir::TypeClass, class_)
		writeRef(class_->class_);
		return;
	endmatch
	writeFieldType(type);
}

void Codegen::writeReturnType(ir::Type* type){
	match(type)
	caseof(ir::TypePrim, prim)
		switch ( prim->kind ){
		case ir::TypePrim::Kind::Null: {
			writer->write("void");
			return;
		}break;
		case ir::TypePrim::Kind::Void: {
			writer->write("void");
			return;
		}break;
		}
	endmatch
	writeFieldType(type);
}

void Codegen::writeFieldType(ir::Type* type) {
	match(type)
	caseof(ir::TypeClass, type){
		auto class_ = type->class_;
		class_->scope->file->cu->require(CompilationUnit::CodegenPrepared);
		writeRef(type->class_);
		writer->write("*");
	}
	caseof(ir::TypeInterface, type){
		type->iface->scope->file->cu->require(CompilationUnit::CodegenPrepared);
		writer->write(IMPL_NS "I<");
		writeRef(type->iface);
		writer->write(">");
	}
	caseof(ir::TypePrim, type){
		switch (type->kind) {
		case ir::TypePrim::Kind::Null: {
			cc->warning("codegen.invalid_type", "null type generated");
			writer->write(IMPL_NS "Object*");
		}break;
		case ir::TypePrim::Kind::Void: {
			cc->warning("codegen.invalid_type", "void type generated");
			writer->write(IMPL_NS "Object*");
		}break;
		case ir::TypePrim::Kind::Any: {
			writer->write(IMPL_NS "Any");
		}break;
		case ir::TypePrim::Kind::Object: {
			writer->write(IMPL_NS "Any");
		}break;
		case ir::TypePrim::Kind::Boolean: {
			writer->write(IMPL_NS "Boolean");
		}break;
		case ir::TypePrim::Kind::Int: {
			writer->write(IMPL_NS "Int");
		}break;
		case ir::TypePrim::Kind::Uint: {
			writer->write(IMPL_NS "UInt");
		}break;
		case ir::TypePrim::Kind::Number: {
			writer->write(IMPL_NS "Number");
		}break;
		case ir::TypePrim::Kind::String: {
			writer->write(IMPL_NS "String");
		}break;
		case ir::TypePrim::Kind::Class: {
			writer->write("const " IMPL_NS "Class*");
		}break;
		case ir::TypePrim::Kind::Function: {
			writer->write(IMPL_NS "Function");
		}break;
		case ir::TypePrim::Kind::Rest: {
			writer->write(IMPL_NS "RestParams*");
		}break;
		default:{
			cc->fatal("codegen.invalid_type", "invalid prim type %1", type);
		}break;
		}
	}
	caseof(ir::TypeVector, type){
		writer->write(IMPL_NS "Vector<");
		codegenVectorEltType(type->eltTy);
		writer->write(">*");
	}
	caseof(ir::TypeVar, var){
		writer->write(IMPL_NS "Object*");
	}
	casedef(_)
		cc->fatal("codegen.bad_type", "internal error: type %1 not implemented", type);
	endmatch
}

void Codegen::writeProxyPass(ir::FunctionSignature* signature, bool first_arg){
	for (auto param : signature->params) {
		if (!first_arg) {
			writer->write(", ");
		} else {
			first_arg = false;
		}
		writer->write(param->codegen_name);
	}
	if (signature->rest) {
		if (!first_arg) {
			writer->write(", ");
		} else {
			first_arg = false;
		}
		writer->write(signature->rest->codegen_name);
	}
}

void Codegen::writeSignature(ir::FunctionSignature* signature, bool first_arg, bool param_names) {
	for (auto param : signature->params) {
		if (!first_arg) {
			writer->write(", ");
		} else {
			first_arg = false;
		}
		writeParamType(param->type);
		if ( param_names ){
			writer->write(' ');
			writer->write(param->codegen_name);
		}
	}

	if (signature->rest) {
		if (!first_arg) {
			writer->write(", ");
		} else {
			first_arg = false;
		}
		writer->write(IMPL_NS "RestParams* ");
		writer->write(signature->rest->codegen_name);
	}
}

void Codegen::uwrapClosurePayload(StringRef value, ir::Type* type){
	match(type)
	caseof(ir::TypePrim, prim)
		switch(prim->kind){
		case ir::TypePrim::Kind::Any:
		case ir::TypePrim::Kind::Boolean:
		case ir::TypePrim::Kind::Int: 
		case ir::TypePrim::Kind::Uint:
		case ir::TypePrim::Kind::Number:
		case ir::TypePrim::Kind::Function:
		case ir::TypePrim::Kind::String: {
			writer->write(IMPL_NS "Box<");
			writeFieldType(prim);
			writer->write(">::Unwrap(");
			writer->write(value);
			writer->write(")");
		}break;
		case ir::TypePrim::Kind::Class: {
			writer->write("((");
			writeFieldType(prim);
			writer->write(")");
			writer->write(value);
			writer->write(")");
		}break;
		default:{
			jl_unreachable;
		}break;
		}
	caseof(ir::TypeClass, class_)
		writer->write("((");
		writeFieldType(class_);
		writer->write(")");
		writer->write(value);
		writer->write(")");
	caseof(ir::TypeInterface, iface)
		writer->write(IMPL_NS "cast<");
		writeFieldType(iface);
		writer->write(">(");
		writer->write(value);
		writer->write(", &");
		writeTypeClass(iface);
		writer->write(")");
	caseof(ir::TypeVector, vec)
		writer->write("((");
		writeFieldType(vec);
		writer->write(")");
		writer->write(value);
		writer->write(")");
	casedef(_)
		jl_unreachable;
	endmatch
}

bool Codegen::isAny(ir::Type* type){
	return type == &ir::TypePrim::Any || type == &ir::TypePrim::Object;
}

void Codegen::writeClassClass(ir::Class* class_){
	writeRef(class_);
	writer->write("::" CLASS_MEMBER);
}

void Codegen::writeInterfaceClass(ir::Interface* iface){
	writeRef(iface);
	writer->write("::" CLASS_MEMBER);
}

StringRef Codegen::generateQualifiedName(ir::Definition* def) {
	StringBuilder sb(tmp);
	if (def->scope->file->cu->getPackage()->getParent() ){
		sb.append(def->scope->file->cu->getPackage()->getFullName());
		sb.append("::");
	}
	if ( def->scope->is_private ){
		sb.append("$private:");
	}
	sb.append(def->name);
	return sb.get(tmp);
}

void Codegen::writeStaticNS(){
	writer->write(static_ns);
}

}