#include "Codegen.h"
#include <jellyflash/icode/ICodePrinter.h>
#include <jellylib/crypto/SHA1.h>
#include <jellylib/utils/StringDecode.h>

namespace jly{

static constexpr i32 InlineThreshold = 3;

void Codegen::genInlineDefinitions(FormattedWriter* writer) {
	mode = CodegenMode::Inline;
	genImpl(writer);
}

void Codegen::genPrivateDefinitions(FormattedWriter* writer) {
	mode = CodegenMode::Private;
	genImpl(writer);
}

void Codegen::genImpl(FormattedWriter* target_writer) {
	this->writer = target_writer;

	if (mode == CodegenMode::Inline) {
		writer->writeln("#pragma once");
	}

	writer->writeln("#include <jfl/common_impl.h>");
	auto package = cu->getPackage();

	writer->write("#include <");
	
	if (mode == CodegenMode::Inline) {
		writer->write(cu_codegen->header_include);
	} else {
		writer->write(cu_codegen->header_impl_include);
	}
	writer->writeln(">");

	visitScopeImpl(&IR->package);
	visitScopeImpl(&IR->internal);

	slice_vector<CompilationUnit*> includes(tmp);
	includes.reserve(cu->codegen->impl_dependencies.size());
	for ( auto other_cu : cu->codegen->impl_dependencies ){
		includes.push_back(other_cu);
		other_cu->require(CompilationUnit::State::CodegenPrepared);
	}
	for (auto other_cu : cu->codegen->strong_dependencies) {
		includes.push_back(other_cu);
		other_cu->require(CompilationUnit::State::CodegenPrepared);
	}
	std::sort(includes.begin(), includes.end(), [](CompilationUnit* cu1, CompilationUnit* cu2) {
		return cu1->codegen->header_impl_include < cu2->codegen->header_impl_include;
	});
	for ( auto other_cu : includes ){
		if ( other_cu != cu ){
			writer->write("#include <");
			writer->write(other_cu->codegen->header_impl_include);
			writer->writeln(">");
		}
	}

	slice_string buffer(tmp);
	{
		StringSink sink(&buffer);
		FormattedWriter tmp_writer(&sink);
		this->writer = &tmp_writer;
		writer->write("namespace ");
		writer->write(cu_codegen->ns);
		writer->writeln("{");

		writeScopeImpl(&IR->package);

		if (IR->internal.definitions.getSize() > 0) {
			writer->write("namespace ");
			writePrivateNS(cu);
			writer->writeln("{");

			writeScopeImpl(&IR->internal);
			writer->writeln("}");
		}

		if (mode == CodegenMode::Private) {
			writer->write("bool ");
			writer->write(PRIVATE_PREFIX STATIC_INITIALIZER_GUARD_ID "_");
			writer->write(cu->getName());
			writer->writeln(" = false;");

			for ( auto scope : { &IR->package, &IR->internal } ){
				for (auto def : scope->definitions) {
					match(def)
					caseof(ir::GlobalVar, var_) {
						if (var_->initializer && var_->initializer->frame_nonconstant && !var_->initializer->constant) {
							writePreFrame(var_->initializer->frame_nonconstant);
						}
					}
					caseof(ir::Class, class_) {
						for (auto field : class_->static_fields) {
							if (field->initializer && field->initializer->frame_nonconstant && !field->initializer->constant) {
								writePreFrame(field->initializer->frame_nonconstant);
							}
						}
					}
					casedef(def) {
					}
					endmatch
				}
			}

			bool has_gcable_fields = false;
			writer->write("static void ");
			writer->write(PRIVATE_PREFIX "StaticGC" "_");
			writer->write(cu->getName());
			writer->writeln("(void* ud){");
			{
				auto func_scope = writer->indented();
				writer->write("using namespace ");
				writeNamespaceForScope(&IR->package);
				writer->writeln(";");
				has_gcable_fields = writeStaticGCImpl(&IR->package) || has_gcable_fields;
				if (IR->internal.definitions.getSize() > 0) {
					writer->write("using namespace ");
					writeNamespaceForScope(&IR->internal);
					writer->writeln(";");
					has_gcable_fields = writeStaticGCImpl(&IR->internal) || has_gcable_fields;
				}
			}
			writer->writeln("}");

			writer->write("static " IMPL_NS "GCRoot " PRIVATE_PREFIX "StaticRoot_");
			writer->write(cu->getName());
			writer->write("(&" PRIVATE_PREFIX "StaticGC" "_");
			writer->write(cu->getName());
			writer->writeln(", nullptr);");

			writer->writeln("jfl_noinline");
			writer->write("void ");
			writer->write(PRIVATE_PREFIX STATIC_INITIALIZER_ID "_");
			writer->write(cu->getName());
			writer->writeln("(){");
			{
				auto func_scope = writer->indented();
				writer->write(PRIVATE_PREFIX STATIC_INITIALIZER_GUARD_ID "_");
				writer->write(cu->getName());
				writer->writeln(" = true;");
				writeStaticInitImpl(&IR->package);
				writeStaticInitImpl(&IR->internal);
				if (has_gcable_fields){
					writer->write(IMPL_NS "gc_add_root(&");
					writer->write(PRIVATE_PREFIX "StaticRoot_");
					writer->write(cu->getName());
					writer->writeln(");");
				}
			}
			writer->writeln("}");
		}

		writer->writeln("}");
	}
	this->writer = target_writer;

	writer->writeln("namespace " CONST_STRING_NS "{");
	for (auto& pair : cu_codegen->string_pool) {
		pair.second->codegen_hash = StringRef(SHA1Signature(pair.second->value)).clone(*mem);

		writer->write("#if !defined(" MACRO_PREFIX "CSTR_");
		writer->write(pair.second->codegen_hash);
		writer->writeln(")");
		writer->write("#define " MACRO_PREFIX "CSTR_");
		writer->write(pair.second->codegen_hash);
		writer->writeln("");

		StringRef contents = pair.second->value;

		if (contents.getSize() > 7) {
			writer->write("inline const " IMPL_NS "StringObject ");
			writer->write(CONST_STRING_DATA_PREFIX);
			writer->write(pair.second->codegen_hash);
			writer->write("{");
			writer->write("&" IMPL_NS "StringObject::jfl_VT, " IMPL_NS "static_object, ");
			writer->write(diag::decimal(contents.getSize()));
			writer->write(", (const uint8_t*)\"");
			EncodeCLiteral(writer, contents);
			writer->writeln("\"};");
		}
		writer->write("inline const " IMPL_NS "String ");
		writer->write(CONST_STRING_PREFIX);
		writer->write(pair.second->codegen_hash);

		if (contents.getSize() > 7) {
			writer->write("((const " IMPL_NS "StringObject*)&" CONST_STRING_DATA_PREFIX);
			writer->write(pair.second->codegen_hash);
			writer->write(")");
		} else {
			writer->write("(" IMPL_NS "StringSmall{");
			writer->write(diag::decimal(contents.getSize()));
			writer->write(", {");
			for (size_t i = 0; i < 7; i++) {
				if (i < contents.getSize()) {
					writer->write(diag::decimal(contents[i]));
				}
				else {
					writer->write("0");
				}
				writer->write(",");
			}
			writer->write("}})");
		}
		writer->writeln(";");

		writer->writeln("#endif");
	}
	writer->writeln("}");

	writer->write(buffer);
}

void Codegen::visitScopeImpl(ir::FileScope* scope){
	for (auto def : scope->definitions) {
		match(def)
		caseof(ir::GlobalVar, var_) {
			if (mode == CodegenMode::Private) {
				requireTypeImpl(var_->type);
				if ( var_->initializer ){
					codegenPreVisitMainFrame(scope, nullptr, var_->initializer->frame_nonconstant);
				}
			}
		}
		caseof(ir::GlobalFunction, func) {
			if (willCompileInCurrentMode(func)) {
				requireSignatureImpl(func->signature);
				codegenPreVisitMainFrame(scope, nullptr, func->frame);
			}
		}
		caseof(ir::Class, class_) {
			if (mode == CodegenMode::Private) {
				if (class_->constructor) {
					visitClassMethodImpl(class_->constructor);
				}
				for (auto field : class_->static_fields) {
					visitClassFieldImpl(field);
				}
				for (auto field : class_->instance_fields) {
					visitClassFieldImpl(field);
				}
			}
			for (auto method : class_->static_methods) {
				if (willCompileInCurrentMode(method)) {
					visitClassMethodImpl(method);
				}
			}
			for (auto method : class_->instance_methods) {
				if (willCompileInCurrentMode(method)) {
					visitClassMethodImpl(method);
				}
			}
		}
		casedef(def) {
			// no codegen, no report
		}
		endmatch
	}
}

void Codegen::requireTypeImpl(ir::Type* type){
	match(type)
	caseof(ir::TypeClass, class_)
		requireDefinitionImpl(class_->class_);
	caseof(ir::TypeInterface, iface)
		requireDefinitionImpl(iface->iface);
	caseof(ir::TypeVector, vec)
		return requireTypeImpl(vec->eltTy);
	endmatch
}

void Codegen::requireDefinitionImpl(ir::Definition* definition){
	cu->codegen->impl_dependencies.insert(definition->scope->file->cu);
}

void Codegen::requireSignatureImpl(ir::FunctionSignature* signature){
	requireTypeImpl(signature->retTy);
	for ( auto param : signature->params ){
		requireTypeImpl(param->type);
	}
}

bool Codegen::writeStaticGCImpl(ir::FileScope* scope){
	bool has_gc = false;
	for (auto def : scope->definitions) {
		match(def)
		caseof(ir::GlobalVar, var_) {
			has_gc = writeGCVisit(var_->type, "", "", var_->codegen_name) || has_gc;
		}
		caseof(ir::Class, class_) {
			for (auto field : class_->static_fields) {
				has_gc = writeGCVisit(field->type, class_->codegen->name, "::", field->codegen_name) || has_gc;
			}
		}
		casedef(def) {
		}
		endmatch
	}
	return has_gc;
}

void Codegen::writeScopeImpl(ir::FileScope* scope) {
	for (auto def : scope->definitions) {
		match(def)
		caseof(ir::GlobalVar, var) {
			if (mode == CodegenMode::Private) {
				writeFieldType(var->type);
				writer->write(" ");
				writer->write(var->codegen_name);

				if (var->initializer && var->initializer->constant) {
					writer->write("{");
					codegenValue(var->initializer->constant);
					writer->write("}");
				} else {
					writer->write("{ /*default*/ ");
					codegenDefaultValue(var->type);
					writer->write("}");
				}

				writer->writeln(";");
			}
		}
		caseof(ir::GlobalFunction, func) {
			if ( !func->m_native && willCompileInCurrentMode(func) ){
				writePreFrame(func->frame);

				writeAttributes(func);
				writeReturnType(func->signature->retTy);
				writer->write(" ");
				writer->write(func->codegen_name);
				writer->write('(');
				writeSignature(func->signature, true, true);
				writer->write(')');
				writer->writeln("{");
				{
					auto func_scope = writer->indented();
					writeMainFrame(func->frame);
				}
				writer->writeln("}");
			}
			if (mode == CodegenMode::Private) {
				writeGenericProxyImpl(func);
			}
		}
		caseof(ir::Class, class_){
			writeClassImpl(class_);
		}
		caseof(ir::Interface, iface){
			if (mode == CodegenMode::Private) {
				writeInterfaceImpl(iface);
			}
		}
		casedef(def){
		}
		endmatch
	}
	if (mode == CodegenMode::Private) {
		writer->write("struct " PRIVATE_PREFIX "StaticInitialier_");
		writer->write(scope->file->cu->getName());
		writer->writeln("{");
		writer->write(PRIVATE_PREFIX "StaticInitialier_");
		writer->write(scope->file->cu->getName()); 
		writer->writeln("(){");
		{
			auto func_scope = writer->indented();
			for (auto def : scope->definitions) {
				match(def)
				caseof(ir::GlobalVar, var) {
					/*
					writer->write(IMPL_NS "register_definition(\"");
					EncodeCLiteral(writer, generateQualifiedName(var));
					writer->write("\", []() -> " IMPL_NS "Any{ return ");
					codegenToAny(tmp->create<icode::ValueAdhoc>(var->type, var->codegen_name));
					writer->writeln("; } );");
					*/
				}
				caseof(ir::GlobalFunction, func) {
					/*
					writer->write(IMPL_NS "register_definition(\"");
					EncodeCLiteral(writer, generateQualifiedName(func));
					writer->write("\", []() -> " IMPL_NS "Any{ " IMPL_NS "Function func(nullptr, &");
					writer->write(PRIVATE_PREFIX GENERIC_PROXY_ID "_");
					writer->write(func->codegen_name);
					writer->writeln("); return *(" IMPL_NS "Any*)&func; });");
					*/
				}
				caseof(ir::Class, class_) {/*
					//if ( class_->uses_closure ){
						writer->write(IMPL_NS "register_definition(\"");
						EncodeCLiteral(writer, generateQualifiedName(class_));
						writer->write("\", []() -> " IMPL_NS "Any{ return (" IMPL_NS "Object*)&");
						writeClassClass(class_);
						writer->writeln("; } );");
					//}
				*/}
				caseof(ir::Interface, iface) {/*
					//if (iface->uses_closure) {
						writer->write(IMPL_NS "register_definition(\"");
						EncodeCLiteral(writer, generateQualifiedName(iface));
						writer->write("\", []() -> " IMPL_NS "Any{ return (" IMPL_NS "Object*)&");
						writeInterfaceClass(iface);
						writer->writeln("; } );");
					//}
				*/}
				casedef(def) {
				}
				endmatch
			}
		}
		writer->writeln("}");
		writer->write("} " PRIVATE_PREFIX "init_");
		writer->write(scope->file->cu->getName());
		writer->writeln(";");
	}
}

void Codegen::writeClassStaticFieldImpl(ir::ClassField* field) {
	writeFieldType(field->type);
	writer->write(" ");
	writer->write(field->owner->codegen->name);
	writer->write("::");
	writer->write(field->codegen_name);
	writer->write(" = " );

	if (field->initializer && field->initializer->constant) {
		writer->write("{");
		codegenValue(field->initializer->constant);
		writer->write("}");
	} else {
		writer->write("{");
		writer->write("/* default */");
		codegenDefaultValue(field->type);
		writer->write("}");
	}

	writer->writeln(";");
}

bool Codegen::doesHaveNativeStubs() {
	auto does_scope_have_native_stubs = [&](ir::FileScope* scope) {
		for (auto def : scope->definitions) {
			match(def)
			caseof(ir::GlobalFunction, func) {
				if (func->m_native) {
					return true;
				}
			}
			caseof(ir::Class, class_) {
				for (auto method : class_->instance_methods) {
					if (method->is_native) {
						return true;
					}
				}
				for (auto method : class_->static_methods) {
					if (method->is_native) {
						return true;
					}
				}
			}
			casedef(def) {
				// no codegen, no report
			}
			endmatch
		}
		return false;
	};
	return (
		does_scope_have_native_stubs(&IR->internal) 
		|| 
		does_scope_have_native_stubs(&IR->package)
	);
}

void Codegen::genNativeStubs(FormattedWriter* writer) {
	this->writer = writer;

	writer->write("#if __has_include(<");
	writer->write(cu_codegen->header_include);
	writer->writeln(">)");

	writer->writeln("#include <jfl/common_impl.h>");
	auto package = cu->getPackage();
	writer->write("#include <");
	writer->write(cu_codegen->header_include);
	writer->writeln(">");

	writer->write("namespace ");
	writer->write(cu_codegen->ns);
	writer->writeln("{");

	writeScopeNativeStubs(&IR->package);

	if (IR->internal.definitions.getSize() > 0) {
		writer->write("namespace ");
		writePrivateNS(cu);
		writer->write("{");
		writeScopeNativeStubs(&IR->internal);
		writer->writeln("}");
	}

	writer->writeln("}");

	writer->writeln("#endif");
}

void Codegen::writeScopeNativeStubs(ir::FileScope* scope){
	auto writeMethodStub = [&](ir::ClassMethod* method){
		writeReturnType(method->signature->retTy);
		writer->write(" ");
		writer->write(method->owner->codegen->name);
		writer->write("::");
		writer->write(method->codegen_name);
		writer->write('(');
		bool first = true;
		if ( !method->is_static ){
			first = false;
			writeParamType(method->owner->boxType);
			writer->write(" " THIS_NAME);
		}
		writeSignature(method->signature, first, true);
		writer->write(')');
		writer->writeln("{");
		{
			auto indent = writer->indented();
			writer->writeln("abort();");
		}
		writer->writeln("}");
	};
	
	for (auto def : scope->definitions) {
		match(def)
		caseof(ir::GlobalFunction, func) {
			if (func->m_native) {
				writeReturnType(func->signature->retTy);
				writer->write(" ");
				writer->write(func->codegen_name);
				writer->write('(');
				writeSignature(func->signature, true, true);
				writer->write(')');
				writer->writeln("{");
				{
					auto func_scope = writer->indented();
					writer->writeln("abort();");
				}
				writer->writeln("}");
			}
		}
		caseof(ir::Class, class_) {
			if (class_->native && class_->native->destructor){
				writer->write(class_->codegen->name);
				writer->write("::~");
				writer->write(class_->codegen->name);
				writer->write("()");
				writer->writeln("{");
				{
					auto func_scope = writer->indented();
					writer->writeln("abort();");
				}
				writer->writeln("}");
			}
			if (class_->native && class_->native->extra_gc) {
				writer->write("void ");
				writer->write(class_->codegen->name);
				writer->write("::" GC_USER_MEMBER "(");
				writer->write(class_->codegen->name);
				writer->writeln("*){");
				writer->writeln("abort();");
				writer->writeln("}");
			}
			for (auto method : class_->instance_methods) {
				if (method->is_native) {
					writeMethodStub(method);
				}
			}
			for (auto method : class_->static_methods) {
				if (method->is_native) {
					writeMethodStub(method);
				}
			}
		}
		casedef(def) {
			// no codegen, no report
		}
		endmatch
	}
}

void Codegen::writeStaticInitImpl(ir::FileScope* scope){
	for (auto def : scope->definitions) {
		match(def)
		caseof(ir::GlobalVar, var_) {
			if (var_->initializer && var_->initializer->frame_nonconstant && !var_->initializer->constant) {
				writer->writeln("([&](){");
				{
					auto lambda_scope = writer->indented();
					writeMainFrame(var_->initializer->frame_nonconstant);
				}
				writer->writeln("})();");
			}
		}
		caseof(ir::Class, class_) {
			for ( auto field : class_->static_fields ){
				if ( field->initializer && field->initializer->frame_nonconstant && !field->initializer->constant ){
					writer->writeln("([&](){");
					{
						auto lambda_scope = writer->indented();
						writeMainFrame(field->initializer->frame_nonconstant);
					}
					writer->writeln("})();");
				}
			}
		}
		casedef(def) {
		}
		endmatch
	}
}

void Codegen::generateStringLiteral(icode::ConstantString* string) {
	if (string->codegen_hash.getSize() == 0) {
		string->codegen_hash = StringRef(SHA1Signature(string->value)).clone(*mem);
	}
	cu_codegen->string_pool.insert({ string->codegen_hash, string });
}

void Codegen::writeTypeClass(ir::Type* type) {
	match(type)
	caseof(ir::TypePrim, prim)
		switch(prim->kind){
		case ir::TypePrim::Kind::Boolean:{
			writer->write(IMPL_NS "Class_Boolean");
		}break;
		case ir::TypePrim::Kind::Int: {
			writer->write(IMPL_NS "Class_Int");
		}break;
		case ir::TypePrim::Kind::Uint: {
			writer->write(IMPL_NS "Class_UInt");
		}break;
		case ir::TypePrim::Kind::Number: {
			writer->write(IMPL_NS "Class_Number");
		}break;
		case ir::TypePrim::Kind::String: {
			writer->write(IMPL_NS "Class_String");
		}break;
		case ir::TypePrim::Kind::Function: {
			writer->write(IMPL_NS "Class_Function");
		}break;
		case ir::TypePrim::Kind::Class: {
			writer->write(IMPL_NS "Class_Class");
		}break;
		case ir::TypePrim::Kind::Object: {
			writer->write(IMPL_NS "Class_Object");
		}break;
		case ir::TypePrim::Kind::Any: {
			writer->write(IMPL_NS "Class_Any");
		}break;
		default:{
			jl_unreachable;
		}break;
		}
	caseof(ir::TypeClass, class_)
		writeClassClass(class_->class_);
	caseof(ir::TypeInterface, iface)
		writeInterfaceClass(iface->iface);
	caseof(ir::TypeVector, vec)
		writer->write(IMPL_NS "VectorClass<");
		writeFieldType(vec->eltTy);
		writer->write(">");
	casedef(_)
		jl_unreachable;
	endmatch
}

bool Codegen::writeGCVisit(ir::Type* type, StringRef object, StringRef glue, StringRef field){
	match(type)
	caseof(ir::TypePrim, prim){
		switch(prim->kind){
		case ir::TypePrim::Kind::Any:
		case ir::TypePrim::Kind::Object: {
			writer->write(IMPL_NS "gc_visit_any(");
			writer->write(object);
			writer->write(glue);
			writer->write(field);
			writer->writeln(");");
			return true;
		}break;
		case ir::TypePrim::Kind::Boolean:
		case ir::TypePrim::Kind::Int:
		case ir::TypePrim::Kind::Null:
		case ir::TypePrim::Kind::Number:
		case ir::TypePrim::Kind::Uint:
		case ir::TypePrim::Kind::Class:
		case ir::TypePrim::Kind::Void:{
			// do nothing
			return false;
		}break;
		case ir::TypePrim::Kind::Function: {
			writer->write(IMPL_NS "gc_visit_function(");
			writer->write(object);
			writer->write(glue);
			writer->write(field);
			writer->writeln(");");
			return true;
		}break;
		case ir::TypePrim::Kind::Rest: {
			writer->write(IMPL_NS "gc_visit_rest(");
			writer->write(object);
			writer->write(glue);
			writer->write(field);
			writer->writeln(");");
			return true;
		}break;
		case ir::TypePrim::Kind::String: {
			writer->write(IMPL_NS "gc_visit_string(");
			writer->write(object);
			writer->write(glue);
			writer->write(field);
			writer->writeln(");");
			return true;
		}break;
		default:{
			jl_unreachable;
		}break;
		}
	}
	caseof(ir::TypeClass, class_){
		writer->write(IMPL_NS "gc_visit((" BASE_OBJECT_TYPE "*)(");
		writer->write(object);
		writer->write(glue);
		writer->write(field);
		writer->writeln("));");
		return true;
	}
	caseof(ir::TypeInterface, iface){
		writer->write(IMPL_NS "gc_visit((" BASE_OBJECT_TYPE "*)(");
		writer->write(object);
		writer->write(glue);
		writer->write(field);
		writer->writeln("." IFACE_OBJECT_MEMBER "));");
		return true;
	}
	caseof(ir::TypeVector, vec){
		writer->write(IMPL_NS "gc_visit((" BASE_OBJECT_TYPE "*)(");
		writer->write(object);
		writer->write(glue);
		writer->write(field);
		writer->writeln("));");
		return true;
	}
	casedef(_){
		jl_unreachable;
	}
	endmatch
}

void Codegen::writeAttributesDecl(ir::ClassMethod* method) {
	if (method->inlined) {
		writer->writeln(INLINE_ATTRIB);
	}
}

void Codegen::writeAttributes(ir::ClassMethod* method) {
	if (method->inlined) {
		writer->writeln(INLINE_ATTRIB);
	}
}

void Codegen::writeAttributesDecl(ir::GlobalFunction* func) {
	if (func->inlined) {
		writer->writeln(INLINE_ATTRIB);
	}
}

void Codegen::writeAttributes(ir::GlobalFunction* func) {
	if (func->inlined) {
		writer->writeln(INLINE_ATTRIB);
	}
}

bool Codegen::willCompileInCurrentMode(ir::ClassMethod* method) {
	return method->inlined == (mode == CodegenMode::Inline);
}

bool Codegen::willCompileInCurrentMode(ir::GlobalFunction* func) {
	return func->inlined == (mode == CodegenMode::Inline);
}

}