#include "Codegen.h"

namespace jly{


void Codegen::codegenCastError(ir::Type* from, ir::Type* to){
	writer->write(IMPL_NS "raise_cast_error(&");
	writeTypeClass(from);
	writer->write(", &");
	writeTypeClass(to);
	writer->write(")");
}

void Codegen::codegenTypeKindName(ir::Type* type, bool is_target){
	match(type)
	caseof(ir::TypePrim, prim)
		switch ( prim->kind ){
		case ir::TypePrim::Kind::Any:{
			writer->write("any");
		}break;
		case ir::TypePrim::Kind::Boolean: {
			writer->write("Boolean");
		}break;
		case ir::TypePrim::Kind::Class: {
			if (is_target) {
				writer->write("Class");
			} else {
				writer->write("Object");
			}
		}break;
		case ir::TypePrim::Kind::Function: {
			writer->write("Function");
		}break;
		case ir::TypePrim::Kind::Int: {
			writer->write("int");
		}break;
		case ir::TypePrim::Kind::Null: {
			writer->write("null");
		}break;
		case ir::TypePrim::Kind::Number: {
			writer->write("Number");
		}break;
		case ir::TypePrim::Kind::Object: {
			writer->write("any");
		}break;
		case ir::TypePrim::Kind::Rest: {
			writer->write("rest");
		}break;
		case ir::TypePrim::Kind::String: {
			writer->write("String");
		}break;
		case ir::TypePrim::Kind::Uint: {
			writer->write("uint");
		}break;
		default:{
			jl_unreachable;
		}break;
		}
	caseof(ir::TypeInterface, iface)
		if ( is_target ){
			writer->write("Interface");
		}else{
			writer->write("Object");
		}
	caseof(ir::TypeClass, class_)
		if (is_target) {
			writer->write("Class");
		} else {
			writer->write("Object");
		}
	caseof(ir::TypeVector, vec)
		if (is_target) {
			writer->write("Class");
		} else {
			writer->write("Object");
		}
	casedef(_)
		jl_unreachable;
	endmatch
}

void Codegen::codegenTypeCastPrefix(ir::Type* to){
	match(to)
	caseof(ir::TypePrim, prim){
		switch (prim->kind) {
		case ir::TypePrim::Kind::Class: {
			writer->write("(" IMPL_NS "Class*)");
		}break;
		case ir::TypePrim::Kind::Any:
		case ir::TypePrim::Kind::Boolean:
		case ir::TypePrim::Kind::Function:
		case ir::TypePrim::Kind::Int:
		case ir::TypePrim::Kind::Null:
		case ir::TypePrim::Kind::Number:
		case ir::TypePrim::Kind::Object:
		case ir::TypePrim::Kind::Rest:
		case ir::TypePrim::Kind::String:
		case ir::TypePrim::Kind::Uint: {
		}break;
		default: {
			jl_unreachable;
		}break;
		}
	}
	caseof(ir::TypeInterface, iface) {
		writer->write("(");
		writeFieldType(iface);
		writer->write(")");
	}
	caseof(ir::TypeClass, class_) {
		writer->write("(");
		writeFieldType(class_);
		writer->write(")");
	}
	caseof(ir::TypeVector, vec){
		writer->write("(");
		writeFieldType(vec);
		writer->write(")");
	}
	casedef(_)
		jl_unreachable;
	endmatch
}

void Codegen::codegenTypeCastValue(icode::Value* value){
	match(value->type)
	caseof(ir::TypePrim, prim){
		switch (prim->kind) {
		case ir::TypePrim::Kind::Class: {
			writer->write("(" IMPL_NS "Object*)");
			codegenValue(value);
		}break;
		case ir::TypePrim::Kind::Any:
		case ir::TypePrim::Kind::Boolean:
		case ir::TypePrim::Kind::Function:
		case ir::TypePrim::Kind::Int:
		case ir::TypePrim::Kind::Null:
		case ir::TypePrim::Kind::Number:
		case ir::TypePrim::Kind::Object:
		case ir::TypePrim::Kind::Rest:
		case ir::TypePrim::Kind::String:
		case ir::TypePrim::Kind::Uint: {
			codegenValue(value);
		}break;
		default: {
			jl_unreachable;
		}break;
		}
	}
	caseof(ir::TypeInterface, iface){
		codegenValue(value);
		writer->write("." IFACE_OBJECT_MEMBER);
	}
	caseof(ir::TypeClass, class_){
		writer->write("(" IMPL_NS "Object*)");
		codegenValue(value);
	}
	caseof(ir::TypeVector, vec){
		writer->write("(" IMPL_NS "Object*)");
		codegenValue(value);
	}
	casedef(_)
		jl_unreachable;
	endmatch
}

void Codegen::codegenTypeCastArgs(ir::Type* type){
	match(type)
	caseof(ir::TypePrim, prim)
		switch (prim->kind) {
		case ir::TypePrim::Kind::Class: {
			writer->write(", &" IMPL_NS "Class_Class");
		}break;
		case ir::TypePrim::Kind::Any:
		case ir::TypePrim::Kind::Boolean:
		case ir::TypePrim::Kind::Function:
		case ir::TypePrim::Kind::Int:
		case ir::TypePrim::Kind::Null:
		case ir::TypePrim::Kind::Number:
		case ir::TypePrim::Kind::Object:
		case ir::TypePrim::Kind::Rest:
		case ir::TypePrim::Kind::String:
		case ir::TypePrim::Kind::Uint: {
		}break;
		default: {
			jl_unreachable;
		}break;
		}
	caseof(ir::TypeInterface, iface)
		writer->write(", &");
		writeInterfaceClass(iface->iface);
	caseof(ir::TypeClass, class_)
		writer->write(", &");
		writeClassClass(class_->class_);
	caseof(ir::TypeVector, vec)
		writer->write(", &");
		writeTypeClass(vec);
	casedef(_)
		jl_unreachable;
	endmatch
}

void Codegen::codegenCastGeneric(icode::Value* from, ir::Type* to, StringRef cast_id){
	ir::Type* ty = from->type;
	if ( cast_id != "is" ){
		codegenTypeCastPrefix(to);
	}
	writer->write(IMPL_NS);
	codegenTypeKindName(ty, false);
	writer->write("_");
	writer->write(cast_id);
	writer->write("_");
	codegenTypeKindName(to, true);
	writer->write("(");
	codegenTypeCastValue(from);
	codegenTypeCastArgs(to);
	writer->write(")");
}

void Codegen::codegenIsNull(icode::Value* value){
	jl_unreachable;
}

bool Codegen::codegenCastObject(icode::Value* from, ir::Type* to, StringRef cast_id){
	match2(from->type, to)
	caseof2(ir::TypeClass, from_class, ir::TypeClass, to_class) {
		if (from_class->class_->all_bases.count(to_class->class_)) {
			writer->write("(");
			writeFieldType(to_class);
			writer->write(")");
			codegenValue(from);
		} else if (to_class->class_->all_bases.count(from_class->class_)) {
			codegenCastGeneric(from, to, cast_id);
		} else {
			writer->write("(");
			writeFieldType(to_class);
			writer->write(")nullptr");
		}
		return true;
	}
	caseof2(ir::TypeInterface, from_iface, ir::TypeClass, to_class) {
		codegenCastGeneric(from, to, cast_id);
		return true;
	}
	caseof2(ir::TypeClass, from_class, ir::TypeInterface, to_iface) {
		codegenCastGeneric(from, to, cast_id);
		return true;
	}
	caseof2(ir::TypeInterface, from_iface, ir::TypeInterface, to_iface) {
		// TODO:: can optimize if from_iface contains to_iface
		codegenCastGeneric(from, to, cast_id);
		return true;
	}
	casedef2(_1, _2) {
		return false;
	}
	endmatch2
}

bool Codegen::codegenIsObject(icode::Value* from, ir::Type* to){
	match2(from->type, to)
	caseof2(ir::TypeClass, from_class, ir::TypeClass, to_class) {
		if (from_class->class_->all_bases.count(to_class->class_)) {
			writer->write("true");
		} else if (to_class->class_->all_bases.count(from_class->class_)) {
			codegenCastGeneric(from, to, "is");
		} else {
			writer->write("false");
		}
		return true;
	}
	caseof2(ir::TypeInterface, from_iface, ir::TypeClass, to_class) {
		codegenCastGeneric(from, to, "is");
		return true;
	}
	caseof2(ir::TypeClass, from_class, ir::TypeInterface, to_iface) {
		if ( from_class->class_->all_implements_set.count(to_iface->iface) ){
			writer->write("true");
		}else{
			codegenCastGeneric(from, to, "is");
		}
		return true;
	}
	caseof2(ir::TypeInterface, from_iface, ir::TypeInterface, to_iface) {
		if (from_iface->iface->all_implements_set.count(to_iface->iface)) {
			writer->write("true");
		} else {
			codegenCastGeneric(from, to, "is");
		}
		return true;
	}
	casedef2(_1, _2) {
		return false;
	}
	endmatch2
}


}
