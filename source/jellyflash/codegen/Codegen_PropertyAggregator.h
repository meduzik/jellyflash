#pragma once
#include "Codegen.h"
#include <jellylib/utils/Murmur.h>
#include <jellylib/utils/StringDecode.h>

namespace jly{

class Codegen::PropertyAggregator {
public:
	PropertyAggregator(Codegen* owner, StringRef unique_namespace):
		owner(owner),
		collection(owner->tmp),
		unique_namespace(unique_namespace)
	{
		writer = owner->writer;
		writer->write("namespace " PRIVATE_PREFIX "prop_");
		writer->write(unique_namespace);
		writer->writeln("{");
	}

	template<class WriteGet, class WriteSet>
	void add(StringRef name, WriteGet&& getter, WriteSet&& setter){
		owner->prop_index++;
		writer->write(IMPL_NS "ClassProperty prop_");
		writer->write(diag::decimal(owner->prop_index));
		writer->write("{\"");
		EncodeCLiteral(writer, name);
		writer->writeln("\",[](" IMPL_NS "Object* thisobj)->" IMPL_NS "Any{");
		{
			getter();
		}
		writer->writeln("}, [](" IMPL_NS "Object* thisobj, " IMPL_NS "Any val){");
		{
			setter();
		}
		writer->writeln("}};");
		collection.insert({name, owner->prop_index});
	}

	template<class WriteNameList, class WriteNameMask>
	void emit(WriteNameList&& write_list, WriteNameMask&& write_mask){
		uz count = collection.size();
		uz slots = next_pot(count * 2);
		mask = slots - 1;
		uz* layout = (uz*)owner->tmp->allocateSpace<uz>(slots);
		for ( uz i = 0; i < slots; i++ ){
			layout[i] = 0;
		}
		for ( auto prop : collection ){
			auto name = prop.first;
			uz hash = murmur3_32(name.getData(), name.getSize(), 0xAFCE5F03) & mask;
			while ( true ){
				if ( layout[hash] ){
					hash = (hash + 1) & mask;
				}else{
					layout[hash] = prop.second;
					break;
				}
			}
		}
		writer->writeln("}");

		owner->prop_list_index++;

		writer->write("const " IMPL_NS "ClassProperty* ");
		write_list();
		writer->writeln("[] = {");
		for (uz i = 0; i < slots; i++) {
			if ( !layout[i] ){
				writer->write("nullptr,");
			}else{
				writer->write("&");
				write_ns();
				writer->write("prop_");
				writer->write(diag::decimal(layout[i]));
				writer->write(",");
			}
			if ( i % 16 == 15 ){
				writer->writeln();
			}
		}
		writer->writeln("};");

		writer->write("const uint32_t ");
		write_mask();
		writer->write(" = ");
		writer->write(diag::decimal(mask));
		writer->writeln(";");
	}

	void write_ns(){
		writer->write(PRIVATE_PREFIX "prop_");
		writer->write(unique_namespace);
		writer->write("::");
	}

	uz get_mask(){
		return mask;
	}
private:
	Codegen* owner;
	FormattedWriter* writer;
	slice_map<StringRef, uz> collection;
	uz mask;
	StringRef unique_namespace;
};


}
