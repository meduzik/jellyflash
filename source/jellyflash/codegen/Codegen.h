#pragma once

#include <jellyflash/codegen/Codegen.h>
#include <jellyflash/compiler/CompilationUnit.h>
#include <jellyflash/ir/Definition.h>
#include <jellyflash/ir/Class.h>
#include <jellyflash/ir/Interface.h>
#include <jellyflash/ir/Type.h>
#include <jellyflash/icode/ICodeCompiler.h>

#define PREFIX_NS "fl"
#define HIDDEN_NS_PREFIX "_"
#define IMPL_NS_BASE "jfl"
#define IMPL_NS IMPL_NS_BASE "::"
#define PRIVATE_PREFIX "jfl_"
#define MACRO_PREFIX "JFL_"

#define SETTER_ID "s"
#define GETTER_ID "g"
#define PARAM_ID "a"
#define PRIVATE_ID "p"
#define PROTECTED_ID "d"
#define LOCAL_ID "l"
#define NS_ID "n"
#define KEYWORD_ID "k"
#define STATIC_ID "S"
#define VTABLE_ID "v"
#define VTABLE_INSTANCE_ID "V"
#define TYPEINFO_ID "T"
#define GENERIC_PROXY_ID "f"
#define INTERFACE_IMPL_ID "imp"

#define THIS_NAME PRIVATE_PREFIX "t"

#define CONST_STRING_NS IMPL_NS "cstr"
#define CONST_STRING_PREFIX PRIVATE_PREFIX
#define CONST_STRING_DATA_PREFIX PRIVATE_PREFIX "data_"

#define INLINE_ATTRIB MACRO_PREFIX "INLINE"

#define IFACE_OBJECT_MEMBER "o"
#define IFACE_VTABLE_MEMBER "v"
#define CLASS_VTABLE_MEMBER PRIVATE_PREFIX "vtable"
#define OBJECT_HEADER_MEMBER PRIVATE_PREFIX "header"
#define CLASS_DYNAMIC_MEMBER PRIVATE_PREFIX "dynamic"
#define CLASS_TYPEINFO_MEMBER PRIVATE_PREFIX "tinfo"
#define CLASS_MEMBER PRIVATE_PREFIX "Class"
#define STATIC_CREATE_MEMBER PRIVATE_PREFIX "ClassCreate"
#define STATIC_DYNAMIC_MEMBER PRIVATE_PREFIX "ClassDynamic"
#define STATIC_PROPLIST_MEMBER PRIVATE_PREFIX "ClassPList"
#define STATIC_PROPLIST_MASK_MEMBER PRIVATE_PREFIX "ClassPListMask"
#define TYPEINFO_MEMBER PRIVATE_PREFIX "TI"
#define VTABLE_HEADER_MEMBER PRIVATE_PREFIX "VT_HDR"
#define VTABLE_HEADER PRIVATE_PREFIX "hdr"

#define GC_MEMBER PRIVATE_PREFIX "GC"
#define GC_USER_MEMBER PRIVATE_PREFIX "GC_user"
#define FINALIZER_MEMBER PRIVATE_PREFIX "Finalize"
#define DYNAMIC_MEMBER PRIVATE_PREFIX "Dynamic"
#define ITERATE_MEMBER PRIVATE_PREFIX "Iterate"
#define PROPLIST_MEMBER PRIVATE_PREFIX "PList"
#define PROPLIST_MASK_MEMBER PRIVATE_PREFIX "PListMask"

#define STATIC_INITIALIZER_ID "CInit"
#define STATIC_INITIALIZER_GUARD_ID "CInitGuard"

#define BASE_OBJECT_TYPE IMPL_NS "Object"
#define BASE_INTERFACE_TYPE IMPL_NS "Interface"
#define TYPE_INFO_TYPE IMPL_NS "TypeInfo"

#define NEW_METHOD_NAME PRIVATE_PREFIX "New"
#define CONSTRUCTOR_METHOD_NAME PRIVATE_PREFIX "Constructor"
#define PRECONSTRUCTOR_METHOD_NAME PRIVATE_PREFIX "Preconstructor"

namespace jly{

inline uz next_pot(uz x) {
	uz acc = 1;
	while (acc < x) {
		acc *= 2;
	}
	return acc;
}

inline uz log2(uz x) {
	uz n = 0;
	while ((1ull << n) < x) {
		n++;
	}
	jl_assert((1ull << n) == x);
	return n;
}


namespace {

bool isAny(ir::Type* ty) {
	return ty == &ir::TypePrim::Any || ty == &ir::TypePrim::Object;
}

bool isNumeric(ir::Type* ty) {
	return ty == &ir::TypePrim::Number || ty == &ir::TypePrim::Int || ty == &ir::TypePrim::Uint;
}

bool isNullable(ir::Type* ty) {
	return !(ty == &ir::TypePrim::Boolean || isNumeric(ty));
}


}

}