#include "Codegen.h"
#include <jellyflash/icode/ICodePrinter.h>
#include <jellylib/crypto/SHA1.h>
#include <jellylib/utils/StringDecode.h>

namespace jly {

void Codegen::writeInterfaceImpl(ir::Interface* iface){
	writer->write("const " IMPL_NS "Class ");
	writer->write(iface->codegen->name);
	writer->write("::" CLASS_MEMBER);
	writer->writeln("{");
	{
		auto table_scope = writer->indented();
		writer->writeln("&" IMPL_NS "Class::jfl_VT,");
		writer->writeln("{},");
		writer->writeln("nullptr,");

		codegenConstantString(tmp->create<icode::ConstantString>(generateQualifiedName(iface)));
		writer->writeln(",");

		writer->write(diag::decimal(iface->hash_base));
		writer->writeln("u,");

		writer->write(diag::decimal(iface->hash_offset));
		writer->writeln("u,");

		writer->writeln(IMPL_NS "class_kind_t::kind_interface,");
	}
	writer->writeln("};");
}



}