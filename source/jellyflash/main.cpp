#include <jellylib/common.h>
#include <jellylib/memory/MemPool.h>
#include <jellyflash/source/SourceInfo.h>
#include <jellyflash/parser/Parser.h>
#include <jellyflash/parser/Lexer.h>
#include <jellyflash/compiler/FileLoader.h>
#include <jellyflash/compiler/CompilerEnv.h>
#include <jellyflash/compiler/CompilerProject.h>
#include <jellyflash/codegen/CodegenInfo.h>
#include <jellylib/utils/Murmur.h>
#include <Windows.h>

using namespace jly;
template<typename TChar, typename TTraits = std::char_traits<TChar>>
class OutputDebugStringBuf : public std::basic_stringbuf<TChar, TTraits> {
public:
	using int_type = int;
	using base = std::basic_stringbuf<TChar, TTraits>;

	OutputDebugStringBuf() : _buffer(256) {
		base::setg(nullptr, nullptr, nullptr);
		base::setp(_buffer.data(), _buffer.data(), _buffer.data() + _buffer.size());
	}

	~OutputDebugStringBuf() {
	}

	static_assert(std::is_same<TChar, char>::value || std::is_same<TChar, wchar_t>::value, "OutputDebugStringBuf only supports char and wchar_t types");

	int sync() { 
		try {
			MessageOutputer<TChar, TTraits>()(base::pbase(), base::pptr());
			base::setp(_buffer.data(), _buffer.data(), _buffer.data() + _buffer.size());
			return 0;
		} catch (...) {
			return -1;
		}
	}

	int_type overflow(int_type c = TTraits::eof()) {
		auto syncRet = sync();
		if (c != TTraits::eof()) {
			_buffer[0] = c;
			base::setp(_buffer.data(), _buffer.data() + 1, _buffer.data() + _buffer.size());
		}
		return syncRet == -1 ? TTraits::eof() : 0;
	}


private:
	jly::vector<TChar>      _buffer;

	template<typename TChar, typename TTraits>
	struct MessageOutputer;

	template<>
	struct MessageOutputer<char, std::char_traits<char>> {
		template<typename TIterator>
		void operator()(TIterator begin, TIterator end) const {
			std::string s(begin, end);
			OutputDebugStringA(s.c_str());
		}
	};

	template<>
	struct MessageOutputer<wchar_t, std::char_traits<wchar_t>> {
		template<typename TIterator>
		void operator()(TIterator begin, TIterator end) const {
			std::wstring s(begin, end);
			OutputDebugStringW(s.c_str());
		}
	};
};


int main(int argc, char* argv[]){
	if ( IsDebuggerPresent() ){
		static OutputDebugStringBuf<char> charDebugOutput;
		std::cerr.rdbuf(&charDebugOutput);
		std::clog.rdbuf(&charDebugOutput);

		static OutputDebugStringBuf<wchar_t> wcharDebugOutput;
		std::wcerr.rdbuf(&wcharDebugOutput);
		std::wclog.rdbuf(&wcharDebugOutput);
	}
	
	MemPool::InitForCurrentThread();

	CompilerProject project;

	for (int i = 1; i < argc; i++) {
		if ( !strcmp(argv[i],"--stdlib") ) {
			if (i >= argc - 1) {
				std::cerr << "stdlib requires argument" << std::endl;
			}
			project.setStdlibPath(argv[i + 1]);
			i++;
		} else if (!strcmp(argv[i], "--output")) {
			if (i >= argc - 1) {
				std::cerr << "output requires argument" << std::endl;
			}
			project.setOutputPath(argv[i + 1]);
			i++;
		} else if (!strcmp(argv[i], "--stubs")) {
			if (i >= argc - 1) {
				std::cerr << "stubs requires argument" << std::endl;
			}
			project.setStubsPath(argv[i + 1]);
			i++;
		} else if (!strcmp(argv[i], "--main")) {
			if (i >= argc - 1) {
				std::cerr << "main requires argument" << std::endl;
			}
			project.setMain(argv[i + 1]);
			i++;
		} else {
			project.loadConfig(argv[i]);
		}
	}
	project.build();
	/*
	picojson::value config;
	picojson::parse(config, 

	std::filesystem::path out_dir("");

	CodegenClassPathInfo cp_builtin;
	auto builtin_dir = out_dir / "builtin";
	cp_builtin.source_path = builtin_dir / "source";
	cp_builtin.include_path = builtin_dir / "include";
	cp_builtin.native_stubs_path = builtin_dir / "stubs";

	CodegenClassPathInfo cp_lua;
	auto lua_dir = out_dir / "lua";
	cp_lua.source_path = lua_dir / "source";
	cp_lua.include_path = lua_dir / "include";
	cp_lua.native_stubs_path = lua_dir / "stubs";

	CodegenClassPathInfo cp_project;
	auto project_dir = out_dir / "project";
	cp_project.source_path = project_dir / "source";
	cp_project.include_path = project_dir / "include";

	CompilerEnv env;

	CompilerContext cc(&env);
	FileLoader loader(&cc);

	// stdlib stubs
	loader.loadClassPath("D:\\Work\\jellyflash-wrapper\\flash_frameworks\\airglobal_port\\scripts", &cp_builtin);
	loader.loadClassPath("D:\\Work\\jellyflash-wrapper\\flash_frameworks\\intrinsics", &cp_builtin);
	loader.loadClassPath("D:\\Work\\jellyflash-wrapper\\flash_frameworks\\jellyflash", &cp_builtin);
	env.loadIntrinsics();

	// lua stubs
	loader.loadClassPath("D:\\Work\\jellyflash-wrapper\\flash_frameworks\\lua\\scripts", &cp_lua);

	// anes
	loader.loadClassPath("D:\\Work\\jellyflash-wrapper\\flash_frameworks\\marpies_fb_ane\\scripts", &cp_project);
	loader.loadClassPath("D:\\Work\\ANE\\flash\\src\\default", &cp_project);
	loader.loadClassPath("D:\\Work\\ANE\\flash\\src\\shared", &cp_project);

	// crypto libs
	loader.loadClassPath("D:\\Dropbox\\matcharena\\shared-libs\\air\\blooddy_crypto-master\\src", &cp_project);

	// fla graphics stubs
	loader.loadClassPath("D:\\Work\\jellyflash-wrapper\\flash_frameworks\\matcharena_extras\\scripts", &cp_project);

	// game project
	loader.loadClassPath("D:\\Dropbox\\matcharena\\client\\air\\src", &cp_project);
	loader.loadClassPath("D:\\Dropbox\\matcharena\\shared-libs\\air\\airfish\\src", &cp_project);
	loader.loadClassPath("D:\\Dropbox\\matcharena\\client\\engine\\shared-src", &cp_project);
	
	auto unit = env.getPackageRoot()->findChild("com")->findChild("meduzik")->findUnit("Main");

	env.compile(unit);
	*/
}
