#include <jellyflash/source/SourceInfo.h>

namespace jly {

StringRef SourceFile::getLine(u32 line) const{
	auto boundaries = lineMap.getLineBoundaries(line);
	return StringRef(data.data() + boundaries.first, boundaries.second - boundaries.first);
}

SourceFile SourceFile::ReadFile(const std::filesystem::path& filePath){
	std::ifstream fileInput(filePath, std::ios::binary);
	if ( !fileInput ){
		throw std::runtime_error("Failed to open file \"" + filePath.u8string() + "\"");
	}
	fileInput.seekg(0, std::ios_base::end);
	auto fileSize = fileInput.tellg();
	jly::vector<u8> contents;
	contents.resize(fileSize);
	fileInput.seekg(0, std::ios_base::beg);
	fileInput.read((char*)contents.data(), fileSize);

	SourceFile file(filePath.u8string(), std::move(contents));
	return file;
}

SourceLineMap::SourceLineMap(){
	lines.push_back(0);
}

u32& SourceLineMap::pushLine(){
	return lines.emplace_back();
}

u32 SourceLineMap::getColByOffset(srcoffset_t offset) const{
	return getLocByOffset(offset).column;
}

u32 SourceLineMap::getLineByOffset(srcoffset_t offset) const{
	return getLocByOffset(offset).line;
}

srcloc_t SourceLineMap::getLocByOffset(srcoffset_t offset) const{
	if ( offset >= lines.back() ){
		uz n = lines.size();
		return {
			(u32)(n - 2),
			lines[n - 1] - lines[n - 2]
		};
	}

	uz begin = 0;
	uz end = lines.size();
	while ( (end - begin) > 1 ){
		uz mid = begin / 2 + end / 2 + (begin & 1) * (end & 1);
		if ( lines[mid] > offset ){
			end = mid;
		}else{
			begin = mid;
		}
	}
	uz line = begin;
	return {(u32)line, offset - lines[line]};
}

std::pair<u32, u32> SourceLineMap::getLineBoundaries(uz line) const{
	if (line >= lines.size()){
		return {lines.back(), lines.back()};
	}else{
		return {lines[line], lines[line + 1]};
	}
}

}
