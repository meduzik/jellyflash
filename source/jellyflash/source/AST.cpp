#include <jellyflash/parser/AST.h>
#include <jellyflash/parser/Token.h>

namespace jly::ast{

Namespace 
	Public(ID(srcspan_t(0),"public")),
	Private(ID(srcspan_t(0), "private")),
	Internal(ID(srcspan_t(0), "internal")),
	Protected(ID(srcspan_t(0), "protected"));

}
